"""
https://gym.openai.com/docs/
"""

#import gym
##env = gym.make('CartPole-v0')
#env = gym.make('MountainCar-v0')
#env.reset()
#for _ in range(1000):
#    env.render()
#    env.step(env.action_space.sample()) # take a random action

#import gym
#env = gym.make('CartPole-v0')
#for i_episode in range(20):
#    observation = env.reset()
#    for t in range(100):
#        env.render()
#        print(observation)
#        action = env.action_space.sample()
#        observation, reward, done, info = env.step(action)
#        if done:
#            print("Episode finished after {} timesteps".format(t+1))
#            break
#env.close()

#import gym
#env = gym.make('CartPole-v0')
#print(env.action_space)
#> Discrete(2)
#print(env.observation_space)
#> Box(4,)

#print(env.observation_space.high)
#> array([ 2.4       ,         inf,  0.20943951,         inf])
#print(env.observation_space.low)
#> array([-2.4       ,        -inf, -0.20943951,        -inf])

#from gym import spaces
#space = spaces.Discrete(8) # Set with 8 elements {0, 1, 2, ..., 7}
#x = space.sample()
#assert space.contains(x)
#assert space.n == 8
#print(x)

import gym
env = gym.make('CartPole-v0')
highscore = 0
for i_episode in range(20): # run 20 episodes
  observation = env.reset()
  points = 0 # keep track of the reward each episode
  while True: # run until episode is done
    env.render()
    action = 1 if observation[2] > 0 else 0 # if angle if positive, move right. if angle is negative, move left
    observation, reward, done, info = env.step(action)
    points += reward
    if done:
      if points > highscore: # record high score
        highscore = points
        break
