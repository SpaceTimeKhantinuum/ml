# source /Users/sebastian/work/git/stk/ml/waveforms/venv-wf/bin/activate
# http://edwardlib.org/getting-started
# https://nbviewer.jupyter.org/github/blei-lab/edward/blob/master/notebooks/getting_started.ipynb

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# setup 'training' data
# i.e. what we want to fit
# for this it is a cosine wave with
# some gaussian noise

x_train = np.linspace(-3, 3, 50)
y_train = np.cos(x_train) + np.random.normal(0, 0.1, size=50)
x_train = x_train.astype(np.float32).reshape((50, 1))
y_train = y_train.astype(np.float32).reshape((50, 1))

#plt.figure()
#plt.scatter(x_train, y_train)
#plt.show()

# now we use a two-layer Bayesian neural network
# with tanh nonlinearities

import tensorflow as tf
from edward.models import Normal

W_0 = Normal(loc=tf.zeros([1, 2]), scale=tf.ones([1,2]))
W_1 = Normal(loc=tf.zeros([2, 1]), scale=tf.ones([2,1]))
b_0 = Normal(loc=tf.zeros(2), scale=tf.ones(2))
b_1 = Normal(loc=tf.zeros(1), scale=tf.ones(1))

x = x_train
# this is just a simple two-layer, feed-forward, perceptron
y = Normal(loc=tf.matmul(tf.tanh(tf.matmul(x, W_0) + b_0), W_1) + b_1, scale=0.1)

# next make inferences about the model from data using variation
# inference.
# Specify a normal approximation over the weights and biases.

qW_0 = Normal(loc=tf.get_variable("qW_0/loc", [1, 2]),
              scale=tf.nn.softplus(tf.get_variable("qW_0/scale", [1, 2])))
qW_1 = Normal(loc=tf.get_variable("qW_1/loc", [2, 1]),
              scale=tf.nn.softplus(tf.get_variable("qW_1/scale", [2, 1])))
qb_0 = Normal(loc=tf.get_variable("qb_0/loc", [2]),
              scale=tf.nn.softplus(tf.get_variable("qb_0/scale", [2])))
qb_1 = Normal(loc=tf.get_variable("qb_1/loc", [1]),
              scale=tf.nn.softplus(tf.get_variable("qb_1/scale", [1])))

# Defining tf.get_variable allows the variational factors' parameters to vary.
# They are all initialized at 0. The standard deviation parameters are
# constrained to be greater than zero according to a softplus transformation

# Now, run variational inference with the Kullback-Leibler divergence
# in order to infer the model's latent variables given data.
# We specify 1000 iterations.

import edward as ed

inference = ed.KLqp({
    W_0: qW_0, b_0: qb_0,
    W_1: qW_1, b_1: qb_1
},
    data = {y: y_train}
)

inference.run(n_iter=1000)


