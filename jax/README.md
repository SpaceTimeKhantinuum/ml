# playing around with jax

```
conda create -n jax python=3.7
conda install -c conda-forge jax
# also installed numpyro
conda install -c conda-forge numpyro
# pycbc
conda install -c conda-forge pycbc
# jupyter
conda install -c conda-forge jupyterlab

# for jaxns - nested sampling in jax
# pip install jax jaxlib numpy matplotlib scipy
# git clone https://github.com/Joshuaalbert/jaxns
# python setup.py install

```

```
conda activate jax
```



## resourses

    - https://colinraffel.com/blog/you-don-t-know-jax.html
    - https://jax.readthedocs.io/en/latest/notebooks/quickstart.html
    - https://github.com/pyro-ppl/numpyro
    - jaxns: https://github.com/Joshuaalbert/jaxns

