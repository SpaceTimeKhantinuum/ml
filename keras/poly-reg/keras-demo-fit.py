#https://gist.github.com/lmc2179/ae86a11beb75d251e0bb31411b191310

import numpy as np
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from matplotlib import pyplot as plt
import seaborn as sns

# model = Sequential([Dense(output_dim=4, input_dim=1),
#                     Activation("tanh"),
#                     Dense(output_dim=4, input_dim=4),
#                     Activation("linear"),
#                     Dense(output_dim=1, input_dim=4)])

model = Sequential()
model.add(Dense(100, activation='softplus', input_dim=1))
model.add(Dense(100, activation='softplus'))
model.add(Dense(100, activation='softplus'))
model.add(Dense(100, activation='softplus'))
model.add(Dense(100, activation='softplus'))
model.add(Dense(100, activation='softplus'))
model.add(Dense(100, activation='softplus'))
model.add(Dense(100, activation='softplus'))
model.add(Dense(1, activation='linear'))

# model = Sequential()
# model.add(Dense(10, activation='relu', input_dim=1, kernel_initializer="uniform"))
# model.add(Dense(10, activation='relu', kernel_initializer="uniform"))
# model.add(Dense(10, activation='relu', kernel_initializer="uniform"))
# model.add(Dense(10, activation='relu', kernel_initializer="uniform"))
# model.add(Dense(1, activation='linear'))

# model.compile(loss='mse', optimizer='nadam')
model.compile(loss='mse', optimizer='adam')

X = np.array([[i] for i in range(10)])
y = np.array([500*np.sin(3*x[0])+(x[0]**2)+1 for x in X]) + np.random.normal(0, 1, 10)

scale = np.max(y)

model.fit(X, y/scale, nb_epoch=5000)
# model.fit(X, y/scale, nb_epoch=800)
# X = np.array([[i] for i in range(10)])
# y_predicted = model.predict(X)
plt.scatter(X.reshape(-1, 1), y, s=2)

# plot at upsampled rate
X = np.array([np.linspace(0,10,100)]).T
y_predicted = model.predict(X)
plt.plot(X.reshape(-1, 1), y_predicted*scale, color='k')
plt.show()