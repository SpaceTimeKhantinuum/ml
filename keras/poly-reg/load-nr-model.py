import numpy as np
import matplotlib.pyplot as plt

def q_from_eta(eta):
    """
    Assumes m1 >= m2
    converts symmetric-mass-ratio to mass-ratio
    input: eta
    output: q
    """
    Seta = sqrt(1. - 4. * eta)
    return (1. + Seta - 2. * eta)/(2. * eta)

def eta_from_q(q):
    """
    converts mass-ratio to symmetric mass-ratio
    input: q
    output: eta
    """
    return q/(1.+q)**2

# the following didn't work
# from keras.models import load_model
# model = load_model('model-softplus.h5')

from keras.models import model_from_json

# load json and create model
json_file = open('model-softplus.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("model-softplus.h5")
print("Loaded model from disk")


# evaluate loaded model on test data
# loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
loaded_model.compile(loss='mean_squared_error', optimizer='adam')


# score = loaded_model.evaluate(X, Y, verbose=0)
# print("%s: %.2f%%" % (loaded_model.metrics_names[1], score[1]*100))


# times = np.array([-600, -500])
## this is the form the model needs the data in
# q = np.array([[-600, 1],[-500, 1]])
# ht = loaded_model.predict(q)


times = np.linspace(-600, 70, 1000)
qlist = np.zeros(len(times)) + 1

X = np.column_stack((times, qlist))
ht = loaded_model.predict(X)

plt.figure()
plt.plot(times, ht)
plt.show()

# looping over mass-ratio

times = np.linspace(-600, 70, 1000)


plt.figure()
for q in range(1,20):
    eta = eta_from_q(q)
    qlist = np.zeros(len(times)) + q
    X = np.column_stack((times, qlist))
    ht = loaded_model.predict(X)
    plt.plot(times, ht/eta, label=q)
    # plt.plot(times, ht, label=q)
plt.legend()
plt.show()

