
# use virtual env at /Users/sebastian/work/git/stk/ml/waveforms/venv-wf

from collections import namedtuple
import numpy as np

import h5py
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from scipy.fftpack import fft, fftfreq, fftshift

import pycbc
from pycbc.waveform.utils import taper_timeseries

def get_lm_mode(h5file, l, m, npts=5000):
    f = h5py.File(h5file, 'r')
    amp_tmp = f['amp_l{0}_m{1}'.format(l, m)]
    amp_x = amp_tmp['X'].value
    amp_y = amp_tmp['Y'].value

    phase_tmp = f['phase_l{0}_m{1}'.format(l, m)]
    phase_x = phase_tmp['X'].value
    phase_y = phase_tmp['Y'].value

    f.close()

    amp_i = interp1d(amp_x, amp_y)
    phase_i = interp1d(phase_x, phase_y)

    t1 = max(amp_x[0], phase_x[0])
    t2 = min(amp_x[-1], phase_x[-1])

    t1,t2=-600,100

    common_times = np.linspace(t1, t2, npts)

    amplist = amp_i(common_times)
    phaselist = phase_i(common_times)

    amp = TimeSeries(x=common_times, y=amplist, amp=None, phi=None)
    phi = TimeSeries(x=common_times, y=phaselist, amp=None, phi=None)

    hlm_tmp = amp.y * np.exp(-1.j * phi.y)

    hlm = TimeSeries(x=common_times, y=hlm_tmp, amp=amp.y, phi=phi.y)

    return hlm

if __name__ == "__main__":

    filepath = '/Users/sebastian/work/data/SXS_BBH_0057_Res5.h5'

    # filepath = '/Users/sebastian/work/data/SXS_BBH_0259_Res5.h5' #q=2.5


    TimeSeries = namedtuple('ts', ['x', 'y', 'amp', 'phi'])

    #h22 = get_lm_mode(filepath, 2, 2, npts=1000)
    h22 = get_lm_mode(filepath, 2, 0, npts=1000)
    # h22 = get_lm_mode(filepath, 2, 2, npts=1000)


    # plt.figure()
    # plt.plot(h22.x, h22.y.real)
    # plt.plot(h22.x, h22.y.imag)
    # plt.plot(h22.x, h22.amp)
    # plt.show()

    # hts = pycbc.types.TimeSeries(h22.y, delta_t=h22.x[1]-h22.x[0])
    hts = pycbc.types.TimeSeries(np.real(h22.y), delta_t=h22.x[1]-h22.x[0])
    # hts = pycbc.types.TimeSeries(np.imag(h22.y), delta_t=h22.x[1]-h22.x[0])

    hts = taper_timeseries(hts,tapermethod="TAPER_START")

    # plt.figure()
    # plt.plot(hts.sample_times, np.real(hts.data))
    # plt.plot(hts.sample_times, hts)
    # plt.show()

    print(len(hts))
    # hfs = hts.to_frequencyseries()
    scipyfft = fft(hts)
    xf = fftfreq(len(hts), h22.x[-1] - h22.x[0])
    # xf = fftshift(xf)

    plt.figure()
    plt.plot(xf, np.abs(scipyfft))
    # plt.plot(hfs.sample_frequencies, np.abs(hfs))
    # plt.plot(hfs.sample_frequencies, np.real(hfs.data))
    # plt.xscale('log')
    plt.yscale('log')
    plt.show()