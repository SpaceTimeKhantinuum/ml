"""
first test using NR data
"""

from collections import namedtuple
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np

import h5py
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d


def get_lm_mode(h5file, l, m, npts=5000):
    f = h5py.File(h5file, 'r')
    amp_tmp = f['amp_l{0}_m{1}'.format(l, m)]
    amp_x = amp_tmp['X'].value
    amp_y = amp_tmp['Y'].value

    phase_tmp = f['phase_l{0}_m{1}'.format(l, m)]
    phase_x = phase_tmp['X'].value
    phase_y = phase_tmp['Y'].value

    f.close()

    amp_i = interp1d(amp_x, amp_y)
    phase_i = interp1d(phase_x, phase_y)

    t1 = max(amp_x[0], phase_x[0])
    t2 = min(amp_x[-1], phase_x[-1])

    t1,t2=-600,100

    common_times = np.linspace(t1, t2, npts)

    amplist = amp_i(common_times)
    phaselist = phase_i(common_times)

    amp = TimeSeries(x=common_times, y=amplist, amp=None, phi=None)
    phi = TimeSeries(x=common_times, y=phaselist, amp=None, phi=None)

    hlm_tmp = amp.y * np.exp(-1.j * phi.y)

    hlm = TimeSeries(x=common_times, y=hlm_tmp, amp=amp.y, phi=phi.y)

    return hlm


def make_nn_model(amp_or_phi, optimizer, fit_t1, fit_t2, activation, epochs=50):
    # create model
    # single layer, fully connected, w/ linear activation function
    # with uniform random weights
    model = Sequential()
    #model.add(Dense(input_dim=1, activation="linear", units=2, kernel_initializer="uniform"))
    model.add(Dense(input_dim=1, activation="elu", units=50, kernel_initializer="uniform"))
    model.add(Dense(input_dim=1, activation="elu", units=10, kernel_initializer="uniform"))
    model.add(Dense(input_dim=1, activation="elu", units=10, kernel_initializer="uniform"))
    model.add(Dense(input_dim=1, activation="elu", units=10, kernel_initializer="uniform"))
    # model.add(Dense(input_dim=1, activation="relu", units=100, kernel_initializer="uniform"))
    # model.add(Dense(input_dim=1, activation="relu", units=100, kernel_initializer="uniform"))
    # model.add(Dropout(0.5))
    # model.add(Dense(units=1, activation='linear'))
    model.add(Dense(units=1, activation=activation))

    weights = model.layers[0].get_weights()
    # print(weights)
    # weights is a list of numpy arrays
    # w_init = weights[0][0][0]
    # b_init = weights[1][0]
    # print('regression model is initialized with weights w: %f, b: %f' %
        #   (w_init, b_init))

    # now we shall train the model
    # out loss function will be mean squared error (mse)
    # and we will optimize using gradient descent (sgd)
    # model.compile(optimizer='sgd', loss='mse')
    # model.compile(optimizer='rmsprop', loss='mse')
    # model.compile(optimizer='adam', loss='mse')
    # model.compile(optimizer='nadam', loss='mse')

    # was using this originally
    model.compile(optimizer=optimizer, loss='mse')
    #model.compile(optimizer=optimizer, loss='binary_crossentropy')


    # feed the data using the fit function

    tt1 = fit_t1
    tt2 = fit_t2

    to_fit_x = h22.x[(h22.x > tt1) & (h22.x < tt2)]
    # to_fit_y = h22.y.real[(h22.x > tt1) & (h22.x < tt2)] / np.max(h22.amp)

    if amp_or_phi == "amp":
        scale = np.max(h22.amp)
        to_fit_y = h22.amp[(h22.x > tt1) & (h22.x < tt2)] / scale
    elif amp_or_phi == "phi":
        scale = np.max(h22.phi)
        to_fit_y = h22.phi[(h22.x > tt1) & (h22.x < tt2)] / scale

    model.fit(to_fit_x, to_fit_y, epochs=epochs, verbose=1)
    return model, scale


if __name__ == "__main__":
    # filepath = '/Users/sebastian/work/data/SXS_BBH_0003_Res5.h5' #older q=1
    # filepath = '/Users/sebastian/work/data/SXS_BBH_0071_Res5.h5' #q=1
    # filepath = '/Users/sebastian/work/data/SXS_BBH_0169_Res5.h5' #q=2
    # filepath = '/Users/sebastian/work/data/SXS_BBH_0168_Res5.h5' #q=3
    filepath = '/Users/sebastian/work/data/SXS_BBH_0167_Res5.h5' #q=4

    # filepath = '/Users/sebastian/work/data/SXS_BBH_0259_Res5.h5' #q=2.5


    TimeSeries = namedtuple('ts', ['x', 'y', 'amp', 'phi'])

    #h22 = get_lm_mode(filepath, 2, 2, npts=500)
    h22 = get_lm_mode(filepath, 2, 2, npts=1000)
    # h22 = get_lm_mode(filepath, 2, 0, npts=1000)
    # h22 = get_lm_mode(filepath, 4, 4, npts=1000)
    # h22 = get_lm_mode(filepath, 5, 5, npts=1000)
    # h22 = get_lm_mode(filepath, 3, 1, npts=1000)
    # h2m2 = get_lm_mode(filepath, 2, -2, npts=5000)

    # plt.figure(figsize=(14, 7))
    # plt.plot(h22.x, h22.y.real, label='h2,2 real', lw=1)
    # plt.plot(h2m2.x, h2m2.y.real, label='h2,-2 real', lw=1)
    # plt.legend()
    # plt.show()

    fit_t1=-400
    fit_t2=100

    model_amp, amp_scale = make_nn_model(amp_or_phi="amp", optimizer='adam', fit_t1=fit_t1, fit_t2=fit_t2, activation='linear', epochs=50)
    model_phi, phi_scale = make_nn_model(amp_or_phi="phi", optimizer='adam', fit_t1=fit_t1, fit_t2=fit_t2, activation='linear', epochs=500)



    t1plot = -600
    t2plot = 100

    to_plot_x = h22.x[(h22.x > t1plot) & (h22.x < t2plot)]

    pred_amp = model_amp.predict(to_plot_x)
    pred_phi = model_phi.predict(to_plot_x)


    #
    # NOTE:
    # IMPORTANT TO UNDO THE PHASE SCALING SO THAT YOU CAN SEE
    # THE OSCILLATIONS IN THE STRAIN
    #
    pred_strain = pred_amp * np.exp(-1.j * pred_phi * phi_scale) #UNDO SCALING BY MULTIPLYING BY phi_scale

    # to_plot_y = h22.amp / amp_scale * np.exp(-1.j * h22.phi / phi_scale)
    to_plot_y = h22.amp / amp_scale * np.exp(-1.j * h22.phi) # do not scale by phi_scale

    plt.figure()
    # plt.plot(h22.x, h22.y.real/np.max(h22.amp), label='h2,2 real TRAIN', lw=1)
    plt.plot(h22.x, to_plot_y.real, label='h2,2 real TRAIN', lw=1)
    plt.plot(to_plot_x, pred_strain.real, label='model', ls='--')
    plt.axvline(fit_t1)
    plt.axvline(fit_t2)
    plt.legend()
    # plt.yscale('log')
    plt.show()

    # plt.figure()
    # plt.plot(h22.x, h22.y.real/np.max(h22.amp), label='h2,2 real TRAIN', lw=1)
    # plt.plot(h22.x, h22.amp/np.max(h22.amp), label='h2,2 abs TRAIN', lw=1)
    # plt.plot(to_plot_x, pred, label='model', ls='--')
    # plt.legend()
    # plt.yscale('log')
    # plt.show()

    # plt.figure()
    # plt.plot(h22.x, h22.phi/ np.max(h22.phi), label='h2,2 real TRAIN', lw=1)
    # plt.plot(to_plot_x, pred, label='model', ls='--')
    # plt.legend()
    # plt.show()



