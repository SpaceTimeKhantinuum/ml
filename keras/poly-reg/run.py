import keras
from keras.models import Sequential
from keras.layers import Dense
import numpy as np


#create training data
#trX = np.linspace(-2, 2, 101)
trX = np.linspace(-1, 1, 101)
trY = 3 * trX**2. + np.random.random(trX.shape) * 0.33

# added in some points to see how it performs when you
# have isolated points beyond your main dataset - turns out - quite good

trX[0] = -2
trX[-1] = 2
trY[0] = 3 * trX[0]**2. + np.random.random(trX[0].shape) * 0.33
trY[-1] = 3 * trX[-1]**2. + np.random.random(trX[-1].shape) * 0.33


trX[1] = -1.5
trX[-2] = 1.5
trY[1] = 3 * trX[1]**2. + np.random.random(trX[1].shape) * 0.33
trY[-2] = 3 * trX[-2]**2. + np.random.random(trX[-2].shape) * 0.33


#create model
#single layer, fully connected, w/ linear activation function
#with uniform random weights
model = Sequential()
#model.add(Dense(input_dim=1, activation="linear", units=2, kernel_initializer="uniform"))
model.add(Dense(input_dim=1, activation="relu", units=200, kernel_initializer="uniform"))
#model.add(Dense(input_dim=1, activation="relu", units=200, kernel_initializer="uniform"))
#model.add(Dense(input_dim=1, activation="relu", units=200, kernel_initializer="uniform"))
#model.add(Dense(input_dim=1, activation="relu", units=200, kernel_initializer="uniform"))
model.add(Dense(units=1, activation='linear'))


weights = model.layers[0].get_weights()
print(weights)
#weights is a list of numpy arrays
w_init = weights[0][0][0]
b_init = weights[1][0]
print('regression model is initialized with weights w: %f, b: %f' % (w_init, b_init)) 

# now we shall train the model
# out loss function will be mean squared error (mse)
# and we will optimize using gradient descent (sgd)
model.compile(optimizer='sgd', loss='mse')

#feed the data using the fit function
model.fit(trX, trY, epochs=2000, verbose=0)

#weights are now updated
weights = model.layers[0].get_weights()
w_final = weights[0][0][0]
w2 = weights[0][0][1]
b_final = weights[1][0]
print('regression model is trained to have weight w: %.2f, b: %.2f' % (w_final, b_final))

trX2 = np.linspace(-2, 2, 101)
pred = model.predict(trX2)
trY2 = 3 * trX2**2. + np.random.random(trX2.shape) * 0.33

import matplotlib.pyplot as plt
plt.figure()
plt.scatter(trX, trY, s=2, color='k', label='train')
plt.scatter(trX2, trY2, s=1, color='C2', label='extra train')
#plt.plot(trx, trx*w_final  + trx**2 * w2 + b_final, label='model')
plt.plot(trX2, pred, label='model')
plt.legend()
plt.show()


