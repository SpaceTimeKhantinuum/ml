"""
first test using NR data

waveform list

filepath = '/Users/sebastian/work/data/SXS_BBH_0071_Res5.h5' #q=1
filepath = '/Users/sebastian/work/data/SXS_BBH_0169_Res5.h5' #q=2
filepath = '/Users/sebastian/work/data/SXS_BBH_0168_Res5.h5' #q=3
filepath = '/Users/sebastian/work/data/SXS_BBH_0167_Res5.h5' #q=4

filepath = '/Users/sebastian/work/data/SXS_BBH_0259_Res5.h5' #q=2.5

# /Users/sebastian/work/data/SXS_BBH_0107_Res5.h5 #5
# /Users/sebastian/work/data/SXS_BBH_0063_Res5.h5 #8
# /Users/sebastian/work/data/SXS_BBH_0303_Res5.h5 #10


"""

from collections import namedtuple
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np

import h5py
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d


def get_lm_mode(h5file, l, m, npts=5000):
    f = h5py.File(h5file, 'r')

    q = f.attrs['mass1']/f.attrs['mass2']
    eta = f.attrs['eta']

    amp_tmp = f['amp_l{0}_m{1}'.format(l, m)]
    amp_x = amp_tmp['X'].value
    amp_y = amp_tmp['Y'].value

    phase_tmp = f['phase_l{0}_m{1}'.format(l, m)]
    phase_x = phase_tmp['X'].value
    phase_y = phase_tmp['Y'].value

    f.close()

    amp_i = interp1d(amp_x, amp_y)
    phase_i = interp1d(phase_x, phase_y)

    t1 = max(amp_x[0], phase_x[0])
    t2 = min(amp_x[-1], phase_x[-1])

    t1,t2=-600,100

    common_times = np.linspace(t1, t2, npts)

    amplist = amp_i(common_times)
    phaselist = phase_i(common_times)

    amp = TimeSeries(x=common_times, y=amplist, amp=None, phi=None, q=None, eta=None)
    phi = TimeSeries(x=common_times, y=phaselist, amp=None, phi=None, q=None, eta=None)

    hlm_tmp = amp.y * np.exp(-1.j * phi.y)

    hlm = TimeSeries(x=common_times, y=hlm_tmp, amp=amp.y, phi=phi.y, q=q, eta=eta)

    return hlm


def make_nn_model(amp_or_phi, optimizer, fit_t1, fit_t2, activation, epochs=50):
    # create model
    # single layer, fully connected, w/ linear activation function
    # with uniform random weights
    model = Sequential()
    #model.add(Dense(input_dim=1, activation="linear", units=2, kernel_initializer="uniform"))
    model.add(Dense(input_dim=1, activation="relu", units=10, kernel_initializer="uniform"))
    model.add(Dense(input_dim=1, activation="relu", units=10, kernel_initializer="uniform"))
    model.add(Dense(input_dim=1, activation="relu", units=10, kernel_initializer="uniform"))
    model.add(Dense(input_dim=1, activation="relu", units=10, kernel_initializer="uniform"))
    # model.add(Dense(input_dim=1, activation="relu", units=100, kernel_initializer="uniform"))
    # model.add(Dense(input_dim=1, activation="relu", units=100, kernel_initializer="uniform"))
    # model.add(Dropout(0.5))
    # model.add(Dense(units=1, activation='linear'))
    model.add(Dense(units=1, activation=activation))

    weights = model.layers[0].get_weights()
    # print(weights)
    # weights is a list of numpy arrays
    # w_init = weights[0][0][0]
    # b_init = weights[1][0]
    # print('regression model is initialized with weights w: %f, b: %f' %
        #   (w_init, b_init))

    # now we shall train the model
    # out loss function will be mean squared error (mse)
    # and we will optimize using gradient descent (sgd)
    # model.compile(optimizer='sgd', loss='mse')
    # model.compile(optimizer='rmsprop', loss='mse')
    # model.compile(optimizer='adam', loss='mse')
    # model.compile(optimizer='nadam', loss='mse')

    # was using this originally
    model.compile(optimizer=optimizer, loss='mse')
    #model.compile(optimizer=optimizer, loss='binary_crossentropy')


    # feed the data using the fit function

    tt1 = fit_t1
    tt2 = fit_t2

    to_fit_x = h22.x[(h22.x > tt1) & (h22.x < tt2)]
    # to_fit_y = h22.y.real[(h22.x > tt1) & (h22.x < tt2)] / np.max(h22.amp)

    if amp_or_phi == "amp":
        scale = np.max(h22.amp)
        to_fit_y = h22.amp[(h22.x > tt1) & (h22.x < tt2)] / scale
    elif amp_or_phi == "phi":
        scale = np.max(h22.phi)
        to_fit_y = h22.phi[(h22.x > tt1) & (h22.x < tt2)] / scale

    model.fit(to_fit_x, to_fit_y, epochs=epochs, verbose=1)
    return model, scale


def load_nr_waveforms(nrfiles, mode_l, mode_m, npts):

    hlms = [None] * len(nrfiles)

    for i, nrfile in enumerate(nrfiles):
        hlms[i] = get_lm_mode(nrfile, mode_l, mode_m, npts=npts)

    return hlms


if __name__ == "__main__":
    nrfiles = [
        '/Users/sebastian/work/data/SXS_BBH_0071_Res5.h5', #q=1
        '/Users/sebastian/work/data/SXS_BBH_0169_Res5.h5', #q=2
        '/Users/sebastian/work/data/SXS_BBH_0168_Res5.h5', #q=3
        '/Users/sebastian/work/data/SXS_BBH_0167_Res5.h5' #q=4
    ]

    # nrfiles = [
    #     '/Users/sebastian/work/data/SXS_BBH_0071_Res5.h5', #q=1
    #     '/Users/sebastian/work/data/SXS_BBH_0167_Res5.h5' #q=4
    # ]

    TimeSeries = namedtuple('ts', ['x', 'y', 'amp', 'phi', 'q', 'eta'])

    hlms = load_nr_waveforms(nrfiles, 2, 2, npts=1000)
    # hlms = load_nr_waveforms(nrfiles, 2, 0, npts=1000)

    npts = len(hlms[0].x)
    ncases = len(nrfiles)

    # multi-dimensional set-up

    # independent variables: time, mass-ratio
    # dependent variable: amplitude or phase

    # x_train = [[time], [mass-ratio]]
    # y_train =

    # x_train.shape = (Npts_time, Npts_mass-ratio)
    # y_train.shape = (Npts_time, 1)

    qlist = [np.array([float("{0:.4}".format(hlms[q].q)) for i in range(npts)]) for q in range(ncases)]
    etalist = [np.array([float("{0:.4}".format(hlms[q].eta)) for i in range(npts)]) for q in range(ncases)]
    # hack to get in the form

    # x_train.shape = (Npts_time, Npts_mass-ratio)
    # x_train = np.column_stack((np.asarray([hlms[0].x, qlist[0]]).T, qlist[1], qlist[2], qlist[3]))
    # y_train = np.row_stack((hlms[0].amp, hlms[1].amp,  hlms[2].amp,  hlms[3].amp))



    one = np.asarray([hlms[0].x, qlist[0]]).T
    two = np.asarray([hlms[0].x, qlist[1]]).T
    th = np.asarray([hlms[0].x, qlist[2]]).T
    fo = np.asarray([hlms[0].x, qlist[3]]).T
    x_train = np.row_stack((one,two, th, fo))

    print(x_train.shape)

    # one = np.asarray([hlms[0].x, qlist[0]]).T
    # two = np.asarray([hlms[0].x, qlist[1]]).T
    # x_train = np.row_stack((one,two))

    ###scales = [etalist[0], etalist[1], etalist[2], etalist[3]]

    ###y_train = np.row_stack((hlms[0].amp / scales[0], hlms[1].amp / scales[1],  hlms[2].amp / scales[2],  hlms[3].amp / scales[3]))

    scale = np.max(hlms[0].amp)

    # y_train = np.row_stack((hlms[0].amp, hlms[1].amp))
    y_train = np.row_stack((hlms[0].amp / scale, hlms[1].amp / scale,  hlms[2].amp / scale,  hlms[3].amp / scale))
    #phase: subtract off initial phase
    # y_train = np.row_stack((hlms[0].phi-hlms[0].phi[0], hlms[1].phi-hlms[1].phi[0],  hlms[2].phi-hlms[2].phi[0],  hlms[3].phi-hlms[3].phi[0]))
    new_shape = x_train.shape[0]
    y_train  = y_train.reshape(new_shape,)

    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # ax.scatter(x_train[:,0], x_train[:,1], y_train)
    # plt.show()

    # act='softplus'
    act='elu'
    #act='relu'

    model = Sequential()
    model.add(Dense(input_dim=2, activation=act, units=10, kernel_initializer="uniform"))
    model.add(Dense(input_dim=1, activation=act, units=10, kernel_initializer="uniform"))
    model.add(Dense(input_dim=1, activation=act, units=10, kernel_initializer="uniform"))
    model.add(Dense(input_dim=1, activation=act, units=10, kernel_initializer="uniform"))
    model.add(Dense(1, activation='linear'))

    model.compile(loss='mean_squared_error', optimizer='adam')

    print(x_train)
    print(y_train)
    import sys;sys.exit()

    model.fit(x_train, y_train, epochs=500)


    # save model
    # serialize model to JSON
    model_json = model.to_json()
    with open("model-{0}.json".format(act), "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("model-{0}.h5".format(act))
    print("Saved model to disk")

    #mass-ratio=1
    # y = model.predict(x_train[:100], verbose=1)
    #mass-ratio=4
    # y = model.predict(x_train[300:], verbose=1)

    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # ax.scatter(x_train[:,0], x_train[:,1], y_train)
    # ax.scatter(x_train[:,0], x_train[:,1], y)
    # plt.show()

    #mass-ratio=1
    # plt.figure()
    # plt.plot(x_train[:100,0], y_train[:100])
    # plt.plot(x_train[:100,0], y)
    # plt.show()

    #mass-ratio=4
    # plt.figure()
    # plt.plot(x_train[300:,0], y_train[300:])
    # plt.plot(x_train[300:,0], y)
    # plt.show()


    # test at q=2.5
    filepath = '/Users/sebastian/work/data/SXS_BBH_0259_Res5.h5' #q=2.5
    q_test_hlm = load_nr_waveforms([filepath], 2, 2, npts=1000)
    q_test_list = [float("{0:.4}".format(q_test_hlm[0].q)) for i in range(npts)]
    eta_test_list = [float("{0:.4}".format(q_test_hlm[0].eta)) for i in range(npts)]
    x_test = np.asarray([q_test_hlm[0].x, q_test_list]).T

    y_test = model.predict(x_test, verbose=1)

    q2_test_hlm = load_nr_waveforms(['/Users/sebastian/work/data/SXS_BBH_0169_Res5.h5'], 2, 2, npts=1000)
    q3_test_hlm = load_nr_waveforms(['/Users/sebastian/work/data/SXS_BBH_0168_Res5.h5'], 2, 2, npts=1000)

    plt.figure()
    plt.plot(q_test_hlm[0].x, q_test_hlm[0].amp, label='q2.5 nr')
    plt.plot(x_test[:,0], y_test * scale,label='q2.5 model')
    ###plt.plot(x_test[:,0], y_test * eta_test_list[0],label='q2.5 model')

    plt.plot(q2_test_hlm[0].x, q2_test_hlm[0].amp,color='k',lw=1, label='q2 nr')
    plt.plot(q3_test_hlm[0].x, q3_test_hlm[0].amp,color='red',lw=1, label='q3 nr')

    q2_test_list = [2 for i in range(npts)]
    x2_test = np.asarray([q2_test_hlm[0].x, q2_test_list]).T

    y2_test = model.predict(x2_test, verbose=1)
    plt.plot(q2_test_hlm[0].x, y2_test*scale,lw=2, label='q2 model', ls='--')
    ###eta_q2 = 0.222222
    ###plt.plot(q2_test_hlm[0].x, y2_test*eta_q2,lw=2, label='q2 model', ls='--')


    plt.legend()
    plt.show()

    import sys; sys.exit()










    # h22 = hlms[0]

    # h22 = get_lm_mode(filepath, 2, 2, npts=1000)
    #h22 = get_lm_mode(filepath, 4, 4, npts=1000)
    # h2m2 = get_lm_mode(filepath, 2, -2, npts=5000)

    # plt.figure(figsize=(14, 7))
    # for hlm in hlms:
    #     plt.plot(hlm.x, hlm.y.real, label='h2,2 real', lw=1)
    # plt.legend()
    # plt.show()


    fit_t1=-400
    fit_t2=100

    model_amp, amp_scale = make_nn_model(amp_or_phi="amp", optimizer='adam', fit_t1=fit_t1, fit_t2=fit_t2, activation='linear', epochs=50)
    # model_phi, phi_scale = make_nn_model(amp_or_phi="phi", optimizer='adam', fit_t1=fit_t1, fit_t2=fit_t2, activation='linear', epochs=500)



    import sys; sys.exit()

    t1plot = -600
    t2plot = 100

    to_plot_x = h22.x[(h22.x > t1plot) & (h22.x < t2plot)]

    pred_amp = model_amp.predict(to_plot_x)
    pred_phi = model_phi.predict(to_plot_x)


    #
    # NOTE:
    # IMPORTANT TO UNDO THE PHASE SCALING SO THAT YOU CAN SEE
    # THE OSCILLATIONS IN THE STRAIN
    #
    pred_strain = pred_amp * np.exp(-1.j * pred_phi * phi_scale) #UNDO SCALING BY MULTIPLYING BY phi_scale

    # to_plot_y = h22.amp / amp_scale * np.exp(-1.j * h22.phi / phi_scale)
    to_plot_y = h22.amp / amp_scale * np.exp(-1.j * h22.phi) # do not scale by phi_scale

    plt.figure()
    # plt.plot(h22.x, h22.y.real/np.max(h22.amp), label='h2,2 real TRAIN', lw=1)
    plt.plot(h22.x, to_plot_y.real, label='h2,2 real TRAIN', lw=1)
    plt.plot(to_plot_x, pred_strain.real, label='model', ls='--')
    plt.axvline(fit_t1)
    plt.axvline(fit_t2)
    plt.legend()
    # plt.yscale('log')
    plt.show()

    # plt.figure()
    # plt.plot(h22.x, h22.y.real/np.max(h22.amp), label='h2,2 real TRAIN', lw=1)
    # plt.plot(h22.x, h22.amp/np.max(h22.amp), label='h2,2 abs TRAIN', lw=1)
    # plt.plot(to_plot_x, pred, label='model', ls='--')
    # plt.legend()
    # plt.yscale('log')
    # plt.show()

    # plt.figure()
    # plt.plot(h22.x, h22.phi/ np.max(h22.phi), label='h2,2 real TRAIN', lw=1)
    # plt.plot(to_plot_x, pred, label='model', ls='--')
    # plt.legend()
    # plt.show()



