"""
example from http://cv-tricks.com/tensorflow-tutorial/keras/
"""


import keras
from keras.models import Sequential
from keras.layers import Dense
import numpy as np


#create training data
trX = np.linspace(-1, 1, 101)
trY = 3 * trX + np.random.random(trX.shape) * 0.33

#create model
#single layer, fully connected, w/ linear activation function
#with uniform random weights
model = Sequential()
model.add(Dense(input_dim=1, activation="linear", units=1, kernel_initializer="uniform"))


weights = model.layers[0].get_weights()
print(weights)
#weights is a list of numpy arrays
w_init = weights[0][0][0]
b_init = weights[1][0]
print('Linear regression model is initialized with weights w: %f, b: %f' % (w_init, b_init)) 

# now we shall train the model
# out loss function will be mean squared error (mse)
# and we will optimize using gradient descent (sgd)
model.compile(optimizer='sgd', loss='mse')

#feed the data using the fit function
model.fit(trX, trY, epochs=200, verbose=0)

#weights are now updated
weights = model.layers[0].get_weights()
w_final = weights[0][0][0]
b_final = weights[1][0]
print('Linear regression model is trained to have weight w: %.2f, b: %.2f' % (w_final, b_final))

import matplotlib.pyplot as plt
plt.figure()
plt.scatter(trX, trY, s=2, color='k', label='train')
plt.plot(trX, trX * w_final + b_final, label='model')
plt.legend()
plt.show()


