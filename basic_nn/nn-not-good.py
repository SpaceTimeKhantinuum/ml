
# I didn't finishe this

# source /Users/sebastian/work/git/stk/ml/waveforms/venv-wf/bin/activate
# following
# https://towardsdatascience.com/how-to-build-your-own-neural-network-from-scratch-in-python-68998a08e4f6
# building a neural network from scratch

import numpy as np


class NeuralNetwork(object):
    def __init__(self, x, y, Nh1=4, Nh2=4):
        """
        Nh1: int, number of neurons in layer 1
        """
        self.input = x
        self.weights1 = np.random.rand(Nh1, self.input.shape[1])
        self.weights2 = np.random.rand(Nh2, Nh1)
        self.y = y
        self.output = np.zeros(y.shape)

        self.layer1 = np.zeros(Nh1)
        self.bias1 = np.zeros(Nh1)
        self.bias2 = np.zeros(Nh2)

    def run(self):
        # this for loop could be done with a np.matmul
        for i, x in enumerate(self.input):
            # print(i)
            # print(x)
            # loop over each input dataset
            # compute layer 1

            nval = np.sum(np.dot(self.weights1, x) + self.bias1)
            self.layer1[i] = self.sigmoid(nval)

        for i, x in enumerate(self.layer1):
            nval = np.sum(np.dot(self.weights2, x) + self.bias2)
            self.output[i] = self.sigmoid(nval)

        self.loss()

    def loss(self):
        err = np.sum((self.y - self.output)**2.)
        print(err)

    #def backpropagate(self):
    #    y = self.y
    #    ybar = self.output
    #    x =
    #    return 2. * (y-ybar) * z*(1.-z) * x

    def sigmoid(self, x):
        return 1. / (1. + np.exp(-x))


    def sigmoid_derivative(self, x):
        return x * (1.0 - x)

# x is a [r, c] = [4, 3] matrix
# this means I have 4 different sets of data
# and each one has a length of 3.
# e.g., 4 different measurements each consisting
# of 3 numbers.
x = np.array([
    [0, 0, 1],
    [1, 1, 1],
    [1, 0, 1],
    [0, 1, 1]
], dtype=np.float64)

# target, y, is a [r, c] = [4, 1] vector
# these are the outputs for each row of x.
# You could think of this as the 3 numbers from a single measurement
# get mapped into # a single number; either 1 or 0.
# We have 4 entires here because we have 4 measurements.
y = np.array([[0, 1, 1, 0]]).T

# the shape of the weights should be such that we can take the
# dot product of it with the inputs, x.

# (r,  c)  = (r,  c) . (r, c)
# (Nh, 3)  = (Nh, 4) . (4, 3)
# where Nh is the number of neurons in the layer
Nh = 4
weights = np.random.rand(Nh, x.shape[1])


NN = NeuralNetwork(x, y)
NN.run()

print(NN.output)
