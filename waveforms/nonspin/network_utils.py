import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import numpy as np
import copy

def make_1d_list_repeat(val, npts):
    """
    returns a numpy array that is val repeated npts times
    """
    return np.asarray([val] * npts)

def prepare_x_train_2d_data_for_neural_net(dim1_data):

    npts_time = dim1_data[0].npts

    x_train = None

    for i in range(len(dim1_data)):
        dim2_list = make_1d_list_repeat(dim1_data[i].q, npts_time)
        times_and_q = np.asarray([dim1_data[i].times, dim2_list]).T
        if x_train is None:
            x_train = np.row_stack((times_and_q))
        else:
            x_train = np.row_stack((x_train, times_and_q))

    return x_train

def prepare_y_train_2d_data_for_neural_net(dim1_data, data_attribute, scaling_method=None, scale_value=None, centre_method=None):
    y_train = None

    for i in range(len(dim1_data)):
        if scaling_method == 'eta':
            scale = getattr(dim1_data[i], 'eta')
        elif scaling_method == 'maximum_of_first':
            scale = np.max(getattr(dim1_data[0], data_attribute))
        elif scaling_method == 'maximum_of_each':
            scale = np.max(getattr(dim1_data[i], data_attribute))
        if scaling_method == 'by_value':
            scale = scale_value
        elif scaling_method is None:
            scale = 1.

        if centre_method == 'subtract_last':
            centre = getattr(dim1_data[i], data_attribute)[-1]
        elif centre_method is None:
            centre = 0.

        if y_train is None:
            y_data = copy.copy(getattr(dim1_data[i], data_attribute))
            y_data -= centre
            y_data /= scale
            y_train = y_data
        else:
            y_data = copy.copy(getattr(dim1_data[i], data_attribute))
            y_data -= centre
            y_data /= scale
            y_train = np.row_stack((y_train, y_data))

    y_train = y_train.reshape( dim1_data[0].npts * len(dim1_data) )

    return y_train

def train_network(x_train, y_train, act, epochs=500, network_topology="default", optimizer="adam", metrics=None, callbacks=None):
    """
    network_topology="default" or "ed_style"
    metrics: {deafult: None, ['accuracy']}
    """
    # act='softplus'
    # act='elu'
    #act='relu'

    if network_topology=="default":
        model = Sequential()
        model.add(Dense(input_dim=2, activation=act, units=10, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=10, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=10, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=10, kernel_initializer="uniform"))
        model.add(Dense(1, activation='linear'))
    elif network_topology=="ed_style":
        model = Sequential()
        model.add(Dense(input_dim=2, activation=act, units=256, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=32, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=32, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=32, kernel_initializer="uniform"))
        model.add(Dense(1, activation='linear'))
    elif network_topology=="ed_style_deeper":
        model = Sequential()
        model.add(Dense(input_dim=2, activation=act, units=256, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=32, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=32, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=32, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=32, kernel_initializer="uniform"))
        model.add(Dense(1, activation='linear'))
    elif network_topology=="ed_style_wider":
        model = Sequential()
        model.add(Dense(input_dim=2, activation=act, units=512, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=32, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=32, kernel_initializer="uniform"))
        model.add(Dense(input_dim=1, activation=act, units=32, kernel_initializer="uniform"))
        model.add(Dense(1, activation='linear'))
    else:
        print("unknown option for 'network_topology' = {0}".format(network_topology))
        import sys; sys.exit()


    # model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])
    if metrics:
        model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=metrics)
    else:
        model.compile(loss='mean_squared_error', optimizer=optimizer)

    model.fit(x_train, y_train, epochs=epochs, callbacks=callbacks)

    return model
