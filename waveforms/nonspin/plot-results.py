#!/usr/bin/env python
import os
import phenom

from phenom import eta_from_q, q_from_eta

import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as IUS
import matplotlib.pyplot as plt

from keras.models import model_from_json

from waveform import SingleModeNRWaveform

def makedirs(dir_name):
    """
    Wrapper function for os.mkdirs.
    recurssive directory maker
    """
    try:
        os.makedirs(dir_name)
    except OSError:
        pass


def load_nn_model(jname, h5name):
    # load json and create model
    json_file = open(jname, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(h5name)
    loaded_model.compile(loss='mean_squared_error', optimizer='adam')
    return loaded_model

def predict_h22(amp_nn, phi_nn, q, times, scale_value=600):
    qlist = np.zeros(len(times)) + q

    X = np.column_stack((times, qlist))

    amp = amp_nn.predict(X)
    phi = phi_nn.predict(X)
    phi *= scale_value

    return amp, phi

def return_masked_array(array, x1, x2):
    mask = (array >= x1) & (array <= x2)
    return array[mask]

def plot_model_and_nr(idx, nrdata, amp_nn, phi_nn, aligned_time=-200, outdir=None, outname=None):
    nn_amp, nn_phi = predict_h22(amp_nn, phi_nn, nrdata[idx].q, nrdata[idx].times)
    phi_scale = nrdata[idx].phi[-1]
    eta_scale = nrdata[idx].eta

    nrphase_int = IUS(nrdata[idx].times, nrdata[idx].phi)
    modelphase_int = IUS(nrdata[idx].times, (nn_phi[:,0] + phi_scale))
    shift = nrphase_int(aligned_time)-modelphase_int(aligned_time)

    masked_times = return_masked_array(nrdata[idx].times, -600, 50)

    nrphase_sampled = nrphase_int(masked_times)
    modelphase_sampled = modelphase_int(masked_times)
    phase_diff = nrphase_sampled - modelphase_sampled - shift

    max_phase_diff = np.max(np.abs(phase_diff))
    avg_phase_diff = np.mean(phase_diff)

    # print("max_phase_diff = {0}".format(max_phase_diff))

    fig, axes = plt.subplots(1,3, figsize=(14,4))
    axes[0].set_title('mass-ratio = {0}'.format(nrdata[idx].q))
    axes[0].plot(nrdata[idx].times, nrdata[idx].hlm.real, label='nr')
    axes[0].plot(nrdata[idx].times, np.real(eta_scale*nn_amp * np.exp(-1.j *  (nn_phi + phi_scale+shift))), label='model')
    axes[0].plot(nrdata[idx].times, eta_scale*nn_amp, label='amp-model')

    axes[0].set_xlim(-800,50)
    axes[0].axvline(-600, color='black', ls='--')
#     axes[0].set_xlim(-1000,50)
    axes[0].legend()

    axes[1].plot(nrdata[idx].times, nrdata[idx].hlm.real)
    axes[1].plot(nrdata[idx].times, np.real(eta_scale*nn_amp * np.exp(-1.j * (nn_phi + phi_scale+shift))))
    axes[1].plot(nrdata[idx].times, eta_scale*nn_amp, label='amp-model')
    axes[1].set_xlim(-100,50)

    axes[2].plot(masked_times, phase_diff)
    axes[2].set_xlim(-600,50)
    axes[2].set_title("max_phase_diff = {0:.4}\n".format(max_phase_diff) + "avg_phase_diff = {0:.4}".format(avg_phase_diff))
    axes[2].axhline(max_phase_diff, color='black', ls='--')
    axes[2].axhline(-max_phase_diff, color='black', ls='--')
    axes[2].axhline(avg_phase_diff, color='black', ls='-.')
    axes[2].axvline(aligned_time, color='black', ls='-.')

    makedirs(outdir)
    plt.savefig(os.path.join(outdir, outname))
    return max_phase_diff

if __name__ == "__main__":

    model_name="4"

    if model_name == "1":
        amp_model = load_nn_model(jname='model-softplus-amp.json', h5name='model-softplus-amp.h5')
        phase_model = load_nn_model(jname='model-softplus-phi.json', h5name='model-softplus-phi.h5')
        outdir='../plots/plots-model-1/'
    if model_name == "2":
        amp_model = load_nn_model(jname='model-softplus-amp.json', h5name='model-softplus-amp.h5')
        phase_model = load_nn_model(jname='model-softplus-phi-2.json', h5name='model-softplus-phi-2.h5')
        outdir='../plots/plots-model-2/'
    if model_name == "3":
        amp_model = load_nn_model(jname='model-softplus-amp.json', h5name='model-softplus-amp.h5')
        phase_model = load_nn_model(jname='model-softplus-phi-3.json', h5name='model-softplus-phi-3.h5')
        outdir='../plots/plots-model-3/'
    if model_name == "4":
        amp_model = load_nn_model(jname='model-softplus-amp.json', h5name='model-softplus-amp.h5')
        phase_model = load_nn_model(jname='model-softplus-phi-4.json', h5name='model-softplus-phi-4.h5')
        outdir='../plots/plots-model-4/'

    nrfiles = [
        '/Users/sebastian/work/data/SXS_BBH_0071_Res5.h5', #q=1
        '/Users/sebastian/work/data/SXS_BBH_0169_Res5.h5', #q=2
        '/Users/sebastian/work/data/SXS_BBH_0168_Res5.h5', #q=3
        '/Users/sebastian/work/data/SXS_BBH_0167_Res5.h5', #q=4
        '/Users/sebastian/work/data/SXS_BBH_0259_Res5.h5', #q=2.5
        '/Users/sebastian/work/data/SXS_BBH_0107_Res5.h5', #5
        '/Users/sebastian/work/data/SXS_BBH_0063_Res5.h5', #8
        '/Users/sebastian/work/data/SXS_BBH_0303_Res5.h5' #10
    ]

    ell = 2
    mm = 2

    npts_time = 1000*10
    npts_mass_ratio = len(nrfiles)

    nrdata = [SingleModeNRWaveform(nrfile, ell, mm, npts_time) for nrfile in nrfiles]

    phase_diff = np.zeros(len(nrdata))

    for idx in range(len(nrdata)):
        outname=nrfiles[idx].split('/')[-1].split('.h5')[0]
        print("working: {0}".format(outname))
        phase_diff[idx] = plot_model_and_nr(idx, nrdata, amp_model, phase_model, aligned_time=0, outdir=outdir, outname='{}.png'.format(outname))

    summed_phase_diff = np.sum(phase_diff)
    print("summed max phase difference = {0:.4}".format(summed_phase_diff))

    np.savetxt(os.path.join(outdir,"sum_max_diff.txt"), np.array([summed_phase_diff]))