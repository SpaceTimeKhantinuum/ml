#!/usr/bin/env python

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

import numpy as np
from waveform import SingleModeNRWaveform

import network_utils as nu

# load data

# nrfiles = [
#     '/Users/sebastian/work/data/SXS_BBH_0071_Res5.h5', #q=1
#     '/Users/sebastian/work/data/SXS_BBH_0169_Res5.h5', #q=2
#     '/Users/sebastian/work/data/SXS_BBH_0168_Res5.h5', #q=3
#     '/Users/sebastian/work/data/SXS_BBH_0167_Res5.h5' #q=4
# ]

nrfiles = [
    '/Users/sebastian/work/data/SXS_BBH_0071_Res5.h5', #q=1
    '/Users/sebastian/work/data/SXS_BBH_0169_Res5.h5', #q=2
    '/Users/sebastian/work/data/SXS_BBH_0167_Res5.h5', #q=4
    '/Users/sebastian/work/data/SXS_BBH_0107_Res5.h5', #5
    '/Users/sebastian/work/data/SXS_BBH_0303_Res5.h5' #10
]

# q1 = SingleModeNRWaveform(nrfiles[0], 2, 2, 1000)

ell = 2
mm = 2

npts_time = 1000
npts_mass_ratio = len(nrfiles)

# t1=None
# t2=None
t1=-600
t2=80

nrdata = [SingleModeNRWaveform(nrfile, ell, mm, npts_time,t1=t1,t2=t2) for nrfile in nrfiles]


# prepare data for input to neural network

# x_train.shape = (npts_time, input_dim)
# y_train.shape = (npts_time, 1)

# input_dim = 2 for the case of modelling the non-spinning sector.
# x_train looks like:
# x_train = [
# [t0, q0],[t1,q0],...[tn,q0],
# [t0, q1],[t1,q1],...[tn,q1],
# [t0, qN],[t1,qN],...[tn,qN],
# ]

x_train=nu.prepare_x_train_2d_data_for_neural_net(nrdata)

print(x_train)
print(x_train.shape)

scaling_method='eta'
# scaling_method='maximum_of_first'
# scaling_method='maximum_of_each'


#TODO: Need to make it easier to scale the data
#TODO: need to implement a centring of the data

data_attribute='amp'
# data_attribute='phi'
y_train=nu.prepare_y_train_2d_data_for_neural_net(nrdata,
    data_attribute=data_attribute,
    scaling_method=scaling_method,
    centre_method=None)


print(y_train)
print(y_train.shape)


## plot the input data
# from mpl_toolkits.mplot3d import Axes3D
# import matplotlib.pyplot as plt
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(x_train[:,0], x_train[:,1], y_train)
# plt.show()

act='softplus'
# act='elu'
# act='relu'

# use this to be able to plot the cost-function vs epoch
from keras.callbacks import History
history = History()

# model = nu.train_network(x_train, y_train, act, epochs=500)
model = nu.train_network(x_train, y_train, act, epochs=500, callbacks=[history])

# save model

version="2"
model_name = "model-{0}-{1}-{2}".format(act,data_attribute,version)

loss=history.history['loss']
x=range(len(loss))
plt.figure()
plt.plot(x, loss)
plt.yscale('log')
plt.savefig(model_name+"-loss.png")

# serialize model to JSON
model_json = model.to_json()
with open("{0}.json".format(model_name), "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("{0}.h5".format(model_name))
print("Saved model to disk")