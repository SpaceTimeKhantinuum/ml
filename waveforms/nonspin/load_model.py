from phenom import eta_from_q, q_from_eta

import numpy as np
import matplotlib.pyplot as plt

from keras.models import model_from_json

# load json and create model
json_file = open('model-softplus-amp.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model_amp = model_from_json(loaded_model_json)
# load weights into new model
loaded_model_amp.load_weights("model-softplus-amp.h5")
print("Loaded amp model from disk")

# load json and create model
json_file = open('model-softplus-phi.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model_phi = model_from_json(loaded_model_json)
# load weights into new model
loaded_model_phi.load_weights("model-softplus-phi.h5")
print("Loaded phi model from disk")


loaded_model_amp.compile(loss='mean_squared_error', optimizer='adam')

loaded_model_phi.compile(loss='mean_squared_error', optimizer='adam')


times = np.linspace(-600, 70, 1000)
qlist = np.zeros(len(times)) + 1

X = np.column_stack((times, qlist))
amp = loaded_model_amp.predict(X)
phi = loaded_model_phi.predict(X)

plt.figure()
plt.plot(times, np.real(amp * np.exp(-1.j * phi)))
plt.show()

# looping over mass-ratio

times = np.linspace(-600, 70, 1000)


plt.figure()
for q in range(1,5):
    eta = eta_from_q(q)
    qlist = np.zeros(len(times)) + q
    X = np.column_stack((times, qlist))
    amp = loaded_model_amp.predict(X) * eta
    phi = loaded_model_phi.predict(X)
    # plt.plot(times, amp, label=q)
    # plt.plot(times, phi, label=q)
    plt.plot(times, np.real(amp * np.exp(-1.j * phi)))
plt.legend()
plt.show()

