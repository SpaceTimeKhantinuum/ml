# idea

Testing how well a domain decomposition can be done using
an unsupervised decision tree.

For the samples in each leaf node we build a surrogate
and fit them with cubic(?) polynomials

Build a GMM of each leaf node so that we can more easily sample
new training data that is mostly constrained to be in
a given leaf node so that we can generate more
training data where we need it.