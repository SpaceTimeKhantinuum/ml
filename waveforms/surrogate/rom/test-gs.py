"""
an example script to compare
the different versions of the
gram-schmidt algorithm
as well as an SVD to compare

to run

$ python test-gs.py

the error should be zero to near machine precision
the condition number should be one to near machine precision

these two conditions are only met for the iterative modified
gram-schmidt algorithm
as well as the SVD.

acknowledgements


 - Iterative algorithms for Gram-Schmidt orthogonalization: Hoffmann 1989
 (https://link.springer.com/article/10.1007/BF02241222)

 - Chad Galley
 https://bitbucket.org/chadgalley/rompy/src/master/greedy.py

"""

import gs
import numpy as np


# test data
nn = 100
x = np.linspace(-1,1,nn)
V = np.array([x**n for n in range(nn)])

print("\n\n")
print("classical gram-schmidt")
print("="*20)
a = gs.cgs(V, return_orthonormal=True)
gs.check_basis(a, check_unit_length=True)

print("\n\n")
print("modified gram-schmidt")
print("="*20)
b = gs.mgs(V, return_orthonormal=True)
gs.check_basis(b, check_unit_length=True)

print("\n\n")
print("iterative modified gram-schmidt")
print("="*20)
c = gs.imgs(V, return_orthonormal=True)
gs.check_basis(c, check_unit_length=True)

print("\n\n\n")
print("my version of Chad Galley's imgs")
print("="*20)
d = gs._imgs_galley(V)
d = gs.compute_Uhat(d)
gs.check_basis(d, check_unit_length=True)


print("\n\n\n")
print("compare with Chad Galley's rompy package")
print("="*20)

import rompy as rp
integration = rp.Integration([-1, 1], num=len(x), rule='riemann')
gramschmidt = rp.GramSchmidt(V, integration)
basis = gramschmidt.make()
basis = gs.compute_Uhat(basis)
gs.check_basis(basis, check_unit_length=True)


print("\n\n\n")
print("compare with SVD")
print("="*20)
_, _, vh = np.linalg.svd(V, full_matrices=True)
gs.check_basis(vh, check_unit_length=True)
