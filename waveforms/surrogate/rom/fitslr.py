import numpy as np

def generate_degrees(maxdegs, max_deg_total=None):
    """
    generates an array of monomial exponents (or degrees)
    that can be used to generate a design matrix to perform
    linear basis polynomial regression
    
    intput:
        maxdegs: list of len number of dimensions
            list of max deg in each dimension
        max_deg_total: int (default: None)
            max degree of a term.
            If None then will use the maximum of `maxdegs`.
        
    returns:
        numpy.ndarray of shape (M, dim)
        M = total number of terms
        dim = dimension
    """
    if max_deg_total == None:
        max_deg_total = np.max(maxdegs)

    # create lists of degrees for each dimension
    xsdims = [range(deg+1) for deg in maxdegs]
    # tensor product them
    xs = np.meshgrid(*xsdims)
    # ravel them into a list into a 1D array
    xs = list(map(np.ravel, xs))
    # degrees is an np.ndarray of shape (M, dim)
    degrees = np.array(list(zip(*xs)))
    # restrict the max degree of each term to be less than max_deg_total+1
    mask_total_deg = np.sum(degrees,axis=-1) < max_deg_total + 1
    # restrict the max degree of each dimension
    mask_degs = [degrees[:,i] < maxdegs[i] + 1 for i in range(len(maxdegs))]
    # make a mega mask of all masks
    mask = np.prod(np.row_stack((mask_total_deg, *mask_degs)), axis=0, dtype=bool)
    return np.array(degrees[mask])

class LinearPolynomialBasisRegression(object):
    def __init__(self):
        pass
    
    def make_degrees(self, maxdegs, max_deg_total):
        return generate_degrees(maxdegs, max_deg_total)
    
    def fit(self, X, y, method, maxdegs, max_deg_total):
        """
        input:
            X: np.ndarray. shape (N, ndim)
            y: np.ndarray. shape (N, 1)
            method:
                'direct': np.linalg.pinv. moore-penrose pseudo inverse
                'least-squres': np.linalg.lstsq. lapack least squares algorithm        
        
        assigns the self.w_ml attribute.
        These are the maximum likelihood coefficients or weights.
        
        """
        N, ndim = X.shape
        
        self.degrees = generate_degrees(maxdegs, max_deg_total).tolist()
        
        Phi = self.make_design_matrix(X, self.degrees)
        
        if method == 'direct':
            self.w_ml = np.dot(np.linalg.pinv(Phi), y)
        elif method == 'least-squres':
            self.w_ml = np.linalg.lstsq(Phi, y, rcond=-1)[0]
        
    def make_design_matrix(self, X, degrees):
        Phi = np.stack([np.prod(X**d, axis=1) for d in degrees], axis=-1)
        return Phi
    
    def predict(self, X):
        
        Phi = self.make_design_matrix(X, self.degrees)
        return np.dot(Phi, self.w_ml)
    
    def loss(self, X, y, method='sum-of-squares'):
        """
        method:
            'sum-of-squres':
            'RMSE': 
            'MSE': 
        """
        
        yhat = self.predict(X)
        
        def mse(y, yhat):
            """
            mean-squared-error function
            """
            errors = (y - yhat)
            loss = np.sum(errors**2, axis=0) / len(y)
            return loss
            
        if method == 'sum-of-squares':
            errors = (y - yhat)
            loss = np.sum(errors**2, axis=0) / 2.0
        elif method == 'MSE':
            loss = mse(y, yhat)
        elif method == 'RMSE':
            loss = np.sqrt(mse(y, yhat))
        else:
            raise ValueError(f"method = {method} not valid.")
            
        return loss