import numpy as np

from sklearn.linear_model import Ridge, LinearRegression, Lasso, ElasticNet, Lars, LassoLars, BayesianRidge, Perceptron
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV

from scipy.stats import uniform
from sklearn.model_selection import RandomizedSearchCV

class Fit(object):
    def __init__(self, domain, values):
        """
        input:
            domain: numpy.ndarray
                N,M matrix where N is the number of entries
                and M is the number of coordinates
            values: numpy.ndarray
                N matrix
        """
        self.domain = domain
        self.values = values
        self.npoints = len(values)
        
        _, self.ndim = self.domain.shape
        print(f"dimensionality of space = {self.ndim}")
        
    def fit(self, method, degree=4, cv=5):
        
        if method == "bayesian":
            self.grid = self._PolynomialRegression_Bayesian(degree=degree)
        elif method == "linear":
            self.grid = self._PolynomialRegression_LinearRegression(degree=degree)
        elif method == "linear_grid":
            self.grid = self._PolynomialRegression_LinearRegression_Grid(max_degree=degree, cv=cv)
        
        # for 1d data
#         self.grid.fit(self.domain.reshape(-1,1), self.values.reshape(-1,1).ravel())
        self.grid.fit(self.domain, self.values)
        
        if method == "bayesian":
            self.model = self.grid.best_estimator_
        elif method == "linear":
            self.model = self.grid
        elif method == "linear_grid":
            self.model = self.grid.best_estimator_
        
        
        
        
    def eval(self, domain):
        return self.model.predict(domain)
        
        
    def _PolynomialRegression_Bayesian(self, degree=6, **kwargs):
        
        pipeline = make_pipeline(PolynomialFeatures(degree),
                                 BayesianRidge(**kwargs))

        spacing = 1./np.logspace(-5,10,20)

        param_grid = {'polynomialfeatures__degree': np.arange(20),
                      'bayesianridge__fit_intercept': [True, False],
                      'bayesianridge__normalize': [True, False],
                      'bayesianridge__alpha_1': spacing,
                      'bayesianridge__alpha_2': spacing,
                      'bayesianridge__lambda_1': spacing,
                      'bayesianridge__lambda_2': spacing
                     }

        grid = RandomizedSearchCV(
            pipeline,
            param_grid,
            random_state=1,
            n_iter=20,
            cv=4,
            verbose=0,
            n_jobs=-1)
        
        return grid

    def _PolynomialRegression_LinearRegression(self, degree=6, **kwargs):
        
        pipeline = make_pipeline(PolynomialFeatures(degree),
                                 LinearRegression(**kwargs))

        return pipeline

    
    
    def _PolynomialRegression_LinearRegression_Grid(self, max_degree, cv, **kwargs):

        def _PolynomialRegression(degree=2, **kwargs):
            return make_pipeline(PolynomialFeatures(degree), LinearRegression(**kwargs))

        param_grid = {
            'polynomialfeatures__degree': np.arange(max_degree),
            'linearregression__fit_intercept': [True, False],
            'linearregression__normalize': [True, False]}


        grid = GridSearchCV(_PolynomialRegression(), param_grid, 
                                 cv=cv, 
                                 scoring='neg_mean_squared_error', 
                                 verbose=0) 

        return grid