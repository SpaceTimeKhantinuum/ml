import numpy as np
import greedyrb
import gprfit
import fitslr

import george
from george import kernels

class Surrogate1D(object):
    def __init__(self, integration):
        self.integration = integration
        
        self.grb = greedyrb.GreedyReducedBasis(
            integration=self.integration
        )
        
        pass
        
    def build_seed_basis(self, ts, ts_coords):
        print("building seed basis")
        self.grb.build_seed_basis(ts=ts, ts_coords=ts_coords)
        
    def run_greedy_sweep(self, ts, ts_coords, greedy_tol=1e-4, verbose=False):
        self.grb.greedy_sweep(ts, ts_coords, greedy_tol=greedy_tol)
        
    def build_eim(self, ts):
        self.grb.build_eim(ts)
        
    def fit_eim(self, method, maxdegs=[5], solver=george.HODLRSolver, verbose=True, jitter=1e-2):
        """
        method in ['lr', 'gpr']
        solver in [george.HODLRSolver, george.BasicSolver]
        """
        self.method = method
        
        fits = []
        
        x_to_fit = np.log(self.grb.greedy_points)

        for i, eim_y in enumerate(self.grb.eim.data):
            
#             yerr = None

            if self.method == 'lr':
                fit = fitslr.LinearPolynomialBasisRegression()
                fit.fit(x_to_fit.reshape(-1, 1), eim_y, method='direct', maxdegs=maxdegs, max_deg_total=None)
                fits.append(fit)
            elif self.method == 'gpr':
                yerr = np.zeros(eim_y.shape) + jitter
                fit = gprfit.GPRFit(x_to_fit, eim_y, yerr=yerr)
                kernel = np.var(eim_y) * kernels.ExpSquaredKernel(0.5, ndim=1, axes=0)
                fit.setup_kernel(kernel)

                fit.fit(solver=solver)
                fit.opt()
                fits.append(fit)

            if verbose:
                import matplotlib
                import matplotlib.pyplot as plt
                xhat = np.linspace(
                    self.grb.greedy_points.min(),
                    self.grb.greedy_points.max(),
                    1000)
                xhat = np.log(xhat)
                if self.method == 'lr':
                    yhat  = fit.predict(xhat.reshape(-1,1))
                elif self.method == 'gpr':
                    yhat, yhat_var  = fit.predict(xhat.reshape(-1,1))

                plt.figure()
                plt.title(f"eim data [{i}]")
                if self.method == 'gpr':
                    plt.fill_between(
                        np.exp(xhat),
                        yhat - np.sqrt(yhat_var),
                        yhat + np.sqrt(yhat_var),
                        color="k", alpha=0.2
                    )
                    
                plt.plot(np.exp(xhat), yhat, label='fit')
                plt.scatter(np.exp(x_to_fit.ravel()), eim_y, label='data')
                plt.legend()
                plt.show()
                plt.close()
        
        self.fits = fits
        
    def predict(self, q):
        q = np.log(q)
        
        alpha = np.zeros(len(self.fits))
        
        if self.method == 'lr':
            for i, fit in enumerate(self.fits):
                alpha[i] = fit.predict(np.array([[q]]))
        elif self.method == 'gpr':
            for i, fit in enumerate(self.fits):
                alpha[i], _ = fit.predict((q))
            
        return np.dot(alpha, self.grb.eim.B)
        
    def validate_surrogate(self, vts, vts_coords):
        """
        vts: validation training set
        vts_coords: validation training set coordinates / parameters
        """
        model_errors = np.zeros(len(vts))
        for i in range(len(vts)):
            q = vts_coords[i]
            yhat = self.predict(q)
            y = vts[i]
            error = self.grb.integration.norm(y-yhat)**2
            model_errors[i] = error
            
        worst_error_index = np.argmax(model_errors)
        worst_case = vts_coords[worst_error_index]
        
        return model_errors, worst_case, worst_error_index
        
        

        
        
    def fit_eim_2d(self, method, maxdegs=[5,5], solver=george.HODLRSolver, verbose=False, jitter=1e-2):
        """
        solver in [george.HODLRSolver, george.BasicSolver]
        """
        
        self.method = method
        
        
        fits = []
        
        logq = np.log(self.grb.greedy_points[:,0])
        chi = self.grb.greedy_points[:,1]
        
        x_to_fit = np.column_stack([[logq, chi]]).T

        for i, eim_y in enumerate(self.grb.eim.data):
            
#             yerr = None

            if self.method == 'lr':
                fit = fitslr.LinearPolynomialBasisRegression()
                fit.fit(x_to_fit, eim_y, method='direct', maxdegs=maxdegs, max_deg_total=None)
                fits.append(fit)
            elif self.method == 'gpr':
                yerr = np.zeros(eim_y.shape) + jitter
                fit = gprfit.GPRFit(x_to_fit, eim_y, yerr=yerr)
                kernel = np.var(eim_y) * kernels.ExpSquaredKernel(0.5, ndim=2, axes=0)
                kernel *= kernels.ExpSquaredKernel(0.5, ndim=2, axes=1)
                fit.setup_kernel(kernel)

                fit.fit(solver=solver)
                fit.opt()
                fits.append(fit)
        
        self.fits = fits
        
        
    def predict_2d(self, q, chi):
        q = np.log(q)
        
        alpha = np.zeros(len(self.fits))

        if self.method == 'lr':
            for i, fit in enumerate(self.fits):
                alpha[i] = fit.predict(np.array([[q,chi]]))
        elif self.method == 'gpr':
            for i, fit in enumerate(self.fits):
                alpha[i], _ = fit.predict([[q,chi]])   
            
        return np.dot(alpha, self.grb.eim.B)
    
    def validate_surrogate_2d(self, vts, vts_coords):
        """
        vts: validation training set
        vts_coords: validation training set coordinates / parameters
        """
        model_errors = np.zeros(len(vts))
        for i in range(len(vts)):
            q, chi = vts_coords[i]
            yhat = self.predict_2d(q, chi)
            y = vts[i]
            error = self.grb.integration.norm(y-yhat)**2
            model_errors[i] = error
            
        worst_error_index = np.argmax(model_errors)
        worst_case = vts_coords[worst_error_index]
        
        return model_errors, worst_case, worst_error_index
        