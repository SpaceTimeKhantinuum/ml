"""
implements gram-schmidt orthogonalisation

This only is suitable for vectors and not
functions. To modify for functions need
to implement some integration rules

some sources for the algorithms

    - http://iacs-courses.seas.harvard.edu/courses/am205/slides/am205_lec08.pdf
    - https://www.math.uci.edu/~ttrogdon/105A/html/Lecture23.html
    - Algorithm from Hoffman, `Iterative Algorithms for Gram-Schmidt Orthogonalization`
    - Chad Galley rompy package
"""

import numpy as np

def cgs(a, return_orthonormal=True):
    """
    classical gram-schimdt algorithm

    input:
    a: numpy.ndarray
        the rows are the individual vectors
        the columns are the components of each vector
        a.shape[0] = number of vectors
        a.shape[1] = number of components === dimensionality of each vector

    return_orthonormal: bool default [True]
        if True then the returned basis will be orthonormal
        if False then the returned basis will just be orthogonal

    returns:
    v: numpy.ndarray
    matrix of orthogonal vectors that span a
    if return_orthonormal=True then it will be othonormal
    """
    n_vectors = a.shape[0]
    n_dim = a.shape[1]

    v = np.zeros(shape=(n_vectors, n_dim))
    r = np.zeros(shape=(n_vectors, n_dim))
    q = np.zeros(shape=(n_vectors, n_dim))

    for j in range(n_vectors):
        v[j] = a[j]

        for i in range(j):
            r[i,j] = np.dot(q[i], a[j])
            v[j] = v[j] - np.dot(r[i,j], q[i])

        r[j,j] = np.linalg.norm(v[j])
        q[j] = v[j] / r[j,j]

    if return_orthonormal:
        return q
    else:
        return v

def mgs(a, return_orthonormal=True):
    """
    modified gram-schimdt algorithm

    input:
    a: numpy.ndarray
        the rows are the individual vectors
        the columns are the components of each vector
        a.shape[0] = number of vectors
        a.shape[1] = number of components === dimensionality of each vector

    return_orthonormal: bool default [True]
        if True then the returned basis will be orthonormal
        if False then the returned basis will just be orthogonal

    returns:
    v: numpy.ndarray
    matrix of orthogonal vectors that span a
    if return_orthonormal=True then it will be othonormal
    """
    n_vectors = a.shape[0]
    n_dim = a.shape[1]

    v = np.zeros(shape=(n_vectors, n_dim))
    r = np.zeros(shape=(n_vectors, n_dim))
    q = np.zeros(shape=(n_vectors, n_dim))

    for i in range(n_vectors):
        v[i] = a[i]

    for i in range(n_vectors):
        r[i,i] = np.linalg.norm(v[i])
        q[i] = v[i] / r[i,i]

        for j in range(i+1, n_vectors):
            r[i,j] = np.dot(q[i], v[j])
            v[j] = v[j] - np.dot(r[i,j], q[i])

    if return_orthonormal:
        return q
    else:
        return v

def imgs(a, tol=0.5, max_iter=3, return_orthonormal=True):
    """
    iterative modified gram-schimdt algorithm
    - heavily based on Chad Galley's code
    https://bitbucket.org/chadgalley/rompy/src/master/greedy.py

    input:
    a: numpy.ndarray
        the rows are the individual vectors
        the columns are the components of each vector
        a.shape[0] = number of vectors
        a.shape[1] = number of components === dimensionality of each vector

    tol: float
        tolerance on the ratio between new and old norm

    max_iter: int
        maximum number of iterations to try

    return_orthonormal: bool default [True]
        if True then the returned basis will be orthonormal
        if False then the returned basis will just be orthogonal

    returns:
    v: numpy.ndarray
    matrix of orthogonal vectors that span a
    if return_orthonormal=True then it will be othonormal
    """
    n_vectors = a.shape[0]
    n_dim = a.shape[1]

    v = np.zeros(shape=(n_vectors, n_dim))
    q = np.zeros(shape=(n_vectors, n_dim))

    for i in range(n_vectors):
        v[i] = a[i]

    for i in range(n_vectors):

        norm = np.linalg.norm(v[i])
        q[i] = v[i] / norm

        flag, ctr = 0, 1
        while flag == 0:

            for j in range(i):
                s = np.dot(q[j], v[i])
                v[i] = v[i] - np.dot(s, q[j])

            new_norm = np.linalg.norm(v[i])

            if new_norm / norm <= tol:
                norm = new_norm
                ctr += 1
                if ctr > max_iter:
                    msg = "WARNING max number of iter reached " \
                    + "basis may not be orthonormal."
                    print(msg)
                    flag = 1
            else:
                flag = 1

        q[i] = v[i] / new_norm

    if return_orthonormal:
        return q
    else:
        return v

def _imgs_galley(a, tol=0.5, max_iter=3):
    """
    implementation of Chad Galley's code
    for the iterative modified gram-schmodt algorithm
    https://bitbucket.org/chadgalley/rompy/src/master/greedy.py

    used only to debug my version.

    One should not need to use this function
    """
    n_vectors = a.shape[0]
    n_dim = a.shape[1]

    basis = np.zeros(shape=(n_vectors, n_dim))
    basis[0] = a[0] / np.linalg.norm(a[0])

    for ii in range(1, n_vectors):
        h = a[ii]
        bb = basis[:ii]

        norm = np.linalg.norm(h)
        e = h / norm

        flag, ctr = 0, 1
        while flag == 0:

            for b in bb:

                e -= b * np.dot(b, e)

            new_norm = np.linalg.norm(e)

            if new_norm / norm <= tol:
                norm = new_norm
                ctr += 1
                if ctr > max_iter:
                    msg = "WARNING max number of iter reached " \
                    + "basis may not be orthonormal."
                    print(msg)
                    flag = 1
            else:
                flag = 1

        basis[ii] = e/new_norm

    return basis

def compute_Uhat(U):
    """
    input:
    U: numpy.ndarray
        the rows are the individual vectors
        the columns are the components of each vector
        U.shape[0] = number of vectors
        U.shape[1] = number of components === dimensionality of each vector

    returns array of same shape where each row
    is now unit length
    """
    n_vectors = U.shape[0]

    Uhat = np.zeros(U.shape)
    for k in range(n_vectors):
        Uhat[k] = U[k] / np.linalg.norm(U[k])
    return Uhat


def check_basis(U, check_unit_length=False):
    """

    input:
    U: numpy.ndarray
        the rows are the individual vectors
        the columns are the components of each vector
        U.shape[0] = number of vectors
        U.shape[1] = number of components === dimensionality of each vector

    check_unit_length: bool default [False]
        if True then will check that each vector has unit length
        if False then will not check that each vector has unit length

    performs a few checks on the basis U
    """
    n_vectors = U.shape[0]
    n_dim = U.shape[1]

    error = np.linalg.norm(np.eye(n_dim) - np.dot(U.T, U))

    print(f"error is {error}")

    cond = np.linalg.cond(U)

    print(f"condition number is {cond}")

    if check_unit_length:
        for i in range(U.shape[0]):
            Umag = np.linalg.norm(U[i])
            np.testing.assert_array_almost_equal(1., Umag, err_msg=f"msg: error for index = {i}")
