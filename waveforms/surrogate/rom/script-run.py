import matplotlib as mpl
import matplotlib.pyplot as plt

font = {'size':14}
plt.rc('font', **font)

import numpy as np

import pycbc
from pycbc.waveform import get_fd_waveform
from pycbc.waveform import amplitude_from_frequencyseries, phase_from_frequencyseries
from pycbc import pnutils

import phenom

qlist = np.linspace(1, 5, 5)
clist = np.linspace(0, 1, 5)

def make_regular_2d_grid(xlist, ylist):
    x, y = np.meshgrid(xlist, ylist)
    return x.reshape(1,-1)[0], y.reshape(1,-1)[0]

def make_regular_3d_grid(xlist, ylist, zlist):
    x, y, z = np.meshgrid(xlist, ylist, zlist)
    return x.reshape(1,-1)[0], y.reshape(1,-1)[0], z.reshape(1,-1)[0]


x, y = make_regular_2d_grid(qlist, clist)

def mask_array(x, y, x1, x2):
    mask = (x>=x1) & (x<=x2)
    newx = x[mask]
    newy = y[mask]
    return newx, newy



def make_training_set(qlist, clist, f_lower=30, delta_f=1,
                     approximant="IMRPhenomD",
                     mtot=100,
                     fstart=30, fend=200, align_phase=True):

    x, y = make_regular_2d_grid(qlist, clist)
    npts = len(x)

    amp = [None]*npts
    phase = [None]*npts

    coords = []

    for i, (q, c) in enumerate(zip(x,y)):
        print(i, q, c)
        coords.append([q,c])
        m1,m2 = phenom.m1_m2_M_q(mtot, q)
        hp, _ = get_fd_waveform(mass1=m1,
                                mass2=m2,
                                spin1z=c,
                                delta_f=delta_f,
                                f_lower=f_lower,
                                approximant=approximant)
        amp_ts = amplitude_from_frequencyseries(hp)
        phase_ts = phase_from_frequencyseries(hp)

        if i==0:
            flist, amp[i] = mask_array(amp_ts.sample_frequencies, amp_ts.data, fstart, fend)
        else:
            _, amp[i] = mask_array(amp_ts.sample_frequencies, amp_ts.data, fstart, fend)

        _, phi_tmp = mask_array(phase_ts.sample_frequencies, phase_ts.data, fstart, fend)
        if align_phase == True:
            #aligned phase
            #subtract off a linear function over the freq interval [fstart, fend]
#             z = np.polyfit(flist[flist<100], phi_tmp[flist<100], 1)
            z = np.polyfit(flist, phi_tmp, 1)
            p = np.poly1d(z)
            phi_tmp -= p(flist)

#         _, phase[i] = mask_array(phase_ts.sample_frequencies, phase_ts.data, fstart, fend)
        phase[i] = phi_tmp
#         phase[i] = phi_tmp * phenom.eta_from_q(q)
#         phase[i] = phi_tmp / q

    return np.array(coords), np.array(flist), np.array(amp), np.array(phase)


coords, f, am, ph = make_training_set(qlist, clist)


def compute_svd(matrix):
    u, s, vh          = np.linalg.svd(matrix, full_matrices=False)
    return u,s,vh

u,s,vh = compute_svd(ph)
# u,s,vh = compute_svd(am)

# Decide on how many column vectors to use as the basis
# basis_size = 3
# basis_size = 4
basis_size = 5
# basis_size = 6
# basis_size = 7
# More vectors -> more accuracy but also more computational cost
# basis_size = -1
basis_set  = vh[0:basis_size,:]



def compute_projection_coeffs(RB, ref_data):
    proj_coeffs = np.array([ np.dot(RB, ref_data[i]) for i in range(len(ref_data)) ] )
    return proj_coeffs

proj_coeffs = compute_projection_coeffs(basis_set, ph)


from keras.models import Sequential
from keras.layers import Dense

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
# https://github.com/dmlc/xgboost/issues/1715


model = Sequential()
model.add(Dense(500, activation='relu', input_shape=(2,), kernel_initializer='glorot_uniform'))
model.add(Dense(500, kernel_initializer='normal', activation='relu'))
model.add(Dense(500, kernel_initializer='normal', activation='relu'))
model.add(Dense(500, kernel_initializer='normal', activation='relu'))
# model.add(Dense(8, activation='relu'))
model.add(Dense(1, activation='linear'))
model.summary()

model.compile(loss='mse', optimizer='adam', metrics=['mse','mae'])

history = model.fit(x=coords, y=proj_coeffs[:,0], epochs=1000, verbose=1)


X = np.column_stack(([1], [0]))
print(model.predict(X))

X = np.column_stack((coords[0][0], coords[0][1]))
print(model.predict(X))

print(proj_coeffs[0,0])





#x = np.linspace(0,10,100)
#y = x
#
#model = Sequential()
#model.add(Dense(1, activation='linear', input_shape=(1,), kernel_initializer='glorot_uniform'))
#model.compile(loss='mse', optimizer='sgd') ## To try our model with an Adam optimizer simple replace 'sgd' with 'Adam'
#history = model.fit(x=x, y=y, validation_split=0.2, batch_size=1, epochs=100)
