import numpy as np

import phenom

import lal
import lalsimulation as lalsim

def gen_wf_params(
        m1=50,
        m2=50,
        S1x=0,
        S1y=0,
        S1z=0,
        S2x=0,
        S2y=0,
        S2z=0,
        distance=1,
        inclination=0,
        phiRef=0,
        longAscNodes=0,
        eccentricity=0,
        meanPerAno=0,
        deltaF=1/16,
        f_min=10,
        f_max=200,
        f_ref=30,
        LALpars=None,
        approximant=lalsim.IMRPhenomD):
    
    
    p=dict(
        m1=m1,
        m2=m2,
        S1x=S1x,
        S1y=S1y,
        S1z=S1z,
        S2x=S2x,
        S2y=S2y,
        S2z=S2z,
        distance=distance,
        inclination=inclination,
        phiRef=phiRef,
        longAscNodes=longAscNodes,
        eccentricity=eccentricity,
        meanPerAno=meanPerAno,
        deltaF=deltaF,
        f_min=f_min,
        f_max=f_max,
        f_ref=f_ref,
        LALpars=LALpars,
        approximant=approximant)
    
    return p
    
    
def gen_wf(p):
    
    p.update({'m1':p['m1']*lal.MSUN_SI})
    p.update({'m2':p['m2']*lal.MSUN_SI})
    
    hp, _ = lalsim.SimInspiralChooseFDWaveform(**p)
    
    
    f = np.arange(hp.data.length) * hp.deltaF
    
    mask = (f >= p['f_min']) & (f < p['f_max'])
    
    hp = hp.data.data[mask]
    f = f[mask]
    
    amp = np.abs(hp)
    phase = np.unwrap(np.angle(hp))
    
    return f, hp, amp, phase


def gen_td_wf_params(
        m1=50,
        m2=50,
        S1x=0,
        S1y=0,
        S1z=0,
        S2x=0,
        S2y=0,
        S2z=0,
        distance=1,
        inclination=0,
        phiRef=0,
        longAscNodes=0,
        eccentricity=0,
        meanPerAno=0,
        deltaT=1/4096,
        f_min=30,
        f_ref=30,
        LALpars=None,
        approximant=lalsim.SEOBNRv4):
    
    
    p=dict(
        m1=m1,
        m2=m2,
        s1x=S1x,
        s1y=S1y,
        s1z=S1z,
        s2x=S2x,
        s2y=S2y,
        s2z=S2z,
        distance=distance,
        inclination=inclination,
        phiRef=phiRef,
        longAscNodes=longAscNodes,
        eccentricity=eccentricity,
        meanPerAno=meanPerAno,
        deltaT=deltaT,
        f_min=f_min,
        f_ref=f_ref,
        params=LALpars,
        approximant=approximant)
    
    return p
    
    
def gen_td_wf(p, t_min=-500, t_max=50):
    
    M = p['m1'] + p['m2']
    
    p.update({'m1':p['m1']*lal.MSUN_SI})
    p.update({'m2':p['m2']*lal.MSUN_SI})
    
    hp, hc = lalsim.SimInspiralChooseTDWaveform(**p)
    

    t = np.arange(hp.data.length) * hp.deltaT + np.float(hp.epoch)
    t = phenom.StoM(t, M)
    
    mask = (t >= t_min) & (t < t_max)
    
    hp = hp.data.data[mask]
    hc = hc.data.data[mask]
    t = t[mask]
    
    h = hp - 1.j*hc
    
    amp = np.abs(h)
    phase = np.unwrap(np.angle(h))
    
    return t, amp, phase