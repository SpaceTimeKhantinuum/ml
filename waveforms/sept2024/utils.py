import warnings
from weakref import ref

warnings.filterwarnings("ignore", "Wswiglal-redir-stdio")
import lal

import prim.hybrid
import prim.spliced_pn
import sxs
from prim.waveform import Waveform
import copy
import pandas as pd
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as IUS
import logging
import os
import phenom
import h5py
import pathlib

logger = logging.getLogger(__name__)

def current_datetime_tag():
    return np.datetime64('now').item().strftime("%Y_%m_%d__%H%M%S")

def current_date_tag():
    return np.datetime64('now').item().strftime("%Y_%m_%d")

def set_lal_data_path(p="/Users/sebastian.khan/Downloads/"):
    """
    download HybSur data file:
        https://git.ligo.org/lscsoft/lalsuite-extra/-/blob/master/data/lalsimulation/NRHybSur3dq8_lal.h5
    set path
    """
    os.environ["LAL_DATA_PATH"] = "/Users/sebastian.khan/Downloads/"


def get_aligned_spin_sxs_dataframe():
    df = sxs.load("simulations").dataframe
    df["reference_mass_ratio_rounded"] = df["reference_mass_ratio"].round(3)
    df["reference_chi_eff_rounded"] = df["reference_chi_eff"].round(3)
    df["reference_dimensionless_spin1z"] = df["reference_dimensionless_spin1"].apply(
        lambda x: x[2]
    )
    df["reference_dimensionless_spin2z"] = df["reference_dimensionless_spin2"].apply(
        lambda x: x[2]
    )

    df["reference_dimensionless_spin1z_rounded"] = df[
        "reference_dimensionless_spin1z"
    ].round(3)
    df["reference_dimensionless_spin2z_rounded"] = df[
        "reference_dimensionless_spin2z"
    ].round(3)

    # get non-precessing simulations, non-eccentric and non-deprecated simulations
    # and BBH
    mask = (
        (df["reference_chi1_perp"] < 0.001)
        & (df["reference_chi2_perp"] < 0.001)
        & (df["reference_eccentricity"] < 0.001)
        & (df["deprecated"] == False)
        & (df["object_types"] == "BHBH")
        & (df["number_of_orbits"] > 0)
    )

    return df[mask]


def get_simulations_grouped_by_mass_ratio_and_aligned_spin_dataframe():
    """
    Returns a dataframe where each row corresponds to a unique (q, chi1z, chi2z) combination.
    We construct a column 'sims' which is a list of the simulation names.
    """
    df = get_aligned_spin_sxs_dataframe()

    # group by
    cols = [
        "reference_mass_ratio_rounded",
        "reference_dimensionless_spin1z_rounded",
        "reference_dimensionless_spin2z_rounded",
    ]
    g = df[cols].reset_index().groupby(by=cols)

    # for each group get the list of simulations in that group
    sims_in_group = [list(df.iloc[g.groups[k]].index) for k in g.groups.keys()]

    # loop over simulations in each group and make a new row for each
    df_params = []
    for sims in sims_in_group:
        df_group = df[df.index.isin(sims)]
        min_orbits = df_group["number_of_orbits"].min()
        max_orbits = df_group["number_of_orbits"].max()
        min_reference_orbital_frequency_mag = df_group["reference_orbital_frequency_mag"].min()
        max_reference_orbital_frequency_mag = df_group["reference_orbital_frequency_mag"].max()
        number_of_sims = len(df_group)
        df_group = df_group.head(1)
        row = pd.DataFrame(
            {
                "reference_mass_ratio_rounded": df_group[
                    "reference_mass_ratio_rounded"
                ],
                "reference_chi_eff_rounded": df_group["reference_chi_eff_rounded"],
                "reference_dimensionless_spin1z_rounded": df_group[
                    "reference_dimensionless_spin1z_rounded"
                ],
                "reference_dimensionless_spin2z_rounded": df_group[
                    "reference_dimensionless_spin2z_rounded"
                ],
                "min_orbits": min_orbits,
                "max_orbits": max_orbits,
                "min_reference_orbital_frequency_mag": min_reference_orbital_frequency_mag,
                "max_reference_orbital_frequency_mag": max_reference_orbital_frequency_mag,
                "number_of_sims": number_of_sims,
                "sims": [sims],
            }
        )
        df_params.append(row)
    df_params = pd.concat(df_params).reset_index(drop=True)
    df_params = df_params.sort_values(by="number_of_sims", ascending=False).reset_index(
        drop=True
    )

    return df_params


def get_reference_sxs_sim_name(metadata) -> str:
    """
    get highest level simulation as this will be our reference simulation in comparisons
    it takens the simulation with the largest number of orbits when levels are the same.
    """
    lev_numbers = [metadata[k].lev_number for k in metadata.keys()]
    num_orbits = [metadata[k].dataframe["number_of_orbits"] for k in metadata.keys()]
    df = pd.DataFrame(
        {"lev": lev_numbers, "num_orbits": num_orbits}, index=list(metadata.keys())
    )
    df = df.sort_values(by=["lev", "num_orbits"], ascending=[False, False])
    return str(df.index[0])


def get_reference_and_other(metadata):
    """
    for a given dict of SXS metadata split them into:
    1. a reference simulation and
    2. other simulations to compare against the reference simulation.

    If there is only one lev then this is returned as the reference simulation and
    the `other_sim_names` list will be empty i.e. `[]`

    Parameters
    ----------
    metadata, dict of SXS metadata with keys given by the simulation name

    Returns
    -------
    reference_sim_name, str
    other_sim_names, str
    """
    sim_names = list(metadata.keys())
    reference_sim_name = get_reference_sxs_sim_name(metadata)
    other_sim_names = sorted(list(set(sim_names) - set([reference_sim_name])))
    return reference_sim_name, other_sim_names


def sxs_load_all_levels(sxs_sim_name: str, ignore_deprecation=False):
    """
    sxs_sim_name: e.g. 'SXS:BBH:1234'
    """
    # load a sim
    sxs_bbh = sxs.load(sxs_sim_name, ignore_deprecation=ignore_deprecation)

    # loop over all possilbe lev numbers to put them into a dict with consistent keys SXS_NAME/LevL

    metadata = {}
    wfs = {}
    for lev_number in sxs_bbh.lev_numbers:
        try:
            level = f"Lev{lev_number}"
            sim_name = f"{sxs_sim_name}/{level}"
            curr_sxs_bbh = sxs.load(sim_name, ignore_deprecation=ignore_deprecation)
            print(curr_sxs_bbh)
            metadata[sim_name] = curr_sxs_bbh
            # .h will download the data if needed
            w = curr_sxs_bbh.h
            # remove junk
            reference_time = curr_sxs_bbh.metadata.reference_time
            reference_index = w.index_closest_to(reference_time)
            w = w[reference_index:]
            wfs[sim_name] = w
        except:
            continue
    return metadata, wfs


def convert_sxs_to_prim_waveform(
    w: sxs.waveforms.waveform_modes.WaveformModes, modes, start_time=None, end_time=None
):
    # convert sxs into prim.Waveform class
    mode_idxs = [w.index(*mode) for mode in modes]

    wf_dict = {}
    wf_dict["t"] = w.t
    wf_dict["hlm"] = {mode: np.array(w[:, i]) for mode, i in zip(modes, mode_idxs)}

    wf = Waveform(times=wf_dict["t"], hlms=wf_dict["hlm"])
    # shift so that first element is t=0
    # then we can use wf.mask to mask time from the beginning of the simulation
    wf.apply_time_shift(-wf.times[0])
    wf.mask(start_time, end_time)

    t0_inspiral_right, _ = (
        wf.compute_amplitude()
        .compute_phase()
        .compute_frequency()
        .compute_time_of_peak()
    )
    wf.apply_time_shift(-t0_inspiral_right)
    wf.compute_amplitude().compute_phase().compute_frequency()
    return wf


def compute_common_time_bounds(wfs, use_max_end_times=False):
    """
    wfs: dict of Waveform objects
    use_max_end_times : bool, default is False
        Default behaviour is to return the common end time. If True though
        then the maximum end time is returned so waveforms don't get cut off
    """
    start_times = []
    end_times = []
    for k in wfs.keys():
        start_times.append(wfs[k].times[0])
        end_times.append(wfs[k].times[-1])

    common_start_time = np.max(start_times)
    if use_max_end_times:
        common_end_time = np.max(end_times)
    else:
        common_end_time = np.min(end_times)
    return common_start_time, common_end_time


def interpolate_wf_onto_same_time_grid(
    wf: Waveform, t0=None, t1=None, N: int = None, new_times=None
):
    wf = copy.deepcopy(wf)
    if new_times is None:
        new_times = np.linspace(t0, t1, N)

    new_hlms = {}
    for mode in wf.hlms.keys():
        iy_real = IUS(wf.times, wf.hlms[mode].real, ext=1)(new_times)
        iy_imag = IUS(wf.times, wf.hlms[mode].imag, ext=1)(new_times)
        new_hlms[mode] = iy_real + 1.0j * iy_imag

    wf.times = new_times
    wf.hlms = new_hlms
    wf.compute_amplitude().compute_phase().compute_frequency()
    return wf


def align_waveforms(
    wf_left,
    wf_right,
    fit_modes,
    n_cycles_before_window=2,
    n_cycles_in_window=10,
    time_shift_guess=0,
    n_tries=10,
    coarse_grid_params={"on": True, "t0": -100, "t1": 100, "N": 1000},
):
    """
    Shift the left waveform to agree with the right waveform.

    coarse_grid_params: dict
        Turn off by setting 'on' to False.
        Parameters to use when resampling left and right waveforms to speed up fitting
    """
    win1, win2 = prim.hybrid.get_window_times(
        wf_right,
        n_cycles_before_window=n_cycles_before_window,
        n_cycles_in_window=n_cycles_in_window,
    )
    # print(win1, win2)

    if coarse_grid_params["on"]:
        t0 = win1 + coarse_grid_params["t0"]
        t1 = win2 + coarse_grid_params["t1"]
        N = coarse_grid_params["N"]
        wf_left_for_fit = interpolate_wf_onto_same_time_grid(
            copy.deepcopy(wf_left), t0=t0, t1=t1, N=N
        )
        wf_right_for_fit = interpolate_wf_onto_same_time_grid(
            copy.deepcopy(wf_right), t0=t0, t1=t1, N=N
        )
    else:
        wf_left_for_fit = copy.deepcopy(wf_left)
        wf_right_for_fit = copy.deepcopy(wf_right)

    results = prim.hybrid.fit_hybrid(
        wf_left_for_fit,
        wf_right_for_fit,
        win1,
        win2,
        fit_modes,
        time_shift_guess,
        n_tries=n_tries,
        max_nfev=None,
    )

    wf_left_shifted = copy.deepcopy(wf_left)
    # here we are shifting the left waveform to agree with the right waveform so the correct transformations are
    # +dt, -dphi, -dpsi
    (
        wf_left_shifted.apply_time_shift(results.params["dt"])
        .apply_phase_shift(-results.params["dphi"])
        .apply_polarisation_shift(-results.params["dpsi"])
        .compute_amplitude()
        .compute_phase()
        .compute_frequency()
    )

    return wf_left_shifted


def hybridise(
    wf_left,
    wf_right,
    fit_modes=None,
    n_cycles_before_window=2,
    n_cycles_in_window=10,
    time_shift_guess=0,
    n_tries=10,
    return_metadata=True,
    coarse_grid_params={"on": False},
):
    """
    This function hybridises the left and right waveforms over the time interval (win1, win2).
    The right waveform gets transformed.
    The hybrid waveform is returned.
    All modes in the input data are returned but only the modes specified in fit_modes will be used in the fit.
    If not given then will use all modes.

    The hybrid gets transformed to align with the right waveform at the end.

    Parameters
    ----------
    wf_left
    wf_right
    fit_modes:
        Modes to use in the fit

    coarse_grid_params: dict.
        Turn off by setting 'on' to False. By default is False.
        Parameters to use when resampling left and right waveforms to speed up fitting
        If True then also supply 't0', 't1' and 'N'

    Returns
    -------
    prim.Waveform
    """
    if fit_modes is None:
        fit_modes = list(wf_left.hlms.keys())

    win1, win2 = prim.hybrid.get_window_times(
        wf_right,
        n_cycles_before_window=n_cycles_before_window,
        n_cycles_in_window=n_cycles_in_window,
    )
    # print(win1, win2)

    if coarse_grid_params["on"]:
        t0 = win1 + coarse_grid_params["t0"]
        t1 = win2 + coarse_grid_params["t1"]
        N = coarse_grid_params["N"]
        wf_left_for_fit = interpolate_wf_onto_same_time_grid(
            copy.deepcopy(wf_left), t0=t0, t1=t1, N=N
        )
        wf_right_for_fit = interpolate_wf_onto_same_time_grid(
            copy.deepcopy(wf_right), t0=t0, t1=t1, N=N
        )
    else:
        wf_left_for_fit = copy.deepcopy(wf_left)
        wf_right_for_fit = copy.deepcopy(wf_right)

    results = prim.hybrid.fit_hybrid(
        wf_left_for_fit,
        wf_right_for_fit,
        win1,
        win2,
        fit_modes,
        time_shift_guess,
        n_tries=n_tries,
        max_nfev=None,
    )

    dt = results.params["dt"]
    dphi = results.params["dphi"]
    dpsi = results.params["dpsi"]

    wf_hybrid = prim.hybrid.build_hybrid(
        wf_left,
        wf_right,
        dt,
        dphi,
        dpsi,
        win1,
        win2,
    )
    # compute derived quantities.
    (
        wf_hybrid
        # .mask(-600)
        .compute_amplitude()
        .compute_phase()
        .compute_frequency()
    )

    # if we rotate back we will get a hybrid that is aligned with the right waveform to make it easy to compare with visually.
    wf_hybrid.apply_time_shift(dt).apply_phase_shift(-dphi).apply_polarisation_shift(
        -dpsi
    )

    if return_metadata:
        return wf_hybrid, dict(results=results, win1=win1, win2=win2)
    else:
        return wf_hybrid


def get_q_chi1_chi2_from_metadata(metadata):
    """
    given sxs metadata get the q, chi1z and chi2z

    offset q because of some PN spin variable issue with h_21 amplitude.
    """
    q = metadata.metadata.reference_mass_ratio
    chi1z = metadata.metadata.reference_dimensionless_spin1[2]
    chi2z = metadata.metadata.reference_dimensionless_spin2[2]

    # exactly equal mass and equal spin causes issues with PN spin variables
    # so we add a tiny bit to q
    q = np.around(q, 3) + 1e-3

    return q, chi1z, chi2z


def get_left_waveform_from_metadata(
    metadata, M, f_min_left, modes, approximant, deltaT=1 / 4096
):
    q, chi1z, chi2z = get_q_chi1_chi2_from_metadata(metadata)

    wf_dict = prim.spliced_pn.generate_spliced_pn_waveform(
        q,
        modes,
        f_min=f_min_left,
        M=M,
        S1z=chi1z,
        S2z=chi2z,
        approximant=approximant,
        deltaT=deltaT,
    )

    wf_left = Waveform(times=wf_dict["t"], hlms=wf_dict["hlm"])
    t0_inspiral_left, _ = (
        wf_left.compute_amplitude()
        .compute_phase()
        .compute_frequency()
        .compute_time_of_peak()
    )
    wf_left.apply_time_shift(-t0_inspiral_left)
    return wf_left


def build_hybrids(
    sxs_sim_name=None,
    wfs=None,
    metadata=None,
    approximants=None,
    hybrid_start_freq_Hz=30,
    modes=None,
    deltaT_approximant=1 / 4096,
    n_cycles_before_window=10,
    n_cycles_in_window=30,
    n_tries=10,
    M=10,
    coarse_grid_params=None,
    n_cycles_before_align_window=0,
    n_cycles_in_align_window=200,
):
    """
    Main function to build higher multipole hybrids of SXS waveforms using multiple approximants.

    Inputs are either sxs_sim_name or the pair wfs and metadata.
    If sxs_sim_name is given then uses sxs_load_all_levels to load all
    levels of that simulation.
    If wfs and metadata are given then we assume the user has used
    sxs_load_all_levels before using this function to e.g. obtain
    data from different NR simulations but for the same point in parameter space.

    approximants, list[str]
        List of approximants to use. They only require a 2,2 mode as we use these only
        to get their prediction for the orbital phase.
        Note that the approximant that appears first in the list is the reference approximant.
    deltaT_approximant, float
        Sample rate used to generate the approximant
    """
    logger.info("start")

    if approximants is None:
        approximants = [
            "SEOBNRv5HM",
            "IMRPhenomTHM",
            "SEOBNRv4HM_PA",
            "NRHybSur3dq8",
            # "SpinTaylorT4",
            # "SpinTaylorT1",
        ]
    if modes is None:
        modes = [(2, 2), (2, 1), (3, 3), (3, 2), (4, 4), (4, 3), (5, 5), (5, 4)]

    if coarse_grid_params is None:
        coarse_grid_params = {"on": True, "t0": -300, "t1": 300, "N": 5000}

    logger.info("getting sxs data")

    if sxs_sim_name is not None:
        logger.info("using sxs_load_all_levels")
        metadata, wfs = sxs_load_all_levels(sxs_sim_name)
    else:
        assert wfs is not None, 'please supply wfs dict'
        assert metadata is not None, 'please supply metadatas dict'
        logger.info('wfs and metadata supplied')

    # metadata = {'SXS:BBH:1412/Lev3':metadata['SXS:BBH:1412/Lev3']}
    # wfs = {'SXS:BBH:1412/Lev3':wfs['SXS:BBH:1412/Lev3']}

    number_of_nr_levs_found = len(wfs.keys())
    levs_found = list(np.sort(list(wfs.keys())))
    logger.info(f"{number_of_nr_levs_found = }")
    logger.info(f"{levs_found = }")
    if number_of_nr_levs_found == 1:
        logger.info("Only one Lev found. Therefore, there are no 'non_reference_sims'")

    logger.info("determining reference simulation")
    reference_sim, non_reference_sims = get_reference_and_other(metadata)

    logger.info(f"{reference_sim = }")
    logger.info(f"{non_reference_sims = }")

    logger.info("converting sxs to prim")
    wfs_nr = {}
    for k in levs_found:
        wfs_nr[k] = convert_sxs_to_prim_waveform(wfs[k], modes=modes)

    # hybrids is a list of prim.Waveform containing the hybrids
    hybrids = []
    hybrid_names = []
    fit_metadatas = {}
    logger.info("Looping over NR sims")

    # could loop over all combinations and use multiprocessing to speed this part up.
    # itertools.product(wfs_nr.keys(), approximants)
    
    for i, k in enumerate(wfs_nr.keys()):
        logger.info(f"working: {k} ({i}/{len(wfs_nr.keys()) - 1})")
        wf_nr = wfs_nr[k]

        logger.info(f"start NR time: {wf_nr.times[0]} M")
        logger.info(f"end NR time: {wf_nr.times[-1]} M")

        logger.info("preparing to hybridise")
        logger.info("computing NR start 2,2 frequency")
        NR_start_frequency = (
            phenom.MftoHz(np.abs(wf_nr.frequencies[2, 2][0]), M) / 2 / np.pi
        )
        NR_start_frequency = np.around(NR_start_frequency, 2)
        logger.info(f"{NR_start_frequency = } Hz (Total Mass = {M})")
        NR_start_frequency_Mf = phenom.HztoMf(NR_start_frequency, M)
        logger.info(f"{NR_start_frequency_Mf = } Mf")

        hybrid_start_freq_Mf = phenom.HztoMf(hybrid_start_freq_Hz, M)
        logger.info(f"{hybrid_start_freq_Hz = } Hz")
        logger.info(f"{hybrid_start_freq_Mf = } Mf")
        
        assert hybrid_start_freq_Hz < NR_start_frequency, f"hybrid start frequency too high. The condition ({hybrid_start_freq_Hz = } < {NR_start_frequency = }) is not satisfied."

        logger.info("getting hybridisation windown")
        win1, win2 = prim.hybrid.get_window_times(
            wf_nr,
            n_cycles_before_window=n_cycles_before_window,
            n_cycles_in_window=n_cycles_in_window,
        )
        logger.info(f"{win1 = } M")
        logger.info(f"{win2 = } M")

        logger.info("looping over approximants")
        wf_hybrids = {}
        hybrids_metadatas = {}
        for approximant in approximants:
            tag = f"{k}__{approximant}"
            hybrid_names.append(tag)
            logger.info(f"{tag = }")
            wf_left = get_left_waveform_from_metadata(
                metadata[k],
                M,
                hybrid_start_freq_Hz,
                modes,
                approximant,
                deltaT_approximant,
            )
            logger.info(f"{wf_left.times[0] = } M")
            logger.info("fitting hybrid")
            wf_, hybrids_metadatas[tag] = hybridise(
                wf_left,
                wf_nr,
                fit_modes=modes,
                n_cycles_before_window=n_cycles_before_window,
                n_cycles_in_window=n_cycles_in_window,
                time_shift_guess=0,
                n_tries=n_tries,
                return_metadata=True,
                coarse_grid_params=coarse_grid_params,
            )

            # time shift peak
            # only really need to do this for the reference hybrid
            t0_wf_, _ = (
                wf_.compute_amplitude()
                .compute_phase()
                .compute_frequency()
                .compute_time_of_peak()
            )
            wf_.apply_time_shift(-t0_wf_)
            wf_.compute_amplitude().compute_phase().compute_frequency()
            hybrids.append(copy.deepcopy(wf_))

            logger.info("updating metadata")
            hybrids_metadatas[tag].update({
                'sxs_sim_name':k,
                'approximant':approximant,
                'sxs_metadata':metadata[k],
                'q':metadata[k].dataframe['reference_mass_ratio'],
                'chi1z':metadata[k].dataframe['reference_dimensionless_spin1'][2],
                'chi2z':metadata[k].dataframe['reference_dimensionless_spin2'][2],
            })

               
            fit_metadatas[tag] = copy.deepcopy(hybrids_metadatas[tag])

    logger.info(f"number of hybrids: {len(hybrids)}")
    logger.info("pick a reference hybrid and align at early times")
    reference_hybrid = f"{reference_sim}__{approximants[0]}"

    other_hybrids = list(set(hybrid_names) - set([reference_hybrid]))
    other_hybrids = sorted(other_hybrids)

    logger.info(f"{reference_hybrid = }")
    logger.info(f"{other_hybrids = }")

    # get indices of reference hybrid and other hybrids
    ref_hyb_idx = np.where(np.array(hybrid_names) == reference_hybrid)[0][0]
    other_hyb_idxs = np.where(np.array(hybrid_names) != reference_hybrid)[0]

    logger.info(f"{ref_hyb_idx = }")
    logger.info(f"{other_hyb_idxs = }")

    logger.info("align other_hybrids against the reference_hybrid at early times")
    wf_hybrids_aligned = {}
    # don't use all of the right waveform to make the alignement code faster
    # wf_right = copy.deepcopy(hybrids[ref_hyb_idx]).mask(win1, win2)
    wf_right = copy.deepcopy(hybrids[ref_hyb_idx])
    wf_hybrids_aligned[reference_hybrid] = copy.deepcopy(hybrids[ref_hyb_idx])

    logger.info("getting alignment window")

    win1, win2 = prim.hybrid.get_window_times(
        wf_right,
        n_cycles_before_window=n_cycles_before_align_window,
        n_cycles_in_window=n_cycles_in_align_window,
    )
    alignement_window = {'win1':win1, 'win2':win2}
    logger.info(f"{win1 = } M")
    logger.info(f"{win2 = } M")

    logger.info("aligning other-hybrids with reference hybrid")
    # this can also be sped up using parallelisation
    for i in other_hyb_idxs:
        k = hybrid_names[i]
        logger.info(k)
        wf_hybrids_aligned[k] = align_waveforms(
            hybrids[i],
            wf_right,
            modes,
            n_cycles_before_window=n_cycles_before_align_window,
            n_cycles_in_window=n_cycles_in_align_window,
            time_shift_guess=0,
            n_tries=n_tries,
            coarse_grid_params=coarse_grid_params,
        )

    logger.info("interpolate all hybrids onto same grid")
    dt = (
        wf_hybrids_aligned[reference_hybrid].times[1]
        - wf_hybrids_aligned[reference_hybrid].times[0]
    )
    common_start, common_end = compute_common_time_bounds(
        wf_hybrids_aligned, use_max_end_times=True
    )

    logger.info(f"{common_start = } M")
    logger.info(f"{common_end = } M")
    logger.info(f"{dt = } M")
    hybrid_times = np.arange(common_start, common_end, dt)

    # this can also be sped up using parallelisation
    for k in wf_hybrids_aligned.keys():
        logger.info(k)
        wf_hybrids_aligned[k] = interpolate_wf_onto_same_time_grid(
            wf_hybrids_aligned[k], new_times=hybrid_times
        )

    # logger.info("fixed time shift to ensure peak is at t=0")
    # # finally ensure that the reference hybrid is a t=0
    # t0_reference, _ = wf_hybrids_aligned[reference_hybrid].compute_time_of_peak()
    # for k in wf_hybrids_aligned.keys():
    #     logger.info(k)
    #     wf_hybrids_aligned[k].apply_time_shift(-t0_reference)

    logger.info("creating metadata dict")
    metadata = {'reference_hybrid':reference_hybrid, 'other_hybrids':other_hybrids, 'hybrids_metadatas':fit_metadatas, 'alignement_window':alignement_window}
    logger.info("done")
    return wf_hybrids_aligned, metadata



def create_hybrid_h5(output_dir, wf_name, waveform: prim.waveform.Waveform, metadata, is_reference:bool, output_dir_exist_ok=True):
    """

    Parameters
    ----------
    output_dir, str
        directory where the h5 file will be saved
    wf_name, str
        name of the h5 file
    waveform, prim.waveform.Waveform
        prim.Waveform object waveform to save.
    metadata, dict
        waveform metadata such as q, chi1z, chi2z, sxs_sim_name, approximant
    is_reference, bool
        whether or not this hybrid is a reference hybrid
    output_dir_exist_ok, bool: default is True.
        If True then throw an error if the output directory already exists.

    Returns
    -------
    Nothing
    """
    output_dir = pathlib.Path(output_dir)
    output_dir.mkdir(exist_ok=output_dir_exist_ok)
    filename = f"{wf_name}.h5"
    with h5py.File(output_dir / filename, 'w') as f:
        f.create_dataset("times", data=waveform.times)
        for l,m in waveform.hlms.keys():
            f.create_dataset(f"hlm_l{l}_m{m}", data=waveform.hlms[(l,m)])

        f.attrs['is_reference'] = is_reference
        f.attrs['q'] = metadata['q']
        f.attrs['chi1z'] = metadata['chi1z']
        f.attrs['chi2z'] = metadata['chi2z']
        f.attrs['sxs_sim_name'] = str(metadata['sxs_sim_name'])
        f.attrs['approximant'] = metadata['approximant']
        f.attrs['modes'] = list(waveform.hlms.keys())



