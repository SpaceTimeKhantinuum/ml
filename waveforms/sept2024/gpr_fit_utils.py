"""
Code to build non-spinning p(h)enom
"""

import warnings
warnings.filterwarnings("ignore", "Wswiglal-redir-stdio")
import lal

import copy
import pandas as pd
import pathlib
import h5py
import tqdm
import sklearn
import numpy as np

from scipy.interpolate import InterpolatedUnivariateSpline as IUS

import pandas as pd

import rfflearn.tuner
import rfflearn.cpu
import sklearn.preprocessing
import sklearn.model_selection

import phenom
import prim.taylort3
import prim.waveform
import prim.collocation

import numpy.typing as npt



DEFAULT_3D_SEARCH_SPACE = {
    "dtype_dim_kernel": "int",
    "range_dim_kernel": {"low": 32, "high": 256},
    "dtype_std_error" : "float",
    "range_std_error" : {"low": 1e-4, "high": 0.1, "log": True},
    "dtype_std_kernel": "float",
    "range_std_kernel": {"low": 1e-3, "high": 1.0, "log": True},
    "show_progress_bar":True,
}

DEFAULT_2D_SEARCH_SPACE = {
    "dtype_dim_kernel": "int",
    "range_dim_kernel": {"low": 32, "high": 256},
    "dtype_std_kernel": "float",
    "range_std_kernel": {"low": 1e-3, "high": 1.0, "log": True},
    "show_progress_bar":True,
}

def save(
    output_dir,
    filename,
    model,
    y_scaler=None,
    x_scaler=None,
    exist_ok=True,
):
    """
    In order to save/load the model we just need to pass the above parameters to the GPR constructor.
    For details see: https://github.com/tiskw/random-fourier-features/blob/main/rfflearn/cpu/rfflearn_cpu_gp.py#L20

    after running self.optimise() you should have a self.model attribute.
    this is required to run self.save().
    """
    output_dir = pathlib.Path(output_dir)
    output_dir.mkdir(exist_ok=exist_ok)
    params = dict(
        dim_kernel=model.kdim, 
        std_kernel=model.kstd, 
        std_error=model.s_e, 
        W=model.W, 
        b=model.b, 
        a=model.a, 
        S=model.S
    )
    with h5py.File(output_dir / filename, 'w') as f:
        for k, v in params.items():
            f.create_dataset(f"{k}", data=v)

        if y_scaler is not None:
            f.attrs['y_mean_'] = y_scaler.mean_
            f.attrs['scale_'] = y_scaler.scale_
        
def load(filename):
    """
    loaxd RFF GPR parameters from h5 file and return the model
    """
    params={}
    with h5py.File(filename, 'r') as f:
        for k, v in f.items():
            params[k] = v[()]

        if 'y_mean_' in f.attrs.keys():
            normalise_y = True
            y_mean_ = f.attrs['y_mean_']
            y_scale_ = f.attrs['y_scale_']
        else:
            normalise_y = False

        if 'x_mean_' in f.attrs.keys():
            normalise_x = True
            x_mean_ = f.attrs['x_mean_']
            x_scale_ = f.attrs['x_scale_']
        else:
            normalise_x = False
            
    model = rfflearn.cpu.RFFGPR(**params)

    output = dict(model=model)

    if normalise_y is True:
        output['y_scaler_params'] = dict(mean_=y_mean_, scale_=y_scale_)
    if normalise_x is True:
        output['x_scaler_params'] = dict(mean_=x_mean_, scale_=x_scale_)
    
    
    return output
        

def optimize(
    X:npt.ArrayLike,
    y:npt.ArrayLike,
    n_trials=100,
    normalise_x=True,
    normalise_y=True,
    test_size=0.3,
    verbose=3,
    std_scale=1,
    fit_noise=True,
    fit_kwargs=None
):
    """
    X,
        X.shape = (N samples, D dimensions)
    y,
        y.shape = (N samples,)
    fit_kwargs, None | dict
        kwargs parameter to pass to `RFF_dim_std_err_tuner` or `RFF_dim_std_tuner`
    std_scale, float
        If using `RFF_dim_std_tuner` then we fix the std_err to be y_train.std()
        which because we normalise is just 1. std_scale is a number that multiplies
        y_train.std(). By default we just use 1.


    Examples
    --------
    mode = (2,2)
    X = phenom.eta_from_q(qs).reshape(-1, 1)
    y = get_peak_amp(wfs, mode)
    fit_kwargs = {
        "range_dim_kernel": {"low": 32, "high": 256},
        "range_std_error" : {"low": 1e-1, "high": 1, "log": False},
        "range_std_kernel": {"low": 1e-3, "high": 50, "log": False},
        "show_progress_bar":True,
    }
    opt_output = gpr_fit_utils.optimize(
        X,
        y,
        n_trials=10,
        normalise_y=True,
        fit_noise=True,
        std_scale=1,
        fit_kwargs=fit_kwargs
    )
        
    """
    if fit_kwargs is None:
        if fit_noise:
            fit_kwargs = DEFAULT_3D_SEARCH_SPACE
        else:
            fit_kwargs = DEFAULT_2D_SEARCH_SPACE

    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size=test_size)

    if normalise_y is True:
        y_ss = sklearn.preprocessing.StandardScaler()
        y_train = y_ss.fit_transform(y_train[:,np.newaxis])[:,0]
        y_test = y_ss.transform(y_test[:,np.newaxis])[:,0]

    if normalise_x is True:
        x_ss = sklearn.preprocessing.StandardScaler()
        X_train = x_ss.fit_transform(X_train)
        X_test = x_ss.transform(X_test)
    
    if fit_noise:
        study = rfflearn.tuner.RFF_dim_std_err_tuner(
            rfflearn.cpu.RFFGPR,
            train_set=(X_train, y_train),
            valid_set=(X_test, y_test),
            verbose=verbose,
            n_trials=n_trials,
            **fit_kwargs
        )
    else:
        std_error=y_train.std() * std_scale
        study = rfflearn.tuner.RFF_dim_std_tuner(
            rfflearn.cpu.RFFGPR,
            train_set=(X_train, y_train),
            valid_set=(X_test, y_test),
            verbose=verbose,
            n_trials=n_trials,
            std_error=std_error,
            **fit_kwargs
        )

    # refit best model with all data?
    model = copy.deepcopy(study.user_attrs["best_model"])
    if normalise_y is True:
        y = y_ss.transform(y[:,np.newaxis])[:,0]
    if normalise_x is True:
        X = x_ss.transform(X)
    model.fit(X, y)


    output = dict(study=study, model=model)
    if normalise_y is True:
        output['y_ss']=y_ss
    if normalise_x is True:
        output['x_ss']=x_ss
    
    return output