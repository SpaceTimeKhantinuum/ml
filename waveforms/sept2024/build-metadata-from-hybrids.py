"""
after building all the hybrids loop over the h5 files
extract their metadata and save to .csv
"""
import pathlib
import h5py
import pandas as pd
import tqdm

def chi_eff_fn(q, chi1, chi2):
    """
    eq.1 https://arxiv.org/abs/2105.10580
    but swaped chi1<->chi2 so that for me q>=1
    """
    num = q*chi1 + chi2
    den = 1+q
    return num/den

hybrid_data_dir = pathlib.Path("/scratch/sebastian.khan/data/ppmHM_2024_11_11")
hybrid_files = list(hybrid_data_dir.glob("*.h5"))

hybrid_metadata = []
for i in tqdm.trange(len(hybrid_files)):
    hybrid_file = hybrid_files[i]
    try:
        with h5py.File(hybrid_file, 'r') as f:
            row = list(f.attrs.items())
            row += [('filepath', hybrid_file)]
            row += [('hybrid_name', hybrid_file.name.split('.h5')[0])]
            hybrid_metadata.append(row)
    except:
        print(i, hybrid_files[i])

df=[]
for hm in hybrid_metadata:
    df_ = pd.DataFrame(hm).set_index(0).T
    df.append(df_)
df = pd.concat(df).reset_index(drop=True)

df['is_reference'] = df['is_reference'].astype(bool)
df['q'] = df['q'].astype(float)
df['chi1z'] = df['chi1z'].astype(float)
df['chi2z'] = df['chi2z'].astype(float)

df['q_rounded'] = df['q'].round(3)
df['chi1z_rounded'] = df['chi1z'].round(3)
df['chi2z_rounded'] = df['chi2z'].round(3)

df['chi_eff'] = chi_eff_fn(df['q'], df['chi1z'], df['chi2z'])
df['chi_eff_rounded'] = df['chi_eff'].round(4)


# index every unique point in parameter space 
ID_qchi_map = df[df['is_reference'] == True][['q_rounded','chi1z_rounded','chi2z_rounded']].reset_index(drop=True).reset_index().rename(columns={'index':'ID'})

df = pd.merge(df, ID_qchi_map, on=['q_rounded','chi1z_rounded','chi2z_rounded'])

# now the dataframe has the column ID which you can use to gather all the simulations belonging to the same point in parameter space

df.to_csv("/scratch/sebastian.khan/data/ppmHM_2024_11_11/metadata.csv", index=False)