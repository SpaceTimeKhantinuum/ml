# run with
# nohup python -u build-and-save-hybrids.py > build-and-save-hybrids.log &

import pathlib
import numpy as np
import utils

import pandas as pd

import copy
import sxs
import phenom

import prim.hybrid
from prim import spliced_pn
from prim.waveform import Waveform
from prim.waveform_generator import generate_waveform

import prim.taylort3

import logging
logging.basicConfig(level=logging.INFO, force=True, format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
logging.getLogger('utils').setLevel('INFO')

SIMS_TO_EXCLUDE = [
    "SXS:BBH:1412", # 2nd longest sim, near equal mass. HMs have oscilations in amplitude.
    "SXS:BBH:3683", # issues with data: https://github.com/sxs-collaboration/sxs/issues/114
    "SXS:BBH:3684", # doesn't have a merger: https://github.com/sxs-collaboration/sxs/issues/114
    "SXS:BBH:3664", # no merger
    "SXS_BBH_2143", # noise frequency during late inspiral?
    "SXS:BBH:3682", # noise frequency during late inspiral?
]

EXIST_OK = True
# BASE_NAME = f"ppmHM_{utils.current_date_tag()}"
# BASE_NAME = "ppmHM_2024_11_11"
BASE_NAME = "ppmHM_2025_03_13"
# OUTPUT_H5_RSEULTS_DIRECTORY = pathlib.Path(f"~/personal/data/{BASE_NAME}").expanduser()
OUTPUT_H5_RSEULTS_DIRECTORY = pathlib.Path(f"/scratch/sebastian.khan/data/{BASE_NAME}").expanduser()

logging.info(f"{BASE_NAME = }")
logging.info(f"{OUTPUT_H5_RSEULTS_DIRECTORY = }")

logging.info("making output direcorty")

try:
    OUTPUT_H5_RSEULTS_DIRECTORY.mkdir(exist_ok=EXIST_OK)
except Exception as e:
    logging.error(str(e))
    raise

logging.info("setting lal data path")
utils.set_lal_data_path('/home/sebastian.khan')

approximants = [
    "SEOBNRv5HM",
    "SEOBNRv4HM_PA",
    # "IMRPhenomTHM",
    # "NRHybSur3dq8",
    # "SpinTaylorT4",
    # "SpinTaylorT1",
]


coarse_grid_params = {"on": True, "t0": -300, "t1": 300, "N": 5000}
# coarse_grid_params = {"on": True, "t0": -300, "t1": 300, "N": 10000}
# coarse_grid_params = {"on": True, "t0": -100, "t1": 100, "N": 1000}




M = 20

df_params = utils.get_simulations_grouped_by_mass_ratio_and_aligned_spin_dataframe()


skip_sim = False
i_start = 0
i_end = len(df_params)
logging.info(f"{i_start = }")
logging.info(f"{i_end = }")
# for idx in df_params.index:
for idx in range(i_start, i_end):
    logging.info(f"{idx = }")
    sims = df_params.loc[[idx]]['sims'].values[0]
    for sim_to_exclude in SIMS_TO_EXCLUDE:
        if sim_to_exclude in sims:
            logging.info(f"skipping simulation: {sim_to_exclude}")
            # logging.info(f"found at idx: {idx}")
            # logging.info(f"other sims in this set: {sims}")
            skip_sim = True
            break
    if skip_sim is True:
        skip_sim = False
        continue

    logging.info(f"working simulations: {sims}")
        
    q, chi1, chi2, chi_eff, min_orbits, max_orbits, min_reference_orbital_frequency_mag, max_reference_orbital_frequency_mag = df_params.loc[[idx]][['reference_mass_ratio_rounded', 'reference_dimensionless_spin1z_rounded', 'reference_dimensionless_spin2z_rounded', 'reference_chi_eff_rounded','min_orbits', 'max_orbits', 'min_reference_orbital_frequency_mag', 'max_reference_orbital_frequency_mag']].values[0]

    logging.info(f"{q = }, {chi1 = }, {chi2 = }, {chi_eff = }, {min_orbits = }, {max_orbits = }, {min_reference_orbital_frequency_mag = }, {max_reference_orbital_frequency_mag = }")

    metadata = {}
    wfs = {}
    for sim in df_params.loc[idx]['sims']:
        m_, w_ = utils.sxs_load_all_levels(sim)
        metadata.update(m_)
        wfs.update(w_)

    all_modes = [(2,2), (2,1), (3,3), (3,2), (4,4), (4,3), (5,5), (5,4)]
    modes_equal_mass_equal_spin = [(2,2), (3,2), (4,4), (5,4)]
    if (np.around(q, 2) == 1) & (np.around(chi1, 3) == np.around(chi2, 3)):
        modes = modes_equal_mass_equal_spin
    else:
        modes = all_modes

    logging.info(f"{modes = }")

    logging.info("determining hybrid start frequency")

    # have been using this one as nominal value
    # some NR waveforms are longer than this.
    # in those cases just use (NR start frequency - 1 Hz) for the hybrid start frequency
    proposed_start_theta = 0.40 
    time_at_theta = prim.taylort3.TaylorT3_t(proposed_start_theta, 0, phenom.eta_from_q(q), 1)
    hybrid_start_freq_Mf = prim.taylort3.TaylorT3_Omega_GW(time_at_theta, 0, phenom.eta_from_q(q), 1, chi1, chi2)/2/np.pi
    hybrid_start_freq_Hz = np.around(phenom.MftoHz(hybrid_start_freq_Mf, M), 3)

    lowest_NR_start_freq_Hz = phenom.MftoHz(min_reference_orbital_frequency_mag * 2 / 2 / np.pi, M)
    if hybrid_start_freq_Hz > lowest_NR_start_freq_Hz:
        hybrid_start_freq_Hz = lowest_NR_start_freq_Hz - 1
    else:
        logging.info(f"{time_at_theta = } M")
        logging.info(f"{hybrid_start_freq_Mf = } Mf")
        
    logging.info(f"{hybrid_start_freq_Hz = } Hz")
    logging.info(f"{lowest_NR_start_freq_Hz = } Hz")

    logging.info("running utils.build_hybrids")

    wf_hybrids, metadata_hybrids = utils.build_hybrids(
        wfs=wfs,
        metadata=metadata,
        approximants=approximants,
        hybrid_start_freq_Hz=hybrid_start_freq_Hz,
        modes=modes,
        deltaT_approximant=1/4096/2,
        n_cycles_before_window=4,
        n_cycles_in_window=8,
        n_tries=10,
        M=M,
        coarse_grid_params = coarse_grid_params,
        # n_cycles_before_align_window=20,
        # n_cycles_in_align_window=100,
        n_cycles_before_align_window=10,
        n_cycles_in_align_window=20,
        # n_cycles_before_align_window=150,
        # n_cycles_in_align_window=200,
    
    )

    logging.info("saving hybrids")
    
    ref_hyb = metadata_hybrids['reference_hybrid']
    for k in wf_hybrids.keys():
        wf_name = k
        wf_name = wf_name.replace(':','_').replace('/','-')
        wf = wf_hybrids[k]
        metadata = metadata_hybrids['hybrids_metadatas'][k]
        if k == ref_hyb:
            is_reference = True
        else:
            is_reference = False
    
        logging.info(f"creating hybrid h5 file: {wf_name}")
        utils.create_hybrid_h5(OUTPUT_H5_RSEULTS_DIRECTORY, wf_name, wf, metadata, is_reference, EXIST_OK)

logging.info('done')
