"""
Based on [scikit-learn's Prediction Intervals for Gradient Boosting Regression](https://scikit-learn.org/1.5/auto_examples/ensemble/plot_gradient_boosting_quantile.html#prediction-intervals-for-gradient-boosting-regression)

A quantile regressor with approximate epistemic uncertainty
through synthetic data and supervised learning.
"""

import os

os.environ["KERAS_BACKEND"] = "jax"
import keras
from keras import layers
import keras.backend as K
import jax
import jax.numpy as jnp


import numpy as np
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.metrics import mean_pinball_loss
import sklearn.mixture
import sklearn.neighbors

from sklearn.experimental import enable_halving_search_cv
from sklearn.model_selection import HalvingRandomSearchCV
from sklearn.metrics import make_scorer
from sklearn.base import clone


#
# Keras quantile loss from [here](https://github.com/sachinruk/KerasQuantileModel/blob/master/Keras%20Quantile%20Model.ipynb)
# and [here](https://github.com/cgarciae/quantile-regression)
def tilted_loss(q, y, f):
    e = y - f
    return jnp.mean(jnp.maximum(q * e, (q - 1) * e), axis=-1)


# def quantile_loss(q, y_true, y_pred):
#     e = y_true - y_pred
#     return jnp.maximum(q * e, (q - 1.0) * e)


def fit_ann(X, y, alpha):
    model = keras.Sequential(
        [
            layers.Input(shape=(X.shape[1],)),
            layers.Dense(64, activation="selu"),
            layers.Dense(64, activation="selu"),
            layers.Dense(64, activation="selu"),
            layers.Dense(64, activation="selu"),
            layers.Dense(1),
        ]
    )

    model.compile(loss=lambda y, f: tilted_loss(alpha, y, f), optimizer="adam")
    model.fit(X, y, epochs=500, batch_size=32, verbose=0)
    return model


#


class EpistemicQuantileRegressor:
    def __init__(self, synthetic_data_domain, N_synth=1000):
        """
        Parameters
        ----------
        synthetic_data_domain, list(2-tuple)
            A list of pairs of numbers understand to be
            the (min, max) values of the domain for this
            dimension. The length of this is the same as the
            number of dimensions.

        N_synth, int
            Number of samples to draw initially when generating
            synthetic data.
        """
        self.synthetic_data_domain = synthetic_data_domain
        self.N_synth = N_synth

    def fit(self, X, y, alphas=[0.05, 0.5, 0.95]):
        gm_kwargs = {"n_components": 10}
        X_synth_out_domain, y_synth_out_domain = self.generate_synthetic_ood_data(
            X, y, gm_kwargs
        )

        X_aug = np.r_[X, X_synth_out_domain]
        y_aug = np.concatenate([y, y_synth_out_domain])

        self.models = self.train_sklearn_gbr(X_aug, y_aug, alphas)

        # self.models = {}
        # for alpha in alphas:
        #     self.models[alpha] = fit_ann(X_aug, y_aug, alpha)

        return self

    def train_sklearn_gbr(self, X, y, alphas):
        models = {}

        for alpha in alphas:
            param_grid = dict(
                learning_rate=[0.05, 0.1, 0.2],
                # max_depth=[2, 5, 10],
                min_samples_leaf=[1, 5, 10, 20],
                min_samples_split=[5, 10, 20, 30, 50],
            )
            neg_mean_pinball_loss_scorer = make_scorer(
                mean_pinball_loss,
                alpha=alpha,
                greater_is_better=False,  # maximize the negative loss
            )
            gbr = GradientBoostingRegressor(
                loss="quantile", alpha=alpha, random_state=0
            )
            models[alpha] = HalvingRandomSearchCV(
                gbr,
                param_grid,
                resource="n_estimators",
                max_resources=600,
                min_resources=50,
                scoring=neg_mean_pinball_loss_scorer,
                n_jobs=2,
                random_state=0,
            ).fit(X, y)

        return models

    def generate_synthetic_ood_data(self, X, y, gm_kwargs):
        """
        First generate X and then compute y
        """
        # fit generative model to X
        self.gmm = sklearn.mixture.GaussianMixture(**gm_kwargs).fit(X)
        # define threshold log-likelihood
        # log-likelihood values below this will be defined to be out-of-domain
        self.threshold_ll = self.gmm.score_samples(X).min()
        # draw samples uniformly over the whole domain defined by synthetic_data_domain
        D = len(self.synthetic_data_domain)
        min_values = [v[0] for v in self.synthetic_data_domain]
        max_values = [v[1] for v in self.synthetic_data_domain]
        X_synth = np.random.uniform(
            low=min_values, high=max_values, size=(self.N_synth, D)
        )

        ll_synth = self.gmm.score_samples(X_synth)
        mask = ll_synth > self.threshold_ll
        X_synth_in_domain = X_synth[mask]
        X_synth_out_domain = X_synth[~mask]

        # targets
        nbrs = sklearn.neighbors.NearestNeighbors(
            n_neighbors=1, algorithm="ball_tree"
        ).fit(X)
        distances, indices = nbrs.kneighbors(X_synth_out_domain)

        scaling_factor = 1

        sigma_systematic = scaling_factor * distances**2
        sigma_statistical = y.std()
        sigma_total = sigma_systematic + sigma_statistical

        y_synth_out_domain = np.random.normal(y[indices], sigma_total)[:, 0]

        return X_synth_out_domain, y_synth_out_domain
