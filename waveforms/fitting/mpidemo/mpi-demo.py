"""
(wispy-dev) [Sebastians-MacBook-Pro 19:59 mpidemo]$ time mpiexec -n 4 python mpi-demo.py
[0.08998295102090076, 0.08420754049883304, 1.6526871003122063, 1.476656541418159, 0.5028272846642443, 0.7239308658372682, 0.2939638019518428, 0.5219619772786269]

real	0m9.338s
user	0m36.045s
sys	0m0.571s
(wispy-dev) [Sebastians-MacBook-Pro 19:59 mpidemo]$ time mpiexec -n 2 python mpi-demo.py
[0.5583656500005136, 0.2816043730690444, 0.2613934427187965, 1.258533037693388, 0.38471780648090115, 1.245404812354363, 0.45844765802434206, 0.08578739535196767]

real	0m17.022s
user	0m33.250s
sys	0m0.414s
"""

def worker(task):
    a, b = task
    return a**2 + b**2

def main(pool):
    # Here we generate some fake data
    import random
    npts = 1000000
    a = [random.random() for _ in range(npts)]
    b = [random.random() for _ in range(npts)]

    tasks = list(zip(a, b))
    results = pool.map(worker, tasks)
    pool.close()

    print(results[:8])

if __name__ == "__main__":
    import sys
    from schwimmbad import MPIPool

    pool = MPIPool()

    if not pool.is_master():
        pool.wait()
        sys.exit(0)

    main(pool)
