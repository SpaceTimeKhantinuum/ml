from scipy import special as sp
import numba

@numba.njit
def test(v, z):
    return sp.jv(v, z)

print(sp.jv(3, 3), test(3., 3.))


@numba.njit
def nhyp2f1(a,b,c,z):
    return sp.hyp2f1(a,b,c,z)

a,b,c,z=1.,2.,3.,0.9

print(sp.hyp2f1(a,b,c,z), nhyp2f1(a,b,c,z))

import numpy as np

zs = np.linspace(0.1,0.9999, 1000000)

a,b,c,z=1.,2.,3.,0.9

import time

t1 = time.time()
scipy = sp.hyp2f1(a,b,c,zs)
t2 = time.time()
print("{}".format(t2-t1))

t1 = time.time()
num = [nhyp2f1(a,b,c,z) for z in zs]
t2 = time.time()
print("{}".format(t2-t1))

