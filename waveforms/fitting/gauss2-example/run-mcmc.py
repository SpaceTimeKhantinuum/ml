import matplotlib
import matplotlib.pyplot as plt
import numpy as np

import emcee
from tqdm import tqdm

matplotlib.rcParams.update({'font.size': 16})

y, x = np.loadtxt('../Gauss2.dat', unpack=True)


plt.figure()
plt.scatter(x, y)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Data to fit")
plt.savefig('data-to-fit.png')
plt.close()


# define log posterior
def log_likelihood(theta, x, y, yerr):
    """
    theta: model parameters to unpack
    x, y : data to fit
    yerr: uncertainty on the y-data
    """
    g1_amp, g1_cen, g1_wid, g2_amp, g2_cen, g2_wid, e_A, e_tau, log_f = theta
    model = ansatz(x, g1_amp, g1_cen, g1_wid, g2_amp, g2_cen, g2_wid, e_A, e_tau)
    sigma2 = yerr ** 2 + model ** 2 * np.exp(2 * log_f)
    return -0.5 * np.sum((y - model) ** 2 / sigma2 + np.log(sigma2))

def exp_model(x, A, tau):
    return A*np.exp(-x/tau)

def gaussian(x, amp, cen, wid):
    return amp * np.exp(-(x-cen)**2 / wid)

# First, we define our "signal model", in this case a simple linear function
def ansatz(x, g1_amp,g1_cen,g1_wid, g2_amp,g2_cen,g2_wid, e_A, e_tau):

    g1 = gaussian(x, g1_amp,g1_cen,g1_wid)
    g2 = gaussian(x, g2_amp,g2_cen,g2_wid)
    ex = exp_model(x, e_A, e_tau)

    return g1 + g2 + ex

def log_prior(theta):
    g1_amp, g1_cen, g1_wid, g2_amp, g2_cen, g2_wid, e_A, e_tau, log_f = theta
    if 90 < g1_amp < 150 and 80 < g1_cen < 110 and 180 < g1_wid < 1010:
        return 0.0
    elif 40 < g2_amp < 150 and 140 < g2_cen < 170 and 180 < g2_wid < 510:
        return 0.0
    elif 80 < e_A < 120 and 70 < e_tau < 120:
        return 0.0
    elif -10 < log_f < 1:
        return 0.0
    return -np.inf

def log_posterior(theta, x, y, yerr):
    log_pr = log_prior(theta)
    # if log prior returns NaN or inf then don't
    # bother computing posterior
    # just return -inf 
    if not np.isfinite(log_pr):
        return -np.inf
    log_pos = log_likelihood(theta, x, y, yerr)
    return log_pr + log_pos



ipos = [100, 100, 200, 50, 150, 190, 100, 100, 1]
ipos = np.array(ipos)

nwalkers, ndim = 32, len(ipos)

pos = ipos + 1e-4*np.random.rand(nwalkers, ndim)

yerr = np.ones(len(x))

nsteps = 5000

#from schwimmbad import MultiPool
#with MultiPool() as pool:
#    sampler = emcee.EnsembleSampler(nwalkers, ndim, log_posterior, args=(x, y, yerr), pool=pool)
#    for i, (pos, lnp, state) in enumerate(tqdm(sampler.sample(pos, iterations=nsteps))):
#        pass

sampler = emcee.EnsembleSampler(nwalkers, ndim, log_posterior, args=(x, y, yerr))
for sample in sampler.sample(pos, iterations=nsteps, progress=True):
    pass
#for i, (pos, lnp, state) in enumerate(tqdm(sampler.sample(pos, iterations=nsteps))):
#    pass


raw_samples = sampler.chain

resh = nsteps * nwalkers

plt.figure();
plt.plot(raw_samples[:,:,0].reshape(resh))
plt.savefig('chain-plot-mcmc.png')
plt.close()






