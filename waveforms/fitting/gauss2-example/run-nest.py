"""
taken from
https://github.com/joshspeagle/dynesty/blob/master/demos/Examples%20--%20Linear%20Regression.ipynb
"""

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

matplotlib.rcParams.update({'font.size': 16})

import dynesty

y, x = np.loadtxt('./Gauss2.dat', unpack=True)


plt.figure()
plt.scatter(x, y)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Data to fit")
plt.savefig('data-to-fit.png')
plt.close()


# define log posterior
def log_likelihood(theta, x, y, yerr):
    """
    theta: model parameters to unpack
    x, y : data to fit
    yerr: uncertainty on the y-data
    """
    g1_amp, g1_cen, g1_wid, g2_amp, g2_cen, g2_wid, e_A, e_tau, log_f = theta
    model = ansatz(x, g1_amp, g1_cen, g1_wid, g2_amp, g2_cen, g2_wid, e_A, e_tau)
    sigma2 = yerr ** 2 + model ** 2 * np.exp(2 * log_f)
    return -0.5 * np.sum((y - model) ** 2 / sigma2 + np.log(sigma2))

def exp_model(x, A, tau):
    return A*np.exp(-x/tau)

def gaussian(x, amp, cen, wid):
    return amp * np.exp(-(x-cen)**2 / wid)

# First, we define our "signal model", in this case a simple linear function
def ansatz(x, g1_amp,g1_cen,g1_wid, g2_amp,g2_cen,g2_wid, e_A, e_tau):

    g1 = gaussian(x, g1_amp,g1_cen,g1_wid)
    g2 = gaussian(x, g2_amp,g2_cen,g2_wid)
    ex = exp_model(x, e_A, e_tau)

    return g1 + g2 + ex

def p4nest(var, low, high):
    """
    transform normal prior bounds to how nest wants it
    """
    return var * (high - low) + low

def prior_transform(utheta):
    """
    This is how you do it in nest
    """
    ug1_amp, ug1_cen, ug1_wid, ug2_amp, ug2_cen, ug2_wid, ue_A, ue_tau, ulog_f = utheta
    g1_amp = p4nest(ug1_amp, 90, 150)
    g1_cen = p4nest(ug1_cen, 80, 110)
    g1_wid = p4nest(ug1_wid, 180, 1010)
    g2_amp = p4nest(ug2_amp, 40, 150)
    g2_cen = p4nest(ug2_cen, 140, 170)
    g2_wid = p4nest(ug2_wid, 180, 510)
    e_A    = p4nest(ue_A, 80, 120)
    e_tau  = p4nest(ue_tau, 70, 120)
    log_f  = p4nest(ulog_f, -10, 1)

    return g1_amp, g1_cen, g1_wid, g2_amp, g2_cen, g2_wid, e_A, e_tau, log_f

def log_prior(theta):
    """
    UNUSED: this is how you would do it in emcee
    """
    g1_amp, g1_cen, g1_wid, g2_amp, g2_cen, g2_wid, e_A, e_tau, log_f = theta
    if 90 < g1_amp < 150 and 80 < g1_cen < 110 and 180 < g1_wid < 1010:
        return 0.0
    elif 40 < g2_amp < 150 and 140 < g2_cen < 170 and 180 < g2_wid < 510:
        return 0.0
    elif 80 < e_A < 120 and 70 < e_tau < 120:
        return 0.0
    elif -10 < log_f < 1:
        return 0.0
    return -np.inf

def log_posterior(theta, x, y, yerr):
    log_pr = log_prior(theta)
    # if log prior returns NaN or inf then don't
    # bother computing posterior
    # just return -inf 
    if not np.isfinite(log_pr):
        return -np.inf
    log_pos = log_likelihood(theta, x, y, yerr)
    return log_pr + log_pos


yerr = np.ones(len(x))

ndim = 9

#dsampler = dynesty.DynamicNestedSampler(log_likelihood, prior_transform, ndim=ndim,
#        bound='multi', sample='rstagger', logl_kwargs={'x':x, 'y':y,'yerr':yerr})
dsampler = dynesty.NestedSampler(log_likelihood, prior_transform, ndim=ndim,
        bound='multi', sample='rstagger', logl_kwargs={'x':x, 'y':y,'yerr':yerr}, nlive=500)
#dsampler.run_nested(dlogz_init=0.05, nlive_init=500, nlive_batch=100)
dsampler.run_nested()
dres = dsampler.results

from dynesty import plotting as dyplot

#truths = [m_true, b_true, np.log(f_true)]
labels = ['g1amp', 'g1cen', 'g1wid', 'g2amp', 'g2cen', 'g2wid', 'eA', 'etau', 'logf']

fig, axes = dyplot.traceplot(dsampler.results, labels=labels,
                             fig=plt.subplots(ndim, 2, figsize=(16, 12)))
fig.tight_layout()
plt.savefig('dynesty.png')

fig, axes = dyplot.cornerplot(dres, show_titles=True, 
                              title_kwargs={'y': 1.04}, labels=labels,
                              fig=plt.subplots(ndim, ndim, figsize=(15, 15)))
plt.savefig('dynesty-corner.png')



# get maxL parameters and plot maxL prediction
g1_amp, g1_cen, g1_wid, g2_amp, g2_cen, g2_wid, e_A, e_tau, log_f = dres['samples'][-1]
yhat = ansatz(x, g1_amp,g1_cen,g1_wid, g2_amp,g2_cen,g2_wid, e_A, e_tau)

plt.figure()
plt.scatter(x, y, label='data')
plt.plot(x, yhat, label='maxL', c='C1')
plt.legend()
plt.savefig('maxL-fit-and-data.png')
plt.close()




