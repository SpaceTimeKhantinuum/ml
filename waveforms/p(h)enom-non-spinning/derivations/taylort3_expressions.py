"""
TaylorT3 v(theta), omega(theta), phi(theta) PN coefficients

These are the PN orbital velocity (v), angular orbital frequency (omega) and the orbital phase (phi).

Calculated using the notebook `deriving-taylort3.ipynb`

For omega(theta) and phi(theta) the non-spinning terms can be compared with [BIOPS arXiv:0907.0700](http://arxiv.org/abs/0907.0700).

For omega(theta) with spinning terms they can be compared with [PhenomT arXiv:2004.08302](http://arxiv.org/abs/2004.08302) however, they use a different spin parameterisation which makes comparisons difficult.

For v(theta) I don't know of any references to compare with but in [arXiv:0710.0158](https://arxiv.org/abs/0710.0158) page 17 there is x(tau) for the equal-mass (non-spinning) case.
"""

__author__      = "Sebastian Khan"

from sympy import *

# v: velocity
v = symbols('v', positive=True, real=True)

# eta: symmetric mass ratio
# M: total mass
# gamma: Euler gamma
# delta: anti-symmetric mass ratio m1-m2 / M
eta, M, gamma, delta = symbols('eta, M, gamma, delta', positive=True, real=True)

# symmetric and anti-symmetric spin
# chi_s = chi1 + chi2 / 2
# chi_a = chi1 - chi2 / 2
chi_s, chi_a = symbols('chi_s, chi_a', real=True)

# TaylorT3
# theta: TaylorT3 time variable
theta = symbols('theta', real=True, positive=True)
# t: time (from T2)
# t_0: reference time (from T2)
t, t_0 = symbols('t, t_0', real=True)

# delta = sqrt(1 - 4\eta)
# delta^2 = 1 - 4\eta
# We need this $\delta = \sqrt{1 - 4\eta}$ specifically to eliminate $\delta^2$ terms and be able to match the results from the literature
delta_in_terms_of_eta = sqrt(1 - 4*eta)

# use this to recover non-spinning terms
non_spin_sub_dict = {chi_s:0, chi_a:0}


# v(theta) coefficients
bN, b0, b1, b2, b3, b4, b5, b6, b6_log, b7 = symbols('b_N, b0, b1, b2, b3, b4, b5, b6, b6_log, b7', real=True)

# omega(theta) coefficients
cN, c0, c1, c2, c3, c4, c5, c6, c6_log, c7 = symbols('c_N, c0, c1, c2, c3, c4, c5, c6, c6_log, c7', real=True)

# phi(theta) coefficients
dN, d0, d1, d2, d3, d4, d5_log, d6, d6_log, d7 = symbols('d_N, d0, d1, d2, d3, d4, d5_log, d6, d6_log, d7', real=True)

############################################################################
# v(theta)
############################################################################

def TaylorT3v_expr():
    """
    The TaylorT2 $t(v)$ expression

    This is

    $$
    v(\theta) = b_N * \theta * ( 1 + ... )
    $$

    """
    return (
        bN * theta * (
            1
            + b1*theta**1
            + b2*theta**2
            + b3*theta**3
            + b4*theta**4
            + b5*theta**5
            + (b6+b6_log*log(2*theta))*theta**6
            + b7*theta**7
        )
    )



def TaylorT3v_subs():
    """
    """
    subs = {
        bN:TaylorT3v_Newtonian(),
        b0:TaylorT3v_0PN(),
        b1:TaylorT3v_05PN(),
        b2:TaylorT3v_1PN(),
        b3:TaylorT3v_15PN(),
        b4:TaylorT3v_2PN(),
        b5:TaylorT3v_25PN(),
        b6:TaylorT3v_3PN(),
        b6_log:TaylorT3v_3PN_log(),
        b7:TaylorT3v_35PN(),
    }
    return subs



def TaylorT3v_Newtonian():
    """
    Leading order term multiplying theta
    """
    return Rational(1, 2)

def TaylorT3v_0PN():
    """
    Constant term
    """
    return 1

def TaylorT3v_05PN():
    """
    Proportional to theta
    """
    return 0

def TaylorT3v_1PN():
    """
    Proportional to theta**2
    """
    return (
        11*eta/96 + Rational(743, 8064)
    )

def TaylorT3v_15PN():
    """
    Proportional to theta**3
    """
    return (
        # non-spinning
        -pi/10
        +
        # spinning
        113*chi_a*delta/480 + chi_s*(Rational(113, 480) - 19*eta/120)
    )

def TaylorT3v_2PN():
    """
    Proportional to theta**4
    """
    return (
        # non-spinning
        871*eta**2/18432 + 13543*eta/258048 + Rational(4461199, 130056192)
        +
        # spinning
        chi_a**2*(5*eta/16 + Rational(-81, 1024)) - 81*chi_a*chi_s*delta/512 + chi_s**2*(eta/256 + Rational(-81, 1024))
    )

def TaylorT3v_25PN():
    """
    Proportional to theta**5
    """
    return (
        # non-spinning
        51*pi*eta/1280 - 32701*pi/322560
        +
        # spinning
        chi_a**3*delta*(Rational(1, 128) - eta/128) + chi_a**2*chi_s*(Rational(3, 128) - 9*eta/128) + chi_a*chi_s**2*delta*(Rational(3, 128) - 3*eta/128) + chi_a*delta*(Rational(1387051, 1935360) - 463*eta/23040) + chi_s**3*(Rational(1, 128) - 3*eta/128) + chi_s*(-301*eta**2/5760 - 177703*eta/241920 + Rational(1387051, 1935360))
    )

def TaylorT3v_3PN():
    """
    Proportional to theta**6
    Non-spinning term manually modified by removing the log terms
    and putting them in `TaylorT3v_3PN_log`.
    This was done to be consistent with the literature.
    """
    return (
        # non-spinning (minus log term)
        175777*eta**3/5308416 - 4096439*eta**2/148635648 - 451*pi**2*eta/6144 + 25070977133*eta/12485394432 + 107*gamma/840 + Rational(-242170811046259, 288412611379200) + 47*pi**2/600
        +
        # spinning
        chi_a**2*(303*eta**2/512 - 2747371*eta/17203200 + Rational(-108937, 206438400)) + chi_a*chi_s*delta*(-2288167*eta/3686400 + Rational(-108937, 103219200)) - 107*pi*chi_a*delta/300 + chi_s**2*(270071*eta**2/1843200 - 2960411*eta/6451200 + Rational(-108937, 206438400)) + chi_s*(331*pi*eta/1200 - 107*pi/300)
    )

def TaylorT3v_3PN_log():
    """
    This coefficient multiplies the log(2*theta)*theta**6 term.
    
    """
    return (
        107/840
    )

def TaylorT3v_35PN():
    """
    Proportional to theta**7
    """
    return (
        # non-spinning
        14809*pi*eta**2/387072 - 75703*pi*eta/774144 - 15419335*pi/130056192
        +
        # spinning
        chi_a**3*delta*(-17*eta**2/512 + 87455*eta/86016 + Rational(-3237, 14336)) + chi_a**2*chi_s*(-1787*eta**2/1536 + 267527*eta/86016 + Rational(-9711, 14336)) + chi_a**2*(-7*pi*eta/8 + 57*pi/256) + chi_a*chi_s**2*delta*(-51*eta**2/512 + 39625*eta/86016 + Rational(-9711, 14336)) + 57*pi*chi_a*chi_s*delta/128 + chi_a*delta*(-115739*eta**2/221184 + 30187*eta/114688 + Rational(4074790483, 1560674304)) + chi_s**3*(-181*eta**2/1536 + 14929*eta/86016 + Rational(-3237, 14336)) + chi_s**2*(-pi*eta/64 + 57*pi/256) + chi_s*(14341*eta**3/55296 - 2237903*eta**2/1548288 - 869712071*eta/390168576 + Rational(4074790483, 1560674304))
    )

############################################################################
# omega(theta)
############################################################################


def TaylorT3omega_expr():
    """
    The TaylorT2 $omega(theta)$ expression

    This is

    $$
    \omega(\theta) = c_N * \theta**3 * ( 1 + ... )
    $$

    """
    return (
        cN * theta**3 * (
            1
            + c1*theta**1
            + c2*theta**2
            + c3*theta**3
            + c4*theta**4
            + c5*theta**5
            + (c6+c6_log*log(2*theta))*theta**6
            + c7*theta**7
        )
    )



def TaylorT3omega_subs():
    """
    """
    subs = {
        cN:TaylorT3omega_Newtonian(),
        c0:TaylorT3omega_0PN(),
        c1:TaylorT3omega_05PN(),
        c2:TaylorT3omega_1PN(),
        c3:TaylorT3omega_15PN(),
        c4:TaylorT3omega_2PN(),
        c5:TaylorT3omega_25PN(),
        c6:TaylorT3omega_3PN(),
        c6_log:TaylorT3omega_3PN_log(),
        c7:TaylorT3omega_35PN(),
    }
    return subs




def TaylorT3omega_Newtonian():
    """
    Leading order term multiplying theta
    """
    return 1/(8*M)

def TaylorT3omega_0PN():
    """
    Constant term
    """
    return 1

def TaylorT3omega_05PN():
    """
    Proportional to theta
    """
    return 0

def TaylorT3omega_1PN():
    """
    Proportional to theta**2
    """
    return (
        11*eta/32 + Rational(743, 2688)
    )

def TaylorT3omega_15PN():
    """
    Proportional to theta**3
    """
    return (
        # non-spinning
        -3*pi/10
        +
        # spinning
        113*chi_a*delta/160 + chi_s*(Rational(113, 160) - 19*eta/40)
    )

def TaylorT3omega_2PN():
    """
    Proportional to theta**4
    """
    return (
        # non-spinning
        371*eta**2/2048 + 56975*eta/258048 + Rational(1855099, 14450688)
        +
        # spinning
        chi_a**2*(15*eta/16 + Rational(-243, 1024)) - 243*chi_a*chi_s*delta/512 + chi_s**2*(3*eta/256 + Rational(-243, 1024))
    )

def TaylorT3omega_25PN():
    """
    Proportional to theta**5
    """
    return (
        # non-spinning
        13*pi*eta/256 - 7729*pi/21504
        +
        # spinning
        chi_a**3*delta*(Rational(3, 128) - 3*eta/128) + chi_a**2*chi_s*(Rational(9, 128) - 27*eta/128) + chi_a*chi_s**2*delta*(Rational(9, 128) - 9*eta/128) + chi_a*delta*(13*eta/128 + Rational(147101, 64512)) + chi_s**3*(Rational(3, 128) - 9*eta/128) + chi_s*(-17*eta**2/64 - 2453*eta/1152 + Rational(147101, 64512))
    )

def TaylorT3omega_3PN():
    """
    Proportional to theta**6
    Non-spinning term manually modified by removing the log terms
    """
    return (
        # non-spinning (minus log term)
        235925*eta**3/1769472 - 30913*eta**2/1835008 - 451*pi**2*eta/2048 + 25302017977*eta/4161798144 + 107*gamma/280 + Rational(-720817631400877, 288412611379200) + 53*pi**2/200
        +
        # spinning
        chi_a**2*(1019*eta**2/512 - 5882229*eta/5734400 + Rational(1188991, 9830400)) + chi_a*chi_s*delta*(Rational(1188991, 4915200) - 2696633*eta/1228800) - 969*pi*chi_a*delta/800 + chi_s**2*(317929*eta**2/614400 - 3553639*eta/2150400 + Rational(1188991, 9830400)) + chi_s*(369*pi*eta/400 - 969*pi/800)
    )

def TaylorT3omega_3PN_log():
    """
    This coefficient multiplies the log(2*theta)*theta**6 term.
    """
    return (
        107/280
    )

def TaylorT3omega_35PN():
    """
    Proportional to theta**7
    """
    return (
        # non-spinning
        141769*pi*eta**2/1290240 - 97765*pi*eta/258048 - 188516689*pi/433520640
        +
        # spinning
        chi_a**3*delta*(-215*eta**2/2048 + 600847*eta/172032 + Rational(-1350103, 1720320)) + chi_a**2*chi_s*(-7855*eta**2/2048 + 614627*eta/57344 + Rational(-1350103, 573440)) + chi_a**2*(-45*pi*eta/16 + 3663*pi/5120) + chi_a*chi_s**2*delta*(-645*eta**2/2048 + 441829*eta/286720 + Rational(-1350103, 573440)) + 3663*pi*chi_a*chi_s*delta/2560 + chi_a*delta*(-138941*eta**2/92160 + 8424467*eta/6193152 + Rational(5386538891, 650280960)) + chi_s**3*(-3823*eta**2/10240 + 170243*eta/286720 + Rational(-1350103, 1720320)) + chi_s**2*(-63*pi*eta/1280 + 3663*pi/5120) + chi_s*(31841*eta**3/46080 - 37576769*eta**2/7741440 - 86900563*eta/13271040 + Rational(5386538891, 650280960))
    )

############################################################################
# phi(theta)
############################################################################

def TaylorT3_phi_expr():
    """
    The TaylorT2 $phi(theta)$ expression

    This is

    $$
    phi(\theta) = d_N * \theta^-5 * ( 1 + ... )
    $$

    """
    return (
        dN * theta**(-5) * (
            1
            + d1*theta**1
            + d2*theta**2
            + d3*theta**3
            + d4*theta**4
            + d5_log*log(theta)*theta**5
            + (d6+d6_log*log(2*theta))*theta**6
            + d7*theta**7
        )
    )



def TaylorT3phi_subs():
    """
    """
    subs = {
        dN:TaylorT3phi_Newtonian(),
        d0:TaylorT3phi_0PN(),
        d1:TaylorT3phi_05PN(),
        d2:TaylorT3phi_1PN(),
        d3:TaylorT3phi_15PN(),
        d4:TaylorT3phi_2PN(),
        d5_log:TaylorT3phi_25PN_log(),
        d6:TaylorT3phi_3PN(),
        d6_log:TaylorT3phi_3PN_log(),
        d7:TaylorT3phi_35PN(),
    }
    return subs


def TaylorT3phi_Newtonian():
    """
    Leading order term multiplying theta
    """
    return -1/eta

def TaylorT3phi_0PN():
    """
    Constant term
    """
    return 1

def TaylorT3phi_05PN():
    """
    Proportional to theta
    """
    return 0

def TaylorT3phi_1PN():
    """
    Proportional to theta**2
    """
    return (
        55*eta/96 + Rational(3715, 8064)
    )

def TaylorT3phi_15PN():
    """
    Proportional to theta**3
    """
    return (
        # non-spinning
        -3*pi/4
        +
        # spinning
        113*chi_a*delta/64 + chi_s*(Rational(113, 64) - 19*eta/16)
    )

def TaylorT3phi_2PN():
    """
    Proportional to theta**4
    """
    return (
        # non-spinning
        1855*eta**2/2048 + 284875*eta/258048 + Rational(9275495, 14450688)
        +
        # spinning
        chi_a**2*(75*eta/16 + Rational(-1215, 1024)) - 1215*chi_a*chi_s*delta/512 + chi_s**2*(15*eta/256 + Rational(-1215, 1024))
    )

def TaylorT3phi_25PN_log():
    """
    Proportional to log(theta)*theta**5
    """
    return (
        # non-spinning
        -5*(13*pi*eta/256 - 7729*pi/21504)
        +
        # spinning
        (-5*chi_a**3*delta*(Rational(3, 128) - 3*eta/128) - 5*chi_a**2*chi_s*(Rational(9, 128) - 27*eta/128) - 5*chi_a*chi_s**2*delta*(Rational(9, 128) - 9*eta/128) - 5*chi_a*delta*(13*eta/128 + Rational(147101, 64512)) - 5*chi_s**3*(Rational(3, 128) - 9*eta/128) - 5*chi_s*(-17*eta**2/64 - 2453*eta/1152 + Rational(147101, 64512)))
    )

def TaylorT3phi_3PN():
    """
    Proportional to theta**6
    Non-spinning term manually modified by removing the log terms
    and putting them in `TaylorT3v_3PN_log`.
    This was done to be consistent with the literature.
    """
    return (
        # non-spinning (minus log term)
        -1179625*eta**3/1769472 + 154565*eta**2/1835008 - 126510089885*eta/4161798144 + 2255*pi**2*eta/2048 - 107*gamma/56 - 53*pi**2/40 + Rational(831032450749357, 57682522275840)
        +
        # spinning
        chi_a**2*(-5095*eta**2/512 + 5882229*eta/1146880 + Rational(-1188991, 1966080)) + chi_a*chi_s*delta*(2696633*eta/245760 + Rational(-1188991, 983040)) + 969*pi*chi_a*delta/160 + chi_s**2*(-317929*eta**2/122880 + 3553639*eta/430080 + Rational(-1188991, 1966080)) + chi_s*(-369*pi*eta/80 + 969*pi/160)
    )

def TaylorT3phi_3PN_log():
    """
    This coefficient multiplies the log(2*theta)*theta**6 term.
    
    """
    return (
        Rational(-107, 56)
    )

def TaylorT3phi_35PN():
    """
    Proportional to theta**7
    """
    return (
        # non-spinning
        -141769*pi*eta**2/516096 + 488825*pi*eta/516096 + 188516689*pi/173408256
        +
        # spinning
        chi_a**3*delta*(1075*eta**2/4096 - 3004235*eta/344064 + Rational(1350103, 688128)) + chi_a**2*chi_s*(39275*eta**2/4096 - 3073135*eta/114688 + Rational(1350103, 229376)) + chi_a**2*(225*pi*eta/32 - 3663*pi/2048) + chi_a*chi_s**2*delta*(3225*eta**2/4096 - 441829*eta/114688 + Rational(1350103, 229376)) - 3663*pi*chi_a*chi_s*delta/1024 + chi_a*delta*(138941*eta**2/36864 - 42122335*eta/12386304 + Rational(-5386538891, 260112384)) + chi_s**3*(3823*eta**2/4096 - 170243*eta/114688 + Rational(1350103, 688128)) + chi_s**2*(63*pi*eta/512 - 3663*pi/2048) + chi_s*(-31841*eta**3/18432 + 37576769*eta**2/3096576 + 86900563*eta/5308416 + Rational(-5386538891, 260112384))
    )

