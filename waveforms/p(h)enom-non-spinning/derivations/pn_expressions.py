"""
PN aligned-spin, circular orbit Energy and Flux upto 3.5PN

We take our expressions mainly from [Data formats for numerical relativity arXiv:0709.0093](http://arxiv.org/abs/0709.0093) however, below are other references that might be useful.

References
    
    - Non-spinning only
        - [BIOPS arXiv:0907.0700](http://arxiv.org/abs/0907.0700) paper we can get the non-spinning Energy (3PN) and Flux (3.5PN)
    - Aligned-spin (both of these have $t(v)$ written down in terms of $\delta, \chi_a, \chi_s$
        - [Frank's thesis](https://publishup.uni-potsdam.de/opus4-ubp/frontdoor/index/index/year/2012/docId/5814)
        - [Bohé+ arXiv:1212.5520)](https://arxiv.org/abs/1212.5520)
    - Black Hole Horizon absorption
        - [Alvi 2001](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.64.104020)
    - Other
        - https://git.ligo.org/lscsoft/lalsuite/blob/master/lalsimulation/lib/LALSimInspiralPNCoefficients.c
"""

__author__      = "Sebastian Khan"

from sympy import *

# v: velocity
v = symbols('v', positive=True, real=True)

# eta: symmetric mass ratio
# M: total mass
# gamma: Euler gamma
# delta: anti-symmetric mass ratio m1-m2 / M
eta, M, gamma, delta = symbols('eta, M, gamma, delta', positive=True, real=True)

# symmetric and anti-symmetric spin
# chi_s = chi1 + chi2 / 2
# chi_a = chi1 - chi2 / 2
chi_s, chi_a = symbols('chi_s, chi_a', real=True)

# PN energy coefficients
eN, e0, e1, e2, e3, e4, e5, e6, e7 = symbols('e_N, e0, e1, e2, e3, e4, e5, e6, e7', real=True)
# PN flux coefficients
fN, f0, f1, f2, f3, f4, f5, f6, f6_log, f7 = symbols('f_N, f0, f1, f2, f3, f4, f5, f6, f6_log, f7', real=True)

# delta = sqrt(1 - 4\eta)
# delta^2 = 1 - 4\eta
# We need this $\delta = \sqrt{1 - 4\eta}$ specifically to eliminate $\delta^2$ terms and be able to match the results from the literature
delta_in_terms_of_eta = sqrt(1 - 4*eta)

# use this to recover non-spinning terms
non_spin_sub_dict = {chi_s:0, chi_a:0}

def Energy_expr():
    """
    The PN Energy expression
    """
    return (
        eN * v**2 * (
            e0
            + e1*v
            + e2*v**2
            + e3*v**3
            + e4*v**4
            + e5*v**5
            + e6*v**6
        )
    )


def Flux_expr():
    """
    The PN Flux expression
    """
    return (
        fN * v**10 * (
            f0
            + f1*v
            + f2*v**2
            + f3*v**3
            + f4*v**4
            + f5*v**5
            + (f6+f6_log*log(4*v))*v**6
            + f7*v**7
        )
    )


def Energy_subs():
    """
    Returns a dictionary that can be used
    to substitute the algebraic
    PN Energy coefficients with their PN experssions
    """
    subs = {
        eN:Energy_Newtonian(),
        e0:Energy_0PN(),
        e1:Energy_05PN(),
        e2:Energy_1PN(),
        e3:Energy_15PN(),
        e4:Energy_2PN(),
        e5:Energy_25PN(),
        e6:Energy_3PN(),
    }
    return subs

def Flux_subs():
    """
    Returns a dictionary that can be used
    to substitute the algebraic
    PN Flux coefficients with their PN experssions
    """
    subs = {
        fN:Flux_Newtonian(),
        f0:Flux_0PN(),
        f1:Flux_05PN(),
        f2:Flux_1PN(),
        f3:Flux_15PN(),
        f4:Flux_2PN(),
        f5:Flux_25PN(),
        f6:Flux_3PN(),
        f6_log:Flux_3PN_log(),
        f7:Flux_35PN(),
    }
    return subs

def Energy_Newtonian():
    """
    Leading order term multiplying v**2
    """
    return -Rational(1,2)*eta*M

def Energy_0PN():
    """
    Constant term
    """
    return 1

def Energy_05PN():
    """
    Proportional to v
    """
    return 0

def Energy_1PN():
    """
    Proportional to v**2
    """
    return (
        Mul(-1, Rational(3,4) + Rational(1, 12)*eta, evaluate=False)
    )

def Energy_15PN():
    """
    Proportional to v**3
    """
    return (
        Rational(8, 3)*delta*chi_a + chi_s * (Rational(8,3) - Rational(4,3)*eta)
    )

def Energy_2PN():
    """
    Proportional to v**4
    """
    return (
        -2*delta*chi_a*chi_s - Rational(1,24)*eta**2 + (4*eta-1)*chi_a**2 + Rational(19,8)*eta - chi_s**2 - Rational(27,8)
    )

def Energy_25PN():
    """
    Proportional to v**5
    """
    return (
        chi_a*(8*delta - Rational(31,9)*delta*eta) + chi_s*(Rational(2,9)*eta**2 - Rational(121,9)*eta + 8)
    )

def Energy_3PN():
    """
    Proportional to v**6
    """
    return (
        -Rational(35,5184)*eta**3 - Rational(155, 96)*eta**2 + eta*(Rational(34445, 576) - Rational(205,96)*pi**2) - Rational(675, 64)
    )


def Flux_Newtonian():
    """
    Leading order term multiplying v**10
    """
    return Rational(32,5) * eta**2

def Flux_0PN():
    """
    Constant term
    """
    return 1

def Flux_05PN():
    """
    Proportional to v**1
    """
    return 0

def Flux_1PN():
    """
    Proportional to v**2
    """
    return (
        Mul(-1, Rational(1247,336) + Rational(35, 12)*eta, evaluate=False)
    )

def Flux_15PN():
    """
    Proportional to v**3
    """
    return (
        -delta*chi_a*Rational(11,4) + chi_s*(3*eta - Rational(11,4)) + 4*pi
    )

def Flux_2PN():
    """
    Proportional to v**4
    """
    return (
        Rational(33,8)*delta*chi_a*chi_s + Rational(65,18)*eta**2 + chi_a**2*(Rational(33,16)-8*eta) + chi_s**2*(Rational(33,16)-Rational(1,4)*eta) + Rational(9271,504)*eta - Rational(44711,9072)
    )

def Flux_25PN():
    """
    Proportional to v**5

    Note we include the BH absoprtion terms see (A.14 in data formats)
    """
    return (
        chi_a * (Rational(701,36)*delta*eta - Rational(59,16)*delta)
    + chi_s * (-Rational(157,9)*eta**2 + Rational(227,9)*eta - Rational(59,16))
    - Rational(583, 24) * pi * eta
    - Rational(8191, 672) * pi
    # BH absorption (A.14 in data formats)
    -Rational(1,4)*(
        (1-3*eta)*chi_s*(1 + 3*chi_s**2 + 9*chi_a**2)
        +(1-eta)*delta*chi_a*(1 + 3*chi_a**2 + 9*chi_s**2)
    )
    )

def Flux_3PN():
    """
    Proportional to v**6
    Does not include the log term
    """
    return (
            -Rational(1712,105)*gamma - Rational(775,324)*eta**3 - Rational(94403,3024)*eta**2
    + eta*(Rational(41, 48)*pi**2 - Rational(134543,7776))
    + Rational(16,3)*pi**2
    + Rational(6643739519, 69854400)
    )

def Flux_3PN_log():
    """
    This coefficient multiplies the log(4*v)*v**6 term
    """
    return (
        -Rational(1712,105)
    )


def Flux_35PN():
    """
    Proportional to v**6
    """
    return (
            eta**2*pi*Rational(193385,3024) 
    + eta*pi*Rational(214745, 1728)
    - pi * Rational(16285, 504)
    )

