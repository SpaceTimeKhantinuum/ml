"""
TaylorT2 t(v) PN coefficients

Calculated using pn_expressions.py and the notebook taylort2-spinning.ipynb

These terms can be directly compared with Eq. A.16 in Data Formats [arXiv:0709.0093](http://arxiv.org/abs/0709.0093)
"""

__author__      = "Sebastian Khan"

from sympy import *

# v: velocity
v = symbols('v', positive=True, real=True)

# eta: symmetric mass ratio
# M: total mass
# gamma: Euler gamma
# delta: anti-symmetric mass ratio m1-m2 / M
eta, M, gamma, delta = symbols('eta, M, gamma, delta', positive=True, real=True)

# symmetric and anti-symmetric spin
# chi_s = chi1 + chi2 / 2
# chi_a = chi1 - chi2 / 2
chi_s, chi_a = symbols('chi_s, chi_a', real=True)

# TaylorT2 t(v) coefficients
# a_6l is the 3PN log term
aN, a0, a1, a2, a3, a4, a5, a6, a6l, a7 = symbols('a_N, a0, a1, a2, a3, a4, a5, a6, a_6l, a7', real=True)

# delta = sqrt(1 - 4\eta)
# delta^2 = 1 - 4\eta
# We need this $\delta = \sqrt{1 - 4\eta}$ specifically to eliminate $\delta^2$ terms and be able to match the results from the literature
delta_in_terms_of_eta = sqrt(1 - 4*eta)

# use this to recover non-spinning terms
non_spin_sub_dict = {chi_s:0, chi_a:0}


def TaylorT2_theta_expr():
    """
    The TaylorT2 $\theta(v)$ expression

    $$
    \theta(v) = a_N * v * ( 1 + ... )^(-1/8)
    $$

    Note how the log term appears as log(4v)
    In BIOPS it appears as log(16v^2)
    
    """
    return (
        aN * v * (
            1
            + a1*v**1
            + a2*v**2
            + a3*v**3
            + a4*v**4
            + a5*v**5
            + (a6+a6l*log(4*v))*v**6
            + a7*v**7
        )**Rational(-1,8)
    )

def TaylorT2_Timing_expr():
    """
    The TaylorT2 $t(v)$ expression

    This is

    $$
    t(v) = a_N / v^8 * ( 1 + ... )
    $$

    Note: this isn't used but kept for comparisons.

    Note how the log term appears as log(4v)
    In BIOPS it appears as log(16v^2)
    """
    return (
        aN * v**(-8) * (
            1
            + a1*v**1
            + a2*v**2
            + a3*v**3
            + a4*v**4
            + a5*v**5
            + (a6+a6l*log(4*v))*v**6
            + a7*v**7
        )
    )



def TaylorT2_theta_subs():
    """
    Returns a dictionary that can be used
    to substitute the algebraic
    TaylorT2 Timing coefficients with their PN experssions
    when expressing TaylorT2 timing as $\theta(v)$.

    This is to be used with `TaylorT2_theta_expr`.

    Note: we exclude the 3PN log term here and have it
    separately here: `TaylorT2_theta_log_subs`. This
    helped speed up the sympy calculation for some reason.
    """
    subs = {
        aN:TaylorT2Theta_Newtonian_theta(),
        a0:TaylorT2Timing_0PN(),
        a1:TaylorT2Timing_05PN(),
        a2:TaylorT2Timing_1PN(),
        a3:TaylorT2Timing_15PN(),
        a4:TaylorT2Timing_2PN(),
        a5:TaylorT2Timing_25PN(),
        a6:TaylorT2Timing_3PN(),
        a7:TaylorT2Timing_35PN(),
    }
    return subs


def TaylorT2_theta_log_subs():
    """
    Separate substitution for the 3PN log term
    """
    return {
        a6l:TaylorT2Timing_3PN_log(),
    }

def TaylorT2Theta_Newtonian_theta():
    """
    Leading order term multiplying v

    This is the value of `TaylorT2Timing_Newtonian`
    when we expression TaylorT2 $t(v)$ in terms of
    the TaylorT3 $\theta$ time variable.
    """
    return 2


def TaylorT2Timing_Newtonian():
    """
    Leading order term multiplying v**8

    Note: This isn't really used in the calculation of TaylorT3
    but is here just in case we need to check this.

    This is because this is the coefficient in it's raw form
    but when we compute TaylorT3 we use the $\theta$ time variable.
    """
    return -5*M/(256*eta)

def TaylorT2Timing_0PN():
    """
    Constant term
    """
    return 1

def TaylorT2Timing_05PN():
    """
    Proportional to v
    """
    return 0

def TaylorT2Timing_1PN():
    """
    Proportional to v**2
    """
    return (
        11*eta/3 + Rational(743, 252)
    )

def TaylorT2Timing_15PN():
    """
    Proportional to v**3
    """
    return (
        # non-spinning
        -32*pi/5
        +
        # spinning
        226*chi_a*delta/15 + chi_s*(Rational(226, 15) - 152*eta/15)
    )

def TaylorT2Timing_2PN():
    """
    Proportional to v**4
    """
    return (
        # non-spinning
        617*eta**2/72 + 5429*eta/504 + Rational(3058673, 508032)
        +
        # spinning
        chi_a**2*(40*eta + Rational(-81, 8)) - 81*chi_a*chi_s*delta/4 + chi_s**2*(eta/2 + Rational(-81, 8))
    )

def TaylorT2Timing_25PN():
    """
    Proportional to v**5
    """
    return (
        # non-spinning
        13*pi*eta/3 - 7729*pi/252
        +
        # spinning
        chi_a*delta*(-2*chi_a**2*eta + 2*chi_a**2 + 26*eta/3 + Rational(147101, 756)) + chi_s**3*(2 - 6*eta) + chi_s*(-18*chi_a**2*eta + 6*chi_a**2 - 6*chi_a*chi_s*delta*eta + 6*chi_a*chi_s*delta - 68*eta**2/3 - 4906*eta/27 + Rational(147101, 756))
    )

def TaylorT2Timing_3PN():
    """
    Proportional to v**6
    Non-spinning term manually modified by removing the log terms
    and putting them in `TaylorT2Timing_3PN_log`.
    This was done to be consistent with the literature
    and I also found that it helped with the sympy
    calculation if I didn't put the log coefficients in until after
    doing some of the computaitons.
    """
    return (
        # non-spinning (minus log term)
        25565*eta**3/1296 - 15211*eta**2/1728 - 451*pi**2*eta/12 + 3147553127*eta/3048192 + 6848*gamma/105 + Rational(-10052469856691, 23471078400) + 128*pi**2/3
        +
        # spinning
        chi_a**2*(964*eta**2/3 - 1541*eta/12 + Rational(6845, 672)) + chi_a*chi_s*delta*(Rational(6845, 336) - 2077*eta/6) - 584*pi*chi_a*delta/3 + chi_s**2*(245*eta**2/3 - 43427*eta/168 + Rational(6845, 672)) + chi_s*(448*pi*eta/3 - 584*pi/3)
    )

def TaylorT2Timing_3PN_log():
    """
    This coefficient multiplies the log(4*v)*v**6 term.
    
    """
    return (
        Rational(6848, 105)
    )

def TaylorT2Timing_35PN():
    """
    Proportional to v**7
    """
    return (
        # non-spinning
        14809*pi*eta**2/378 - 75703*pi*eta/756 - 15419335*pi/127008
        +
        # spinning
        chi_a**3*delta*(-34*eta**2 + 87455*eta/84 + Rational(-3237, 14)) + chi_a**2*chi_s*(-3574*eta**2/3 + 267527*eta/84 + Rational(-9711, 14)) + chi_a**2*(-896*pi*eta + 228*pi) + chi_a*chi_s**2*delta*(-102*eta**2 + 39625*eta/84 + Rational(-9711, 14)) + 456*pi*chi_a*chi_s*delta + chi_a*delta*(-115739*eta**2/216 + 30187*eta/112 + Rational(4074790483, 1524096)) + chi_s**3*(-362*eta**2/3 + 14929*eta/84 + Rational(-3237, 14)) + chi_s**2*(-16*pi*eta + 228*pi) + chi_s*(14341*eta**3/54 - 2237903*eta**2/1512 - 869712071*eta/381024 + Rational(4074790483, 1524096))
    )