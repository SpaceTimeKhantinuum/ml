"""
code to derive TaylorT3
"""

__author__      = "Sebastian Khan"


from sympy import *

# v: velocity
v = symbols('v', positive=True, real=True)

# eta: symmetric mass ratio
# M: total mass
# gamma: Euler gamma
# delta: anti-symmetric mass ratio m1-m2 / M
eta, M, gamma, delta = symbols('eta, M, gamma, delta', positive=True, real=True)

# symmetric and anti-symmetric spin
# chi_s = chi1 + chi2 / 2
# chi_a = chi1 - chi2 / 2
chi_s, chi_a = symbols('chi_s, chi_a', real=True)

# TaylorT3
# theta: TaylorT3 time variable
theta = symbols('theta', real=True, positive=True)
# t: time (from T2)
# t_0: reference time (from T2)
t, t_0 = symbols('t, t_0', real=True)

# delta = sqrt(1 - 4\eta)
# delta^2 = 1 - 4\eta
# We need this $\delta = \sqrt{1 - 4\eta}$ specifically to eliminate $\delta^2$ terms and be able to match the results from the literature
delta_in_terms_of_eta = sqrt(1 - 4*eta)

# use this to recover non-spinning terms
non_spin_sub_dict = {chi_s:0, chi_a:0}


# v(theta) coefficients
bN, b0, b1, b2, b3, b4, b5, b6, b6_log, b7 = symbols('b_N, b0, b1, b2, b3, b4, b5, b6, b6_log, b7', real=True)

# omega(theta) coefficients
cN, c0, c1, c2, c3, c4, c5, c6, c6_log, c7 = symbols('c_N, c0, c1, c2, c3, c4, c5, c6, c6_log, c7', real=True)


############################################################################
# v(theta)
############################################################################

def TaylorT3v_expr():
    """
    The TaylorT2 $t(v)$ expression

    This is

    $$
    v(\theta) = b_N * \theta * ( 1 + ... )
    $$

    """
    return (
        bN * theta * (
            1
            + b1*theta**1
            + b2*theta**2
            + b3*theta**3
            + b4*theta**4
            + b5*theta**5
            + b6*theta**6
            + b7*theta**7
        )
    )



############################################################################
# omega(theta)
############################################################################


def TaylorT3omega_expr():
    """
    The TaylorT2 $omega(theta)$ expression

    This is

    $$
    \omega(\theta) = c_N * \theta**3 * ( 1 + ... )
    $$

    """
    return (
        cN * theta**3 * (
            1
            + c1*theta**1
            + c2*theta**2
            + c3*theta**3
            + c4*theta**4
            + c5*theta**5
            + (c6+c6_log*log(2*theta))*theta**6
            + c7*theta**7
        )
    )


