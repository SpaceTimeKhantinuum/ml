import sympy

# sympy.init_printing()
# x_sym, a_sym, om_rd = sympy.symbols('x_sym a_sym om_rd')

# x_sym: generic name I use for independent variables for sympy
x_sym = sympy.symbols("x_sym")

import copy
from scipy.interpolate import InterpolatedUnivariateSpline as IUS
import numpy as np
import prim.taylort3
import prim.collocation
import prim.spliced_pn
import phenom


# we can apply an affine transformation
# to the PN inspiral portion
# of the waveform in order to
# get the data in the appropriate
# form for the PN-like ansatz


def transformation_affine_foward(x, a=1, b=0):
    """
    return y=a*x + b
    """
    return a * x + b


def transformation_affine_reverse(y, a=1, b=0):
    """
    return x=(y-b)/a
    """
    return (y - b) / a


def get_taylort3_inspiral_omega_affine_params(t, tc, eta, M, chi1, chi2, mode):
    """
    get inspiral angular GW frequency TaylorT3 Newtonian term
    and TaylorT3 approximation

    Parameters
    ----------
    mode: tuple e.g (2,2)

    returns: Newtonaian Term and Full TaylorT3 approximation
    """
    omega_N = prim.taylort3.TaylorT3_Omega_GW_Newt(t, tc, eta, M)
    omega_pn = prim.taylort3.TaylorT3_Omega_GW(t, tc, eta, M, chi1, chi2)

    omega_pn = mode[1] / 2 * omega_pn
    omega_N = mode[1] / 2 * omega_N

    return omega_N, omega_pn


def get_taylort3_inspiral_amp_affine_params(omega_22, eta, M, chi1, chi2, mode):
    """
    get inspiral amplitude TaylorT3 Newtonian term
    and TaylorT3 approximation

    returns: Newtonaian Term and Full TaylorT3 approximation
    """

    x = prim.taylort3.x_from_omega_22(omega_22, M=M)

    amp_N = prim.spliced_pn.pn_h_pre_factor(eta, x)
    if mode == (2, 2):
        amp_pn = prim.spliced_pn.pn_h_22(x, eta, 0, chi1, chi2)
    elif mode == (2, 1):
        amp_pn = prim.spliced_pn.pn_h_21(x, eta, 0, chi1, chi2)
    elif mode == (3, 3):
        amp_pn = prim.spliced_pn.pn_h_33(x, eta, 0, chi1, chi2)
    elif mode == (3, 2):
        amp_pn = prim.spliced_pn.pn_h_32(x, eta, 0, chi1, chi2)
    elif mode == (4, 4):
        amp_pn = prim.spliced_pn.pn_h_44(x, eta, 0, chi1, chi2)
    elif mode == (4, 3):
        amp_pn = prim.spliced_pn.pn_h_43(x, eta, 0, chi1, chi2)
    elif mode == (5, 5):
        amp_pn = prim.spliced_pn.pn_h_55(x, eta, 0, chi1, chi2)
    elif mode == (5, 4):
        amp_pn = prim.spliced_pn.pn_h_54(x, eta, 0, chi1, chi2)
    else:
        raise ValueError(f"{mode = } unknown")
    # see eq. 16 and 17 in https://journals.aps.org/prd/pdf/10.1103/PhysRevD.109.104045
    # for definitions
    amp_pn = np.abs(amp_pn)

    return amp_N, amp_pn


def prepare_fit_data(waveform, t_start, t_end, target, dt=None, mode=(2, 2)):
    """
    waveform: instance of waveform
    t_start, t_end: float. start and end times that the returned data will cover
    target: str. name of target attribute to model e.g. 'amplitudes', 'phases', 'frequencies'

    returns the time and the target
    """
    # eta = waveform.eta
    times = waveform.times
    mask = (times >= t_start) & (times <= t_end)

    t = times[mask]
    # y = waveform.__getattribute__(target)
    y = waveform.__getattribute__(target)[mode]
    y = y[mask]

    if dt != None:
        # interpolate with new spacing
        t_new = np.arange(t[0], t[-1], dt)
        y = IUS(t, y)(t_new)
        t = t_new

    return t, y


class InspiralFrequencyFitter:
    def __init__(self, mode, metadata, tc=0, M=1, dt=None, dof=4, swap_sign=True):
        self.mode = mode
        self.metadata = metadata
        self.tc = tc
        self.M = M
        self.dt = dt
        self.dof = dof
        self.swap_sign = swap_sign

        self.cps = np.linspace(0.42, 0.8, self.dof)
        self.base_ansatz = np.sum([x_sym ** (8 + i) for i in range(self.dof)])

        # need q_rounded so that we don't get things like 0.25000001 which can cause NaN in PN
        self.q = self.metadata["q_rounded"]
        self.eta = phenom.eta_from_q(self.q)
        self.chi1 = self.metadata["chi1z"]
        self.chi2 = self.metadata["chi2z"]

        # last collocation point is at t = -200M. Where we assume the modes are aligned
        # such that the peak is at t = 0 M
        self.theta_cp_fixed = prim.taylort3.TaylorT3_theta(
            -200, self.tc, self.eta, self.M
        )
        self.cps[-1] = self.theta_cp_fixed

        self.collocation_points_theta = {
            0: self.cps,
        }

        self.collocation_points_M = {}
        for k in self.collocation_points_theta:
            self.theta_array = np.array(self.collocation_points_theta[k])
            self.collocation_points_M[k] = prim.taylort3.TaylorT3_t(
                self.theta_array, self.tc, self.eta, self.M
            )

    def prepare(self, waveform):
        # Time range avaiable to use for fitting and testing
        self.pad = 20
        self.t_start = self.collocation_points_M[0][0] - self.pad
        # t_start = -80000
        # self.t_start = waveform.times[0]
        self.t_end = self.collocation_points_M[0][-1] + self.pad

        t, self.y = prepare_fit_data(
            waveform,
            self.t_start,
            self.t_end,
            "frequencies",
            dt=self.dt,
            mode=self.mode,
        )
        if self.swap_sign:
            # NOTE: swap sign because my PN taylort3 has opposite sign... very important
            self.y = -self.y

        #
        # PN parameters
        # pn_kwargs isn't used??
        pn_kwargs = dict(t=t, tc=self.tc, eta=self.eta, M=self.M)
        # Get PN transformation variables
        self.omega_N, self.omega_pn = get_taylort3_inspiral_omega_affine_params(
            t, self.tc, self.eta, self.M, self.chi1, self.chi2, self.mode
        )

        # apply tranformation
        y_transformed = transformation_affine_reverse(
            self.y, self.omega_N, self.omega_pn
        )

        # prepare ansatz
        theta = prim.taylort3.TaylorT3_theta(x_sym, self.tc, self.eta, self.M)
        pn_sub_dict = {"x_sym": theta}
        self.ansatz = self.base_ansatz.subs(pn_sub_dict)
        self.sub_dict = {}

        self.x = t
        self.y_transformed = y_transformed

        self.x_theta = prim.taylort3.TaylorT3_theta(self.x, self.tc, self.eta, self.M)

    def fit(self, hybrid):
        """
        The fit's x coords as functions of M
        the target variable is in the transformed space.
        """
        self.prepare(hybrid)

        self.collocation_points = self.collocation_points_M
        # self.rhs_override = rhs_override

        # interpolate target so we can evaluate
        # it and it's derivative at any x value
        self.iy_transformed = IUS(self.x, self.y_transformed)

        # evaluate interpolant at corresponding derivative order
        # and location
        self.rhs = {}
        for k in self.collocation_points.keys():
            v = map(self.iy_transformed.derivative(k), self.collocation_points[k])
            v = np.array(list(v))
            self.rhs[k] = v

        self.cm = prim.collocation.CollocationModel(
            collocation_points=self.collocation_points,
            collocation_values=self.rhs,
            ansatz=self.ansatz,
            sub_dict=self.sub_dict,
        )

    def predict(self, times, eta, chi1=0, chi2=0, M=1, tc=0):

        # Get PN transformation variables
        # omega_N and omega_pn are for the 2,2 mode
        omega_N, omega_pn = get_taylort3_inspiral_omega_affine_params(
            times, tc, eta, M, chi1, chi2, self.mode
        )

        yhat = self.cm.predict(times)
        yhat = transformation_affine_foward(yhat, omega_N, omega_pn)
        if self.swap_sign:
            yhat = -yhat
        return yhat


class InspiralAmplitudeFitter:
    # https://gitlab.com/SpaceTimeKhantinuum/ml/-/blob/master/waveforms/oct2022/uncertainty/nr_only/scratch_2/inspiral-amp-model.ipynb
    def __init__(
        self,
        mode,
        metadata,
        ins_freq_model_22,
        x_start_power,
        tc=0,
        M=1,
        dt=None,
        dof=2,
    ):
        """
        x_start_power: this is the related highest PN order + 1. different modes are known to different PN orders.
        e.g. we use 4PN (non-spinning) for 22 mode amplitude (i.e. 8/2). But the 32 mode is known up to x^3 (i.e. 6/2)
        This means that for the 22 mode fit_pn_order should be 9 and for 32 mode it should be set to 7
        """
        self.mode = mode
        self.metadata = metadata
        self.ins_freq_model_22 = ins_freq_model_22
        self.tc = tc
        self.M = M
        self.dt = dt
        self.dof = dof
        self.x_start_power = x_start_power

        self.cps = np.linspace(0.42, 0.8, self.dof)
        self.base_ansatz = np.sum(
            [x_sym ** ((self.x_start_power + i) / 2) for i in range(self.dof)]
        )
        # maybe the above should be (self.x_start_power + i) // 2) or something like Rational((self.x_start_power + i), 2)

        # need q_rounded so that we don't get things like 0.25000001 which can cause NaN in PN
        self.q = self.metadata["q_rounded"]
        self.eta = phenom.eta_from_q(self.q)
        self.chi1 = self.metadata["chi1z"]
        self.chi2 = self.metadata["chi2z"]

        # last collocation point is at t = -200M. Where we assume the modes are aligned
        # such that the peak is at t = 0 M
        self.theta_cp_fixed = prim.taylort3.TaylorT3_theta(
            -200, self.tc, self.eta, self.M
        )
        self.cps[-1] = self.theta_cp_fixed

        self.collocation_points_theta = {
            0: self.cps,
        }

        self.collocation_points_M = {}
        for k in self.collocation_points_theta:
            self.theta_array = np.array(self.collocation_points_theta[k])
            self.collocation_points_M[k] = prim.taylort3.TaylorT3_t(
                self.theta_array, self.tc, self.eta, self.M
            )

        # also compute the collocation_points in units of the PN expansion
        # x-parameter. This is what we use as the independent variable.
        self.collocation_points_x = copy.deepcopy(self.collocation_points_M)
        for k in self.collocation_points_M:
            xs = self.collocation_points_M[k]
            om = np.abs(
                self.ins_freq_model_22.predict(xs, self.eta, self.chi1, self.chi2)
            )
            x = prim.taylort3.x_from_omega_22(om)
            self.collocation_points_x[k] = x  # np.around(x, 8)

    def prepare(self, waveform):
        # Time range avaiable to use for fitting and testing
        self.pad = 20
        self.t_start = self.collocation_points_M[0][0] - self.pad
        # t_start = -80000
        # self.t_start = waveform.times[0]
        self.t_end = self.collocation_points_M[0][-1] + self.pad

        t, self.y = prepare_fit_data(
            waveform,
            self.t_start,
            self.t_end,
            "amplitudes",
            dt=self.dt,
            mode=self.mode,
        )
        #
        # PN parameters
        # Get PN transformation variables
        # omega_22 is the prediction from the inspiral frequency model
        # prediction for the 22 mode. We only need 22 mode because
        # we use it to estimate the orbital ang frequency which is used
        # to compute the PN x parameter.
        self.omega_22 = self.ins_freq_model_22.predict(
            t, self.eta, self.chi1, self.chi2
        )
        # take abs of this because in the current convention the 22 orb ang freq
        # is negative
        self.omega_22 = np.abs(self.omega_22)

        self.amp_N, self.amp_pn = get_taylort3_inspiral_amp_affine_params(
            self.omega_22, self.eta, self.M, self.chi1, self.chi2, self.mode
        )

        # apply tranformation
        y_transformed = transformation_affine_reverse(self.y, self.amp_N, self.amp_pn)

        # prepare ansatz
        # theta = prim.taylort3.TaylorT3_theta(x_sym, self.tc, self.eta, self.M)
        # pn_sub_dict = {"x_sym": theta}
        # self.ansatz = self.base_ansatz.subs(pn_sub_dict)
        self.ansatz = self.base_ansatz.copy()
        self.sub_dict = {}

        self.t = t
        self.x = prim.taylort3.x_from_omega_22(self.omega_22)
        self.y_transformed = y_transformed

        # self.x_theta = prim.taylort3.TaylorT3_theta(self.x, self.tc, self.eta, self.M)

    def fit(self, hybrid):
        """
        The fit's x coords as functions of M
        the target variable is in the transformed space.
        """
        self.prepare(hybrid)

        self.collocation_points = self.collocation_points_x
        # self.rhs_override = rhs_override

        # interpolate target so we can evaluate
        # it and it's derivative at any x value
        self.iy_transformed = IUS(self.x, self.y_transformed)

        # evaluate interpolant at corresponding derivative order
        # and location
        self.rhs = {}
        for k in self.collocation_points.keys():
            v = map(self.iy_transformed.derivative(k), self.collocation_points[k])
            v = np.array(list(v))
            self.rhs[k] = v

        self.cm = prim.collocation.CollocationModel(
            collocation_points=self.collocation_points,
            collocation_values=self.rhs,
            ansatz=self.ansatz,
            sub_dict=self.sub_dict,
        )

    def predict(self, times, eta, chi1=0, chi2=0, M=1):

        omega_22 = self.ins_freq_model_22.predict(times, eta, chi1, chi2)
        # take abs of this because in the current convention the 22 orb ang freq
        # is negative
        omega_22 = np.abs(omega_22)

        amp_N, amp_pn = get_taylort3_inspiral_amp_affine_params(
            omega_22, eta, M, chi1, chi2, self.mode
        )

        x = prim.taylort3.x_from_omega_22(omega_22)

        yhat = self.cm.predict(x)
        yhat = transformation_affine_foward(yhat, amp_N, amp_pn)
        return yhat
