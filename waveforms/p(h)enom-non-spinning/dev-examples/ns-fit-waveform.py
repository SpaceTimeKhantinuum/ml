import pandas as pd
import pathlib

import h5py
import prim.waveform
import prim.collocation
import ast
import numpy as np
import phenom

import time_shift
import estimators


import matplotlib.pyplot as plt
import scienceplots

plt.style.use(["science", "notebook", "muted"])
plt.rcParams["text.usetex"] = False


def convert_modes_string_to_list(modes_string):
    """
    the 'modes' column in the metadata is a string e.g.

    '[[2 2]\n [3 2]\n [4 4]\n [5 4]]'

    This function does a hacky convertion into a python list i.e.

    [[2, 2], [3, 2], [4, 4], [5, 4]]

    Then convert to list of tuples

    [(2, 2), (3, 2), (4, 4), (5, 4)]
    """
    v = ast.literal_eval(modes_string.replace("\n", "").replace(" ", ","))

    v = [tuple(sublist) for sublist in v]
    return v


def get_ns_metadata(root_dir):
    # get path to non-spinning waveforms that we have locally
    # list of pathlib objects
    ns_h5files = list(root_dir.glob("*.h5"))
    ns_h5files = pd.DataFrame({"filepath": ns_h5files})
    ns_h5files["filename"] = ns_h5files["filepath"].apply(
        lambda x: pathlib.Path(x).name
    )

    # read full metadata
    metadata = pd.read_csv(root_dir / "metadata.csv")
    metadata["filename"] = metadata["filepath"].apply(lambda x: pathlib.Path(x).name)

    # get a dataframe of metadata that only contains the local files
    metadata = pd.merge(metadata, ns_h5files, on="filename")
    metadata = metadata.drop(columns=["filepath_x"])
    metadata = metadata.rename(columns={"filepath_y": "filepath"})

    metadata["modes"] = metadata["modes"].apply(convert_modes_string_to_list)
    return metadata


def load_hybrid_h5_file(hybrid_file, return_metadata=False):
    with h5py.File(hybrid_file, "r") as f:
        hybrid_metadata = list(f.attrs.items()) + [("filename", hybrid_file)]

        times = f["times"][:]
        hlms = {}
        for l, m in f.attrs["modes"]:
            hlms[l, m] = f[f"hlm_l{l}_m{m}"][:]

        hybrid_wf = prim.waveform.Waveform(times, hlms)

    if return_metadata:
        return hybrid_metadata, hybrid_wf
    else:
        return hybrid_wf


def plot_waveforms(wfs, modes, labels=None, xlim=None, ylim=None):
    if labels is None:
        labels = list(range(len(wfs)))
    fig, axes = plt.subplots(len(modes), 1, figsize=(14, 10), dpi=100)
    for j, wf in enumerate(wfs):
        for i, mode in enumerate(modes):
            axes[i].plot(wf.times, wf.hlms[mode].real, label=labels[j])
            axes[i].plot(wf.times, wf.amplitudes[mode])
            axes[i].legend(loc="center left", bbox_to_anchor=(1, 0.5))
            axes[i].set_title(rf"$(\ell, m) = {mode}$")
            if xlim:
                axes[i].set_xlim(*xlim)
            if ylim:
                axes[i].set_ylim(*ylim)
            axes[i].axvline(0, c="k", ls="--")
            axes[i].set_yscale("log")
    fig.suptitle("waveform mode plot", y=0.99)
    plt.xlabel(r"$T [M]$")
    fig.tight_layout()
    return fig


if __name__ == "__main__":
    # wf_index: pick a waveform to fit
    # unequal mass example
    # wf_index = 1
    # equal mass example
    wf_index = 3
    root_dir = pathlib.Path(
        "/Users/sebastian.khan/personal/data/ppmHM_2024_11_05__ns_data_sample"
    )

    metadata = get_ns_metadata(root_dir)

    # print(metadata)

    metadata = metadata.iloc[wf_index]

    print("waveform to model")
    print(metadata)

    q = metadata["q"]
    chi1z = metadata["chi1z"]
    chi2z = metadata["chi2z"]
    modes = metadata["modes"]
    hybrid_name = metadata["hybrid_name"]
    eta = phenom.eta_from_q(q)

    hyb_wf = load_hybrid_h5_file(metadata["filepath"])

    hyb_wf.compute_amplitude()

    # print("plotting input hybrid waveform")
    xlim = None
    # xlim = (-100, 100)
    # fig = plot_waveforms([hyb_wf], modes, labels=["data"], xlim=xlim, ylim=None)
    # plt.show()

    print("begin modelling")

    print("compute peak time shifts")
    # the first thing we do is shift all mode data such that their amplitudes
    # peak at t=0M. We model/note down the time shift DeltaT_lm.
    # After we reconstruct the modes we need to undo this time shift using out model.
    deltaT_dict = time_shift.compute_time_shift(hyb_wf)

    # print(deltaT_dict)

    # Now we have the time shift of each mode relative to the sum-total we can
    # shift each mode. But maybe this is only done for the merger model?
    # We use a frequency domain time shift.
    # Note that this will introduce unphysical wrap around but the size of the time-shift
    # should be small enough that it won't mess up any modelling.
    # Especially because we will only use this for the merger model with is a model
    # over the time interval around around (-100, 30) M.
    # Will there be any issue with the phase? Need to connect to the inspral model.

    # for mode in modes:
    #     dt_lm = deltaT_dict[mode]
    #     hyb_wf_tilde = np.fft.fft(hyb_wf.hlms[mode])
    #     dt = hyb_wf.times[1] - hyb_wf.times[0]
    #     freqs = np.fft.fftfreq(len(hyb_wf.hlms[mode]), dt)
    #     hyb_wf_tilde = hyb_wf_tilde * np.exp(2j * np.pi * dt_lm * freqs)
    #     hyb_wf.hlms[mode] = np.fft.ifft(hyb_wf_tilde)
    #
    # hyb_wf.compute_amplitude()

    print("fit inspiral frequency")

    hyb_wf.compute_frequency()
    ins_freq_models = {}
    for mode in modes:
        ins_freq_models[mode] = estimators.InspiralFrequencyFitter(
            mode=mode, metadata=metadata, tc=0, M=1, dt=None, dof=4, swap_sign=True
        )
        ins_freq_models[mode].fit(hyb_wf)

    times_ = hyb_wf.times[hyb_wf.times < -200]
    ins_freq_yhats = {}
    for mode in modes:
        ins_freq_yhats[mode] = ins_freq_models[mode].predict(
            times_,
            phenom.eta_from_q(metadata["q"]),
        )

    for mode in modes:
        plt.figure()
        plt.plot(hyb_wf.times, hyb_wf.frequencies[mode], label="hyb")
        plt.plot(times_, ins_freq_yhats[mode], label="model", ls="--")
        plt.title(mode)
        plt.legend()
        plt.show()

    # we need the inspiral frequency model in order to model the inspiral amplitude.
    # this is because we use the inspiral frequency model to help deal with cases
    # where the TaylorT3 frequency changes sign and the resulting amplitude
    # becomes imaginary (or something)
    print("fit inspiral amplitude")
    # hyb_wf.compute_amplitude()
    # mode = (5, 4)
    # non-spinning start powers (related to highest known pn order)
    x_start_power = {
        (2, 2): 9,
        (2, 1): 7,
        (3, 2): 7,
        (3, 3): 7,
        (4, 4): 7,
        (4, 3): 7,
        (5, 5): 7,
        (5, 4): 7,
    }

    ins_amp_models = {}
    for mode in modes:
        ins_amp_models[mode] = estimators.InspiralAmplitudeFitter(
            mode=mode,
            metadata=metadata,
            ins_freq_model_22=ins_freq_models[2, 2],
            x_start_power=x_start_power[mode],
            tc=0,
            M=1,
            dt=None,
            dof=2,
        )
        ins_amp_models[mode].fit(hyb_wf)

    times_ = hyb_wf.times[hyb_wf.times < -200]
    ins_amp_yhats = {}
    for mode in modes:
        ins_amp_yhats[mode] = ins_amp_models[mode].predict(times_, eta)

    for mode in modes:
        plt.figure()
        plt.plot(hyb_wf.times, hyb_wf.amplitudes[mode], label="hyb")
        plt.plot(times_, ins_amp_yhats[mode], label="model", ls="--")
        plt.title(mode)
        # for v in ins_amp_model.collocation_points_M[0]:
        #     plt.axvline(v, c="k", ls="--")
        plt.legend()
        plt.show()

    # print("fit merger frequency")
    # print("fit ringdown frequency")
    # print("fit merger amplitude")
    # print("fit ringdown amplitude")
