# code from https://gitlab.com/SpaceTimeKhantinuum/ml/-/blob/master/waveforms/sept2024/gpr-fit-utils-fit-merger-example.ipynb

import numpy as np
import tqdm


def compute_time_shift(wf):
    """
    Computes the time difference between t=0M and the peak of the amplitude
    for each mode. If a mode doesn't exist in `wf` then `np.nan` is returned.

    We assume the following modes:
    (2,2), (2,1), (3,3), (3,2), (4,4), (4,3), (5,5), (5,4)

    Input is a prim.Waveform

    Output is a dictionary with a key for each mode and an additional
    key 'all' for all the modes summed together.
    """

    # for each mode lets compute the time difference between the peak amplitude
    # of each mode and the sum over all modes. A list of len(wfs) i.e. one for
    # each waveform we will turn this into a dataframe where the columns are
    # the modes

    all_modes_individually = [
        [(2, 2)],
        [(2, 1)],
        [(3, 3)],
        [(3, 2)],
        [(4, 4)],
        [(4, 3)],
        [(5, 5)],
        [(5, 4)],
    ]
    modes_to_loop = [None] + all_modes_individually
    columns = [
        "all",
        (2, 2),
        (2, 1),
        (3, 3),
        (3, 2),
        (4, 4),
        (4, 3),
        (5, 5),
        (5, 4),
    ]
    row = []
    for mode in modes_to_loop:
        try:
            t0, _ = wf.compute_amplitude().compute_time_of_peak(modes=mode)
        except KeyError:
            t0 = np.nan
        row.append(t0)
    deltaT_dict = dict(zip(columns, row))
    return deltaT_dict
