# p(h)enom - non-spinning, higher modes
Main workspace to develop a non-spinning, higher mode
probabilistic phenomenological waveform model of
hybrid NR waveforms.

## Env setup

```
$ conda create -n ppmhm python=3.10
$ conda activate ppmhm
$ pip install gw-prim
$ conda install -c conda-forge lalsuite
$ pip install pyseobnr
$ conda install -c conda-forge sxs
$ pip install matplotlib scienceplots pandas h5py
$ pip install ipykernel
$ python -m ipykernel install --user --name ppmhm
$ pip install numpy==1.26.4
$ conda install -c conda-forge numpy=1.26.4
$ pip install ipywidgets
```

Not the best way to setup the env. Will sort it out later.
