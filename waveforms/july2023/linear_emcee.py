"""
https://dfm.io/posts/mixture-models/
"""

import numpy as np
import emcee
from scipy.optimize import curve_fit

# def log_likelihood(theta, x, y, yerr):
#     """
#     this models the error as a function of yhat i.e. `model`
#     """
#     m, b, log_f = theta
#     model = m * x + b
#     sigma2 = yerr**2 + model**2 * np.exp(2 * log_f)
#     return -0.5 * np.sum((y - model) ** 2 / sigma2 + np.log(sigma2))

def log_likelihood(theta, x, y, ansatz):
    """
    this models the error as a constant
    
    ansatz is a function of theta, x i.e. ansatz(theta, x)
    """
    m, b, log_f = theta
    # model = m * x + b
    model = ansatz(theta, x)
    sigma2 = np.exp(2 * log_f)
    return -0.5 * np.sum((y - model) ** 2 / sigma2 + np.log(sigma2))

def log_likelihood_scaled_noise(theta, x, y, ansatz):
    """
    this models the error as a constant
    
    ansatz is a function of theta, x i.e. ansatz(theta, x)
    """
    m, b, log_f, log_noise_m = theta
    # model = m * x + b
    model = ansatz(theta, x)
    sigma2 = np.exp(log_noise_m) * np.exp(2 * log_f)
    return -0.5 * np.sum((y - model) ** 2 / sigma2 + np.log(sigma2))

def log_prior(p, bounds):
    """
    log_prior_ = lambda p: log_prior(p, bounds)
    """
    if not all(b[0] < v < b[1] for v, b in zip(p, bounds)):
        return -np.inf
    return 0

def log_probability(theta, x, y, log_prior_func, log_likelihood_func):
    """
    log_prior_func is a function of theta only
    i.e.
        log_prior_ = lambda p: log_prior(p, bounds)
        
    log_likelihood_func(theta, x, y) = lambda theta, x, y: log_likelihood(theta, x, y, ansatz)
    """
    lp = log_prior_func(theta)
    if not np.isfinite(lp):
        return -np.inf
    return lp + log_likelihood_func(theta, x, y)



def estimate_maxL_theta(x, y, f, p0_dist=None, ntries=10):
    """
    f = lambda x, m, b: ansatz([m, b], x)
    
    we try multiple different guesses and pick the one that minimises
    the mse as the initial guess for mcmc
    """
    popts = []
    pcovs = []
    errors = []
    for i in range(ntries):
        if p0_dist == None:
            p0 = None
        else:
            p0 = p0_dist(1)[0]
        popt, pcov = curve_fit(f, x, y, p0=p0)
        error = np.mean(np.square(y - f(x, *popt)))
        errors.append(error)
        popts.append(popt)
        pcovs.append(pcov)
    min_idx = np.argmin(errors)
    return popts[min_idx]


def run_sampler(x, y, f, ansatz, log_probability, n_walkers=32, nsteps=5000, p0_dist=None, ntries=10):
    # maxL estimate
    popt = estimate_maxL_theta(x, y, f, p0_dist, ntries)
        

    n_dim = 3
    start_loc = np.concatenate([popt, [-1]])
    # n_dim = 4
    # start_loc = np.concatenate([popt, [-1, 1]])
    pos = start_loc + 1e-4 * np.random.randn(n_walkers, n_dim)


    sampler = emcee.EnsembleSampler(
        n_walkers, n_dim, log_probability, args=(x, y)
    )
    sampler.run_mcmc(pos, nsteps, progress=True)
    return sampler, popt