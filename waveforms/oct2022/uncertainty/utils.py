import numpy as np 
import h5py
from scipy.interpolate import InterpolatedUnivariateSpline as IUS
import phenom
import lal
import lalsimulation as lalsim
import phenom


import numpy as np

import phenom

import lal
import lalsimulation as lalsim

from scipy.interpolate import InterpolatedUnivariateSpline as IUS


def gen_fd_wf_params(
        m1=50,
        m2=50,
        S1x=0,
        S1y=0,
        S1z=0,
        S2x=0,
        S2y=0,
        S2z=0,
        distance=1,
        inclination=0,
        phiRef=0,
        longAscNodes=0,
        eccentricity=0,
        meanPerAno=0,
        deltaF=1/16,
        f_min=10,
        f_max=200,
        f_ref=30,
        LALpars=None,
        approximant=lalsim.IMRPhenomD):
    p = dict(
        m1=m1,
        m2=m2,
        S1x=S1x,
        S1y=S1y,
        S1z=S1z,
        S2x=S2x,
        S2y=S2y,
        S2z=S2z,
        distance=distance,
        inclination=inclination,
        phiRef=phiRef,
        longAscNodes=longAscNodes,
        eccentricity=eccentricity,
        meanPerAno=meanPerAno,
        deltaF=deltaF,
        f_min=f_min,
        f_max=f_max,
        f_ref=f_ref,
        LALpars=LALpars,
        approximant=approximant)

    return p


def gen_fd_wf(p, f_min=None, f_max=None, units="Hz"):
    """
    units: {str:"Mf"} other choice is "Hz"
    f_min, f_max are in units of input `units`
    returned frequencies are in units of input `units`
    """
    p = p.copy()

    M = p['m1'] + p['m2']

    p.update({'m1': p['m1']*lal.MSUN_SI})
    p.update({'m2': p['m2']*lal.MSUN_SI})

    hp, _ = lalsim.SimInspiralChooseFDWaveform(**p)

    f = np.arange(hp.data.length) * hp.deltaF

    if units == 'Mf':
        # convert to geometric units
        f = phenom.HztoMf(f, M)
    elif units == "Hz":
        pass

    if f_min is None:
        f_min = f[0]
    if f_max is None:
        f_max = f[-1]

    mask = (f >= f_min) & (f <= f_max)

    hp = hp.data.data[mask]
    f = f[mask]

    amp = np.abs(hp)
    phase = np.unwrap(np.angle(hp))

    return f, amp, phase


def gen_td_wf_params(
        m1=50,
        m2=50,
        S1x=0,
        S1y=0,
        S1z=0,
        S2x=0,
        S2y=0,
        S2z=0,
        distance=1,
        inclination=0,
        phiRef=0,
        longAscNodes=0,
        eccentricity=0,
        meanPerAno=0,
        deltaT=1/4096,
        f_min=30,
        f_ref=30,
        LALpars=None,
        approximant=lalsim.SEOBNRv4):
    p = dict(
        m1=m1,
        m2=m2,
        s1x=S1x,
        s1y=S1y,
        s1z=S1z,
        s2x=S2x,
        s2y=S2y,
        s2z=S2z,
        distance=distance,
        inclination=inclination,
        phiRef=phiRef,
        longAscNodes=longAscNodes,
        eccentricity=eccentricity,
        meanPerAno=meanPerAno,
        deltaT=deltaT,
        f_min=f_min,
        f_ref=f_ref,
        params=LALpars,
        approximant=approximant)

    return p


def gen_td_wf(p, t_min=-500, t_max=50):
    """
    returned times are in geometric units
    """
    p = p.copy()

    M = p['m1'] + p['m2']

    p.update({'m1': p['m1']*lal.MSUN_SI})
    p.update({'m2': p['m2']*lal.MSUN_SI})

    hp, hc = lalsim.SimInspiralChooseTDWaveform(**p)

    t = np.arange(hp.data.length) * hp.deltaT + np.float(hp.epoch)
    t = phenom.StoM(t, M)

    if t_min is None:
        t_min = t[0]
    if t_max is None:
        t_max = t[-1]

    mask = (t >= t_min) & (t <= t_max)

    hp = hp.data.data[mask]
    hc = hc.data.data[mask]
    t = t[mask]

    h = hp - 1.j*hc

    amp = np.abs(h)
    phase = np.unwrap(np.angle(h))

    return t, amp, phase


def gen_td_modes_wf_params(
        m1=50,
        m2=50,
        S1x=0,
        S1y=0,
        S1z=0,
        S2x=0,
        S2y=0,
        S2z=0,
        distance=1,
        deltaT=1/4096,
        phiRef=0.,
        f_min=10,
        f_ref=10,
        LALpars=None,
        approximant=lalsim.SEOBNRv4P,
        lmax_dummy=2):
    """
    lmax_dummy {int: 2}: old option for XLALSimInspiralChooseTDModes.
    """

    p = dict(
        m1=m1,
        m2=m2,
        S1x=S1x,
        S1y=S1y,
        S1z=S1z,
        S2x=S2x,
        S2y=S2y,
        S2z=S2z,
        phiRef=phiRef,
        r=distance,
        deltaT=deltaT,
        f_min=f_min,
        f_ref=f_ref,
        LALpars=LALpars,
        lmax=lmax_dummy,
        approximant=approximant)

    return p


def gen_td_modes_wf(p, modes=[[2, 2]], eob_all_ell_2_modes=False):
    """
    input:
        p {dict} normally the output of gen_td_modes_wf_params
        modes {list of 2-tuples: [[2,2]]}
            modes to generate
            Note: Depending on the waveform model used
            you might need to explicitly provide both positive and
            negative modes.
        eob_all_ell_2_modes {bool: False}
            only valid if SEOBNRv4P or SEOBNRv4PHM models are chosen.
            If True then returns all ell=2 modes and overwrites input 'modes'
    returns:
        times {array} in units of seconds
        hlms {dict} contains time domain hlm modes
    """
    p = p.copy()

    if [2, 2] not in modes:
        raise NotImplementedError("[2,2] mode not in modes.\
Currently we assume that this mode exists.")

    if p['LALpars'] is None:
        p['LALpars'] = lal.CreateDict()

    ma = lalsim.SimInspiralCreateModeArray()
    for l, m in modes:
        lalsim.SimInspiralModeArrayActivateMode(ma, l, m)
    lalsim.SimInspiralWaveformParamsInsertModeArray(p['LALpars'], ma)

    M = p['m1'] + p['m2']
    p.update({'m1': p['m1']*lal.MSUN_SI})
    p.update({'m2': p['m2']*lal.MSUN_SI})

    hlms_lal = lalsim.SimInspiralChooseTDModes(**p)

    hlms = {}

    if p['approximant'] in [lalsim.SEOBNRv4P, lalsim.SEOBNRv4PHM]:
        if eob_all_ell_2_modes:
            modes = [[2, 2], [2, 1], [2, 0], [2, -1], [2, -2]]

    for l, m in modes:
        tmp = lalsim.SphHarmTimeSeriesGetMode(hlms_lal, l, m)
        if l == 2 and m == 2:
            length_22 = tmp.data.length
            dt_22 = tmp.deltaT
            epoch_22 = tmp.epoch
        hlms.update({(l, m): tmp.data.data})

    assert p['deltaT'] == dt_22, f"input deltaT = {p['deltaT']} does not match waveform dt = {dt_22}."

    t = np.arange(length_22) * dt_22 + np.float(epoch_22)

    return t, hlms


def peak_align_shift(x, amp, npts=1e6, dx_npts=10):
    """
    computes time shift to peak align at zero
    """
    peak_i = amp.argmax()
    iamp = IUS(x, amp)
    dx = x[1] - x[0]
    new_x = np.linspace(x[peak_i] - dx_npts*dx,
                        x[peak_i] + dx_npts*dx, int(npts))

    new_a = iamp(new_x)
    peak_i_new = new_a.argmax()
    tshift = new_x[peak_i_new]

    return tshift


def peak_align_interp(x, y, shift):
    """
    returns new y-data (on same x grid)
    with a peak closer to time = zero.
    """

    new_iy = IUS(x - shift, y)
    new_y = new_iy(x)
    return new_y


def td_amp_scale(mtot, distance):
    """
    mtot in solar masses
    distance in m
    M*G/c^2 * M_sun / dist
    """
    return mtot * lal.MRSUN_SI / distance


def fd_amp_scale(mtot, distance):
    """
    mtot in solar masses
    distance in m
    M*G/c^2 * M_sun / dist
    """
    return mtot * lal.MRSUN_SI * mtot * lal.MTSUN_SI / distance



class SingleModeNRWaveform(object):
    def __init__(self, nrfile, ell, mm, npts, t1=None, t2=None):

        self.nrfile = nrfile
        self.npts = npts
        self.t1=t1
        self.t2=t2

        self.get_lm_mode(self.nrfile, ell, mm, self.npts)


    def get_lm_mode(self, nrfile, ell, mm, npts):
        
        if '.h5' in nrfile:
            f = h5py.File(nrfile, 'r')

            self.eta = f.attrs['eta']
            # this try/except is here because the GTech waveforms
            # didn't put mass1, mass2 in their h5 files...
            # so I hope they correctly scaled to a total mass of 1...
            try:
                self.q = f.attrs['mass1']/f.attrs['mass2']
            except:
                self.q = phenom.q_from_eta(self.eta)
            

            amp_tmp = f['amp_l{0}_m{1}'.format(ell, mm)]
            amp_x = amp_tmp['X'][()]
            amp_y = amp_tmp['Y'][()]

            phase_tmp = f['phase_l{0}_m{1}'.format(ell, mm)]
            phase_x = phase_tmp['X'][()]
            phase_y = phase_tmp['Y'][()]

            f.close()
        else:
            # bam
            self.sim_dir = os.path.dirname(nrfile)
            self.bbh_file = glob.glob( os.path.join(self.sim_dir, '*.bbh') )[0]
            # strict=False because of DuplicateSectionError
            config = configparser.ConfigParser(strict=False)

            config.read(self.bbh_file)
            mass1_tmp = float(config['metadata']['mass1'])
            mass2_tmp = float(config['metadata']['mass2'])
            self.initial_sep = float(config['metadata']['initial-separation'])

            if mass1_tmp >= mass2_tmp:
                mass1 = mass1_tmp
                mass2 = mass2_tmp
            else:
                mass1 = mass2_tmp
                mass2 = mass1_tmp


            self.mass1 = mass1
            self.mass2 = mass2
            self.mtot = self.mass1 + self.mass2
            self.q = self.mass1 / self.mass2
            self.eta = self.mass1 * self.mass2 / (self.mtot)**2.
            
            times, re_hlm, im_hlm = np.loadtxt(nrfile, unpack=True)

            # SIGN CONVENTION HERE NOTE SURE WHAT IS CORRECT FOR BAM
            hlm = re_hlm - 1.j * im_hlm
            
            amp_x = times
            amp_y = np.abs(hlm)

            phase_x = times
            phase_y = np.unwrap(np.angle(hlm))
            
        # shift so that amp peak is at t=0 - will need to be more careful with HMs
        amp_peak_idx = amp_y.argmax()
        amp_peak_time = amp_x[amp_peak_idx]
        amp_x = amp_x - amp_peak_time
        phase_x = phase_x - amp_peak_time

        amp_i = IUS(amp_x, amp_y)
        phase_i = IUS(phase_x, phase_y)

        if self.t1 is None:
            self.t1 = max(amp_x[0], phase_x[0])
        if self.t2 is None:
            self.t2 = min(amp_x[-1], phase_x[-1])
            
        # t1,t2=-600,100

        common_times = np.linspace(self.t1, self.t2, npts)

        amplist = amp_i(common_times)
        phaselist = phase_i(common_times)

        self.times = common_times
        self.amp = amplist
        self.phi = phaselist
        # self.hlm["{0}, {1}".format(ell, mm)] = self.amp * np.exp(-1.j * self.phi)
        self.hlm = self.amp * np.exp(-1.j * self.phi)
        
    def resample_data(self, new_time_array):
        """
        new_time_array : numpy.array

        redefines the amp, phi and hlm attributes to be sampled on
        the new_time_array
        """
        amp_i = IUS(self.times, self.amp)
        phi_i = IUS(self.times, self.phi)

        self.npts = len(new_time_array)
        self.times = new_time_array
        self.amp = amp_i(new_time_array)
        self.phi = phi_i(new_time_array)
        self.hlm = self.amp * np.exp(-1.j * self.phi)
        
class NRStrain(object):
    """
    stores Psi4 data aligned such that the peak of Psi4 is at t=0
    """
    def __init__(self, nrfile, ell, mm, npts_time, t1, t2):
        self.nrfile = nrfile
        self.ell = ell
        self.mm = mm
        self.npts_time = npts_time
        self.t1 = t1
        self.t2 = t2
        
        self.nrdata = SingleModeNRWaveform(self.nrfile, self.ell, self.mm, self.npts_time, t1=self.t1, t2=self.t2)
    
        self.eta = self.nrdata.eta
        self.q = float("{:.2f}".format(self.nrdata.q))
        
        amp = self.nrdata.amp
        phase = self.nrdata.phi
        
        max_idx_amp = np.argmax(amp)
        time_shift = self.nrdata.times[max_idx_amp]
        
        new_times = self.nrdata.times - time_shift
        
        self.times = np.linspace(self.t1, self.t2, self.npts_time)
        self.amp = IUS(new_times, amp)(self.times)
        self.phase = IUS(new_times, phase)(self.times)
        
        self.hlm = self.amp * np.exp(-1.j * self.phase)

        eta, chi1z, chi2z = self.eta, 0., 0.
        self.fin_spin = phenom.remnant.FinalSpin0815(eta, chi1z, chi2z)
        self.fring = phenom.remnant.fring(eta, chi1z, chi2z, self.fin_spin)
        self.fdamp = phenom.remnant.fdamp(eta, chi1z, chi2z, self.fin_spin)
        self.final_mass = 1.0 - phenom.EradRational0815(eta, chi1z, chi2z)
        
class WaveformGeneration(object):
    def __init__(self, approximant=None, f_min=20, t_min=-2000, t_max=100, npts=1000, nrfile=None, q=None):
        self.approximant = approximant
        self.f_min = f_min
        self.t_min = t_min
        self.t_max = t_max
        self.npts = npts
        self.nrfile = nrfile
        self.q = q
        
        self.times = np.linspace(self.t_min, self.t_max, self.npts)
        
        if self.nrfile:
            self.label = self.nrfile.split('/')[-1].split('.h5')[0]
            self.nrstrain = NRStrain(self.nrfile, 2, 2, self.npts, t1=self.t_min, t2=self.t_max)
            self.q = self.nrstrain.q
            times = self.nrstrain.times
            amp = self.nrstrain.amp
            phase = self.nrstrain.phase
            
            
            tpeak = peak_align_shift(times, amp, npts=1e6, dx_npts=10)
            
            self.amp = IUS(times - tpeak, amp)(self.times)
            self.phase = IUS(times - tpeak, phase)(self.times)
            self.phase -= self.phase[0]
            self.freq = IUS(self.times, self.phase).derivative()(self.times)

            self.h22 = self.amp * np.exp(1.j * self.phase)
            self.Reh22 = np.real(self.h22)
            self.Imh22 = np.imag(self.h22)
            
            
        else:
            self.label = lalsim.GetStringFromApproximant(self.approximant)
            mtotal = 20
            m1, m2 = phenom.m1_m2_M_q(mtotal, self.q)
            params = gen_td_wf_params(
                m1=m1,
                m2=m2,
                approximant=self.approximant,
                f_min=self.f_min
            )
            times, amp, phase = gen_td_wf(params, t_min=None, t_max=None)
            
            tpeak = peak_align_shift(times, amp, npts=1e6, dx_npts=10)
            
            self.amp = IUS(times-tpeak, amp)(self.times) / td_amp_scale(mtotal, 1) / lal.SpinWeightedSphericalHarmonic(0,0,-2,2,2).real
            self.phase = IUS(times-tpeak, phase)(self.times)
            self.phase -= self.phase[0]
            self.freq = IUS(self.times, self.phase).derivative()(self.times)

            self.h22 = self.amp * np.exp(1.j * self.phase)
            self.Reh22 = np.real(self.h22)
            self.Imh22 = np.imag(self.h22)