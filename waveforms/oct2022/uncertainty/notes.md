# introduction

project: self-aware semi-parametric waveform modelling

the main idea is to take into account the NR uncertainty in waveform modelling

semi-parametric: we use phenomenological ansatz (parametric) and the use
GPR (non-parametric) to model the model parameters over the parameter space.
(To map from physical to model parameters).
We use a linear model so that we can trivially use the collocation-point method
which allows us to express the model parameters as values of the waveforms
themselves at particular points in time.
This is important because it permits a highly interpretable explanation
of the uncertainty in model parameters.

semi-parametric modelling is encouraging as it can be used to uplift
both non-parametric and parametric modelling by addressing some of the
main issues with both modelling approaches and can be though of as an
application of scientific machine learning (SciML). SciML is a discipline
which adds extra structure to standard machine learning due to an
understanding of underlying physical process. This is can greatly reduce
the size of your training dataset and has been exploited by
physical modellers in the past as standard, however, it now goes by a new name.

## thigns to mention

 - the model knows how certain it is. we can thus trivially know where in
parameter space we should place new NR simulations (ref Doctor)
 - we might be able to make it differentiable so that we can use it with HMC
 - we can marginalise over waveform uncertainty just by sampling the model








