# dirs

- `scratch_2`: used to produce paper results as of June 2023
- `scratch_2_mod`: same as `scratch_2` but we change some GPR settings to test impact on calibration e.g. normalise_y, different kernel, different alpha
- `scratch_3`: new dir to experiment and try to improve uncertainty model
