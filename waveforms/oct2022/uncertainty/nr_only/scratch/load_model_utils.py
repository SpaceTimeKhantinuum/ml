



# https://stackoverflow.com/a/31365136/12840171
# need to use cl
import cloudpickle

import os
import glob

import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as IUS

import lal
import pycbc.types

"""
load custom module
"""
os.sys.path.append("/Users/sebastian.khan/personal/git/stk/ml/waveforms/oct2022/uncertainty")
import collocation as col
import utils

# custom pn module for inspiral
import pn

import phenom



def _load_samples_model(model_dir):
    """
    returns a dictionary of models. These contain the
    full GPR models.
    this is used when sampling from the model.
    """
    # this loads the "models" pickle files
    # these are the objects that are ready to be called to generate samples for the model
    # model_dir = "model_050123"
    # model_dir = "model_100123"
    model_filenames = glob.glob(os.path.join(model_dir, "models*.cpkl"))
    # print(model_filenames)

    models = {}
    for model_filename in model_filenames:
        # print(f"loading model: {model_filename}")
        k = model_filename.split("/")[-1].split(".")[0].split("models_")[-1]
        # print(f"k: {k}")
        with open(model_filename, 'rb') as my_file:
            models[k] = cloudpickle.load(my_file)
            
    return models

def _load_mean_model(model_dir):
    """
    returns a dictionary of models. These contain the GPR models
    to make the mean predictions.
    """
    # this loads the "mean_models" pickle files
    # these are the objects that are ready to be called to generate the mean prediction of the model

    # model_dir = "model_050123"
    # model_dir = "model_100123"
    model_filenames = glob.glob(os.path.join(model_dir, "mean_models*.cpkl"))
    # print(model_filenames)

    mean_models = {}
    for model_filename in model_filenames:
        # print(f"loading model: {model_filename}")
        k = model_filename.split("/")[-1].split(".")[0].split("mean_models_")[-1]
        # print(f"k: {k}")
        with open(model_filename, 'rb') as my_file:
            mean_models[k] = cloudpickle.load(my_file)
            
    return mean_models

def load_fitted_model(model_dir="model_100123"):
    """
    function to load offline model components
    """
    mean_models = _load_mean_model(model_dir)
    samples_models = _load_samples_model(model_dir)
    
    output = {
        'mean_models': mean_models,
        'samples_models': samples_models,
    }
    
    return output


##
# custom model specific classes/functions
##

def get_fdamp_from_q(q):
    """
    estimate ringdown damping frequency from mass-ratio `q`.
    """
    eta = phenom.eta_from_q(q)
    fin_spin = phenom.remnant.FinalSpin0815(eta, 0, 0)
    fdamp = phenom.remnant.fdamp(eta, 0, 0, fin_spin)
    return fdamp

def get_fring_from_q(q):
    """
    estimate ringdown frequency from mass-ratio `q`.
    """
    eta = phenom.eta_from_q(q)
    fin_spin = phenom.remnant.FinalSpin0815(eta, 0, 0)
    fring = phenom.remnant.fring(eta, 0, 0, fin_spin)
    return fring

# class ParameterSpaceFits(object):
#     """
#     collect together the results from the individual fits and then fit them using GPR
#     """
#     def __init__(self, wffs):
        
#         self.wffs = wffs
        
#         # the independent variable
#         # if it were aligned spin it would be a (N, 3) matrix
#         # where N is the number of waveforms and the columns
#         # would be ['q', 'chi1', 'chi2']
#         self.xs = np.array([wff.waveform.q for wff in self.wffs])
        
#         # useful for setting length of loops
#         self.num_collocation_points = {}
#         for d in self.wffs[0].collocation_points.keys():
#             self.num_collocation_points[d] = len(self.wffs[0].collocation_points[d])
            
#         # ys: stores the targets for parameter space fits.
#         # it has the same structure as rhs.
#         # i.e. the keys are integers and correspond to the
#         # derivative order
#         # for each key the values are a list of length equal
#         # to the number of collocation points
#         ys = {}
#         # signs: records the sign i.e. -ve or +ve for each data point
#         # we need this is we use the log(abs()) transformation to be able
#         # to recover the original sign of the data.
#         signs = {}
#         # loop over derivatives
#         for d in self.wffs[0].cm.rhs.keys():
#             ys[d] = []
#             signs[d] = []
#             # loop over collocation points
#             for c in range(self.num_collocation_points[d]):
#                 t = np.array([wff.cm.rhs[d][c] for wff in self.wffs])
#                 ys[d].append(t)
#                 s = [np.sign(t_) for t_ in t][0] # assumption: take sign from first waveform
#                 signs[d].append(s)
#         self.ys = ys
#         self.signs = signs
        
#     def compute_alphas(self):
#         """
#         sets the self.alphas attribute
#         use this when you have multiple observations i.e. multiple mass-ratio x simulations
#         for each collocation point we compute the standard deviation over observations
#         and use this as a proxy for the uncertainty in that data point.
#         for data points where we only have one observation we assume that their uncertainty
#         is given by the median value of the distribution of standard deviations
#         """
#         alphas = {}
#         for d in self.wffs[0].cm.rhs.keys():
#             alphas[d] = []
#             for c in range(self.num_collocation_points[d]):
#                 # create a dataframe so we can group by easily
#                 df=pd.DataFrame({'x':self.xs, 'y':self.ys[d][c]})
#                 # group by mass-ratio and compute standard deviation
#                 df2=df.groupby(by='x').std()
#                 df2=df2.rename(columns={'y':'alpha'})
#                 # remove nans (these are the cases were only one simulation exists)
#                 # and compute the median value of the distribution of stds
#                 median_std = np.median(df2['alpha'].values[~np.isnan(df2['alpha'].values)])

#                 # impute nans with median value
#                 # join back onto original dataframe so that we have a value of alpha
#                 # for every data point
#                 df3=pd.merge(df, df2.fillna(median_std), left_on='x', right_index=True).sort_index()
#                 alphas[d].append(df3['alpha'].values)
#         self.alphas = alphas
        
#     def fit(self, GaussianProcessRegressor_kwargs, log_abs_transform=True, use_estimate_alpha=True):
#         """
#         use_estimate_alpha: if this is true then will use an estimate for alpha based on the data
#         """
#         if use_estimate_alpha == True:
#             assert hasattr(self, 'alphas'), "attribute alphas not set"
#         GaussianProcessRegressor_kwargs = GaussianProcessRegressor_kwargs.copy()
#         self.log_abs_transform = log_abs_transform
#         # loop of self.ys and build a fit
#         yhats = {}
#         gprs = {}
#         # loop over derivatives
#         for d in self.ys.keys():
#             # loop over collocation points
#             yhats[d] = []
#             gprs[d] = []
#             for c in range(self.num_collocation_points[d]):
#                 x = self.xs
#                 y = self.ys[d][c]
#                 if self.log_abs_transform:
#                     y = np.log(np.abs(y))
#                 if use_estimate_alpha == True:
#                     GaussianProcessRegressor_kwargs.update({'alpha':self.alphas[d][c]})
#                 gpr = GaussianProcessRegressor(**GaussianProcessRegressor_kwargs).fit(x[:,np.newaxis], y)
#                 gprs[d].append(gpr)
                
#         self.gprs = gprs
                
#     def predict_sample(self, x, num_samples, random_state=1):
#         mus = {}
#         for d in self.gprs.keys():
#             mus[d] = []
#             for c in range(self.num_collocation_points[d]):
#                 mu = self.gprs[d][c].sample_y(x, num_samples, random_state=random_state)
#                 if self.log_abs_transform:
#                     s = np.array(self.signs[d][c])
#                     mu = s * np.exp(mu)
#                 mus[d].append(mu.T)
#         return mus
    
#     def predict_mean(self, x):
#         mus = {}
#         for d in self.gprs.keys():
#             mus[d] = []
#             for c in range(self.num_collocation_points[d]):
#                 mu = self.gprs[d][c].predict(x)
#                 if self.log_abs_transform:
#                     s = np.array(self.signs[d][c])
#                     mu = s * np.exp(mu)
#                 # we add a new axis in first position so that we get a shape (1, n) so that it is consistent with 
#                 # how we treat many samples which a shape (n_samples, n)
#                 mus[d].append(mu[np.newaxis, :])
#                 # mus[d].append([mu])
#                 # mus[d].append(mu)
#         return mus



def rd_ansatz_log(t, a, b):
    return np.log(a) - t*b

def connect_rd(t0, y0, fdamp):
    """
    y0 == y(t0)
    this is just fixing the constant offset between the phenom model from peak amplitude
    to about t0=30M.
    We assume the slope is given by the approximately known damping frequency
    We assume exponential decay so it's linear in log-space
    """
    b = fdamp*2*np.pi
    log_a = np.log(y0) + b * t0
    return np.exp(log_a), b

    
def model_full_amp(q, t, inspiral_model, merger_model, early_ringdown_model, t_ins=-300, t_m_erd=0, t_erd_end=30, random_state=None, n_samples=1, Tc=1000):
# def model_full_amp(q, t, inspiral_model, merger_model, early_ringdown_model, t_ins=-100, t_m_erd=0, t_erd_end=30, random_state=None, n_samples=1):
    if random_state == None:
        random_state = np.random.randint(0, 1000000)
        
    eta=phenom.eta_from_q(q)
    fdamp = get_fdamp_from_q(q)
        
    y = np.zeros(shape=(n_samples, len(t)))
    
    mask = t < t_ins
    inspiral_times = t[mask]

    mask = (t >= t_ins) & (t < t_m_erd)
    merger_times = t[mask]

    mask = (t >= t_m_erd) & (t < t_erd_end)
    ringdown_times = t[mask]
    
    mask = t >= t_erd_end
    late_ringdown_times = t[mask]

    # compute all inspirals
    pn_kwargs=dict(t=inspiral_times, tc=Tc, eta=eta, M=1)
    pn_residual = np.abs(pn.Hhat22_T3(**pn_kwargs))
    # pn_residual = np.zeros_like(inspiral_times)
    y_ins = inspiral_model(q=q, t=inspiral_times, random_state=random_state, pn_residual=pn_residual, n_samples=n_samples) 
    
    # compute all mergers
    y_merger = merger_model(q=q, t=merger_times, random_state=random_state, n_samples=n_samples)
    
    # compute all early ringdowns
    y_early_ringdown = early_ringdown_model(q=q, t=ringdown_times, random_state=random_state, n_samples=n_samples)
    
    # compute all late ringdowns
    # note that the `b`'s only depend on mass-ratio
    # so we only have one of them and not n_samples of them
    
    # this is the value of the final point
    # which we use to connect the exponential ringdown
    y0 = early_ringdown_model(q=q, t=t_erd_end, random_state=random_state, n_samples=n_samples)
    y0 = y0[np.newaxis, :]
    
    a, b = connect_rd(t0=t_erd_end, y0=y0.T, fdamp=fdamp)

    y_late_ringdown = np.exp(rd_ansatz_log(late_ringdown_times, a, b))
    
    amps = np.concatenate((y_ins, y_merger, y_early_ringdown, y_late_ringdown), axis=1)
    
    amps = eta * amps
    
    return amps


def model_full_freq(q, t, inspiral_model, merger_model, ringdown_model, t_ins=-300, t_m_erd=0, random_state=None, n_samples=1, Tc=1000):
    """
    n_samples is only used when you specify that you want to sample the model and the output shape is going to be (n_samples, n_time)
    if you want to compute the mean model (i.e. use the mean from the GP) then n_samples should be left at 1 and the output will be (1, n_time)
    """
    if random_state == None:
        random_state = np.random.randint(0, 1000000)
        
    y = np.zeros(shape=(n_samples, len(t)))
    
    mask = t < t_ins
    inspiral_times = t[mask]

    mask = (t >= t_ins) & (t < t_m_erd)
    merger_times = t[mask]

    mask = t >= t_m_erd
    ringdown_times = t[mask]

    # compute all inspirals
    pn_kwargs=dict(t=inspiral_times, tc=Tc, eta=phenom.eta_from_q(q), M=1)
    pn_residual = pn.TaylorT3_Omega_new(**pn_kwargs)
    # pn_residual = np.zeros_like(inspiral_times)
    y_ins = inspiral_model(q=q, t=inspiral_times, random_state=random_state, pn_residual=pn_residual, n_samples=n_samples) 

    y_merger = merger_model(q=q, t=merger_times, random_state=random_state, n_samples=n_samples)

    y_ringdown = ringdown_model(q=q, t=ringdown_times, random_state=random_state, n_samples=n_samples)

    return np.concatenate((y_ins, y_merger, y_ringdown), axis=1)


class PhenProb(object):
    def __init__(self, model_dir):
        self.model_dict = load_fitted_model(model_dir)
        
    def generate_amplitude(self, q, t, n_samples=-1, random_state=None):
        """
        n_samples: if -1 then will use the mean, otherwise generates samples
        """
        
        if n_samples == -1:
            inspiral_model=self.model_dict['mean_models']['amp_inspiral']
            merger_model=self.model_dict['mean_models']['amp_merger']
            early_ringdown_model=self.model_dict['mean_models']['amp_ringdown']
            n_samples = 1
        else:
            inspiral_model=self.model_dict['samples_models']['amp_inspiral']
            merger_model=self.model_dict['samples_models']['amp_merger']
            early_ringdown_model=self.model_dict['samples_models']['amp_ringdown']
        
        yhats = model_full_amp(
            q,
            t,
            inspiral_model=inspiral_model,
            merger_model=merger_model,
            early_ringdown_model=early_ringdown_model,
            random_state=random_state,
            n_samples=n_samples,
        )
        
        return yhats
    
    
    def generate_frequency(self, q, t, n_samples=-1, random_state=None):
        """
        n_samples: if -1 then will use the mean, otherwise generates samples
        """
        
        if n_samples == -1:
            inspiral_model=self.model_dict['mean_models']['freq_inspiral']
            merger_model=self.model_dict['mean_models']['freq_merger']
            ringdown_model=self.model_dict['mean_models']['freq_ringdown']
            n_samples = 1
        else:
            inspiral_model=self.model_dict['samples_models']['freq_inspiral']
            merger_model=self.model_dict['samples_models']['freq_merger']
            ringdown_model=self.model_dict['samples_models']['freq_ringdown']
        
        yhats = model_full_freq(
            q,
            t,
            inspiral_model=inspiral_model,
            merger_model=merger_model,
            ringdown_model=ringdown_model,
            random_state=random_state,
            n_samples=n_samples,
        )
        
        return yhats
    
    
    def generate_phase(self, q, t, n_samples=-1, random_state=None):
        """
        n_samples: if -1 then will use the mean, otherwise generates samples
        """
        
        freqs = self.generate_frequency(q, t, n_samples, random_state)
        ifreqs = [IUS(t, f) for f in freqs]
        phis = np.array([ifreq.antiderivative()(t) for ifreq in ifreqs])
        return phis
        
        
    def generate_h22(self, q, t, n_samples=-1, random_state=None):
        """
        n_samples: if -1 then will use the mean, otherwise generates samples
        """
        
        amps = self.generate_amplitude(q, t, n_samples, random_state)
        phis = self.generate_phase(q, t, n_samples, random_state)
        
        # check sign
        h22s = amps * np.exp(1.j * phis)
        
        return h22s
    
    def generate_pycbc_hp_hc(self, q, times, M, delta_t, distance=1e6 * lal.PC_SI, n_samples=-1, random_state=None, theta=0, phi=0):
        """
        n_samples: if -1 then will use the mean, otherwise generates samples
        
        q: mass-ratio >= 1
        times: times in units of M
        M: in units of Msun
        delta_t: output delta_t in seconds
        distance: in metres
        """
        dt_M = times[1] - times[0]
        times_s = phenom.MtoS(times, M)
        
        epoch = phenom.MtoS(times[0], M)
        
        h22 = self.generate_h22(q, times, n_samples, random_state)
        
        h22 *= utils.td_amp_scale(M, distance)
        h22 *= lal.SpinWeightedSphericalHarmonic(theta, phi, -2, 2, 2)
        
        hp = h22.real
        hc = h22.imag

        t0 = phenom.MtoS(times[0], M)
        t1 = phenom.MtoS(times[-1], M)
        new_times = np.arange(t0, t1, delta_t)
        
        hp = [IUS(times_s, hp_)(new_times) for hp_ in hp]
        hc = [IUS(times_s, hc_)(new_times) for hc_ in hc]
        hp = [pycbc.types.TimeSeries(hp_, delta_t=delta_t, epoch=epoch) for hp_ in hp]
        hc = [pycbc.types.TimeSeries(hc_, delta_t=delta_t, epoch=epoch) for hc_ in hc]
        
        return hp, hc
    
    