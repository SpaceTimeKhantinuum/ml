import os
import numpy as np
import pandas as pd
from scipy.interpolate import InterpolatedUnivariateSpline as IUS

import scipy.integrate
import scipy.optimize

from functools import partial

import phenom

import pycbc.types
import pycbc.psd
import pycbc.filter
import lal

# custom module
os.sys.path.append("/Users/sebastian.khan/personal/git/stk/ml/waveforms/oct2022/uncertainty")
import utils

def load_nr_data(catalogue_dir="/Users/sebastian.khan/personal/data/non_spinning_catalogue_dec_2022", names_to_drop=['RIT-BBH-0957-n084']):
    
    df = pd.read_csv(os.path.join(catalogue_dir, 'metadata.csv'))
    times = np.load(os.path.join(catalogue_dir, 'times.npy'))
    strains = np.load(os.path.join(catalogue_dir, 'strains.npy'))
    
    if names_to_drop != None:
        idxs_to_keep = df[~df['name'].isin(names_to_drop)].index
        strains = strains[idxs_to_keep]
        df = df.iloc[idxs_to_keep].copy().reset_index(drop=True)

    amps=np.array([np.abs(strain) for strain in strains])
    phases=np.array([np.unwrap(np.angle(strain)) for strain in strains])
    freqs=np.array([IUS(times, phase).derivative()(times) for phase in phases])
    
    
    # compute start frequency of NR data at 100 Msun
    nr_start_freq = phenom.StoM(freqs[:,0]/(2*np.pi), 100)
    df['start_freq_100Msun'] = nr_start_freq
    
    
    output = {
        'df':df,
        'times':times,
        'strains':strains,
        'amps':amps,
        'phases':phases,
        'freqs':freqs,
    }
    
    return output


def get_nr_strain(q, strain, times, M, delta_t, distance=1e6 * lal.PC_SI, theta=0, phi=0, t_nr_start=-600, t_nr_end=80):
    """
    t_nr_start=-700
    t_nr_end=80
    These are the start and end times of the NR data.
    For times less than this the model was not trained.
    For times greater than this the NR is not accurate.
    """
    eta = phenom.eta_from_q(q)
    dt_M = times[1] - times[0]
    
    mask = (times > t_nr_start) & (times < t_nr_end)
    times = times[mask]
    strain = strain[mask]
    
    times_s = phenom.MtoS(times, M)
    # delta_t = phenom.MtoS(dt_M, M)
    
    epoch = phenom.MtoS(times[0], M)
    h = strain * utils.td_amp_scale(M, distance)*lal.SpinWeightedSphericalHarmonic(theta,phi,-2,2,2)
    hp = h.real
    hc = h.imag
    
    t0 = phenom.MtoS(times[0], M)
    t1 = phenom.MtoS(times[-1], M)
    new_times = np.arange(t0, t1, delta_t)
    hp = IUS(times_s, hp)(new_times)
    hc = IUS(times_s, hc)(new_times)
    
    hp = pycbc.types.TimeSeries(hp, delta_t=delta_t, epoch=epoch)
    hc = pycbc.types.TimeSeries(hc, delta_t=delta_t, epoch=epoch)
    return hp, hc


def my_coalign_waveforms(hp1, hc1, hp2, hc2, M,
                         left_trim_M=50,
                         end_time_M=80,
                         psd=None,
                         low_frequency_cutoff=None,
                         high_frequency_cutoff=None,
                         resize=True,
                         resize_buffer_factor=2,
                        ):
    """
    https://github.com/gwastro/pycbc/blob/master/pycbc/waveform/utils.py#L44
    Generalised to work with hp and hc.
    
    M: total mass in Msun
    left_trim_M: number in M to crop off the start
    end_time_M: last time in M to crop to
    resize_buffer_factor: pad right with zeros proportional to this to reduce fft rotation artifacts
    """
    from pycbc.filter import matched_filter
    mlen = pycbc.waveform.utils.ceilpow2(max(len(hp1), len(hp2)))
    mlen = mlen * resize_buffer_factor

    hp1 = hp1.copy()
    hp2 = hp2.copy()
    
    hc1 = hc1.copy()
    hc2 = hc2.copy()

    if resize:
        hp1.resize(mlen)
        hp2.resize(mlen)
        hc1.resize(mlen)
        hc2.resize(mlen)
    elif len(hp1) != len(hp2) or len(hp2) % 2 != 0:
        raise ValueError("Time series must be the same size and even if you do "
                         "not allow resizing")
        
    snr = matched_filter(hp1, hp2, psd=psd,
                         low_frequency_cutoff=low_frequency_cutoff,
                         high_frequency_cutoff=high_frequency_cutoff)

    _, l =  snr.abs_max_loc()
    rotation =  snr[l] / abs(snr[l])
    hp1 = (hp1.to_frequencyseries() * rotation).to_timeseries()
    hp1.roll(l)
    
    hc1 = (hc1.to_frequencyseries() * rotation).to_timeseries()
    hc1.roll(l)

    hp1 = pycbc.types.TimeSeries(hp1, delta_t=hp2.delta_t, epoch=hp2.start_time)
    hc1 = pycbc.types.TimeSeries(hc1, delta_t=hc2.delta_t, epoch=hc2.start_time)
    
    # drop first 200M
    left_trim = phenom.MtoS(left_trim_M, M)
    # right_trim = hp1.sample_times[-1] - 0.03
    # right_trim = hp1.sample_times[-1] - phenom.MtoS(60, 100)
    right_trim = hp1.sample_times[-1] - phenom.MtoS(end_time_M, M)
    to_trim = [left_trim, right_trim]
    
    return hp1.crop(*to_trim), hc1.crop(*to_trim), hp2.crop(*to_trim), hc2.crop(*to_trim)

    # return hp1, hc1, hp2, hc2


def compute_match(h1, h2, f_low, psd=None):
    # Resize the waveforms to the same length
    tlen = max(len(h1), len(h2))
    h1.resize(tlen)
    h2.resize(tlen)

    # Generate the aLIGO ZDHP PSD
    if psd == 'zdhp':
        delta_f = 1.0 / h1.duration
        flen = tlen//2 + 1
        psd = pycbc.psd.aLIGOZeroDetHighPower(flen, delta_f, f_low)

    # Note: This takes a while the first time as an FFT plan is generated
    # subsequent calls are much faster.
    m, _ = pycbc.filter.match(h1, h2, psd=psd, low_frequency_cutoff=f_low)
    # m = pycbc.filter.overlap(h1, h2, psd=psd, low_frequency_cutoff=f_low)
    return m