"""
based on tinygp tutorial: https://tinygp.readthedocs.io/en/stable/tutorials/quickstart.html
"""
import jax
import jax.numpy as jnp
from tinygp import kernels, GaussianProcess
import numpy as np
import jaxopt

# jax.config.update("jax_enable_x64", True)


def build_gp(theta, X):
    amp = jnp.exp(2 * theta["log_amp"])
    scale = jnp.exp(theta["log_scale"])
    diag = jnp.exp(theta["log_diag"])
    kernel = amp * kernels.Matern32(scale=scale)
    return GaussianProcess(
        kernel, X, diag=diag, mean=theta["mean"]
    )

def build_neg_log_likelihood(build_gp_fn):
    def neg_log_likelihood(theta, X, y):
        gp = build_gp_fn(theta, X)
        return -gp.log_probability(y)
    return neg_log_likelihood


def fit(X, y, loss_fun, init):
    solver = jaxopt.ScipyMinimize(fun=loss_fun)
    soln = solver.run(init, X=X, y=y)
    # print(f"Final negative log likelihood: {soln.state.fun_val}")
    return soln


def build_predict_func(build_gp_fn, theta, X_train, y_train):
    """
    X: inference locations
    theta: gp parameters
    """
    gp = build_gp_fn(theta, X_train)
    def predict(X):
        """returns loc and std"""
        cond_gp = gp.condition(y_train, X, diag=np.exp(theta['log_diag'])).gp
        return cond_gp.loc, np.sqrt(cond_gp.variance)
    return predict


class GPRModel(object):
    def __init__(self, build_gp_fn, theta_init):
        self.build_gp_fn = build_gp_fn
        self.theta_init = theta_init
        self.loss_fun = build_neg_log_likelihood(self.build_gp_fn)


    def fit(self, X_train, y_train):
        self.X_train = X_train
        self.y_train = y_train
        self.soln = fit(self.X_train, self.y_train, self.loss_fun, self.theta_init)
        self.theta = self.soln.params

        # generate the predict function
        self.predict_func = build_predict_func(build_gp, self.theta, self.X_train, self.y_train)

    def predict(self, X):
        return self.predict_func(X)