# redo

This directory is a refactoring of the probabilistic phenomenological workflow
to make the work more reproducible.

## Workflow

1. data preparation
    - process raw data
        - data_prepartion/prepare_bam_data.ipynb
        - data_prepartion/prepare_non_bam_data.ipynb
    - aggregate processed nr data
        - data_prepartion/nr_catalogue.ipynb

Run the above notebooks in order.
The output of `data_prepartion/nr_catalogue.ipynb` is

- strains.npy
- times.npy
- metadata.csv

The data has been aligned and resampled to the same time grid (hence the one times.npy file).
The data is read to be modelled.

2. data modelling

We split the data into amplitude and frequency and model.
The amplitude inspiral model depends on the frequency inspiral *model*.
This is because TaylorT3 can show pathological behaviour at late times for high mass-ratios.
We can somewhat correct this by using our *model*.

The modelling notebooks can therefore be run in any order with the exception of
inspiral-freq-model.ipynb which must be run *before* inspiral-amp-model.ipynb.

## Package - April 2024

Putting together a quick python package for people to use the model

Implemented in [chirpy](https://gitlab.com/SpaceTimeKhantinuum/chirpy)
