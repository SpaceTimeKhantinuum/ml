import ffi_utils

import numpy as np
import h5py
import configparser

from scipy.interpolate import InterpolatedUnivariateSpline as IUS

import phenom

class NRLoader(object):
    def __init__(self, handler, nr_hdf5_filename=None, psi4_filename=None, metadata_filename=None, safety_factor=0.5, ell=None, mm=None, dt=0.1, psi4_t1=None, psi4_t2=None):
        """
        code: e.g. 'nr_hdf5', 'bam'

        nr_hdf5 kwargs
         - nr_hdf5_filename
         - ell
         - mm

        bam kwargs
         - psi4_filename
         - metadata_filename
         - safety_factor: for ffi
         - psi4_t1, psi4_t2. used to mask the psi4 data between these times
        """
        self.handler = handler
        self.nr_hdf5_filename = nr_hdf5_filename
        self.psi4_filename = psi4_filename
        self.metadata_filename = metadata_filename
        self.safety_factor = safety_factor
        self.ell = ell
        self.mm = mm
        self.dt = dt
        self.psi4_t1 = psi4_t1
        self.psi4_t2 = psi4_t2

        if self.handler == 'bam':
            assert self.psi4_filename != None, "expecting psi4_filename"
            assert self.metadata_filename != None, "expecting metadata_filename"
            self.get_bam_strain_from_psi4(self.psi4_filename, self.metadata_filename, safety_factor=self.safety_factor, dt=self.dt, psi4_t1=self.psi4_t1, psi4_t2=self.psi4_t2)
        elif self.handler == 'nr_hdf5':
            assert self.nr_hdf5_filename != None, "expecting nr_hdf5_filename"
            assert self.ell != None, "expecting ell"
            assert self.mm != None, "expecting mm"
            self.get_hdf5_strain(nr_hdf5_filename=self.nr_hdf5_filename, ell=self.ell, mm=self.mm, dt=self.dt)

    def get_bam_strain_from_psi4(self, psi4_filename, bbh_metadata_filename, safety_factor, dt, psi4_t1=None, psi4_t2=None):
        # cut off frequency for FFI
        self.f0 = ffi_utils.getCutoffFrequencyFromTwoPuncturesBBH(meta_filename=bbh_metadata_filename, safety_factor=safety_factor)

        times, re_psi4, im_psi4 = np.loadtxt(psi4_filename, unpack=True)

        if psi4_t1 == None:
            psi4_t1 = times[0]
        if psi4_t2 == None:
            psi4_t2 = times[-1]


        mask = (times >= psi4_t1) & (times <= psi4_t2)
        times = times[mask]
        re_psi4 = re_psi4[mask]
        im_psi4 = im_psi4[mask]

        psi4_3col = np.column_stack((times, re_psi4, im_psi4))

        strain = ffi_utils.psi4ToStrain(psi4_3col, self.f0)
        strain = strain[:,1].conj()
        # strain_2col = strain

        re_strain = strain.real
        im_strain = strain.imag

        i_re_strain = IUS(times, re_strain)
        i_im_strain = IUS(times, im_strain)

        times = np.arange(times[0], times[-1], dt)
        re_strain = i_re_strain(times)
        im_strain = i_im_strain(times)

        strain = re_strain - 1.j*im_strain

        self.times = times
        self.strain = strain


        config = configparser.ConfigParser(strict=False)
        config.read(bbh_metadata_filename)

        # spin1x = float(config['metadata']['initial-bh-spin1x'])
        # spin1y = float(config['metadata']['initial-bh-spin1y'])
        # spin1z = float(config['metadata']['initial-bh-spin1z'])
        # spin2x = float(config['metadata']['initial-bh-spin2x'])
        # spin2y = float(config['metadata']['initial-bh-spin2y'])
        # spin2z = float(config['metadata']['initial-bh-spin2z'])
        mass1_tmp = float(config['metadata']['mass1'])
        mass2_tmp = float(config['metadata']['mass2'])

        if mass1_tmp >= mass2_tmp:
            mass1 = mass1_tmp
            mass2 = mass2_tmp
        else:
            mass1 = mass2_tmp
            mass2 = mass1_tmp

        self.Mtotal = mass1+mass2
        self.q = mass1/mass2
        self.eta = phenom.eta_from_q(self.q)


    def get_hdf5_strain(self, nr_hdf5_filename, ell, mm, dt):
        f = h5py.File(nr_hdf5_filename, 'r')

        eta = f.attrs['eta']
        # this try/except is here because the GTech waveforms
        # use 'irreducible_mass1' instead of 'mass1'..
        try:
            q = f.attrs['mass1']/f.attrs['mass2']
            self.Mtotal = f.attrs['mass1']+f.attrs['mass2']
        except:
            q = f.attrs['irreducible_mass1']/f.attrs['irreducible_mass2']
            self.Mtotal = f.attrs['irreducible_mass1']+f.attrs['irreducible_mass2']

        self.q = q
        self.eta = eta
        
        amp_tmp = f['amp_l{0}_m{1}'.format(ell, mm)]
        amp_x = amp_tmp['X'][()]
        amp_y = amp_tmp['Y'][()]

        phase_tmp = f['phase_l{0}_m{1}'.format(ell, mm)]
        phase_x = phase_tmp['X'][()]
        phase_y = phase_tmp['Y'][()]

        f.close()

        amp_i = IUS(amp_x, amp_y)
        phase_i = IUS(phase_x, phase_y)

        t1 = max(amp_x[0], phase_x[0])
        t2 = min(amp_x[-1], phase_x[-1])
            
        # common_times = np.linspace(t1, t2, npts)
        common_times = np.arange(t1, t2, dt)

        amplist = amp_i(common_times)
        phaselist = phase_i(common_times)

        times = common_times
        amp = amplist
        phi = phaselist
        strain = amp * np.exp(-1.j * phi)

        self.times = times
        self.strain = strain
