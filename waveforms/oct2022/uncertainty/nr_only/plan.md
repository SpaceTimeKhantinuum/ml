# Introduction

project: include NR uncertainty in a waveform model

data: non-spinning NR waveforms (2,2) mode only from various NR codes

data-preparation: align wawveforms with $\Delta t$ and $\Delta \phi$ such that the peak of the (2,2) amplitude is zero and the phase is zero at the initial frequency.