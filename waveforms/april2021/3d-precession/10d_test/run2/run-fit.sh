#!/bin/bash

mkdir -p logs

echo "running config_001"
nohup python ../../fit_resnet.py -v --config-file config_001.toml > logs/config_001.out 2> logs/config_001.err &

echo "running config_002"
nohup python ../../fit_resnet.py -v --config-file config_002.toml > logs/config_002.out 2> logs/config_002.err &

echo "running config_003"
nohup python ../../fit_resnet.py -v --config-file config_003.toml > logs/config_003.out 2> logs/config_003.err &
