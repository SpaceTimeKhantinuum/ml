#!/bin/bash

mkdir -p logs

echo "running config_001"
#nohup python ../../fit_resnet_checkpoint.py -v --config-file config_001.toml > logs/config_001.out 2> logs/config_001.err &
nohup python ../../fit_resnet_checkpoint.py --force -v --config-file config_001.toml > logs/config_001_001.out 2> logs/config_001_001.err &
