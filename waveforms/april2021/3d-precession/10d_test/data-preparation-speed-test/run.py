# for 1e6 waveforms the new method needs about 200GB and 3-4mins to complete
# the old method would need something like an hour perhaps?
# saving the results to disk is only 82GB

#In [1]: import numpy as np
#
#In [2]: x = np.load("test_X.npy")
#
#In [3]: size_in_bytes = x.nbytes
#
#In [4]: size_in_bytes
#Out[4]: 87920000000
#
#In [5]: size_in_bytes/1e9
#Out[5]: 87.92

import numpy as np

import datetime

from sklearn.preprocessing import MinMaxScaler
import pickle

import datetime
import wispy.logger

import sys



sys.path.append('../../')
import compute_preprocessing

from compute_preprocessing import apply_pre_process_forward, apply_pre_process_reverse


def new_convert_input_for_ann_10D(
        times,
        q,
        chi1, theta1, phi1,
        chi2, theta2, phi2,
        inclination, phiRef
        ):
    times = times.reshape(-1,1) # for broadcasting
    X = []
    for _q, _chi1, _theta1, _phi1, _chi2, _theta2, _phi2, _inclination, _phiRef in zip(q, chi1, theta1, phi1, chi2, theta2, phi2, inclination, phiRef):
        sub_matrix = np.array([[_q, _chi1, _theta1, _phi1, _chi2, _theta2, _phi2, _inclination, _phiRef]]) * np.ones([times.shape[0],9])
        X.append(np.concatenate((times, sub_matrix), axis=1))
    X = np.asarray(X)
    return X.reshape(X.shape[0]*X.shape[1], X.shape[2])

def convert_input_for_ann_10D(
        times,
        q,
        chi1, theta1, phi1,
        chi2, theta2, phi2,
        inclination, phiRef
        ):
    X = []
    for _q, _chi1, _theta1, _phi1, _chi2, _theta2, _phi2, _inclination, _phiRef in zip(q, chi1, theta1, phi1, chi2, theta2, phi2, inclination, phiRef):
        for t in times:
            X.append([t, _q, _chi1, _theta1, _phi1, _chi2, _theta2, _phi2, _inclination, _phiRef])
    X = np.asarray(X)
    return X

def new_convert_input_for_ann(
        times,
        q,
        chi1, theta1
        ):
    times = times.reshape(-1,1) # for broadcasting
    X = []
    for _q, _chi1, _theta1 in zip(q, chi1, theta1):
        sub_matrix = np.array([[_q, _chi1, _theta1]]) * np.ones([times.shape[0],3])
        X.append(np.concatenate((times, sub_matrix), axis=1))
    X = np.asarray(X)
    return X.reshape(X.shape[0]*X.shape[1], X.shape[2])

def convert_input_for_ann(times, q, chi1, theta1):
    X = []
    for _q, _chi1, _theta1 in zip(q, chi1, theta1):
        for t in times:
            X.append([t, _q, _chi1, _theta1])
    X = np.asarray(X)
    return X


if __name__ == "__main__":

    logger = wispy.logger.init_logger(level=2)
    
#     doc = {
#         'data':{
#             'coords_train':'../training_data_1e3/coords.npy',
#             'times':'../training_data_1e3/times.npy',
#             'times_scaler':'../pre_processing_1e3/times_scaler.pkl',
#             'y_preprocessing_params':'../pre_processing_1e3/amp_preprocessing_params.npz',
#             'y_train':'../training_data_1e3/amplitude.npy'
#         }
#     }

#     doc = {
#         'data':{
#             'coords_train':'../training_data_1e5/coords.npy',
#             'times':'../training_data_1e5/times.npy',
#             'times_scaler':'../pre_processing_1e5/times_scaler.pkl',
#             'y_preprocessing_params':'../pre_processing_1e5/amp_preprocessing_params.npz',
#             'y_train':'../training_data_1e5/amplitude.npy'
#         }
#     }

    doc = {
        'data':{
            'coords_train':'../training_data_1e6/coords.npy',
            'times':'../training_data_1e6/times.npy',
            'times_scaler':'../pre_processing_1e6/times_scaler.pkl',
            'y_preprocessing_params':'../pre_processing_1e6/amp_preprocessing_params.npz',
            'y_train':'../training_data_1e6/amplitude.npy'
        }
    }
    
    # load coords
    filename = doc['data']['coords_train']
    logger.info(f"loading coords file: {filename}")
    coords_train = np.load(filename)

    # load times data
    filename = doc['data']['times']
    logger.info(f"loading times file: {filename}")
    times = np.load(filename)

    pkl_filename = doc['data']['times_scaler']
    with open(pkl_filename, 'rb') as file:
        times_scaler = pickle.load(file)

    logger.info("transforming times data")
    times_scaled = times_scaler.transform(times[:, np.newaxis])[:, 0]

    filename = doc['data']['y_train']
    logger.info(f"loading y-train file: {filename}")
    y_train_raw = np.load(filename)

    filename = doc['data']['y_preprocessing_params']
    logger.info(f"loading y-preprocessing-params file: {filename}")
    y_processing_params = np.load(filename)


    logger.info("applying preprocessing to raw training data")
    y_train = apply_pre_process_forward(y_train_raw, y_processing_params['mean'], y_processing_params['max'])

    del y_train_raw


#     if coords_train.shape[0] == 9:
#         logger.info("building training set input for 10 dimensions")
#         start_time = datetime.datetime.now()
#         X_train = convert_input_for_ann_10D(
#                     times_scaled,
#                     coords_train[0], coords_train[1], coords_train[2],
#                     coords_train[3], coords_train[4], coords_train[5],
#                     coords_train[6], coords_train[7], coords_train[8]
#                     )
#         end_time = datetime.datetime.now()
#         duration = end_time-start_time
#         print(f"duration: {duration}")
#     elif coords_train.shape[0] == 3:
#         logger.info("building training set input for 4 dimensions")
#         X_train = convert_input_for_ann(times_scaled, coords_train[0], coords_train[1], coords_train[2])
#     else:
#         logger.error("coords_train.shape error: only 3 or 9 supported currently")
#         logger.error("Exiting")
#         sys.exit(1)

    logger.info("running y_train.ravel()")
    y_train = y_train.ravel().reshape(-1, 1)
    
#     logger.info(f"X_train.shape: {X_train.shape}")
    logger.info(f"y_train.shape: {y_train.shape}")

    
    logger.info("new method")
    logger.info("building training set input for 10 dimensions")
    start_time = datetime.datetime.now()
    new_X_train = new_convert_input_for_ann_10D(
                times_scaled,
                coords_train[0], coords_train[1], coords_train[2],
                coords_train[3], coords_train[4], coords_train[5],
                coords_train[6], coords_train[7], coords_train[8]
                )
    end_time = datetime.datetime.now()
    duration2 = end_time-start_time
    print(f"duration: {duration2}")
    
#     print("test")
#     np.testing.assert_array_almost_equal(X_train, new_X_train)
    
#     print("strict test")
#     np.testing.assert_array_equal(X_train, new_X_train)
