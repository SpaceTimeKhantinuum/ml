run0 - standard-ish run to check what would happen if we did use 10D input.

run1 - using a resnet. found in other cases `upscale/quick_run_[4,5]` that it works very well.

run2 - same as run1 but with 1e5 waveforms

run3 - using 1e5 waveforms - trying smaller batch sizes and fewer epochs

run4 - similar to run3 but using the new checkpointing code

run5 - using larger network, larger batch size and with and without BN

building larger training set with 1e5 size
