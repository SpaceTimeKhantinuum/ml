#!/bin/bash

mkdir -p logs

echo "running config_001"
nohup python ../../fit.py -v --config-file config_001.toml > logs/config_001.out 2> logs/config_001.err &

echo "running config_002"
nohup python ../../fit.py -v --config-file config_002.toml > logs/config_002.out 2> logs/config_002.err &
