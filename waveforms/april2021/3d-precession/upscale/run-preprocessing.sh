#!/bin/bash


echo "running compute_preprocessing_1e3"
nohup python ../compute_preprocessing.py -v --config-file compute_preprocessing_1e3.toml > compute_preprocessing_1e3.out 2> compute_preprocessing_1e3.err &
echo "running compute_preprocessing_5e3"
nohup python ../compute_preprocessing.py -v --config-file compute_preprocessing_5e3.toml > compute_preprocessing_5e3.out 2> compute_preprocessing_5e3.err &
echo "running compute_preprocessing_1e4"
nohup python ../compute_preprocessing.py -v --config-file compute_preprocessing_1e4.toml > compute_preprocessing_1e4.out 2> compute_preprocessing_1e4.err &
echo "running compute_preprocessing_5e4"
nohup python ../compute_preprocessing.py -v --config-file compute_preprocessing_5e4.toml > compute_preprocessing_5e4.out 2> compute_preprocessing_5e4.err &
echo "running compute_preprocessing_1e5"
nohup python ../compute_preprocessing.py -v --config-file compute_preprocessing_1e5.toml > compute_preprocessing_1e5.out 2> compute_preprocessing_1e5.err &