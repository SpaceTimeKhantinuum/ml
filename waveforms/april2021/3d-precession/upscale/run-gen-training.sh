#!/bin/bash

echo "running data_generation_1e3"
nohup python ../data_generation.py -v --config-file data_generation_1e3.toml > data_generation_1e3.out 2> data_generation_1e3.err &
echo "running data_generation_5e3"
nohup python ../data_generation.py -v --config-file data_generation_5e3.toml > data_generation_5e3.out 2> data_generation_5e3.err &
echo "running data_generation_1e4"
nohup python ../data_generation.py -v --config-file data_generation_1e4.toml > data_generation_1e4.out 2> data_generation_1e4.err &
echo "running data_generation_5e4"
nohup python ../data_generation.py -v --config-file data_generation_5e4.toml > data_generation_5e4.out 2> data_generation_5e4.err &
echo "running data_generation_1e5"
nohup python ../data_generation.py -v --config-file data_generation_1e5.toml > data_generation_1e5.out 2> data_generation_1e5.err &