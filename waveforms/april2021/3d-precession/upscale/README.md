# Experimenting with larger datasets

## May 4th 2021

Begin.

To make this model work we need two things.

1. A network that can fit the data
2. Enough data such that the network generalises well

Here we use the same base network but on increasingly larger training sets and plot the learning curves.

| training size | validation size |
|-|-|
|1e3|1e3|
|5e3|''|
|1e4|''|
|5e4|''|
|1e5|''|

Parameter space

$q \in [1, 3]$

$\chi_1 \in [0, 0.99]$

$\theta_1 \in [0, \pi]$

For each training set size we run the fit for amplitude and phase.
So we have 10 fits to do in total.

The `quick_run_*` directories are an random set of runs

`base2` seems to give low losses but the performance of the model is not good. strange.
Best to go with `linear` Mscale networks.

The `phase_quick_run_1` and `quick_run_1` (which models the amplitude)
are compared in `compare_quick_run_1` directory. This shows the nice performance
of the model on the training set. This is still where the training set is
of size 1e3 and so too small to have good generalisation. The next thing to
do is to take the best networks from these two directories and increase the
training set size to see how large it needs to be to generalisation well.
If at all possible.

## Chat with Mark and Frank

Reason for poor behaviour in ringdown could be because there are less data
here. The frequency is higher and with the fixed sample rate there is less
data. So maybe using a smaller deltaT it will help.

Alternatively could weight the samples more in this region using
`sample_weight`.

Need to try the phase, expand the domain, add (theta, phi) and try SEOBNRv4PHM.

Need to try and increase the mini-batch-size to see if we can get same accuracy
faster.

batch size of 1000000 is too big
even batch size of 500000 is too big

## Run table

|run prefix| training set size |notes|
|-|-|-|
|run0| -|initial run to test batch size **invalid-changed fitting code** |
|quick_run_1|1e3|different models selecting model 009 for future tests|
|phase_quick_run_1|1e3|different models selecting model 008 for future tests|
|run1| [1e3, 5e3, 1e4, 5e4, 1e5] | amplitude - taking model 009 from quick_run_1 and looking at training and testing errors as a function of training set size - only running for 1e3 epochs to get some early results|
|run2| [1e3, 5e3, 1e4, 5e4, 1e5] | phase - taking model 008 from phase_quick_run_1 and looking at training and testing errors as a function of training set size - only running for 1e3 epochs to get some early results |
|run3   | same as run2 | same as run2 but using larger network (actually the network from run1) |
|run4| same as run1 | same as run1 but using 10 times larger batch size (batch_size = 100000)|
|quick_run_2|1e3|using the model from run1 but with different batch sizes and only using the smallest training set to test things before moving to larger training sets|
|quick_run_3|1e4|same as quick_run_2 but using 10 times larger data set and more larger batch sizes but only 100 epochs|
|quick_run_4|1e4|using a resnet. same as quick_run_3 using the best performing batch size i.e. run '009' - some other runs too such as comparing batch_norm=true/false|
|quick_run_5|1e5|basically the same as quick_run_4 but using the largest training set|
|quick_run_6|1e5|same as quick_run_5 but a simpler network to see if we can reduce validation error and some batch normalisation|



## run2

Even though the model config_005.toml doesn't have a good looing loss curve
the performance of the model isn't so bad and I think if we just train it
for longer it might converge and get better.

So I am running this case for another 2-3 days to see what happens.

See config_005_continue_001.toml


## Batch size

Should investigate batch-size vs epoch vs learning rate vs number of weight updates
