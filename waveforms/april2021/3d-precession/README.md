# introduction

this directory explores using a neural network to model/fit the amplitude and phase from precessing systems in a reduced parameter space for testing purposes. We use the (mass-ratio, spin1x, spin1z) parameter space.

# design

1. data generation
    - `data_generation.py`
1. compute pre-processing
    - `compute_preprocessing.py`
1. fitting
    - `fit.py`
1. evalation
    - `evaluate.py`