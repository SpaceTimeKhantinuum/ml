#!/bin/bash
#echo "running data generation - training"
#python ../data_generation.py -v --config-file data_generation.toml
#echo "running data generation - validation"
#python ../data_generation.py -v --config-file data_generation_val.toml
echo "running compute_preprocessing mean zero"
python ../compute_preprocessing_mean_zero.py -v --config-file compute_preprocessing.toml
#echo "running fit.py"
#python ../fit_mscale.py -v --config-file fit_mscale.toml
