#!/usr/bin/env python

"""
example
python fit.py -v --config-file fit.toml
"""

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.use("agg")

# from cycler import cycler
# from itertools import cycle

# mpl.rcParams.update(mpl.rcParamsDefault)
plt.style.use("ggplot")
mpl.rcParams.update({"font.size": 16})


import tensorflow as tf
import tensorflow_addons as tfa

import numpy as np
import os
import sys
import argparse
import datetime
# import multiprocessing as mp
# import functools
import subprocess
from tqdm import tqdm
import wispy
import wispy.logger
import wispy.utils
import wispy.callbacks
import wispy.model_utils

import pugna.layers
import pugna.activations

# https://datascience.stackexchange.com/questions/58884/how-to-create-custom-activation-functions-in-keras-tensorflow
from tensorflow.keras.utils import get_custom_objects
get_custom_objects().update({'srelu': tf.keras.layers.Activation(pugna.activations.sReLU)})
get_custom_objects().update({'s2relu': tf.keras.layers.Activation(pugna.activations.s2relu)})


import tomlkit
from tomlkit import parse

from sklearn.preprocessing import MinMaxScaler
import pickle

from compute_preprocessing import apply_pre_process_forward, apply_pre_process_reverse


def check_gpu():
    logger.info("running 'tf.config.list_physical_devices('GPU')'")
    logger.info(tf.config.list_physical_devices("GPU"))

    try:
        logger.info("running 'nvidia-smi -L'")
        subprocess.call(["nvidia-smi", "-L"])
    except FileNotFoundError:
        logger.info("could not run 'nvidia-smi -L'")


def set_gpu_memory_growth():
    gpus = tf.config.list_physical_devices('GPU')
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            logger.info("running: tf.config.experimental.set_memory_growth")
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            logger.info(e)


def convert_input_for_ann_10D(
        times,
        q,
        chi1, theta1, phi1,
        chi2, theta2, phi2,
        inclination, phiRef
        ):
    X = []
    for _q, _chi1, _theta1, _phi1, _chi2, _theta2, _phi2, _inclination, _phiRef in zip(q, chi1, theta1, phi1, chi2, theta2, phi2, inclination, phiRef):
        for t in times:
            X.append([t, _q, _chi1, _theta1, _phi1, _chi2, _theta2, _phi2, _inclination, _phiRef])
    X = np.asarray(X)
    return X


def convert_input_for_ann(times, q, chi1, theta1):
    X = []
    for _q, _chi1, _theta1 in zip(q, chi1, theta1):
        for t in times:
            X.append([t, _q, _chi1, _theta1])
    X = np.asarray(X)
    return X

def build_and_fit_model(
    x,
    y,
    batch_size=32,
    nscales=1,
    epochs=1000,
    units=[300,300],
    lr=1e-3,
    activations=['relu','relu'],
    mscale_units=300,
    mscale_activation='s2relu',
    validation_data=None,
    validation_freq=1,
    scale_name='linear',
    use_mscale=False,
    amsgrad=False,
    threshold=None,
    loss='mae',
    metrics=['mse'],
    ReduceLROnPlateau=False,
    factor=0.95,
    patience=40,
    min_lr=1e-4
):

    if 'existing_model_file' in doc['fit'].keys():
        logger.info("found existing model file")
        logger.info(f"doc['fit']['existing_model_file']: {doc['fit']['existing_model_file']}")
        logger.info("loading model")
        model = tf.keras.models.load_model(
                doc['fit']['existing_model_file'],
                custom_objects={
                    'Mscale': pugna.layers.Mscale,
                    'Activation':tf.keras.layers.Activation,
                    'name':pugna.activations.s2relu
                    }
                )
        logger.info("model loaded")
    else:
        logger.info("building new model")
        assert len(units) == len(activations), "units and activations length must match"

        input_shape = x.shape[1]
        output_shape = y.shape[1]

        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.InputLayer(input_shape=(input_shape,)))
        if use_mscale:
            model.add(pugna.layers.Mscale(mscale_units, nscales, scale_name=scale_name))

            if mscale_activation == 'srelu':
                model.add(tf.keras.layers.Activation(pugna.activations.sReLU))
            elif mscale_activation == 's2relu':
                model.add(tf.keras.layers.Activation(pugna.activations.s2relu))
            elif mscale_activation == 'relu':
                model.add(tf.keras.layers.Activation(tf.keras.activations.relu))
            elif mscale_activation == 'tanh':
                model.add(tf.keras.layers.Activation(tf.keras.activations.tanh))

        for i in range(len(units)):
            model.add(tf.keras.layers.Dense(units[i], activation=activations[i]))

        model.add(tf.keras.layers.Dense(output_shape, activation="linear"))

        # if lr == None:
        #     boundaries = [5000, 5100]
        #     values = [1e-3, 1e-4, 1e-5]
        #     # boundaries = [500, 600]
        #     # values = [1e-3, 1e-4, 1e-5]
        #     learning_rate_fn = keras.optimizers.schedules.PiecewiseConstantDecay(
        #         boundaries, values)
        #     optimizer = tf.keras.optimizers.Adam(learning_rate_fn, amsgrad=amsgrad)
        # else:
        #     optimizer = tf.keras.optimizers.Adam(lr, amsgrad=amsgrad)

        optimizer = tf.keras.optimizers.Adam(lr, amsgrad=amsgrad)
        # optimizer = tf.keras.optimizers.Adam(lr, amsgrad=amsgrad, clipnorm=0.5)
        # optimizer = tf.keras.optimizers.Adam(lr, amsgrad=amsgrad, clipvalue=5.0)
        # optimizer = tf.keras.optimizers.Nadam(lr)
        # optimizer = tf.keras.optimizers.SGD(lr)

        model.compile(loss=loss, optimizer=optimizer, metrics=metrics)

    logger.info(model.summary())

    callbacks=[]
    # tqdm_callback = tfa.callbacks.TQDMProgressBar(show_epoch_progress=False)
    # callbacks.append(tqdm_callback)

    if ReduceLROnPlateau:
        logger.info("using ReduceLROnPlateau")
        callbacks.append(
            tf.keras.callbacks.ReduceLROnPlateau(
                monitor='val_loss',
                factor=factor,
                patience=patience,
                min_lr=min_lr)
            )
    
    if threshold:
        threshold_callback = wispy.callbacks.ThresholdCallback(threshold)
        callbacks.append(threshold_callback)

    logger.info("starting fit")

    history = model.fit(x, y, batch_size=batch_size, epochs=epochs,
                        validation_data=validation_data, callbacks=callbacks,
                        verbose=1, validation_freq=validation_freq)

    logger.info("fit finished")

    return history, model


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="""read in training data, apply pre-processing parameters and run model.fit()""",
    )

    parser.add_argument(
        "--config-file", type=str, help="path to workflow config.toml file"
    )
    parser.add_argument(
        "-v",
        help="""increase output verbosity
        no -v: WARNING(")
        -v: INFO
        -vv: DEBUG""",
        action="count",
        dest="verbose",
        default=0,
    )
    parser.add_argument(
        "--force",
        action="store_true",
        help="if given then will not throw an error if output directory exists",
    )

    overall_start_time = datetime.datetime.now()

    args = parser.parse_args()
    args_dict = vars(args)

    # https://stackoverflow.com/questions/14097061/easier-way-to-enable-verbose-logging
    level = min(2, args.verbose)  # capped to number of levels
    logger = wispy.logger.init_logger(level=level)

    logger.info(f"Using TensorFlow v{tf.__version__}")
    logger.info(f"wispy version: {wispy.__version__}")

    logger.info("==========")
    logger.info("printing command line args")
    for k in args_dict.keys():
        logger.info(f"{k}: {args_dict[k]}")
    logger.info("==========")

    with open(args.config_file, "r") as f:
        text = f.read()

    doc = parse(text)

    logger.info("==========")
    logger.info("printing toml config contents")
    wispy.utils.recursive_dict_print(doc, print_fn=logger.info)
    logger.info("==========")

    if doc["gpu"]["CUDA_VISIBLE_DEVICES"]:
        logger.info("setting CUDA_VISIBLE_DEVICES")
        os.environ["CUDA_VISIBLE_DEVICES"] = doc["gpu"]["CUDA_VISIBLE_DEVICES"]
        if doc["gpu"]["CUDA_VISIBLE_DEVICES"] != "-1":
            check_gpu()
            set_gpu_memory_growth()

    logger.info(f"making output dir: {doc['output']}")
    os.makedirs(f"{doc['output']}", exist_ok=args.force)

    # load coords
    filename = doc['data']['coords_train']
    logger.info(f"loading coords file: {filename}")
    coords_train = np.load(filename)

    # load times data
    filename = doc['data']['times']
    logger.info(f"loading times file: {filename}")
    times = np.load(filename)

    pkl_filename = doc['data']['times_scaler']
    with open(pkl_filename, 'rb') as file:
        times_scaler = pickle.load(file)

    logger.info("transforming times data")
    times_scaled = times_scaler.transform(times[:, np.newaxis])[:, 0]

    filename = doc['data']['y_train']
    logger.info(f"loading y-train file: {filename}")
    y_train_raw = np.load(filename)

    filename = doc['data']['y_preprocessing_params']
    logger.info(f"loading y-preprocessing-params file: {filename}")
    y_processing_params = np.load(filename)


    logger.info("applying preprocessing to raw training data")
    y_train = apply_pre_process_forward(y_train_raw, y_processing_params['mean'], y_processing_params['max'])

    del y_train_raw

    logger.info('loading validation data')

    # load coords
    filename = doc['data']['coords_val']
    logger.info(f"loading coords validation file: {filename}")
    coords_val = np.load(filename)

    filename = doc['data']['y_val']
    logger.info(f"loading y-validation file: {filename}")
    y_val_raw = np.load(filename)

    logger.info("applying preprocessing to raw validation data")
    y_val = apply_pre_process_forward(y_val_raw, y_processing_params['mean'], y_processing_params['max'])

    del y_val_raw

    if coords_train.shape[0] == 9:
        logger.info("building training set input for 10 dimensions")
        X_train = convert_input_for_ann_10D(
                    times_scaled,
                    coords_train[0], coords_train[1], coords_train[2],
                    coords_train[3], coords_train[4], coords_train[5],
                    coords_train[6], coords_train[7], coords_train[8]
                    )
    elif coords_train.shape[0] == 3:
        logger.info("building training set input for 4 dimensions")
        X_train = convert_input_for_ann(times_scaled, coords_train[0], coords_train[1], coords_train[2])
    else:
        logger.error("coords_train.shape error: only 3 or 9 supported currently")
        logger.error("Exiting")
        sys.exit(1)
    y_train = y_train.ravel().reshape(-1, 1)

    logger.info(f"X_train.shape: {X_train.shape}")
    logger.info(f"y_train.shape: {y_train.shape}")

    if coords_val.shape[0] == 9:
        logger.info("building validation set input for 10 dimensions")
        X_val = convert_input_for_ann_10D(
                    times_scaled,
                    coords_val[0], coords_val[1], coords_val[2],
                    coords_val[3], coords_val[4], coords_val[5],
                    coords_val[6], coords_val[7], coords_val[8]
                    )
    elif coords_val.shape[0] == 3:
        logger.info("building validation set input for 4 dimensions")
        X_val = convert_input_for_ann(times_scaled, coords_val[0], coords_val[1], coords_val[2])
    else:
        logger.error("coords_val.shape error: only 3 or 9 supported currently")
        logger.error("Exiting")
        sys.exit(1)
    y_val = y_val.ravel().reshape(-1, 1)

    logger.info(f"X_val.shape: {X_val.shape}")
    logger.info(f"y_val.shape: {y_val.shape}")

    validation_data=(X_val, y_val)

    starttime = datetime.datetime.now()
    history, model = build_and_fit_model(
        x=X_train,
        y=y_train,
        batch_size=doc['fit']['batch_size'],
        lr=doc['fit']['lr'],
        validation_data=validation_data,
        validation_freq=doc['fit']['validation_freq'],
        activations=doc['fit']['activations'],
        epochs=doc['fit']['epochs'],
        nscales=doc['fit']['nscales'],
        units=doc['fit']['units'],
        scale_name=doc['fit']['scale_name'],
        use_mscale=doc['fit']['use_mscale'],
        amsgrad=doc['fit']['amsgrad'],
        mscale_units=doc['fit']['mscale_units'],
        mscale_activation=doc['fit']['mscale_activation'],
        loss=doc['fit']['loss'],
        metrics=doc['fit']['metrics'],
        ReduceLROnPlateau=doc['fit']['ReduceLROnPlateau'],
        factor=0.95,
        patience=40,
        min_lr=1e-4,
        threshold=doc['fit']['threshold']
    )
    endtime = datetime.datetime.now()
    duration = endtime - starttime

    logger.info("fit complete")
    logger.info(f"The time cost: {duration}")

    logger.info("saving model")

    filename = os.path.join(doc['output'], "model.h5")
    model.save(f"{filename}")

    if history.history['mse']:
        logger.info("plotting history")
        plt.figure(figsize=(14, 4))
        plt.subplot(1, 2, 1)
        plt.plot(history.history['mse'], label='mse')
        plt.yscale('log')
        plt.legend()

    if history.history['val_mse']:
        plt.subplot(1, 2, 2)
        plt.plot(history.history['val_mse'], label='val_mse')
        plt.yscale('log')
        plt.legend()

    if history.history['mse']:
        filename = os.path.join(f"{doc['output']}", 'loss.png')
        plt.savefig(filename)
        plt.close()

    filename = os.path.join(f"{doc['output']}", "history.pickle")
    logger.info(f"saving history: {filename}")
    wispy.model_utils.save_history(history.history, filename)

    filename = os.path.join(f"{doc['output']}", "duration.pickle")
    logger.info(f"saving duration: {filename}")
    wispy.model_utils.save_datetime(duration, filename)

    overall_end_time = datetime.datetime.now()
    overall_duration = overall_end_time - overall_start_time
    logger.info(f"total time: {overall_duration}")
    logger.info("finished!")
