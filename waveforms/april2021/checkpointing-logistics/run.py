#!/usr/bin/env python

"""[summary]
When training on very large datasets it might not be possible
to complete the training in one session.
Also training may be interrupted by computer failures.

To guard against this codes will checkpoint after a set amount of
time or a set number of iterations.

After a set number of checkpointing or set amount of time the code will
exit.

Upon re-running the same code it should realise that there is a checkpoint
and pick-up where it left off.

When working with TensorFlow we just checkpoint after every epoch and run
until the process gets killed for what ever reason. The trick here is going to
be to code up how we restart the run and keep track of the histories.

help:
https://www.tensorflow.org/guide/keras/train_and_evaluate#checkpointing_models

This script is used to work out a way how to do this.
"""

import matplotlib as mpl
import matplotlib.pyplot as plt

# mpl.use("agg")

# from cycler import cycler
# from itertools import cycle

# mpl.rcParams.update(mpl.rcParamsDefault)
plt.style.use("ggplot")
mpl.rcParams.update({"font.size": 16})

import argparse
import datetime
import os

import wispy.logger
import wispy.utils
import wispy.resnet

from tomlkit import parse

import tensorflow as tf

import numpy as np

def generate_train_test_data(n_train, n_test):
    """
    example from https://github.com/xuzhiqin1990/F-Principle/blob/master/main_1d_20190924.py
    """
    x = np.linspace(-1, 1, n_train)[:,np.newaxis]
    x_test = np.sort(np.random.uniform(-1, 1, size=(n_test, 1)), axis=0)

    f=1
    om = 2*np.pi*f
    y1 = np.sin(om*x)
    y1_test = np.sin(om*x_test)

    f=4
    om = 2*np.pi*f
    y2 = np.sin(om*x)
    y2_test = np.sin(om*x_test)

    y = y1+y2
    y_test = y1_test + y2_test

    return x, y, x_test, y_test

def get_uncompiled_model(model_name, input_shape, output_shape,
        width, num_blocks=2, batch_norm=True, momentum=0.9
    ):
    assert model_name in ['resnet', 'dense']

    if model_name == 'resnet':
        model = wispy.resnet.ResNet(
            input_shape=input_shape,
            output_shape=output_shape,
            width=width,
            num_blocks=num_blocks,
            batch_norm=batch_norm,
            momentum=momentum
        )
    elif model_name == 'dense':
        inputs = tf.keras.Input(shape=(input_shape,))
        x = tf.keras.layers.Dense(width, activation="relu")(inputs)
        if batch_norm:
            x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        x = tf.keras.layers.Dense(width, activation="relu")(x)
        if batch_norm:
            x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        x = tf.keras.layers.Dense(width, activation="relu")(x)
        if batch_norm:
            x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        x = tf.keras.layers.Dense(width, activation="relu")(x)
        if batch_norm:
            x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        x = tf.keras.layers.Dense(width, activation="relu")(x)
        if batch_norm:
            x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        x = tf.keras.layers.Dense(width, activation="relu")(x)
        if batch_norm:
            x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        x = tf.keras.layers.Dense(width, activation="relu")(x)
        if batch_norm:
            x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        x = tf.keras.layers.Dense(width, activation="relu")(x)
        if batch_norm:
            x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        x = tf.keras.layers.Dense(width, activation="relu")(x)
        if batch_norm:
            x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        x = tf.keras.layers.Dense(width, activation="relu")(x)
        if batch_norm:
            x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        # x = tf.keras.layers.Dense(width, activation="relu")(x)
        # if batch_norm:
        #     x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        # x = tf.keras.layers.Dense(width, activation="relu")(x)
        # if batch_norm:
        #     x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        # x = tf.keras.layers.Dense(width, activation="relu")(x)
        # if batch_norm:
        #     x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        # x = tf.keras.layers.Dense(width, activation="relu")(x)
        # if batch_norm:
        #     x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        # x = tf.keras.layers.Dense(width, activation="relu")(x)
        # if batch_norm:
        #     x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        # x = tf.keras.layers.Dense(width, activation="relu")(x)
        # if batch_norm:
        #     x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        # x = tf.keras.layers.Dense(width, activation="relu")(x)
        # if batch_norm:
        #     x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        # x = tf.keras.layers.Dense(width, activation="relu")(x)
        # if batch_norm:
        #     x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        # x = tf.keras.layers.Dense(width, activation="relu")(x)
        # if batch_norm:
        #     x = tf.keras.layers.BatchNormalization(momentum=momentum)(x)
        outputs = tf.keras.layers.Dense(output_shape, activation="linear")(x)
        model = tf.keras.Model(inputs=inputs, outputs=outputs)
    return model

def get_compiled_model(
        model_name,
        input_shape, output_shape, width,
        lr, amsgrad, loss, metrics, num_blocks=2, batch_norm=True, momentum=0.9
    ):
    model = get_uncompiled_model(
            model_name=model_name,
            input_shape=input_shape,
            output_shape=output_shape,
            width=width,
            num_blocks=num_blocks,
            batch_norm=batch_norm,
            momentum=momentum
        )

    optimizer = tf.keras.optimizers.Adam(lr, amsgrad=amsgrad)

    model.compile(loss=loss, optimizer=optimizer, metrics=metrics)

    return model


def get_checkpoints(checkpoint_dir):
    checkpoints = [checkpoint_dir + "/" + name for name in os.listdir(checkpoint_dir)]
    return checkpoints

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="""example how to checkpoint restart""",
    )

    parser.add_argument(
        "--config-file", type=str, help="path to workflow config.toml file"
    )
    parser.add_argument(
        "-v",
        help="""increase output verbosity
        no -v: WARNING(")
        -v: INFO
        -vv: DEBUG""",
        action="count",
        dest="verbose",
        default=0,
    )
    parser.add_argument(
        "--force",
        action="store_true",
        help="if given then will not throw an error if output directory exists",
    )

    # start timing, used to monitor for checkpointing
    overall_start_time = datetime.datetime.now()

    args = parser.parse_args()
    args_dict = vars(args)

    # https://stackoverflow.com/questions/14097061/easier-way-to-enable-verbose-logging
    level = min(2, args.verbose)  # capped to number of levels
    logger = wispy.logger.init_logger(level=level)

    logger.info("==========")
    logger.info("printing command line args")
    for k in args_dict.keys():
        logger.info(f"{k}: {args_dict[k]}")
    logger.info("==========")

    with open(args.config_file, "r") as f:
        text = f.read()

    doc = parse(text)

    logger.info("==========")
    logger.info("printing toml config contents")
    wispy.utils.recursive_dict_print(doc, print_fn=logger.info)
    logger.info("==========")

    x, y, x_test, y_test = generate_train_test_data(n_train=100000, n_test=100)

    # plt.figure()
    # plt.plot(x, y, label='train')
    # plt.plot(x_test, y_test, 'o-', label='test')
    # plt.legend()
    # plt.show()
    # plt.savefig('test-train-data.png')
    # plt.close()

    input_shape = x.shape[1]
    output_shape = y.shape[1]
    num_blocks = 2
    # num_blocks = 1
    width = 128
    lr = 1e-3
    amsgrad = False
    loss = 'mse'
    metrics = None
    batch_norm = True

    momentum = 0.5

    model_name = "resnet"
    # model_name = "dense"

    epochs = 200
    # epochs = 2

    # batch_size = 10000
    batch_size = 100000

    model = get_compiled_model(
        model_name=model_name,
        input_shape=input_shape, output_shape=output_shape, width=width,
        lr=lr, amsgrad=amsgrad, loss=loss, metrics=metrics, momentum=momentum,
        num_blocks=num_blocks,
        batch_norm=batch_norm
        )

    model.summary()

    history = model.fit(x, y, epochs=epochs, validation_data=(x_test, y_test), batch_size=batch_size)

    plt.figure()
    plt.plot(history.history['loss'], label='loss')
    plt.plot(history.history['val_loss'], label='val-loss')
    plt.legend()
    plt.yscale('log')
    plt.title(f"{model_name}")
    plt.axhline(1e-5)
    plt.savefig(f'{model_name}-loss.png')
    plt.close()

    # plt.figure()
    # plt.plot(x, y, label='train')
    # plt.plot(x_test, y_test, 'o-', label='test')
    # plt.plot(x_test, model.predict(x_test), 'o--', label='predict test')
    # # plt.plot(x_test, model(x_test))
    # plt.legend()
    # plt.show()
    # # plt.savefig('test-train-data.png')
    # plt.close()

    # batch norm with a well chosen momentum behaves similarly to resnet
    # however a reset seems to outperform a standard net by an order of magnitude
    # with networks with the same number of parameters

    # new information: no matter the size of the training set
    # batch normalisation ONLY worked well, for training and validation,
    # when the ENTIRE dataset was parsed, i.e., full batch training.
    # Not sure why....

    # GPU memory probably not enough for full batch - maybe try CPU.