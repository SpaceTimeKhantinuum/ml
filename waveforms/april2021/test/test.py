import tensorflow as tf
import tensorflow_addons as tfa
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np
import glob
import os

import phenom

import tempfile
import datetime

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.use("agg")

# from cycler import cycler
# from itertools import cycle

# mpl.rcParams.update(mpl.rcParamsDefault)
plt.style.use("ggplot")
mpl.rcParams.update({"font.size": 16})

import multiprocessing as mp
import functools

from scipy.interpolate import InterpolatedUnivariateSpline as IUS

import wispy.lalutils
import wispy.model_utils
import wispy.callbacks

import lalsimulation as lalsim

import pugna.layers
import pugna.activations

# https://datascience.stackexchange.com/questions/58884/how-to-create-custom-activation-functions-in-keras-tensorflow
from tensorflow.keras.utils import get_custom_objects
get_custom_objects().update({'srelu': tf.keras.layers.Activation(pugna.activations.sReLU)})
get_custom_objects().update({'s2relu': tf.keras.layers.Activation(pugna.activations.s2relu)})

from sklearn.preprocessing import MinMaxScaler

def set_gpu_memory_growth():
    gpus = tf.config.list_physical_devices('GPU')
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            print("running: tf.config.experimental.set_memory_growth")
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)

set_gpu_memory_growth()

def build_and_fit_model(
    x,
    y,
    batch_size=None,
    nscales=1,
    epochs=1000,
    units=300,
    lr=1e-3,
    activation='srelu',
    validation_data=None,
    validation_freq=1,
    scale_name='linear',
    use_mscale=False,
    amsgrad=False
):
    if batch_size is None:
        batch_size = x.shape[0]

    input_shape = x.shape[1]
    output_shape = y.shape[1]

    loss = 'mae'
    metrics = ['mse']

    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.InputLayer(input_shape=(input_shape,)))
    if use_mscale:
        model.add(pugna.layers.Mscale(units, nscales, scale_name=scale_name))
    else:
        model.add(tf.keras.layers.Dense(units))
    if activation == 'srelu':
        model.add(tf.keras.layers.Activation(pugna.activations.sReLU))
    elif activation == 's2relu':
        model.add(tf.keras.layers.Activation(pugna.activations.s2relu))
    elif activation == 'relu':
        model.add(tf.keras.layers.Activation(tf.keras.activations.relu))
    elif activation == 'tanh':
        model.add(tf.keras.layers.Activation(tf.keras.activations.tanh))

    model.add(tf.keras.layers.Dense(units, activation=activation))
    model.add(tf.keras.layers.Dense(units, activation=activation))
#     model.add(tf.keras.layers.Dense(units, activation=activation))
#     model.add(tf.keras.layers.Dense(units, activation=activation))
    model.add(tf.keras.layers.Dense(output_shape, activation="linear"))

#     optimizer = tf.keras.optimizers.Adam(lr)
#     optimizer = tf.keras.optimizers.Adam(lr, amsgrad=amsgrad)
#     optimizer = tf.keras.optimizers.Adam(lr, amsgrad=True)
    #optimizer = tf.keras.optimizers.Adam(lr, amsgrad=True, clipnorm=0.5)
    #optimizer = tf.keras.optimizers.Adam(lr, amsgrad=True, clipvalue=5.0)
    #optimizer = tf.keras.optimizers.Nadam(lr)
    # optimizer = tf.keras.optimizers.SGD(lr)

    if lr == None:
        boundaries = [5000, 5100]
        values = [1e-3, 1e-4, 1e-5]
        # boundaries = [500, 600]
        # values = [1e-3, 1e-4, 1e-5]
        learning_rate_fn = keras.optimizers.schedules.PiecewiseConstantDecay(
            boundaries, values)
        optimizer = tf.keras.optimizers.Adam(learning_rate_fn, amsgrad=amsgrad)
    else:
        optimizer = tf.keras.optimizers.Adam(lr, amsgrad=amsgrad)

    model.compile(loss=loss, optimizer=optimizer, metrics=metrics)

    print(model.summary())


    callbacks=[]
    tqdm_callback = tfa.callbacks.TQDMProgressBar(show_epoch_progress=False)
    callbacks.append(tqdm_callback)

    threshold_callback = wispy.callbacks.ThresholdCallback(1e-9)
    callbacks.append(threshold_callback)

    history = model.fit(x, y, batch_size=batch_size, epochs=epochs, validation_data=validation_data, callbacks=callbacks, verbose=0, validation_freq=validation_freq)

    plt.figure(figsize=(14, 4))
    plt.subplot(1, 2, 1)
    plt.plot(history.history['mse'], label='mse')
    plt.yscale('log')
    plt.legend()

    if validation_data:
        plt.subplot(1, 2, 2)
        plt.plot(history.history['val_mse'], label='val_mse')

        plt.yscale('log')
        plt.legend()

    plt.savefig('loss.png')
    plt.close()

    return history, model

def gen_data_2d_s1x_time(s1x_min, s1x_max, s1x_num, q=1, M=100, nproc=1, t_min=-500):

    s1x_arr = np.linspace(s1x_min, s1x_max, s1x_num)

    ps = []
    for s1x in s1x_arr:
        m1, m2 = phenom.m1_m2_M_q(M, q)
        ps.append(wispy.lalutils.gen_td_wf_params(m1=m1, m2=m2, S1x=s1x, approximant=lalsim.SEOBNRv4P, f_min=5, distance=1e6))

    times = []
    amp = []
    phase = []

#     for p in ps:
#         _times, _amp, _phase = wispy.lalutils.gen_td_wf(p, t_max=40)
#         times.append(_times)
#         amp.append(_amp)
#         phase.append(_phase)

#     def mapable(p):
#         _times, _amp, _phase = wispy.lalutils.gen_td_wf(p, t_max=40)
#         return (_times, _amp, _phase)

    func = functools.partial(wispy.lalutils.gen_td_wf, t_max=40, t_min=t_min)

    with mp.Pool(nproc) as pool:
        returned = pool.map(func, ps)
#     print(len(returned))
#     print(len(returned[0]))
#     print(returned[0])
    for i in range(len(returned)):
        times.append(returned[i][0])
        amp.append(returned[i][1])
        phase.append(returned[i][2])



    # find common times and interpolate data onto common time grid
    dt_M = 0.5
    common_tmin = np.max(list(map(np.min, times)))
    common_tmax = np.min(list(map(np.max, times)))

    print(f"common_tmin = {common_tmin}")
    print(f"common_tmax = {common_tmax}")

    common_times = np.arange(common_tmin, common_tmax, dt_M)

    ntimes = len(common_times)
    print(f"ntimes = {ntimes}")

    n_cases = len(ps)

    amps = np.zeros(shape=(n_cases, ntimes))
    phases = np.zeros(shape=(n_cases, ntimes))

    for i in range(n_cases):
        amps[i] = IUS(times[i], amp[i])(common_times)
        phases[i] = IUS(times[i], phase[i])(common_times)

    return common_times, amps, phases, s1x_arr


def convert_input_for_ann(times, q):
    times = np.atleast_1d(times)
    q = np.atleast_1d(q)
    XX, YY = np.meshgrid(times, q)
    X = np.stack((XX.ravel(), YY.ravel()), axis=1) #axis=1 is same as column_stack

    return X

def compute_pre_process(ys):
    ys = ys.copy()
    ys_mean = np.mean(ys, axis=0)[np.newaxis, :] # mean waveform
    ys -= ys_mean
    ys_max = np.max(ys)
#     ys /= ys_max

    return ys_mean, ys_max

def apply_pre_process_forward(ys, y_mean ,y_max):
    ys = ys.copy()
    ys -= y_mean
    ys /= y_max

    return ys

def apply_pre_process_reverse(ys, y_mean ,y_max):
    ys = ys.copy()
    ys *= y_max
    ys += y_mean

    return ys


# common_times, amps, phases, q_arr = gen_data_2d_q_time(q_min=1, q_max=2, q_num=10, M=100, nproc=2)
common_times, amps, phases, q_arr = gen_data_2d_s1x_time(s1x_min=0, s1x_max=0.99, s1x_num=20, q=3, M=100, nproc=4, t_min=-2000)

# _, amps_val, phases_val, q_arr_val = gen_data_2d_q_time(q_min=1.1, q_max=2.1, q_num=8, M=100, nproc=2)
_, amps_val, phases_val, q_arr_val = gen_data_2d_s1x_time(s1x_min=0.1, s1x_max=0.98, s1x_num=5, q=3, M=100, nproc=4, t_min=-2000)

y_mean, y_max = compute_pre_process(amps)

amps_prime = apply_pre_process_forward(amps, y_mean, y_max)

# scale times to between -1, 1
times_scaler = MinMaxScaler(feature_range=(-1,1)) # this works better than (0,1)
# times_scaler = MinMaxScaler(feature_range=(0,1))
common_times_scaled = times_scaler.fit_transform(common_times[:, np.newaxis])[:,0]

# X = convert_input_for_ann(common_times, q_arr)
X = convert_input_for_ann(common_times_scaled, q_arr)

y = amps_prime.ravel().reshape(-1, 1)

# X_val = convert_input_for_ann(common_times, q_arr_val)
X_val = convert_input_for_ann(common_times_scaled, q_arr_val)

amps_val_prime = apply_pre_process_forward(amps_val, y_mean, y_max)


y_val = amps_val_prime.ravel().reshape(-1, 1)

# training data X and y

print(f"num cases: {q_arr.shape}")

print(f"X.shape = {X.shape}")
print(f"y.shape = {y.shape}")

# validation data X and y

print(f"num cases: {q_arr_val.shape}")

print(f"X_val.shape = {X_val.shape}")
print(f"y_val.shape = {y_val.shape}")

starttime = datetime.datetime.now()
amp_history, model_amp = build_and_fit_model(
    x=X,
    y=y,
    batch_size=1000,
    lr=1e-3,
    validation_data=(X_val, y_val),
    validation_freq=10000,
    activation='s2relu', epochs=100000, nscales=4, units=300, scale_name='base2', use_mscale=True,
    amsgrad=False
)
endtime = datetime.datetime.now()
duration = endtime - starttime

print("fit complete")
print(f"The time cost: {duration}")

print("saving model")

filename = "model.h5"
model_amp.save(f"{filename}")

def compare_models(outname, model, q_arr, y, index, times=common_times, compare_in_model_space=False):
    times = times.copy()

    y=y.copy()

    if compare_in_model_space:
        y = apply_pre_process_forward(y, y_mean, y_max)
    y = y[index]

    X = convert_input_for_ann(times, q_arr[index])
    yhat = model.predict(X).T
#     print(yhat.shape)
#     # reverse processing
    if not compare_in_model_space:
        yhat = apply_pre_process_reverse(yhat, y_mean, y_max)
    yhat = yhat[0]
#     yhat = yhat[index]

    if not compare_in_model_space:
        times = times_scaler.inverse_transform(times[:,np.newaxis])[:,0]



    plt.figure(figsize=(18, 6))
    plt.subplot(2, 1, 1)
    plt.plot(times, y, label='true')
    plt.plot(times, yhat, label='yhat', ls='--')
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
#     plt.xlim(-100, 20)
#     plt.ylim(-70, -40)
#     plt.ylim(0.4, 0.9)

    plt.subplot(2, 1, 2)
    plt.plot(times, y-yhat, label='y-yhat')
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
#     plt.ylim(-0.01,0.01)

    title = f"(approx,q)={np.around(q_arr[index], 3)}"
    plt.suptitle(title)

    plt.savefig(outname)
    plt.close()


compare_models(outname='plt0_1.png', model=model_amp, q_arr=q_arr, y=amps, index=0, compare_in_model_space=True, times=common_times_scaled)
compare_models(outname='plt0_2.png', model=model_amp, q_arr=q_arr, y=amps, index=0, compare_in_model_space=False, times=common_times_scaled)


compare_models(outname='plt-1_1.png', model=model_amp, q_arr=q_arr, y=amps, index=-1, compare_in_model_space=True, times=common_times_scaled)
compare_models(outname='plt-1_2.png', model=model_amp, q_arr=q_arr, y=amps, index=-1, compare_in_model_space=False, times=common_times_scaled)

print('done!')