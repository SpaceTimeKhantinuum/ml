#!/usr/bin/env python

"""
example
python compute_preprocessing.py -v --config-file compute_preprocessing.toml

loads in the coprec data, computes the preprocessing variables
e.g., the max-abs-value of all waveforms
and saves it to an .npz file.
This is read in during the fit to scale the data correctly.

"""

import numpy as np
import os
import argparse
import datetime
# import multiprocessing as mp
# import functools
from tqdm import tqdm
import wispy
import wispy.logger
import wispy.utils

import tomlkit
from tomlkit import parse

from sklearn.preprocessing import MinMaxScaler
import pickle


def compute_pre_process(ys):
    """
    Returns the max value over waveforms
    which are then used by the forward and reverse functions to
    actually apply the transformations
    """
    ys = ys.copy()
    ys_max = np.max(np.abs(ys))

    return ys_max


def apply_pre_process_forward(ys, y_max):
    ys = ys.copy()
    ys /= y_max

    return ys


def apply_pre_process_reverse(ys, y_max):
    ys = ys.copy()
    ys *= y_max

    return ys


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="""read in training data and calculate pre-processing parameters""",
    )

    parser.add_argument(
        "--config-file", type=str, help="path to workflow config.toml file"
    )
    parser.add_argument(
        "-v",
        help="""increase output verbosity
        no -v: WARNING(")
        -v: INFO
        -vv: DEBUG""",
        action="count",
        dest="verbose",
        default=0,
    )
    parser.add_argument(
        "--force",
        action="store_true",
        help="if given then will not throw an error if output directory exists",
    )

    overall_start_time = datetime.datetime.now()

    args = parser.parse_args()
    args_dict = vars(args)

    # https://stackoverflow.com/questions/14097061/easier-way-to-enable-verbose-logging
    level = min(2, args.verbose)  # capped to number of levels
    logger = wispy.logger.init_logger(level=level)

    logger.info(f"wispy version: {wispy.__version__}")

    logger.info("==========")
    logger.info("printing command line args")
    for k in args_dict.keys():
        logger.info(f"{k}: {args_dict[k]}")
    logger.info("==========")

    with open(args.config_file, "r") as f:
        text = f.read()

    doc = parse(text)

    logger.info("==========")
    logger.info("printing toml config contents")
    wispy.utils.recursive_dict_print(doc, print_fn=logger.info)
    logger.info("==========")

    logger.info(f"making output dir: {doc['output']}")
    os.makedirs(f"{doc['output']}", exist_ok=args.force)

    # load times data
    filename = os.path.join(doc['input'], 'times.npy')
    logger.info(f"loading times file: {filename}")
    times = np.load(filename)

    # scale times: by default: between -1, 1
    logger.info("looking for 'new_start_time'")
    if "new_start_time" in doc.keys():
        logger.info("found 'new_start_time'")
        new_start_time = float(doc["new_start_time"])
    else:
        logger.info("not found 'new_start_time', using default")
        new_start_time = -1.0

    logger.info("looking for 'new_end_time'")
    if "new_end_time" in doc.keys():
        logger.info("found 'new_end_time'")
        new_end_time = float(doc["new_end_time"])
    else:
        logger.info("not found 'new_end_time', using default")
        new_end_time = 1.0

    logger.info("building time_scaler")
    times_scaler = MinMaxScaler(feature_range=(new_start_time, new_end_time))
    logger.info("fit.transform time_scaler")
    times_scaled = times_scaler.fit_transform(times[:, np.newaxis])[:, 0]
    logger.info("removing old times var")

    del times

    pkl_filename = os.path.join(doc["output"], "times_scaler.pkl")
    logger.info(f"dumping scaler to pickle file: {pkl_filename}")
    with open(pkl_filename, 'wb') as file:
        pickle.dump(times_scaler, file)

    expected_files = ["alpha.npy",
                      "amp_21.npy",
                      "amp_22.npy",
                      "beta.npy",
                      "gamma.npy",
                      "phi_21.npy",
                      "phi_22.npy"]

    y_max_dict = {}

    for expected_file in expected_files:
        key = expected_file.split('.npy')[0]
        logger.info(f"key: {key}")
        filename = os.path.join(doc['input'], expected_file)
        logger.info(f"loading file: {filename}")
        data = np.load(filename)
        y_max = compute_pre_process(data)
        del data
        logger.info(f"computed y_max: {y_max}")
        y_max_dict[key] = y_max

    out = os.path.join(doc["output"], "preprocessing_params.npz")
    logger.info(f"saving data processing constants: {out}")
    np.savez(out, **y_max_dict)

    overall_end_time = datetime.datetime.now()
    overall_duration = overall_end_time - overall_start_time
    logger.info(f"total time: {overall_duration}")
    logger.info("finished!")
