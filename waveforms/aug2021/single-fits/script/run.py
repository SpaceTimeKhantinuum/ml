import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
matplotlib.rcParams.update({'font.size': 16})
import numpy as np
import os
import datetime
import tensorflow as tf
import tensorflow_addons as tfa
import sklearn.preprocessing
import wispy.callbacks
from scipy.interpolate import InterpolatedUnivariateSpline as IUS

import random


import pugna.activations
from tensorflow.keras.utils import get_custom_objects
get_custom_objects().update({'srelu': pugna.activations.sReLU})
get_custom_objects().update({'s2relu': pugna.activations.s2relu})

def set_gpu_memory_growth():
    gpus = tf.config.list_physical_devices('GPU')
    if gpus:
        # Currently, memory growth needs to be the same across GPUs
        print("running: tf.config.experimental.set_memory_growth")
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)


os.environ["CUDA_VISIBLE_DEVICES"] = '2'
set_gpu_memory_growth()

plot_dir = "plots"
os.makedirs(plot_dir, exist_ok=True)

# epochs=1000000
# batch_size=2000
# patience=10000
epochs=100000
batch_size=20000
#patience=int(epochs/100)
patience=int(epochs/10)

def load_data(name, root_dir="../", take_first=None, indicies=None):
    times = np.load(os.path.join(root_dir,"training_data/times.npy"))
    X = np.load(os.path.join(root_dir,"training_data/coords.npy")).T
    X_val = np.load(os.path.join(root_dir,"validation_data/coords.npy")).T
    
    y = np.load(os.path.join(root_dir, f"training_data/{name}.npy"))
    y_val = np.load(os.path.join(root_dir,f"validation_data/{name}.npy"))

    scale = np.max(np.abs(y))
    y /= scale
    y_val /= scale

    if indicies is not None:
#         mask = times >= -10
        mask = times >= times[0]
        return times[mask], X[indicies], y[indicies][:,mask], X_val[indicies], y_val[indicies][:,mask]
    
    elif take_first:
#         take_first = len(X)+1
#         mask = times >= -10
        mask = times >= times[0]
        return times[mask], X[:take_first], y[:take_first,mask], X_val[:take_first], y_val[:take_first,mask]

    else:
        return times, X[:take_first], y[:take_first], X_val[:take_first], y_val[:take_first]

# root_dir = "../"
# root_dir = "/home/sebastian.khan/git/stk/ml/waveforms/aug2021/test-3d"
root_dir = "/home/sebastian.khan/git/stk/ml/waveforms/aug2021/test-5d"

# times, X, y, X_val, y_val = load_data('beta', take_first=2)
# times, X, y, X_val, y_val = load_data('beta', take_first=4)
# times, X, y, X_val, y_val = load_data('beta', take_first=5)
# times, X, y, X_val, y_val = load_data('beta', take_first=10)
# times, X, y, X_val, y_val = load_data('beta', root_dir, take_first=100)
# times, X, y, X_val, y_val = load_data('beta', indicies=[0,3,4,7,8])
# times, X, y, X_val, y_val = load_data('beta', indicies=[0,3,1])
times, X, y, X_val, y_val = load_data('beta', root_dir)


def build_model(input_shape, output_shape, units, activation, n_hidden_layers):
    
    input_layer = tf.keras.Input(shape=(input_shape,))
    x = input_layer
    for i in range(n_hidden_layers):
        x = tf.keras.layers.Dense(units, activation=activation)(x)
        
#     dense_skip = tf.keras.layers.Dense(units, activation=activation)(input_layer)
#     x = tf.keras.layers.add([dense_skip, x])
    output_layer = tf.keras.layers.Dense(output_shape)(x)
    
    return tf.keras.models.Model(inputs=input_layer, outputs=output_layer)



model_1_params = dict(
    input_shape=X.shape[1],
    output_shape=y.shape[1],
    units=1024,
    activation='s2relu',
    n_hidden_layers=6
)
model_1 = build_model(**model_1_params)
print(model_1.summary())
model_1.compile(optimizer=tf.keras.optimizers.Adam(1e-3), loss='mse')


callbacks=[
    tf.keras.callbacks.ReduceLROnPlateau(factor=0.8, patience=patience, min_lr=1e-6),
    wispy.callbacks.ThresholdCallback(1e-10),
    # tfa.callbacks.TQDMProgressBar(show_epoch_progress=False)
]
print("starting fit")
start_time = datetime.datetime.now()
history_1 = model_1.fit(X, y, epochs=epochs, batch_size=batch_size, validation_data=(X_val, y_val), verbose=1, callbacks=callbacks)
end_time = datetime.datetime.now()
duration = end_time - start_time
print(f"duration: {duration}")



plt.figure()
plt.plot(history_1.history['loss'], label='loss 1', c='C0')
plt.plot(history_1.history['val_loss'], label='val 1', c='C0', ls='--')
plt.axhline(1e-05, c='k', ls='--')
plt.yscale('log')
plt.xscale('log')
# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.legend()
plt.tight_layout()
plt.savefig(f'{plot_dir}/history.png')

plt.figure()
plt.plot(history_1.history['lr'], label='lr', c='C0')
plt.yscale('log')
plt.xscale('log')
# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.legend()
plt.tight_layout()
plt.savefig(f'{plot_dir}/lr.png')

# plot 10 random samples
for index in random.sample(range(X.shape[0]), 10 if X.shape[0] >= 10 else 2):
    yhat1 = model_1.predict(X[index].reshape(1,-1))[0]
    plt.figure(figsize=(10,8))
    plt.subplot(2, 1, 1)
    plt.plot(times, y[index], label='data')
    plt.plot(times, yhat1, label='model 1')
    plt.legend()
    # plt.xlim(-100, 100)
    plt.subplot(2, 1, 2)
    plt.plot(times, y[index]-yhat1)
    plt.suptitle(index)
    plt.tight_layout()
    plt.savefig(f"{plot_dir}/{index}.png")
    plt.close()
