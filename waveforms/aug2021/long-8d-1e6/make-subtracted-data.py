"""
load in the alpha, phi_22 and phi_21 data.
subtract off initial value
save data
"""

import numpy as np
import os

files = ["alpha.npy", "phi_22.npy", "phi_21.npy"]

input_dirs = ["training_data", "validation_data"]
output_dirs = ["subtracted_training_data", "subtracted_validation_data"]


for input_dir, output_dir in zip(input_dirs, output_dirs):
    print(f"working: {input_dir}")
    print(f"making output dir: {output_dir}")
    os.makedirs(output_dir, exist_ok=False)

    for file in files:
        print(f"loading: {file}")
        y = np.load(os.path.join(input_dir, file))
        y = y - y[:,0][:,np.newaxis]
        print("saving")
        np.save(os.path.join(output_dir, file),y)

print("done")
