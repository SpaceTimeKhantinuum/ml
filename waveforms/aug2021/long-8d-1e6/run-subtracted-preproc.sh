#!/bin/bash

OUTPUT=data_logs
mkdir -p $OUTPUT

NAME="subtracted_compute_preprocessing"
echo "running ${NAME}.toml"
nohup python ../compute_preprocessing.py -v --config-file ${NAME}.toml > $OUTPUT/${NAME}.out 2> $OUTPUT/${NAME}.err &
