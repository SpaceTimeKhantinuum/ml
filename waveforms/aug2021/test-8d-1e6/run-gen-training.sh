#!/bin/bash

OUTPUT=data_logs
mkdir -p $OUTPUT

NAME="data_generation_coprec_training"
echo "running ${NAME}.toml"
nohup python ../data_generation_coprec.py -v --config-file ${NAME}.toml > $OUTPUT/${NAME}.out 2> $OUTPUT/${NAME}.err &

#NAME="data_generation_coprec_val"
#echo "running ${NAME}.toml"
#nohup python ../data_generation_coprec.py -v --config-file ${NAME}.toml > $OUTPUT/${NAME}.out 2> $OUTPUT/${NAME}.err &
