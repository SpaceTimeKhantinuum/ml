#!/bin/bash

mkdir -p logs

CONFIG="config_009"
#PART="001"
#echo "running $CONFIG part:$PART"
#nohup python ../../fit.py -v --config-file $CONFIG.toml > logs/${CONFIG}_${PART}.out 2> logs/${CONFIG}_${PART}.err &


# to continue training
#PART="002"
PART="003"
echo "running $CONFIG part:$PART"
nohup python ../../fit.py -v --config-file $CONFIG.toml --force > logs/${CONFIG}_${PART}.out 2> logs/${CONFIG}_${PART}.err &
