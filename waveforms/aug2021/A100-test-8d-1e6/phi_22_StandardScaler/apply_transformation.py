import numpy as np
from sklearn.preprocessing import StandardScaler


y = np.load("/home/sebastian.khan/data/wispy/aug2021/test-8d-1e6/training_data/phi_22.npy")

trans = StandardScaler()
y_prime = trans.fit_transform(y)

print(np.mean(y))
print(np.std(y))
print(np.mean(y_prime))
print(np.std(y_prime))

np.save("standardscaler_phi_22.npy", y_prime)

del y
y = np.load("/home/sebastian.khan/data/wispy/aug2021/test-8d-1e6/validation_data/phi_22.npy")
y_prime = trans.transform(y)

print(np.mean(y))
print(np.std(y))
print(np.mean(y_prime))
print(np.std(y_prime))

np.save("standardscaler_val_phi_22.npy", y_prime)

# we also need to set the pre-processing to "1." so that
# it doesn't mess up the StandardScaler
pre = np.load("/home/sebastian.khan/data/wispy/aug2021/test-8d-1e6/pre_processing/preprocessing_params.npz")
pre = dict(pre)
#print(pre)
pre['phi_22'] = np.array(1.)

np.savez("preprocessing_params.npz", **pre)

#pre = dict(np.load("preprocessing_params.npz"))
#print(pre)
