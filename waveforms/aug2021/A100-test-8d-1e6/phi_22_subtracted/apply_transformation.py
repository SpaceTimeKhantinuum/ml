import numpy as np

print("working training")
y = np.load("/home/sebastian.khan/data/wispy/aug2021/test-8d-1e6/training_data/phi_22.npy")
y = y - y[:,0][:,np.newaxis]
np.save("training_data/phi_22.npy", y)

print("working validation")
y = np.load("/home/sebastian.khan/data/wispy/aug2021/test-8d-1e6/validation_data/phi_22.npy")
y = y - y[:,0][:,np.newaxis]
np.save("validation_data/phi_22.npy", y)

print("done")
