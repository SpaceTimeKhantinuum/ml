import numpy as np

beta = np.load("training_data/beta.npy")

new_beta = beta - beta[:,0][:,np.newaxis]

np.save("training_data_sub/beta.npy", new_beta)

beta = np.load("validation_data/beta.npy")

new_beta = beta - beta[:,0][:,np.newaxis]

np.save("validation_data_sub/beta.npy", new_beta)
