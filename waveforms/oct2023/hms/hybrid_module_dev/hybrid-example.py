import matplotlib.pyplot as plt
import lalsimulation as lalsim
import waveform_generator
import phenom
import numpy as np


def generate_pn_waveform(q, modes, M=50, f_min=30, S1z=0, S2z=0, approximant=lalsim.SpinTaylorT1):
    """

    returns a dictionary with keys 't' and 'hlm' where
    'hlm' is a dictionary with keys (l,m).
    the time is in units of M
    and the amplitude of the hlm data have been divided by the
    comman time domain amplitude scaling factor.
    """
    m1, m2 = phenom.m1_m2_M_q(M, q)
    p = waveform_generator.gen_td_modes_wf_params(
            approximant=approximant, m1=m1, m2=m2, f_min=f_min, f_ref=f_min,
            S1z=S1z, S2z=S2z,
        )
    t, hlm = waveform_generator.gen_td_modes_wf(p, modes=modes)
    amp_scale = waveform_generator.td_amp_scale(M, p['r'])
    for lm in hlm.keys():
        hlm[lm] = hlm[lm] / amp_scale
    t_M = phenom.StoM(t, M)
    wf = dict(t=t_M, hlm=hlm)
    return wf

# need a 'generate_nr_waveform' function.
# input: nr filename and modes list
# output: nested dictionary with 't', 'hlm' and also 'metadata'.

#modes = [(2,2),(2,1),(2,0),(3,3),(3,2),(3,1),(4,4),(4,3)]
modes = [(2,2),(2,1),(3,3),(3,2),(4,4),(4,3)]

# could estimate 5,5 mode using phi_22/2 * 5 and the 5,5 amplitude
# expression?


#nr_hdf5_filename="/Users/sebastian.khan/personal/data/SXS_BBH_0180_Res4.h5" #q=1
#nr_hdf5_filename="/Users/sebastian.khan/personal/data/SXS_BBH_0169_Res3.h5" #q=2
nr_hdf5_filename="/Users/sebastian.khan/personal/data/SXS_BBH_0167_Res5.h5" #q=4
#nr_hdf5_filename="/Users/sebastian.khan/personal/data/SXS_BBH_0107_Res5.h5" #q=5
#nr_hdf5_filename="/Users/sebastian.khan/personal/data/SXS_BBH_0063_Res5.h5" #q=8
#nr_hdf5_filename="/Users/sebastian.khan/personal/data/SXS_BBH_0303_Res3.h5" #q=10

dt=0.1
wf_nr = waveform_generator.get_hdf5_strain(nr_hdf5_filename, modes, dt)

print(wf_nr['metadata'])

wf_pn = generate_pn_waveform(q=wf_nr['metadata']['q'], modes=modes, f_min=20)

fig, axs = plt.subplots(len(modes), 1, figsize=(14, len(modes)*4))
for i, lm in enumerate(modes):
    axs[i].plot(wf_pn['t'], wf_pn['hlm'][lm].real)
    axs[i].plot(wf_nr['t'], wf_nr['hlm'][lm].real)
    #axs[i].plot(wf_pn['t'], np.abs(wf_pn['hlm'][lm]))
    #axs[i].plot(wf_nr['t'], np.abs(wf_nr['hlm'][lm]))
plt.show()



# wf1 = gen_waveform(pn)
# wf2 = gen_waveform(nr)

# hybrid = hybridise(wf1, wf2, modes_to_use_to_estimate_hybridisation_parameters=[(2,2),(2,1),(3,3),...])

# default parameters would be
# hybridise the start of NR
# so need to deterine the PN time corresponding to the NR start frequency.

# idea to build a functional code where the main 'object'
# we manipulate will just be a python dictionary
# type(wf) == dict
# wf['times'] = array of times
# wf['hlm'] = dict
# wf['hlm'] with keys [(2,2), (2,1), ...]
# wf['amp'] with keys [(2,2), (2,1), ...]
# wf['phi'] with keys [(2,2), (2,1), ...]
# wf['freq'] with keys [(2,2), (2,1), ...]

# i.e. we input wf and we output the modified wf
# could even have an option to do it in  place or not.
# def apply_shift_and_rotation(wf, dt, dphi, dpsi):
# wf = wf.copy()
# time shift
# wf['times'] = wf['times'] - dt
# rotations
# for lm in wf['hlm'].keys():
# wf['hlm'][lm] * np.exp(1.j*lm[1]*dphi) * np.exp(1.j*2*dsi)
# return wf


