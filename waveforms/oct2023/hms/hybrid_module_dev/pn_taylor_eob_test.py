import pn_taylor_eob
import numpy as np
import matplotlib.pyplot as plt
wf = pn_taylor_eob.generate_pn_waveform_eob(2)
plt.figure()
for mode in wf['hlm'].keys():
    plt.plot(wf['t'][:-1], np.diff(np.unwrap(np.angle(wf['hlm'][mode]))), label=mode)
    #plt.plot(wf['t'], wf['hlm'][mode].real, label=mode)
    #plt.plot(wf['t'], np.abs(wf['hlm'][mode]), label=mode)
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.show()
