from typing import Dict

import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as IUS
from scipy.signal import savgol_filter



def MultimodeHybridModel(x, dt, dphi, dpsi, hlm_nr_x=None, hlm_nr_y=None):
    """
    This function is designed to be used with lmfit.
    It can fit multiple modes by concatenating them.

    Use this to help
    https://lmfit.github.io/lmfit-py/faq.html#how-can-i-fit-multiple-data-sets
    
    input:
        x: time arrray to evaluate model on
        dt: time shift parameter to estimate
        dphi: orbital phase shift parameter to estimate
        dpsi: polarisation angle parameter to estimate
        hlm_nr_x: time array for NR multipoles
        hlm_nr_y: dict of NR multipoles with keys example [(2,2), (2,1), ...]

    returns:
        array where all hlm_nr_y multipole data has been
        concatenated, selecting only the input `x` times
        with the time, phase and polarisation shift applied.
    """
    lms = list(hlm_nr_y.keys())
    i_hlm_real={}
    i_hlm_imag={}
    for k in lms:
        i_hlm_real[k] = IUS(hlm_nr_x, hlm_nr_y[k].real, ext=3)
        i_hlm_imag[k] = IUS(hlm_nr_x, hlm_nr_y[k].imag, ext=3)

    zs = []
    for k in lms:
        m = k[1]
        z = i_hlm_real[k](x+dt) + 1.j*i_hlm_imag[k](x+dt)
        z *= np.exp(1.j*m*dphi)
        z *= np.exp(1.j*2*dpsi)
        zs.append(z)

    return np.concatenate(zs)



def estimate_time_shift(wf1, wf2, mode=(2,2)):
    """
    given then time and frequncy of two waveforms
    find the time in `t1` where the frequency `f1`
    equals the initial frequency of `f2`.
    
    this function is quite strict and will only work
    if f(t) can be inverted.

    input:
        wf1: pn waveform dict with 't' and 'freq'
        wf2: nr waveform dict. use 'mask' key to choose which times to use in calculation
        mode: which mode to. defaults to (2,2)

    returns:
        dt: time shift to apply to t2
        pn_time_of_nr: the time in t1 coordindates where f1 = f2[0]
    """
    # note: most of this code is redundent e.g. we only need
    # to interpolate t(-f) for PN only.
    # also PN waveforms are more likely to be long
    # so we can cut costs by only interpolating a subset of the data.
    # e.g. only the last half.
    wf1 = wf1.copy()
    wf2 = wf2.copy()
    
    t1 = wf1['t']
    f1 = wf1['freq'][mode]
    t2 = wf2['t']
    f2 = wf2['freq'][mode]

    if 'mask' in wf2:
        t2 = t2[wf2['mask']]
        f2 = f2[wf2['mask']]

    ###
    # fit for t(-f)
    half_time = t1[len(t1)//2]
    mask = np.where( (t1 <= half_time) )
    polyorder = 3
    z = np.polyfit(-f1[mask], t1[mask], polyorder)
    itimes1 = np.poly1d(z)
    
    # import matplotlib.pyplot as plt
    # plt.figure()
    # plt.plot(f1[mask], t1[mask])
    # # plt.plot(f1, t1)
    # plt.plot(f1[mask], itimes1(-f1[mask]), ls='--')
    # # plt.axhline(half_time)
    # plt.show()
    # print(itimes1(-f2[0]))

    ###

    # t(-f) for PN waveform
    itimes1 = IUS(-f1, t1)
    # print(itimes1(-f2[0]))
    

    # negative sign because t(-f) interpolation
    # f2[0] can become an input frequency that we want to match at.
    nr_initial_frequency = -f2[0]
    # pn time of nr initial frequency
    pn_time_of_nr = itimes1(nr_initial_frequency)
    # time shift required to shift NR times to align
    # them such that the pn and nr frequencies are equal
    # at the NR initial frequency but on the PN time axis.
    dt_ = t2[0] - pn_time_of_nr
    return dt_, float(pn_time_of_nr)



def estimate_window_from_phase(wf, dt_, t0, n_cycles_before_window=3, n_cyles_in_window=3, mode=(2,2), polyorder=3):
    """
    wf: dict. has 't' and 'phase' keys. use 'mask' key to choose which times to use in calculation
    t0: time of start of waveform
    n_cycles_before_window: number of GW cycles after t0 and then the hybridisation windown starts
    n_cyles_in_window: number of GW cycles in hybridisation window
    polyorder: default 3. polyorder of polynomial to model t(phi) and phi(t). we use the first half of the NR data to fit.

    returns
    win1: start of window (M)
    win2: end of window (M)
    """
    wf = wf.copy()
    times = wf['t']
    phase = wf['phase'][mode]

    #####
    # model t(phi) and phi(t) using the beginning of NR frequency with a polynomial (cubic by default)
    half_time = times[len(times)//2]
    mask = np.where( (times <= half_time) )
    # t(phi)
    z = np.polyfit(phase[mask], times[mask], polyorder)
    i_t_of_phi = np.poly1d(z)

    # phi(t)
    z = np.polyfit(times[mask], phase[mask], polyorder)
    i_phi_of_t = np.poly1d(z)

    phase_at_t0 = float(i_phi_of_t(t0))

    win1_phase = phase_at_t0 - n_cycles_before_window*2*np.pi
    win2_phase = win1_phase - n_cyles_in_window*2*np.pi
    win1 = i_t_of_phi(win1_phase)
    win2 = i_t_of_phi(win2_phase)

    return float(win1), float(win2)


def compute_amplitude(wf: Dict):
    wf = wf.copy()
    amps = {}
    for k in wf['hlm'].keys():
        amps[k] = np.abs(wf['hlm'][k])
    wf['amp'] = amps
    return wf
    
def compute_phase(wf: Dict):
    wf = wf.copy()
    phases = {}
    for k in wf['hlm'].keys():
        phases[k] = np.unwrap(np.angle(wf['hlm'][k]))
    wf['phase'] = phases
    return wf
    
def compute_frequency(wf: Dict, filter_params=None):
    """
    filter_params: dict with keys: 'window_length' in terms of number of samples and 'polyorder'
    """
    wf = wf.copy()
    freqs = {}
    for k in wf['hlm'].keys():
        phase = np.unwrap(np.angle(wf['hlm'][k]))
        freqs[k] = IUS(wf['t'], phase).derivative()(wf['t'])
        if filter_params != None:
            freqs[k] = savgol_filter(freqs[k], filter_params['window_length'], filter_params['polyorder'])            
    wf['freq'] = freqs
    return wf






