# https://moble.github.io/PostNewtonian.jl/dev/interface/high-level/

using PostNewtonian

# Initial values of the masses, spins, and orbital angular frequency
M₁ = 0.6
M₂ = 0.4
χ⃗₁ = [0.7, 0.1, 0.7]
χ⃗₂ = [-0.7, 0.1, 0.7]
Ωᵢ = 0.01

inspiral = orbital_evolution(M₁, M₂, χ⃗₁, χ⃗₂, Ωᵢ)

using Plots  # Requires also installing `Plots` in your project

plot(inspiral.t, inspiral[:χ⃗₁ˣ], label=raw"$\vec{\chi}_1^x$")
plot!(inspiral.t, inspiral[:χ⃗₁ʸ], label=raw"$\vec{\chi}_1^y$")
plot!(inspiral.t, inspiral[:χ⃗₁ᶻ], label=raw"$\vec{\chi}_1^z$")
plot!(xlabel="Time (𝑀)", ylabel="Dimensionless spin components")

t′ = inspiral.t[end]-5_000 : 0.5 : inspiral.t[end]

inspiral = inspiral(t′)

h = inertial_waveform(inspiral)

plot(inspiral.t, real.(h[1, :]), label=raw"$\Re\left\{h_{2,-2}\right\}$")
plot!(inspiral.t, imag.(h[1, :]), label=raw"$\Im\left\{h_{2,-2}\right\}$")
plot!(inspiral.t, abs.(h[1, :]), label=raw"$\left|h_{2,-2}\right|$", linewidth=3)
plot!(xlabel="Time (𝑀)", ylabel="Mode weights", ylim=(-0.5,0.5))
