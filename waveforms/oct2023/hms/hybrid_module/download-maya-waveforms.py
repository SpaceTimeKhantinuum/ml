# https://mayawaves.github.io/mayawaves/html/source/notebooks/catalog_utils.html
from mayawaves.utils.catalogutils import Catalog
from mayawaves.utils.catalogutils import Parameter as p
import os
import sys
import pandas as pd
from tqdm import tqdm

RUN = False

if RUN is False:
    print("RUN set to False, exiting.")
    sys.exit()
else:
    print("Running...")


# save path
MAYA_ROOT_SAVE_PATH = "/Users/sebastian.khan/personal/data/2024/MAYA"
MAYA_DATA_SAVE_PATH = os.path.join(MAYA_ROOT_SAVE_PATH, "data")
MAYA_METADATA_SAVE_PATH = os.path.join(MAYA_ROOT_SAVE_PATH, "metadata")
os.makedirs(MAYA_DATA_SAVE_PATH, exist_ok=True)
os.makedirs(MAYA_METADATA_SAVE_PATH, exist_ok=True)


# init catalog
catalog = Catalog()

# find all non-precessing simulations
non_precessing_sims = catalog.nonspinning_simulations + catalog.aligned_spin_simulations


# loop over all non-prec sims
sim_names = []
all_sim_params = []
for sim in non_precessing_sims:
    # get simulation parameters
    sim_pars = catalog.get_parameters_for_simulation(str(sim))
    # get parameters to filter on
    # here we want long-ish simulations and non-eccentric ones
    merger_time = sim_pars[p.MERGE_TIME]
    eccentricity = sim_pars[p.ECCENTRICITY]
    if (merger_time > 1000) and (eccentricity < 0.01):
        # save simulation name and metadata
        sim_names.append(sim)
        d_ = {par.value: value for par, value in sim_pars.items()}
        all_sim_params.append(d_)


# metadata table for simulations
df = pd.DataFrame(all_sim_params)
df["sim_name"] = sim_names
# save metadata as csv
df.to_csv(os.path.join(MAYA_METADATA_SAVE_PATH, "metadata.csv"), index=False)

# now loop over all simulation in the metadata table and download them in lvc format
for i in tqdm(range(len(df))):
    sim_name = df.iloc[i]["sim_name"]
    print(f"downloading sim_name: {sim_name}")
    catalog.download_waveforms(
        sim_name, save_wf_path=MAYA_DATA_SAVE_PATH, lvcnr_format=True
    )
