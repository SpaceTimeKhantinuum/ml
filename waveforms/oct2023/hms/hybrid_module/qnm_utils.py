import numpy as np
import qnm
import phenom

def get_angular_ringdown_frequency_from_bbh(q, l, m, n=0):
    eta = phenom.eta_from_q(q)
    remnant_spin = phenom.remnant.FinalSpin0815(eta, 0, 0)
    e_rad = phenom.remnant.EradRational0815(eta, 0, 0)

    mc = qnm.modes_cache(s=-2,l=l,m=m,n=n)
    omega, _, _ = mc(a=remnant_spin)
    remnant_mass = (1 - e_rad)
    return -np.real(omega) / remnant_mass


def get_angular_damping_frequency_from_bbh(q, l, m, n=0):
    eta = phenom.eta_from_q(q)
    remnant_spin = phenom.remnant.FinalSpin0815(eta, 0, 0)
    e_rad = phenom.remnant.EradRational0815(eta, 0, 0)

    mc = qnm.modes_cache(s=-2,l=l,m=m,n=n)
    omega, _, _ = mc(a=remnant_spin)
    remnant_mass = (1 - e_rad)
    return -np.imag(omega) / remnant_mass


def get_mixing_coefficient(q, lp, l, m, n=0):
    """
    refs: https://arxiv.org/abs/1212.5553v2 eq 14 (and around there)
    should double check this more carefully


    lp = l' (spherical l?)

    for the 32 mode we have
    h32_S = s_l' h32_Y + h22_Y

    https://qnm.readthedocs.io/en/latest/README.html#spherical-spheroidal-decomposition
    Here ℓmin=max(|m|,|s|) and ℓmax can be chosen at run time. The C coefficients are returned as a complex ndarray,
    with the zeroth element corresponding to ℓmin. To
    avoid indexing errors, you can get the ndarray of ℓ values by calling qnm.angular.ells, e.g.

    ells = qnm.angular.ells(s=-2, m=2, l_max=mc.l_max)
    """
    eta = phenom.eta_from_q(q)
    remnant_spin = phenom.remnant.FinalSpin0815(eta, 0, 0)
    e_rad = phenom.remnant.EradRational0815(eta, 0, 0)


    mc = qnm.modes_cache(s=-2,l=l,m=m,n=n)
    _, _, C = mc(a=remnant_spin)
    ells = qnm.angular.ells(s=-2, m=m, l_max=mc.l_max)
    C_dict = {ells[i]:C[i] for i in range(len(ells))}

    slp = C_dict[lp]
    sl = C_dict[l]

    return slp/sl
