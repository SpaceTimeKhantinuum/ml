# https://sxs.readthedocs.io/en/stable/tutorials/00-Introduction/
import sxs
import os
import sys
from tqdm import tqdm
import shutil
import glob

# Parameters
# ----------
RUN = True
# SXS_ROOT_SAVE_PATH = "/Users/sebastian.khan/personal/data/2024/SXS"
SXS_ROOT_SAVE_PATH = "/Volumes/ancient/2024-non-precessing-nr-data/SXS"
SXS_CACHE_PATH = "/Users/sebastian.khan/.sxs/cache"
extrapolation_order = 4

if RUN is False:
    print("RUN set to False, exiting.")
    sys.exit()
else:
    print("Running...")


# save path
SXS_DATA_SAVE_PATH = os.path.join(SXS_ROOT_SAVE_PATH, "data")
SXS_METADATA_SAVE_PATH = os.path.join(SXS_ROOT_SAVE_PATH, "metadata")
os.makedirs(SXS_DATA_SAVE_PATH, exist_ok=True)
os.makedirs(SXS_METADATA_SAVE_PATH, exist_ok=True)

catalog = sxs.load("catalog")

df = catalog.table  # pyright: ignore

# select BBH
df = df[df["object_types"] == "BHBH"]

# filter, non-eccentric and non-precessing
mask = (
    (df["reference_eccentricity"] < 0.001)
    & (df["reference_chi1_perp"] < 1e-3)
    & (df["reference_chi2_perp"] < 1e-3)
)
df = df[mask]
df = df.reset_index().rename(columns={"index": "sim_name"})  # pyright:ignore

# Save metadata
df.to_csv(os.path.join(SXS_METADATA_SAVE_PATH, "metadata.csv"), index=False)


# download waveforms
for i in tqdm(range(len(df))):
    sim_name = str(df.iloc[i]["sim_name"])
    w = sxs.load(f"{sim_name}/Lev/rhOverM", extrapolation_order=extrapolation_order)
    src = glob.glob(os.path.join(SXS_CACHE_PATH, sim_name + "*"))[0]
    print(src)
    dest = os.path.join(SXS_DATA_SAVE_PATH)
    shutil.move(src, dest)

print("done")
