# import warnings
# warnings.filterwarnings("ignore", "Wswiglal-redir-stdio")

import lal
import lalsimulation as lalsim

import h5py
from scipy.interpolate import InterpolatedUnivariateSpline as IUS
import numpy as np
import phenom

# from my old code: https://gitlab.com/SpaceTimeKhantinuum/ml/-/blob/master/waveforms/oct2022/uncertainty/utils.py

# SEOBNRv5
import pyseobnr.generate_waveform


def td_amp_scale(mtot, distance):
    """
    mtot in solar masses
    distance in m
    M*G/c^2 * M_sun / dist
    """
    return mtot * lal.MRSUN_SI / distance


def gen_td_modes_wf_params(
        m1=50,
        m2=50,
        S1x=0,
        S1y=0,
        S1z=0,
        S2x=0,
        S2y=0,
        S2z=0,
        distance=1,
        deltaT=1/4096,
        phiRef=0.,
        f_min=10,
        f_ref=10,
        LALpars=None,
        approximant=lalsim.SEOBNRv4P,
        lmax_dummy=4):
    """
    distance: meters
    lmax_dummy {int: 2}: old option for XLALSimInspiralChooseTDModes.
    """

    if approximant in ['SEOBNRv5HM', 'SEOBNRv5PHM']:
        # pyseobnr
        distance_Mpc = distance / lal.PC_SI / 1e6
        p = {
            "mass1": m1,
            "mass2": m2,
            "spin1x": S1x,
            "spin1y": S1y,
            "spin1z": S1z,
            "spin2x": S2x,
            "spin2y": S2y,
            "spin2z": S2z,
            "deltaT": deltaT,
            "f22_start": f_min,
            "phi_ref": phiRef,
            "distance": distance_Mpc,
            "inclination": 0,
            # "f_max": 0.5 / deltaT, # this is the default value in pyseobnr i.e. Nyquist
            "approximant": approximant,
        }

    else:
        # lalsuite
        p = dict(
            m1=m1,
            m2=m2,
            S1x=S1x,
            S1y=S1y,
            S1z=S1z,
            S2x=S2x,
            S2y=S2y,
            S2z=S2z,
            phiRef=phiRef,
            r=distance, # in metres
            deltaT=deltaT,
            f_min=f_min,
            f_ref=f_ref,
            LALpars=LALpars,
            lmax=lmax_dummy,
            approximant=approximant)

    return p


def gen_td_modes_wf(p, modes=[(2, 2)]):
    """
    use this to generate PN waveforms with lalsimulation
    
    input:
        p {dict} normally the output of gen_td_modes_wf_params
        modes {list of 2-tuples: [(2,2)]}
            modes to generate
            Note: Depending on the waveform model used
            you might need to explicitly provide both positive and
            negative modes.
    returns:
        times {array} in units of seconds
        hlms {dict} contains time domain hlm modes
    """
    p = p.copy()

    if (2, 2) not in modes:
        raise NotImplementedError("(2,2) mode not in modes.\
Currently we assume that this mode exists.")

    if p['LALpars'] is None:
        p['LALpars'] = lal.CreateDict()

    # amplitude order?
    # lalsim.SimInspiralWaveformParamsInsertPNAmplitudeOrder(p['LALpars'], -1)

    ma = lalsim.SimInspiralCreateModeArray()
    for l, m in modes:
        lalsim.SimInspiralModeArrayActivateMode(ma, l, m)
    lalsim.SimInspiralWaveformParamsInsertModeArray(p['LALpars'], ma)

    M = p['m1'] + p['m2']
    p.update({'m1': p['m1']*lal.MSUN_SI})
    p.update({'m2': p['m2']*lal.MSUN_SI})

    hlms_lal = lalsim.SimInspiralChooseTDModes(**p)

    hlms = {}

    for l, m in modes:
        tmp = lalsim.SphHarmTimeSeriesGetMode(hlms_lal, l, m)
        if l == 2 and m == 2:
            length_22 = tmp.data.length
            dt_22 = tmp.deltaT
            epoch_22 = tmp.epoch
        hlms.update({(l, m): tmp.data.data})

    assert p['deltaT'] == dt_22, f"input deltaT = {p['deltaT']} does not match waveform dt = {dt_22}."

    t = np.arange(length_22) * dt_22 + float(epoch_22)

    return t, hlms




def get_hdf5_strain(nr_hdf5_filename, modes, dt):
    f = h5py.File(nr_hdf5_filename, 'r')

    eta = f.attrs['eta']
    # this try/except is here because the GTech waveforms
    # use 'irreducible_mass1' instead of 'mass1'..
    try:
        q = f.attrs['mass1']/f.attrs['mass2']
        Mtotal = f.attrs['mass1']+f.attrs['mass2']
    except:
        q = f.attrs['irreducible_mass1']/f.attrs['irreducible_mass2']
        Mtotal = f.attrs['irreducible_mass1']+f.attrs['irreducible_mass2']

    #print((f.attrs.keys()))
    spin1z = f.attrs['spin1z']
    spin2z = f.attrs['spin2z']

    amp_x={}
    amp_y={}
    phase_x={}
    phase_y={}
    for lm in modes:

        amp_tmp = f['amp_l{0}_m{1}'.format(lm[0], lm[1])]
        amp_x[lm] = amp_tmp['X'][()]
        amp_y[lm] = amp_tmp['Y'][()]

        phase_tmp = f['phase_l{0}_m{1}'.format(lm[0], lm[1])]
        phase_x[lm] = phase_tmp['X'][()]
        phase_y[lm] = phase_tmp['Y'][()]

    f.close()

    t1 = max(amp_x[2,2][0], phase_x[2,2][0])
    t2 = min(amp_x[2,2][-1], phase_x[2,2][-1])

    times = np.arange(t1, t2, dt)

    hlm = {}
    for lm in modes:
        amp_i = IUS(amp_x[lm], amp_y[lm])
        phase_i = IUS(phase_x[lm], phase_y[lm])

        amp = amp_i(times)
        phase = phase_i(times)

        hlm[lm] = amp * np.exp(1.j * phase)

    # TODO: output f.attrs as metadata
    metadata = {
        'q':q,
        'filename':nr_hdf5_filename,
        'M':Mtotal,
        'spin1z':spin1z,
        'spin2z':spin2z,
    }
    wf = dict(t=times, hlm=hlm, metadata=metadata)
    return wf






def generate_waveform(q, modes, M=50, f_min=30, S1z=0, S2z=0, approximant=lalsim.SpinTaylorT1, deltaT=1/4096, phiRef=0, f_ref=None):
    """
    input: mass-ratio: >= 1
    modes: list of [(l,m)]
    M: total mass, default 50, we scale out this
    f_min: start frequency in Hz, default 30
    S1z: default 0
    S2z: default 0

    returns a dictionary with keys 't' and 'hlm' where
    'hlm' is a dictionary with keys (l,m).
    the time is in units of M
    and the amplitude of the hlm data have been divided by the
    comman time domain amplitude scaling factor.
    """
    if f_ref == None:
        f_ref = f_min
    m1, m2 = phenom.m1_m2_M_q(M, q)
    p = gen_td_modes_wf_params(
            approximant=approximant, m1=m1, m2=m2, f_min=f_min, f_ref=f_ref, phiRef=phiRef,
            S1z=S1z, S2z=S2z,
            deltaT=deltaT,
        )
    
    if approximant in ['SEOBNRv5HM', 'SEOBNRv5PHM']:
        # pyseobnr
        p.update({'mode_array':modes})
        wfm_gen = pyseobnr.generate_waveform.GenerateWaveform(p)
        # Generate mode dictionary
        t, hlm = wfm_gen.generate_td_modes()
        # convert Mpc to metres
        r = p['distance'] * lal.PC_SI * 1e6
    else:
        # lalsuite
        t, hlm = gen_td_modes_wf(p, modes=modes)
        r = p['r']
        
    amp_scale = td_amp_scale(M, r)
    for lm in hlm.keys():
        hlm[lm] = hlm[lm] / amp_scale
    t_M = phenom.StoM(t, M)
    wf = dict(t=t_M, hlm=hlm)
    return wf
