"""
module to find nr simulations
"""

import os

def find_files(root_dir:str, extension:str):
  """
  This function searches through a directory and its subdirectories for files with the extension ".bbh".

  Args:
      root_dir (str): The directory to start searching from.
      extension (str): Search for files with the given extension. e.g. "bbh"

  Yields:
      str: The path to a file with the given `extension`
  """
  for dirpath, dirnames, filenames in os.walk(root_dir):
    for filename in filenames:
      if filename.endswith(extension):
        yield os.path.join(dirpath, filename)

# Example usage
root_dir = "/Volumes/ancient/"
#root_dir = "/Volumes/ancient/get-nr-data/bam/data/gluster/gw-data/NR_data/backup-minion-data/ReducedData"
#extension = 'bbh'
#extension = 'hdf5'
extension = 'h5'
for fil in find_files(root_dir, extension):
  print(f"Found {extension} file: {fil}")
