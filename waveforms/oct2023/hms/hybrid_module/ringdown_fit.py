import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as IUS
import lmfit

import waveform_utils as wu
import qnm_utils as qu

def ComplexExponentiallyDampedSinusoid(x, t0, phi0, amp, omega, decay):
    """
    x: time coord
    t0: time shift
    phi0: phase at x-t0 = 0
    amp: amplitude at x-t0 = 0
    omega: angular frequency
    decay: decay constant i.e. angular damping time == 1/angular damping frequency
    post_fn:
        either np.real or np.imag to get real or imag part
    """
    # y = amp * np.exp(-(x-t0)*decay) * np.exp(1.j * ((x-t0)*omega + phi0))

    sigma = omega + 1.j*decay

    y = amp * np.exp(1.j * (sigma*(x-t0) + phi0))
    return y


def ConstantComplexExponential(x, p, zeta):
    """
    x: time coord
    p: amplitude
    zeta: phase
    """
    return np.ones(len(x)) * p * np.exp(1.j*zeta)


def InterpolatedData(x, data=None, domain=None):
    """
    Note: you have to set initial paramters data, domain to None to work with lmfit.Model
    as these are not parameters to fit but extra options
    """
    if data.dtype == complex:
        # interpolate real and imag
        iy_real = IUS(domain, data.real)
        iy_imag = IUS(domain, data.imag)
        y = iy_real(x) + 1.j*iy_imag(x)
    elif data.dtype == float:
        # interpolate
        iy = IUS(domain, data)
        y = iy(x)
    else:
        raise ValueError(f'data.dtype = {data.dtype} unsupported')

    return y

def amplitude_at_time(t, hlm, t0):
    """
    t: time array.
    hlm: complex array.
    t0: value to compute |hlm| at.

    interpolate the amplitude of the complex data hlm
    and evaluate it at the time t0.
    """
    # initial amplitude we can know/guess
    amp0 = float(IUS(t, np.abs(hlm))(t0))
    return amp0


def prepare_fit(wf: wu.Waveform, l, m, start_time, end_time):
    """
    This function prepares the data for a ringdown fit.

    For the given (l,m) mode select points where the time
    is in the domain [start_time, end_time] (called the fit domain)

    We return the times and the complex data hlm
    that are in the fit domain.
    """
    # define data to fit
    x = wf.times
    y = wf.hlms[l,m]

    # mask for fit domain
    mask = np.where((x >= start_time) & (x <= end_time))
    x=x[mask]
    y=y[mask]

    return x, y


def fit_lm_mode(wf: wu.Waveform, l, m, q, start_time, end_time, n_tries):
    """
    Use this function to fit a single ComplexExponentiallyDampedSinusoid
    to the data
    """
    x, y = prepare_fit(wf, l, m, start_time, end_time)
    amp0 = amplitude_at_time(x, y, start_time)
    model = lmfit.Model(ComplexExponentiallyDampedSinusoid)
    omega = qu.get_angular_ringdown_frequency_from_bbh(q, l, m)
    decay = qu.get_angular_damping_frequency_from_bbh(q, l, m)
    # try multiple times to avoid bad initial guess
    frs=[]
    for i in range(n_tries):
        params = model.make_params(
                t0=dict(value=start_time, min=-10, vary=False),
                amp=dict(value=amp0, min=0),
                omega=dict(value=omega, vary=False),
                decay=dict(value=decay, vary=False),
                phi0=dict(value=np.random.uniform(-np.pi, np.pi), min=-np.pi, max=np.pi),
        )

        # fit
        fr = model.fit(y, params, x=x)
        frs.append(fr)

    # get index of fit with the smallest chi-square statistic
    best_idx = np.argmin([frs[i].chisqr for i in range(len(frs))])

    return frs[best_idx]


def fit_lm_mixed_mode(fit_result_lm_mode: lmfit.model.ModelResult, wf: wu.Waveform, lp, l, m, q, start_time, end_time, n_tries):
    """
    Use this function to fit modes that have mode mixing.
    e.g. the (3,2) and (2,2)

    which is the (lp, m) mode and the (l, m) mode.

    fit_result_lm_mode: this is the output from the `fit_lm_mode` function.
    """
    x, y = prepare_fit(wf, lp, m, start_time, end_time)
    amp0 = amplitude_at_time(x, y, start_time)

    s_mixing = qu.get_mixing_coefficient(q, lp, l, m)
    p_value = np.abs(s_mixing)
    Zeta_value = np.angle(s_mixing)

    ## parent mode data
    ## use best fit
    # domain = fit_result_lm_mode.userkws['x']
    # np.testing.assert_array_equal(x, domain)
    # data = fit_result_lm_mode.best_fit
    ## or 
    ## eval model
    data = fit_result_lm_mode.eval(x=x)
    
    model = lmfit.Model(ComplexExponentiallyDampedSinusoid) \
        + lmfit.Model(ConstantComplexExponential) \
        * lmfit.Model(InterpolatedData, data=data, domain=x)


    omega = qu.get_angular_ringdown_frequency_from_bbh(q, lp, m)
    decay = qu.get_angular_damping_frequency_from_bbh(q, lp, m)
    
    # try multiple times to avoid bad initial guess
    frs=[]
    for i in range(n_tries):
        params = model.make_params(
                t0=dict(value=start_time, min=-30, max=50, vary=False),
                phi0=dict(value=np.random.uniform(-np.pi, np.pi), min=-np.pi, max=np.pi, vary=True),
                amp=dict(value=amp0, min=1e-8, vary=True),
                omega=dict(value=omega, vary=False),
                decay=dict(value=decay, vary=False),
                p=dict(value=p_value, min=1e-6, max=1, vary=False),
                zeta=dict(value=Zeta_value, min=-np.pi, max=np.pi, vary=False),
        )

        # fit
        fr = model.fit(y, params, x=x)
        frs.append(fr)

    # get index of fit with the smallest chi-square statistic
    best_idx = np.argmin([frs[i].chisqr for i in range(len(frs))])

    return frs[best_idx]
