import pathlib
import re
import numpy as np
import phenom
import ffi_utils

def convert_string_to_number(input_str):
    """
    https://chat.openai.com/c/9840ebc1-dea8-4724-8c54-7821b2c90820
    """
    try:
        # Try to convert to an integer first
        result = int(input_str)
        return result
    except ValueError:
        try:
            # Try to convert to a float if it's not an integer
            result = float(input_str)
            return result
        except ValueError:
            # It's not a valid integer or float, so keep it as a string
            return input_str
        
class BAMSimulation:
    def __init__(self, simulation_directory):
        """
        simulation_directory: path to the directory containing the .bbh file
        
        This class is an abstraction of the Psi4 mode data from a BAM simulation.
        
        Useful attributes:
        
        - self.sections is a dictionary with 'metadata' and 'body-data' keys. however, the (key,values) have been converted to class attributes
        - access a list of filenames for each extraction raidus using self.r{i} e.g. self.r1, self.r2
        - the dictionary self.extraction_radii tells you the radius in M for each r1, r2, ...
        
        """
        self.simulation_directory = pathlib.Path(simulation_directory)
        self.extracted_simulation_name = self.simulation_directory.name
        # extracted_simulation_name is the name of the parent directory, we will compare this with self.simulation_name which
        # is found using the metadata.
        try:
            self.par_file = list(self.simulation_directory.glob(f'{self.extracted_simulation_name}.bbh'))[0]
        except:
            raise ValueError("Couldn't fine .bbh file")
        # assert len()
        self.parse_par_file()
        
        self.convert_section_dict_to_attributes('metadata')
        self.convert_section_dict_to_attributes('body-data')
        
        assert self.simulation_name == self.extracted_simulation_name, 'simulation names do not match'
        
        self.compute_derived_quantities()
        self.set_psi4_times()
        self.set_waveform_duration()
        self.set_strain_times()

    def extract_metadata_from_psi3come_filename(self, filename):
        """
        ty chatgpt
        
        given 'Psi4ModeDecomp/psi3col.r1.l4.l2.m-2.gz'
        extract r1, l4, l2, m-2 from it
        """
        # extraction radius
        pattern = r'r\d+'
        r_value = re.findall(pattern, filename)[0]
        # extraction level and ell multipole
        pattern = r'l\d+'
        l_values = re.findall(pattern, filename)
        lev_value = l_values[0]
        ell_value = l_values[1]
        # mm multipole
        pattern = r'm-?\d+'
        mm_value = re.findall(pattern, filename)[0]

        # key-value combined
        d = {
            'r':r_value,
            'l':lev_value,
            'ell':ell_value,
            'm':mm_value
        }

        return d
    
    
    def parse_par_file(self):
        # Define the file's name.
        filename = self.par_file
        # Open the file and read its content.
        with open(filename) as f:
            content = f.readlines()

        sections={}
        section_counter = 0
        section_names=[]
        section_dicts={}
        keys=[]
        for i, line in enumerate(content):
            if line[0] == '[':
                # first check if we have key,value to save
                if len(keys) > 0:
                    section_dict = {key:value for key, value in zip(keys,values)}
                    section_dicts[section_name] = section_dict


                # if the line starts with a '[' then we know wew have started a new section
                # and we ensure the name is unique with a counter index
                # section names are encoded as '__section_name__'
                section_name = line.strip().split('[')[1:][0].split(']')[0]
                section_name = "__" + section_name + f"__{section_counter}"
                section_counter += 1
                section_names.append(section_name)

                # new section so we setup new lists to store keys and values for
                # lines in this section
                keys=[]
                values=[]

                # go to next line in file
                continue

            # we decode "key = value" in the following way
            # we assume that valid lines with have a len(split('='))==2
            key_value_string = line
            key_value_string = key_value_string.split('=')
            if len(key_value_string) != 2:
                continue
            keys.append(key_value_string[0].strip())
            values.append(key_value_string[1].strip())

            # edge-case: we need to save the keys and values if we are at the end of the file
            if i == len(content)-1:
                section_dict = {key:value for key, value in zip(keys,values)}
                section_dicts[section_name] = section_dict

        # collect results
        sections={}
        for section_name in section_names:
            sections[section_name]=section_dicts[section_name]


        # get extration radii map
        # relate the extration radius number (key) to the extraction radius in M (value)
        rename_section_map={}
        r_map = {}
        section_names_to_keep=[]
        for k in sections.keys():
            if 'psi4t-data' in k:
                rM = int(sections[k].pop('extraction-radius'))
                d = self.extract_metadata_from_psi3come_filename(sections[k]['2,2'])
                rNumber = d['r']
                r_map[rNumber] = rM 
                new_name = k.split('__')[1] + f"-{rNumber}"
                rename_section_map[k] = new_name
                setattr(self, rNumber, sections[k])
            else:
                new_name = k.split('__')[1]
                rename_section_map[k] = new_name
                section_names_to_keep.append(k)
        self.extraction_radii = r_map
        
        # find largest extraction radius
        ks = list(self.extraction_radii.keys())
        vs = list(self.extraction_radii.values())
        self.largest_extraction_radius = ks[np.argmax(vs)]
        self.largest_extraction_radius_value = vs[np.argmax(vs)]
        
        

        # rename sections to make them nicer
        new_sections = {rename_section_map[k]:sections[k] for k in section_names_to_keep}
        self.sections = new_sections.copy()
        del new_sections
        
        
        # loop over psi4 file names and combine with parent directory
        # for r in r_map.keys():
        #     d = getattr(self, r)
        #     for k in d.keys():
        #         d[k] = str(pathlib.Path(self.simulation_directory, d[k]))
        
        
        
    def set_psi4_times(self):
        """
        there should only be one time array so lets extract that
        note that the times from the strain are slightly different
        """
        self.psi4_times, _ = self.load_psi4(2,2)
        
        
    def set_strain_times(self):
        """
        time array from strain
        """
        self.strain_times, _ = self.load_strain(2,2)
        
    
    def set_waveform_duration(self):
        """
        how long in 'M' is the simulation
        """
        assert hasattr(self, 'psi4_times'), "run self.set_psi4_times()"
        self.duration = self.psi4_times[-1]
        
        
    def load_psi4(self, l, m, r=None, return_3col=False):
        """
        r='r1'
        l=2
        m=2
        
        if r is None then defaults to largest extraction radius
        """
        if r == None:
            r = self.largest_extraction_radius
        lm=f'{l},{m}'
        psi4_filename = getattr(self, r)[lm]
        psi4_path = str(pathlib.Path(self.simulation_directory, psi4_filename))
        if return_3col:
            return np.loadtxt(psi4_path)
        else:
            times, re_psi4, im_psi4 = np.loadtxt(psi4_path, unpack=True)
            psi4 = re_psi4 - 1.j*im_psi4
            return times, psi4
                
        
    def load_strain(self, l, m, r=None, safety_factor=0.2):
        """
        runs load_psi4 but additionally computes strain using FFI method
        
        safety_factor is the number that multiplies self.EstStartAngFreq22
        which is will be the frequency cut-off parameter in FFI
        """
        f0 = safety_factor * self.EstStartAngFreq22
        psi4 = self.load_psi4(l, m, r, return_3col=True)
        strain = ffi_utils.psi4ToStrain(psi4, f0)
        # at some point I realised to conj BAM data
        # is this still true for higher modes?
        times = strain[:,0].real
        strain = strain[:,1].conj()
        
        return times, strain
        
        
    def convert_section_dict_to_attributes(self, name):
        """
        name is either 'metadata' or 'body-data'
        
        this converts the (key,value) pairs from self.sections
        into class attributes and converts them to either
        int, float or leave as str.
        """

        for k in self.sections[name].keys():
            v = self.sections[name][k]
            assert type(v) == str, 'values must be strings to being with'
            # 
            v_new = convert_string_to_number(v)
            # if (type(v_new) == str and len(v_new) == 0):
            #     v_new = None
            k_new = k.replace('-','_')
            setattr(self, k_new, v_new)
            
            
    def compute_derived_quantities(self):
        """
        computes things like total mass, mass-ratio etc
        """
        if self.mass1 > self.mass2:
            self.mass1_is_primary=True
            self.mass2_is_primary=False
            self.primary_mass = self.mass1
            self.secondary_mass = self.mass2
        else:
            self.mass1_is_primary=False
            self.mass2_is_primary=True
            self.primary_mass = self.mass2
            self.secondary_mass = self.mass1
        self.total_mass = self.mass1 + self.mass2
        
        # check that the total mass is 1, within error
        np.testing.assert_almost_equal(self.total_mass, 1, decimal=7, err_msg='total mass is not equal to 1')
        
        self.mass_ratio = self.primary_mass / self.secondary_mass
        self.eta = phenom.eta_from_q(self.mass_ratio)
        
        # estimate of the start angular frequency of the 22 mode
        self.EstStartAngFreq22 = ffi_utils.getCutoffFrequencyFromTwoPuncturesBBH(self.par_file, 1)