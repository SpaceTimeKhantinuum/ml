# Any python imports you need go here
import numpy as np
import matplotlib.pyplot as plt

# Start the Julia session
from juliacall import Main as jl

# Import `PostNewtonian` in the Julia session
jl.seval("using PostNewtonian")

# Declare some parameters
delta = 0.0
chi1 = np.array([0.1, 0.2, 0.3])
chi2 = np.array([-0.2, 0.1, -0.3])
Omega_i = 0.01

# Call into Julia to run some function
w = jl.GWFrames.PNWaveform("TaylorT1", delta, chi1, chi2, Omega_i)

# to see what attributes `w` has use `dir`
# dir(w)
# w.data will have a shape of (number of modes, number of time samples)
# however, when you do w.data.to_numpy() it looks like it's actually (time, modes)

w_np = w.data.to_numpy()

plt.figure()
plt.plot(w.t, np.abs(w_np[:,0]), label='2,-2')
plt.plot(w.t, np.abs(w_np[:,4]), label='2,2')
plt.legend()
plt.show()

# Plot the magnitudes of all the modes as functions of time
# plt.semilogy(w.t, np.abs(w.data))
# plt.show()
