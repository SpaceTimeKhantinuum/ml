# domain decomposition ROM

instead of modelling the entire space first do a domain decomposition
e.g. unsupervised tree or kmeans and then fit a separate model in each domain


```
conda create --name domain python=3.9
conda activate domain
conda install -c apple tensorflow-deps
pip install tensorflow-macos
pip install tensorflow-metal
pip install pandas matplotlib numpy pycbc lalsuite forked-rompy scikit-learn scipy corner jupyterlab gw-phenom
pip install k-means-constrained
```
