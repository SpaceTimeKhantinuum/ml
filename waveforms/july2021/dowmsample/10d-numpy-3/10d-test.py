"""
loads in the very large e.g. 1e9 row training set and random downsamples by 1000 rows
saves data as a numpy npy
"""
import numpy as np
import tensorflow as tf
import tqdm
import os
from multiprocessing import Pool
import functools
import glob
import uuid
import datetime
import random


def _bytes_feature(value):
    """Returns a bytes_list from a string / byte."""
    if isinstance(value, type(tf.constant(0))):  # if value ist tensor
        value = value.numpy()  # get value of tensor
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _float_feature(value):
    """Returns a floast_list from a float / double."""
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def _int64_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def serialize_array(array):
    array = tf.io.serialize_tensor(array)
    return array


def parse_single_sample(x, y):
    """
    x: X_train
    y: y_train
    """

    # define the dictionary -- the structure -- of our single example
    data = {
        'x': _bytes_feature(serialize_array(x)),
        'y': _bytes_feature(serialize_array(y)),
    }

    # create an Example, wrapping the single features
    out = tf.train.Example(features=tf.train.Features(feature=data))

    return out


def v3_func_to_map(task, filename, splits, out_dir):
    x = task[0]
    y = task[1]
    unique_fname = str(uuid.uuid4())[:8]
    current_shard_name = os.path.join(
        out_dir, "{}_of_{}_{}.tfrecords".format(unique_fname, splits, filename))
    writer = tf.io.TFRecordWriter(current_shard_name)
    # print(f"\n{unique_fname}", flush=True)
    for index in range(len(x)):
        current_x = x[index]
        current_y = y[index]
        # create the required Example representation
        out = parse_single_sample(x=current_x, y=current_y)
        writer.write(out.SerializeToString())
    writer.close()
    return unique_fname


def v3_pre_chunk_write_training_data_to_sharded_tf_record(x, y, filename: str = "test-records", splits: int = 10, out_dir: str = "./", n_cpu=1):
    """
    x: X_train
    y: y_train
    splits: number of shards
    """

    num_samples = len(x)

    indicies = np.arange(num_samples)
    split_indicies = np.array_split(indicies, splits)
    print(
        f"\nUsing {splits} shard(s) with something like {len(split_indicies[0])} samples in each shard", flush=True)

    x = np.array_split(x, splits)
    y = np.array_split(y, splits)

    task = [(x[i], y[i]) for i in range(splits)]

    with Pool(n_cpu) as p:
        p.map(functools.partial(v3_func_to_map, filename=filename,
              splits=splits, out_dir=out_dir), task)

    return


if __name__ == "__main__":

    tf.config.threading.set_intra_op_parallelism_threads(1)
    tf.config.threading.set_inter_op_parallelism_threads(1)

    output_dir = "train"
    os.makedirs(f"{output_dir}", exist_ok=False)

    splits = 1
    downsample_by = 10
    print(f"downsample_by: {downsample_by}", flush=True)

    print("loading X_train", flush=True)
    X_train = np.load("../../10d-test/processed_training_data_1e6/X_train.npy")
    # X_train = X_train
    print("loading y_train", flush=True)
    y_train = np.load("../../10d-test/processed_training_data_1e6/y_train.npy")
    # y_train = y_train

    print("sizes before downsampling", flush=True)
    print(f"X_train.shape: {X_train.shape}",flush=True)
    print(f"y_train.shape: {y_train.shape}",flush=True)

    n_samples = len(X_train)
    print(f"n_samples = {n_samples}", flush=True)
    k=int(n_samples / downsample_by)
    print(f"k = {k}", flush=True)
    mask = np.array(random.sample(range(n_samples), k=k))
    X_train = X_train[mask]
    y_train = y_train[mask]

    print("sizes after downsampling", flush=True)
    print(f"X_train.shape: {X_train.shape}",flush=True)
    print(f"y_train.shape: {y_train.shape}",flush=True)

    np.save(os.path.join(output_dir, "X_train.npy"), X_train)
    np.save(os.path.join(output_dir, "y_train.npy"), y_train)

    del X_train
    del y_train

    output_dir = "val"
    os.makedirs(f"{output_dir}", exist_ok=False)

    X_val = np.load("../../10d-test/processed_training_data_1e3/X_val.npy")
    y_val = np.load("../../10d-test/processed_training_data_1e3/y_val.npy")

    np.save(os.path.join(output_dir, "X_val.npy"), X_val)
    np.save(os.path.join(output_dir, "y_val.npy"), y_val)

    del X_val
    del y_val

    print("done!", flush=True)
