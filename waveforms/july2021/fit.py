#!/usr/bin/env python

"""
example
python fit.py -v --config-file config.toml (--force to continue training)

checkpoints:
https://www.tensorflow.org/guide/keras/train_and_evaluate#checkpointing_models
"""

from compute_preprocessing import apply_pre_process_forward
from tomlkit import parse
from tensorflow.keras.utils import get_custom_objects
import pugna.activations
import pugna.layers
import pugna.models
import wispy.mscalev2
import wispy.mscalev3
import wispy.mscalev4
import wispy.mscalev5
import wispy.model_utils
import wispy.callbacks
import wispy.utils
import wispy.logger
import wispy.resnet
import wispy
import subprocess
import datetime
import argparse
import sys
import glob
import os
import numpy as np
import math
import random
import pickle
import tensorflow_addons as tfa
import tensorflow as tf
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.use("agg")

# from cycler import cycler
# from itertools import cycle

# mpl.rcParams.update(mpl.rcParamsDefault)
plt.style.use("ggplot")
mpl.rcParams.update({"font.size": 16})


# import multiprocessing as mp
# import functools


# https://stackoverflow.com/a/59789336/12840171
# this is the way to add custome activations - do not do
# get_custom_objects().update({'srelu': tf.keras.layers.Activation(pugna.activations.sReLU)})
# get_custom_objects().update({'s2relu': tf.keras.layers.Activation(pugna.activations.s2relu)})
# instead to
# otherwise problems with saving and loading
get_custom_objects().update({'srelu': pugna.activations.sReLU})
get_custom_objects().update({'s2relu': pugna.activations.s2relu})


def check_gpu():
    logger.info("running 'tf.config.list_physical_devices('GPU')'")
    logger.info(tf.config.list_physical_devices("GPU"))

    try:
        logger.info("running 'nvidia-smi -L'")
        subprocess.call(["nvidia-smi", "-L"])
    except FileNotFoundError:
        logger.info("could not run 'nvidia-smi -L'")


def set_gpu_memory_growth():
    gpus = tf.config.list_physical_devices('GPU')
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            logger.info("running: tf.config.experimental.set_memory_growth")
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            logger.info(e)


def get_uncompiled_model(model_name, model_params_dict):
    assert model_name in ['resnet', 'mscalev2', 'mscalev1', 'mscalev3', 'mscalev4', 'mscalev5']

    if model_name == 'resnet':
        model = wispy.resnet.ResNet(**model_params_dict)
    elif model_name == 'mscalev2':
        model = wispy.mscalev2.build_model(**model_params_dict)
    elif model_name == 'mscalev1':
        model = pugna.models.mscalednn.build_model_2(**model_params_dict)
    elif model_name == 'mscalev3':
        model = wispy.mscalev3.build_model(**model_params_dict)
    elif model_name == 'mscalev4':
        model = wispy.mscalev4.build_model(**model_params_dict)
    elif model_name == 'mscalev5':
        model = wispy.mscalev5.build_model(**model_params_dict)

    return model


def get_compiled_model(model_name, model_params_dict, extra_params_dict):
    model = get_uncompiled_model(
        model_name=model_name, model_params_dict=model_params_dict)

    optimizer = tf.keras.optimizers.Adam(
        extra_params_dict['lr'], amsgrad=extra_params_dict['amsgrad'])

    model.compile(loss=extra_params_dict['loss'],
                  optimizer=optimizer, metrics=extra_params_dict['metrics'])

    return model


def get_checkpoints(checkpoint_dir):
    checkpoints = [checkpoint_dir + "/" +
                   name for name in os.listdir(checkpoint_dir)]
    return checkpoints


def make_or_restore_model(model_name, model_params_dict, extra_params_dict, checkpoint_dir):
    # Either restore the latest model, or create a fresh one
    # if there is no checkpoint available.
    checkpoints = get_checkpoints(checkpoint_dir)
    if checkpoints:
        latest_checkpoint = max(checkpoints, key=os.path.getctime)
        logger.info(f"Restoring from {latest_checkpoint}")
        return tf.keras.models.load_model(latest_checkpoint)
    logger.info("Creating a new model")
    return get_compiled_model(model_name, model_params_dict, extra_params_dict)


def parse_tfr_element(element, dtype=tf.float64):
    data = {
        'x': tf.io.FixedLenFeature([], tf.string),
        'y': tf.io.FixedLenFeature([], tf.string),
    }

    content = tf.io.parse_single_example(element, data)
    # content = tf.io.parse_example(element, data)

    x = content['x']
    y = content['y']

    x = tf.io.parse_tensor(x, out_type=dtype)
    y = tf.io.parse_tensor(y, out_type=dtype)

    return (x, y)


# def load_dataset(filenames, cache=False):
def load_dataset(filenames):
    ignore_order = tf.data.Options()
    ignore_order.experimental_deterministic = False  # disable order, increase speed
    dataset = tf.data.TFRecordDataset(
        filenames, buffer_size=1000000000 #1000MB in bytes
    )  # automatically interleaves reads from multiple files
    dataset = dataset.with_options(
        ignore_order
    )  # uses data as soon as it streams in, rather than in its original order
    dataset = dataset.map(
        parse_tfr_element, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    # if cache:
        # dataset = dataset.cache()
    # dataset = dataset.cache()
    return dataset


def get_dataset(filenames, batch_size, shuffle_size, buffer_size):
    dataset = load_dataset(filenames)
    # options = tf.data.Options()
    # options.experimental_threading.max_intra_op_parallelism = 1
    # dataset = dataset.with_options(options)
    dataset = dataset.shuffle(shuffle_size)
    dataset = dataset.batch(batch_size)
    dataset = dataset.prefetch(buffer_size=buffer_size)
    return dataset


class GetSequenceDataset(tf.keras.utils.Sequence):
    """
    https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
    https://biswajitsahoo1111.github.io/post/reading-multiple-files-in-tensorflow-2-using-sequence/#sec_2
    """
    def __init__(self, X_filenames, y_filenames, batch_size, shuffle=True):
        self.X_filenames=X_filenames
        self.y_filenames=y_filenames
        self.batch_size=batch_size
        self.shuffle=shuffle
        self.on_epoch_end()

    def __len__(self):
        return int(np.ceil(len(self.X_filenames) / float(self.batch_size)))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        X_filenames_batch = [self.X_filenames[k] for k in indexes]
        y_filenames_batch = [self.y_filenames[k] for k in indexes]

        # Generate data
        X, y = self.__data_generation(X_filenames_batch, y_filenames_batch)
        Xshape = X.shape
        yshape = y.shape

        return X.reshape(Xshape[0]*Xshape[1], Xshape[2]), y.reshape(yshape[0]*yshape[1], yshape[2])
        # return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.X_filenames))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, X_filenames_batch, y_filenames_batch):
        # Generate data
        X=[]
        y=[]
        for i in range(len(X_filenames_batch)):
            X.append(np.load(X_filenames_batch[i]))
            y.append(np.load(y_filenames_batch[i]))

        return np.asarray(X), np.asarray(y)

class DataGenv2(tf.keras.utils.Sequence):
    def __init__(self, X, t, y, X_batch_size, t_batch_size, shuffle=True):
        """
        X: array of physical parameters
        t: array of times
        y: array of targets
        X_batch_size: number of samples to take from X
        t_batch_size: number of samples to take from t
            total mini-batch size is `X_batch_size`*`t_batch_size`
            
        in this function we just take batches of the physical parameters
        and randomly add `t_batch_size` samples to it.
        
        https://medium.com/analytics-vidhya/write-your-own-custom-data-generator-for-tensorflow-keras-1252b64e41c3
        https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
        """
        self.X = X
        self.t = t
        self.y = y
        self.lenX = len(self.X)
        self.lenT = len(self.t)
        self.batch_size = X_batch_size
        self.t_batch_size = t_batch_size
        self.shuffle = shuffle
        
        # init self.indexes
        self.on_epoch_end()
        
    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(self.lenX)
        if self.shuffle == True:
            np.random.shuffle(self.indexes)
    
    def __getitem__(self, idx):
        
        x_start_idx = idx * self.batch_size
        x_end_idx = (idx + 1) * self.batch_size
        indicies = self.indexes[x_start_idx:x_end_idx]
        
        # an random batch of data
        x_batch = self.X[indicies]
        
        # get random indicies for time data
        t_batch_idx = random.sample(range(self.lenT), self.t_batch_size)
        
        t_batch = self.t[t_batch_idx]
        
        # `ones` is used to broadcast the physical parameters to the same shape as the times so that we can concat them
        ones = np.ones(shape=(t_batch.shape[0], x_batch.shape[1]))    

        X_train = []
        for x in x_batch:
            sub_matrix = np.array([x]) * ones
            X_train.append(np.concatenate((t_batch, sub_matrix), axis=1))
        X_train = np.asarray(X_train)
        X_train = X_train.reshape(X_train.shape[0]*X_train.shape[1], X_train.shape[2])
        y_train = self.y[indicies][:,t_batch_idx].reshape(-1,1)
        
        return X_train, y_train
    
    def __len__(self):
        return math.ceil(self.lenX / self.batch_size)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="""read in training data and run model.fit()""",
    )

    parser.add_argument(
        "--config-file", type=str, help="path to workflow config.toml file"
    )
    parser.add_argument(
        "-v",
        help="""increase output verbosity
        no -v: WARNING(")
        -v: INFO
        -vv: DEBUG""",
        action="count",
        dest="verbose",
        default=0,
    )
    parser.add_argument(
        "--force",
        action="store_true",
        help="if given then will not throw an error if output directory exists",
    )

    overall_start_time = datetime.datetime.now()

    args = parser.parse_args()
    args_dict = vars(args)

    # https://stackoverflow.com/questions/14097061/easier-way-to-enable-verbose-logging
    level = min(2, args.verbose)  # capped to number of levels
    logger = wispy.logger.init_logger(level=level)

    logger.info(f"Using TensorFlow v{tf.__version__}")
    logger.info(f"wispy version: {wispy.__version__}")
    logger.info(f"PID: {os.getpid()}")

    logger.info("==========")
    logger.info("printing command line args")
    for k in args_dict.keys():
        logger.info(f"{k}: {args_dict[k]}")
    logger.info("==========")

    with open(args.config_file, "r") as f:
        text = f.read()

    doc = parse(text)

    logger.info("==========")
    logger.info("printing toml config contents")
    wispy.utils.recursive_dict_print(doc, print_fn=logger.info)
    logger.info("==========")

    if doc["gpu"]["CUDA_VISIBLE_DEVICES"]:
        logger.info("setting CUDA_VISIBLE_DEVICES")
        os.environ["CUDA_VISIBLE_DEVICES"] = doc["gpu"]["CUDA_VISIBLE_DEVICES"]
        if doc["gpu"]["CUDA_VISIBLE_DEVICES"] != "-1":
            check_gpu()
            set_gpu_memory_growth()

    logger.info(f"making output dir: {doc['output']}")
    os.makedirs(f"{doc['output']}", exist_ok=args.force)

    if doc['data']['type'] == "dataset":
        logger.info("using 'type': 'dataset'")
        if 'sample_weight' in doc.keys():
            logger.error(
                "sample_weight not currently compatible with 'type'='dataset'")
            logger.error("Exiting")
            sys.exit(1)

        if 'buffer_size' in doc['fit'].keys():
            buffer_size = doc['fit']['buffer_size']
        else:
            buffer_size = tf.data.experimental.AUTOTUNE

        # "./train/*.tfrecords"
        logger.info("running 'get_dataset' for training data")
        filenames = glob.glob(
            doc['data']['train_dataset_pattern'], recursive=False)
        train_dataset = get_dataset(
            filenames, doc['fit']['batch_size'], doc['fit']['shuffle_size'], buffer_size)
        logger.info("running 'get_dataset' for validation data")
        filenames = glob.glob(
            doc['data']['validation_dataset_pattern'], recursive=False)
        validation_dataset = get_dataset(
            filenames, doc['fit']['batch_size'], doc['fit']['shuffle_size'], buffer_size)
        logger.info("setting input_shape and output_shape")
        input_shape = doc['data']['input_shape']
        output_shape = doc['data']['output_shape']

    elif doc['data']['type'] == 'sequence':
        logger.info("using 'type': 'sequence'")
        if 'sample_weight' in doc.keys():
            logger.error(
                "sample_weight not currently compatible with 'type'='sequence'")
            logger.error("Exiting")
            sys.exit(1)

        X_filenames = glob.glob(
                doc['data']['X_train_dataset_pattern'], recursive=False)
        logger.info(f"found {len(X_filenames)} files")
        y_filenames = glob.glob(
                doc['data']['y_train_dataset_pattern'], recursive=False)
        logger.info(f"found {len(y_filenames)} files")
        # build training dataset sequence
        train_dataset = GetSequenceDataset(X_filenames=X_filenames, y_filenames=y_filenames, batch_size=doc['fit']['batch_size'], shuffle=True)

        X_filenames = glob.glob(
                doc['data']['X_validation_dataset_pattern'], recursive=False)
        logger.info(f"found {len(X_filenames)} files")
        y_filenames = glob.glob(
                doc['data']['y_validation_dataset_pattern'], recursive=False)
        logger.info(f"found {len(y_filenames)} files")
        # build validation dataset sequence
        validation_dataset = GetSequenceDataset(X_filenames=X_filenames, y_filenames=y_filenames, batch_size=doc['fit']['batch_size'], shuffle=False)

        logger.info("setting input_shape and output_shape")
        input_shape = doc['data']['input_shape']
        output_shape = doc['data']['output_shape']

    elif doc['data']['type'] == 'sequence-2':
        logger.info("using 'type': 'sequence-2'")
        if 'sample_weight' in doc.keys():
            logger.error(
                "sample_weight not currently compatible with 'type'='sequence-2'")
            logger.error("Exiting")
            sys.exit(1)
        # load coords
        filename = doc['data']['coords_train_file']
        logger.info(f"loading coords file: {filename}")
        coords_train = np.load(filename)
        coords_train = coords_train.T

        # load times data
        filename = doc['data']['times_file']
        logger.info(f"loading times file: {filename}")
        times = np.load(filename)

        pkl_filename = doc['data']['times_scaler']
        with open(pkl_filename, 'rb') as file:
            times_scaler = pickle.load(file)

        logger.info("transforming times data")
        times_scaled = times_scaler.transform(times[:, np.newaxis])

        filename = doc['data']['y_train_file']
        logger.info(f"loading y-train file: {filename}")
        y_train_raw = np.load(filename)

        logger.info("setting input_shape and output_shape")
        input_shape = coords_train.shape[1] + 1 # add one for time
        output_shape = 1

        filename = doc['data']['y_preprocessing_params_file']
        logger.info(f"loading y-preprocessing-params file: {filename}")
        y_processing_params = np.load(filename)

        logger.info("applying preprocessing to raw training data")
        y_train = apply_pre_process_forward(
            y_train_raw, y_processing_params['mean'], y_processing_params['max'])

        del y_train_raw


        logger.info("making training dataset")
        train_dataset = DataGenv2(
                X=coords_train,
                t=times_scaled,
                y=y_train,
                X_batch_size=doc['fit']['batch_size'],
                t_batch_size=doc['fit']['t_batch_size'],
                shuffle=True)

        del coords_train
        del y_train

        logger.info('loading validation data')

        # load coords
        filename = doc['data']['coords_val_file']
        logger.info(f"loading coords validation file: {filename}")
        coords_val = np.load(filename)
        coords_val = coords_val.T

        filename = doc['data']['y_val_file']
        logger.info(f"loading y-validation file: {filename}")
        y_val_raw = np.load(filename)

        logger.info("applying preprocessing to raw validation data")
        y_val = apply_pre_process_forward(
            y_val_raw, y_processing_params['mean'], y_processing_params['max'])

        del y_val_raw

        logger.info("making validation dataset")
        validation_dataset = DataGenv2(
                X=coords_val,
                t=times_scaled,
                y=y_val,
                X_batch_size=doc['fit']['batch_size'],
                t_batch_size=doc['fit']['t_batch_size'],
                shuffle=False)

        del coords_val
        del y_val


    elif doc['data']['type'] == "numpy":
        logger.info("using 'type': 'numpy'")
        filename = doc['data']['X_train']
        logger.info(f"loading X-train file: {filename}")
        X_train = np.load(filename)

        filename = doc['data']['y_train']
        logger.info(f"loading y-train file: {filename}")
        y_train = np.load(filename)
        y_train = y_train.ravel().reshape(-1, 1)

        logger.info('loading validation data')

        filename = doc['data']['X_val']
        logger.info(f"loading X-validation file: {filename}")
        X_val = np.load(filename)

        filename = doc['data']['y_val']
        logger.info(f"loading y-validation file: {filename}")
        y_val = np.load(filename)
        y_val = y_val.ravel().reshape(-1, 1)

        logger.info(f"X_train.shape: {X_train.shape}")
        logger.info(f"y_train.shape: {y_train.shape}")

        logger.info(f"X_val.shape: {X_val.shape}")
        logger.info(f"y_val.shape: {y_val.shape}")

        validation_data = (X_val, y_val)

        if 'sample_weight' in doc.keys():
            sample_weight = np.ones(len(X_train))
            mask = (X_train[:, 0] > doc['sample_weight']['t_start']) & (
                X_train[:, 0] <= doc['sample_weight']['t_end'])
            sample_weight[mask] = doc['sample_weight']['weight']
        else:
            sample_weight = None

        input_shape = X_train.shape[1]
        output_shape = y_train.shape[1]

    common_params_dict = dict(
        input_shape=input_shape,
        output_shape=output_shape
    )

    model_name = doc['model']['model_name']
    logger.info(f"model_name: {model_name}")

    model_params_dict = {
        **common_params_dict,
        **doc['model'][model_name]
    }

    if model_name in ["mscalev3", "mscalev4", "mscalev5"]:
        logger.info("figuring out dtype...")
        assert model_params_dict['dtype'] in [
            'float32', 'float64'], "unknow dtype"
        if model_params_dict['dtype'] == 'float32':
            model_params_dict['dtype'] = np.float32
        if model_params_dict['dtype'] == 'float64':
            model_params_dict['dtype'] = np.float64

    logger.info("==========")
    logger.info("printing model_params_dict contents")
    wispy.utils.recursive_dict_print(model_params_dict, print_fn=logger.info)
    logger.info("==========")

    checkpoint_dir = os.path.join(doc['output'], 'checkpoint')
    logger.info(f"making checkpoint dir: {checkpoint_dir}")
    # Prepare a directory to store all the checkpoints.
    # os.makedirs(f"{checkpoint_dir}", exist_ok=args.force)
    os.makedirs(f"{checkpoint_dir}", exist_ok=True)

    model = make_or_restore_model(
        model_name=doc['model']['model_name'],
        model_params_dict=model_params_dict,
        extra_params_dict=doc['fit'],
        checkpoint_dir=checkpoint_dir)

    model.summary()

    logger.info("setting up callbacks")

    callbacks = []
    if doc['callbacks']['lrs'] == 'ReduceLROnPlateau':
        logger.info("adding ReduceLROnPlateau")
        reduce_lr_callback = tf.keras.callbacks.ReduceLROnPlateau(
            **doc['callbacks']['ReduceLROnPlateau'])
        callbacks.append(reduce_lr_callback)
    elif doc['callbacks']['lrs'] == 'CosineDecayRestarts':
        logger.info("adding CosineDecayRestarts")
        learning_rate_fn = tf.keras.experimental.CosineDecayRestarts(
            **doc['callbacks']['CosineDecayRestarts'])
        lrs_callback = tf.keras.callbacks.LearningRateScheduler(
            learning_rate_fn)
        callbacks.append(lrs_callback)

    if 'threshold' in doc['callbacks'].keys():
        logger.info("adding threshold")
        threshold_callback = wispy.callbacks.ThresholdCallback(
            doc['callbacks']['threshold']['threshold'])
        callbacks.append(threshold_callback)

    if 'checkpoint' in doc['callbacks'].keys():
        logger.info("setting up tf.keras.callbacks.ModelCheckpoint")
        checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
            filepath=os.path.join(checkpoint_dir, "cp-{epoch:05d}.ckpt"),
            **doc['callbacks']['checkpoint']
        )
        callbacks.append(checkpoint_callback)

    checkpoints = get_checkpoints(checkpoint_dir)
    if checkpoints:
        latest_checkpoint = max(checkpoints, key=os.path.getctime)
        initial_epoch = int(latest_checkpoint.split(
            '/')[-1].split('-')[1].split('.')[0])
    else:
        initial_epoch = 0

    logger.info(f"initial_epoch = {initial_epoch}")

    output_file = os.path.join(doc['output'], 'training.log')
    logger.info(f"setting up CSV logger: {output_file}")
    csv_logger = tf.keras.callbacks.CSVLogger(output_file, append=True)
    callbacks.append(csv_logger)

    if len(callbacks) == 0:
        callbacks = None

    logger.info("making args for model.fit")
    if doc['data']['type'] == "dataset":
        fit_kwargs = dict(
            x=train_dataset,
            epochs=doc['fit']['epochs'],
            validation_data=validation_dataset,
            callbacks=callbacks,
            initial_epoch=initial_epoch,
        )
    elif doc['data']['type'] == "sequence":
        fit_kwargs = dict(
            x=train_dataset,
            epochs=doc['fit']['epochs'],
            validation_data=validation_dataset,
            callbacks=callbacks,
            initial_epoch=initial_epoch,
        )
    elif doc['data']['type'] == "sequence-2":
        fit_kwargs = dict(
            x=train_dataset,
            epochs=doc['fit']['epochs'],
            validation_data=validation_dataset,
            callbacks=callbacks,
            initial_epoch=initial_epoch,
        )
    elif doc['data']['type'] == "numpy":
        fit_kwargs = dict(
            x=X_train,
            y=y_train,
            epochs=doc['fit']['epochs'],
            validation_data=validation_data,
            batch_size=doc['fit']['batch_size'],
            callbacks=callbacks,
            initial_epoch=initial_epoch,
            sample_weight=sample_weight
        )

    logger.info("starting fit")
    starttime = datetime.datetime.now()
    history = model.fit(**fit_kwargs)

    endtime = datetime.datetime.now()
    duration = endtime - starttime

    logger.info("fit complete")
    logger.info(f"The time cost: {duration}")

    logger.info("saving model using 'SavedModel' format")
    filename = os.path.join(doc['output'], "model")
    model.save(f"{filename}")

    logger.info("saving model using 'HDF5' format")
    filename = os.path.join(doc['output'], "model.h5")
    model.save(f"{filename}")

    if history.history['mse']:
        logger.info("plotting history")
        plt.figure(figsize=(14, 4))
        plt.subplot(1, 2, 1)
        plt.plot(history.history['mse'], label='mse')
        plt.yscale('log')
        plt.legend()

    if history.history['val_mse']:
        plt.subplot(1, 2, 2)
        plt.plot(history.history['val_mse'], label='val_mse')
        plt.yscale('log')
        plt.legend()

    if history.history['mse']:
        filename = os.path.join(f"{doc['output']}", 'loss.png')
        plt.savefig(filename)
        plt.close()

    filename = os.path.join(f"{doc['output']}", "history.pickle")
    logger.info(f"saving history: {filename}")
    wispy.model_utils.save_history(history.history, filename)

    filename = os.path.join(f"{doc['output']}", "duration.pickle")
    logger.info(f"saving duration: {filename}")
    wispy.model_utils.save_datetime(duration, filename)

    overall_end_time = datetime.datetime.now()
    overall_duration = overall_end_time - overall_start_time
    logger.info(f"total time: {overall_duration}")
    logger.info("finished!")
