"""
https://www.tensorflow.org/tutorials/load_data/tfrecord#tfrecord_files_in_python

https://towardsdatascience.com/a-practical-guide-to-tfrecords-584536bc786c
https://colab.research.google.com/drive/1xU_MJ3R8oj8YYYi-VI_WJTU3hD1OpAB7?usp=sharing#scrollTo=rmTv61HFAv57
"""
import numpy as np
import tensorflow as tf
import tqdm
import os
from multiprocessing import Pool
import functools
import glob
import uuid
import datetime


def _bytes_feature(value):
    """Returns a bytes_list from a string / byte."""
    if isinstance(value, type(tf.constant(0))):  # if value ist tensor
        value = value.numpy()  # get value of tensor
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _float_feature(value):
    """Returns a floast_list from a float / double."""
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def _int64_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def serialize_array(array):
    array = tf.io.serialize_tensor(array)
    return array


def parse_single_sample(x, y):
    """
    x: X_train
    y: y_train
    """

    # define the dictionary -- the structure -- of our single example
    data = {
        'x': _bytes_feature(serialize_array(x)),
        'y': _bytes_feature(serialize_array(y)),
    }

    # create an Example, wrapping the single features
    out = tf.train.Example(features=tf.train.Features(feature=data))

    return out


def v3_func_to_map(task, filename, splits, out_dir):
    x = task[0]
    y = task[1]
    unique_fname = str(uuid.uuid4())[:8]
    current_shard_name = os.path.join(
        out_dir, "{}_of_{}_{}.tfrecords".format(unique_fname, splits, filename))
    writer = tf.io.TFRecordWriter(current_shard_name)
    # print(f"\n{unique_fname}", flush=True)
    for index in range(len(x)):
        current_x = x[index]
        current_y = y[index]
        # create the required Example representation
        out = parse_single_sample(x=current_x, y=current_y)
        writer.write(out.SerializeToString())
    writer.close()
    return unique_fname


def v3_pre_chunk_write_training_data_to_sharded_tf_record(x, y, filename: str = "test-records", splits: int = 10, out_dir: str = "./", n_cpu=1):
    """
    x: X_train
    y: y_train
    splits: number of shards
    """

    num_samples = len(x)

    indicies = np.arange(num_samples)
    split_indicies = np.array_split(indicies, splits)
    print(
        f"\nUsing {splits} shard(s) with something like {len(split_indicies[0])} samples in each shard", flush=True)

    x = np.array_split(x, splits)
    y = np.array_split(y, splits)

    task = [(x[i], y[i]) for i in range(splits)]

    with Pool(n_cpu) as p:
        p.map(functools.partial(v3_func_to_map, filename=filename,
              splits=splits, out_dir=out_dir), task)

    return


if __name__ == "__main__":

    tf.config.threading.set_intra_op_parallelism_threads(1)
    tf.config.threading.set_inter_op_parallelism_threads(1)

    output_dir = "train"
    os.makedirs(f"{output_dir}", exist_ok=False)

    splits = 1000

    X_train = np.load("../../10d-test/processed_training_data_1e6/X_train.npy")
    X_train = X_train
    y_train = np.load("../../10d-test/processed_training_data_1e6/y_train.npy")
    y_train = y_train

    print(f"samples per shard: {X_train.shape[0]/splits}", flush=True)

    print("working training", flush=True)
    start_time = datetime.datetime.now()
    v3_pre_chunk_write_training_data_to_sharded_tf_record(
        X_train, y_train, filename="train-records", splits=splits, out_dir=f"{output_dir}", n_cpu=20)
    print("completed training processing", flush=True)
    end_time = datetime.datetime.now()
    duration = end_time - start_time
    print(f"total time: {duration}", flush=True)

    del X_train
    del y_train

    output_dir = "val"
    os.makedirs(f"{output_dir}", exist_ok=False)

    splits = 1

    X_val = np.load("../../10d-test/processed_training_data_1e3/X_val.npy")
    y_val = np.load("../../10d-test/processed_training_data_1e3/y_val.npy")

    print("working val", flush=True)
    start_time = datetime.datetime.now()
    v3_pre_chunk_write_training_data_to_sharded_tf_record(
        X_val, y_val, filename="val-records", splits=splits, out_dir=f"{output_dir}", n_cpu=1)
    print("completed validation processing", flush=True)
    end_time = datetime.datetime.now()
    duration = end_time - start_time
    print(f"total time: {duration}", flush=True)

    del X_val
    del y_val

    print("done!", flush=True)
