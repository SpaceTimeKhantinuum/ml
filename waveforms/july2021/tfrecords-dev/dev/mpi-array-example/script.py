"""[summary]

Using this script to develop a method to
take a large array, split it up into
relatively even sized pieces, send them to
different processes and inside each process
it saves it in a different file.

I think I have to use something like MPI to do this.


$ mpiexec -n 4 python script.py

http://education.molssi.org/parallel-programming/03-distributed-examples-mpi4py/index.html

"""

from mpi4py import MPI
import numpy as np
import os

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

# init data for all ranks
X = None

# X_split is a list of arrays
# it is relatively equal split of the `X` array
# used to send the data to specific ranks
X_split = None

# indicies of X_split to send to each rank
# a list of length size
# each element of the list is an array
# whose elements are the indicies of X_split that each rank
# should opporate on
split_idx = None
splits = 10

# make data on rank 0
data_size = 100
if rank == 0:
    X = np.random.normal(size=data_size)
    print(f"X.shape: {X.shape}")
    X_split = np.array_split(X, splits)
    print(f"len(X_split): {len(X_split)}")

    split_idx = np.arange(splits)
    # size - 1 because we don't do work on rank = 0
    split_idx = np.array_split(split_idx, size-1)
    print(f"split_idx: {split_idx}")

    # loop over other rank and send information to them
    for i in range(1, size):
        print(f"i = {i}")
        print(f"sending from 0 to rank {i}")
        js = split_idx[i-1]
        for j in js:
            print(f"j : {j}")
            comm.send(X_split[j], dest=i)
            comm.wait()

for i in range(1, size):
    data = comm.recv(source=0)
    print(f"rank: {rank}, data: {data}")

print(f"rank: {rank}, splits: {splits}")


# The below code will cause each rank to iterate over particles with a stride
# of world_size and an initial offset of
# my_rank. For example, if you run on 4 ranks, rank 0 will
# iterate over particles 0, 4, 8, 12, etc., while rank 1 will
# iterate over particles 1, 5, 9, 13, etc.
# for i in range(rank, splits, size):
#     print(f"rank: {rank}, i: {i}")

# # scatter the data to all ranks (including rank 0)
# X = comm.scatter(X, root=0)

# print(f"rank: {rank}, X.shape: {X}")
