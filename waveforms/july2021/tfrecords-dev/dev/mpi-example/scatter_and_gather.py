"""[summary]

mpiexec -n 4 python scatter_and_gather.py
"""

from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

# generate large array of data on RANK 0
numData = 1000000
data = None

if rank == 0:
    data = np.random.normal(loc=10, size=numData)

# init emtpy arrays to receive the partial data
# 'd' == double
partial = np.empty(int(numData/size), dtype='d')

# send data to workers
comm.Scatter(data, partial, root=0)

# create array on rank 0 to receive the processed data
reduced = None
if rank == 0:
    reduced = np.empty(size, dtype='d')

# average the partial arrays and then gather them to rank 0
# so the average of each chunk
p_avg = np.average(partial)
print(f"rank: {rank}: p_avg: {p_avg}, partial.shape: {partial.shape}")
comm.Gather(p_avg, reduced, root=0)

# then average the averages
if rank == 0:
    print(f'Full average: {np.average(reduced)}')
