import os
from mpi4py import MPI

comm = MPI.COMM_WORLD

size = comm.Get_size()

rank = comm.Get_rank()

PID = os.getpid()

if rank == 0:
    print(f"size: {size}")
print(f"rank: {rank} has PID: {PID}")
