from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
    data = dict(
        key1=[1, 2, 3],
        key2=('abc', 'xyz')
    )
else:
    data = None

data = comm.bcast(data, root=0)

# append rank ID to the data
data['key1'].append(rank)

print(f"Rank: {rank}, data: {data}")
