from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
    msg = 'hello world'
    # msg = np.array([1, 2, 3, 3])
    comm.send(msg, dest=1)
elif rank == 1:
    s = comm.recv()
    print(f'rank {rank}: {s}')
    print(type(s))
