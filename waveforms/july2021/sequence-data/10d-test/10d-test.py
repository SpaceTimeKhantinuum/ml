"""
https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly


this script reads in the very large single data file and
splits it into many smaller data files (.npy)

"""
import numpy as np
import tensorflow as tf
import tqdm
import os
from multiprocessing import Pool
import functools
import glob
import uuid
import datetime




def v3_func_to_map(task, filename, splits, out_dir):
    x = task[0]
    y = task[1]
    unique_fname = str(uuid.uuid4())[:8]
    X_current_shard_name = os.path.join(
        out_dir, "X_{}_of_{}_{}.npy".format(unique_fname, splits, filename))
    y_current_shard_name = os.path.join(
        out_dir, "y_{}_of_{}_{}.npy".format(unique_fname, splits, filename))
    np.save(X_current_shard_name, x)
    np.save(y_current_shard_name, y)

    return unique_fname


def v3_pre_chunk_write_training_data_to_sharded_tf_record(x, y, filename: str = "test-records", splits: int = 10, out_dir: str = "./", n_cpu=1):
    """
    x: X_train
    y: y_train
    splits: number of shards
    """

    num_samples = len(x)

    indicies = np.arange(num_samples)
    split_indicies = np.array_split(indicies, splits)
    print(
        f"\nUsing {splits} shard(s) with something like {len(split_indicies[0])} samples in each shard", flush=True)

    x = np.array_split(x, splits)
    y = np.array_split(y, splits)

    task = [(x[i], y[i]) for i in range(splits)]

    with Pool(n_cpu) as p:
        p.map(functools.partial(v3_func_to_map, filename=filename,
              splits=splits, out_dir=out_dir), task)

    return


if __name__ == "__main__":

    tf.config.threading.set_intra_op_parallelism_threads(1)
    tf.config.threading.set_inter_op_parallelism_threads(1)

    output_dir = "train"
    os.makedirs(f"{output_dir}", exist_ok=False)

    splits = 100000

    X_train = np.load("../../10d-test/processed_training_data_1e6/X_train.npy")
    X_train = X_train
    y_train = np.load("../../10d-test/processed_training_data_1e6/y_train.npy")
    y_train = y_train

    print(f"samples per shard: {X_train.shape[0]/splits}", flush=True)

    print("working training", flush=True)
    start_time = datetime.datetime.now()
    v3_pre_chunk_write_training_data_to_sharded_tf_record(
        X_train, y_train, filename="train", splits=splits, out_dir=f"{output_dir}", n_cpu=30)
    print("completed training processing", flush=True)
    end_time = datetime.datetime.now()
    duration = end_time - start_time
    print(f"total time: {duration}", flush=True)

    del X_train
    del y_train

    output_dir = "val"
    os.makedirs(f"{output_dir}", exist_ok=False)

    splits = 1000

    X_val = np.load("../../10d-test/processed_training_data_1e3/X_val.npy")
    y_val = np.load("../../10d-test/processed_training_data_1e3/y_val.npy")

    print("working val", flush=True)
    start_time = datetime.datetime.now()
    v3_pre_chunk_write_training_data_to_sharded_tf_record(
        X_val, y_val, filename="val", splits=splits, out_dir=f"{output_dir}", n_cpu=1)
    print("completed validation processing", flush=True)
    end_time = datetime.datetime.now()
    duration = end_time - start_time
    print(f"total time: {duration}", flush=True)

    del X_val
    del y_val

    print("done!", flush=True)
