import numpy as np
import glob
import os


def rename_files(pattern="train/X*.npy"):
    files = glob.glob(pattern)
    max_files = len(files)

    for i, fil in enumerate(files, 1):
        base, after = fil.split('/')
        split = after.split('_')
        split[1] = str(i)

        new_name = "_".join(split)

        new_name = os.path.join(base, new_name)

        # print(f"old: {fil}")
        # print(f"new: {new_name}")

        os.rename(fil, new_name)


# pattern="train/X*.npy"
# print(f"working: {pattern}")
# rename_files(pattern=pattern)

# pattern="train/y*.npy"
# print(f"working: {pattern}")
# rename_files(pattern=pattern)

pattern="val/X*.npy"
print(f"working: {pattern}")
rename_files(pattern=pattern)

pattern="val/y*.npy"
print(f"working: {pattern}")
rename_files(pattern=pattern)
