Here I try separating out the time and the other physical parameters.

Using an Mscale network for the time and another network for the physical parameters.

The networks will be joined in some way though so they should not be entirely independent


generate training data
```
./run-gen-training.sh
./run-gen-validation.sh
./run-preprocessing.sh
./run-create-processed-training.sh
```