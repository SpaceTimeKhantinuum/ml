#!/bin/bash

mkdir -p logs

CONFIG="config_001"
echo "running $CONFIG"
nohup python ../fit.py -v --config-file $CONFIG.toml > logs/$CONFIG.out 2> logs/$CONFIG.err &
