#!/usr/bin/env python

"""
example
python fit_mscale.py -v --config-file fit_mscale.toml
"""

from compute_preprocessing import apply_pre_process_forward
import pickle
from tomlkit import parse
from tensorflow.keras.utils import get_custom_objects
import wispy.mscalev2
import wispy.model_utils
import wispy.callbacks
import wispy.utils
import wispy.logger
import wispy
import datetime
import argparse
import sys
import os
import numpy as np
import tensorflow_addons as tfa
import tensorflow as tf
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.use("agg")

# from cycler import cycler
# from itertools import cycle

# mpl.rcParams.update(mpl.rcParamsDefault)
plt.style.use("ggplot")
mpl.rcParams.update({"font.size": 16})

# new


def convert_input_for_ann_10D(
        times,
        q,
        chi1, theta1, phi1,
        chi2, theta2, phi2,
        inclination, phiRef
):
    times = times.reshape(-1, 1)  # for broadcasting
    X = []
    for _q, _chi1, _theta1, _phi1, _chi2, _theta2, _phi2, _inclination, _phiRef in zip(q, chi1, theta1, phi1, chi2, theta2, phi2, inclination, phiRef):
        sub_matrix = np.array([[_q, _chi1, _theta1, _phi1, _chi2, _theta2,
                                _phi2, _inclination, _phiRef]]) * np.ones([times.shape[0], 9])
        X.append(np.concatenate((times, sub_matrix), axis=1))
    X = np.asarray(X)
    return X.reshape(X.shape[0]*X.shape[1], X.shape[2])

# def convert_input_for_ann_10D(
    # times,
    # q,
    # chi1, theta1, phi1,
    # chi2, theta2, phi2,
    # inclination, phiRef
    # ):
    # X = []
    # for _q, _chi1, _theta1, _phi1, _chi2, _theta2, _phi2, _inclination, _phiRef in zip(q, chi1, theta1, phi1, chi2, theta2, phi2, inclination, phiRef):
    # for t in times:
    # X.append([t, _q, _chi1, _theta1, _phi1, _chi2, _theta2, _phi2, _inclination, _phiRef])
    # X = np.asarray(X)
    # return X

# new


def convert_input_for_ann(
        times,
        q,
        chi1, theta1
):
    times = times.reshape(-1, 1)  # for broadcasting
    X = []
    for _q, _chi1, _theta1 in zip(q, chi1, theta1):
        sub_matrix = np.array([[_q, _chi1, _theta1]]) * \
            np.ones([times.shape[0], 3])
        X.append(np.concatenate((times, sub_matrix), axis=1))
    X = np.asarray(X)
    return X.reshape(X.shape[0]*X.shape[1], X.shape[2])

# def convert_input_for_ann(times, q, chi1, theta1):
    # X = []
    # for _q, _chi1, _theta1 in zip(q, chi1, theta1):
    # for t in times:
    # X.append([t, _q, _chi1, _theta1])
    # X = np.asarray(X)
    # return X


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="""read in training data, apply pre-processing parameters and save to disk""",
    )

    parser.add_argument(
        "--config-file", type=str, help="path to workflow config.toml file"
    )
    parser.add_argument(
        "-v",
        help="""increase output verbosity
        no -v: WARNING(")
        -v: INFO
        -vv: DEBUG""",
        action="count",
        dest="verbose",
        default=0,
    )
    parser.add_argument(
        "--force",
        action="store_true",
        help="if given then will not throw an error if output directory exists",
    )

    overall_start_time = datetime.datetime.now()

    args = parser.parse_args()
    args_dict = vars(args)

    # https://stackoverflow.com/questions/14097061/easier-way-to-enable-verbose-logging
    level = min(2, args.verbose)  # capped to number of levels
    logger = wispy.logger.init_logger(level=level)

    logger.info(f"Using TensorFlow v{tf.__version__}")
    logger.info(f"wispy version: {wispy.__version__}")

    logger.info("==========")
    logger.info("printing command line args")
    for k in args_dict.keys():
        logger.info(f"{k}: {args_dict[k]}")
    logger.info("==========")

    with open(args.config_file, "r") as f:
        text = f.read()

    doc = parse(text)

    logger.info("==========")
    logger.info("printing toml config contents")
    wispy.utils.recursive_dict_print(doc, print_fn=logger.info)
    logger.info("==========")

    logger.info(f"making output dir: {doc['output']}")
    os.makedirs(f"{doc['output']}", exist_ok=args.force)

    # load coords
    filename = doc['data']['coords_train']
    logger.info(f"loading coords file: {filename}")
    coords_train = np.load(filename)

    # load times data
    filename = doc['data']['times']
    logger.info(f"loading times file: {filename}")
    times = np.load(filename)

    pkl_filename = doc['data']['times_scaler']
    with open(pkl_filename, 'rb') as file:
        times_scaler = pickle.load(file)

    logger.info("transforming times data")
    times_scaled = times_scaler.transform(times[:, np.newaxis])[:, 0]

    filename = doc['data']['y_train']
    logger.info(f"loading y-train file: {filename}")
    y_train_raw = np.load(filename)

    filename = doc['data']['y_preprocessing_params']
    logger.info(f"loading y-preprocessing-params file: {filename}")
    y_processing_params = np.load(filename)

    logger.info("applying preprocessing to raw training data")
    y_train = apply_pre_process_forward(
        y_train_raw, y_processing_params['mean'], y_processing_params['max'])

    del y_train_raw

    logger.info('loading validation data')

    # load coords
    filename = doc['data']['coords_val']
    logger.info(f"loading coords validation file: {filename}")
    coords_val = np.load(filename)

    filename = doc['data']['y_val']
    logger.info(f"loading y-validation file: {filename}")
    y_val_raw = np.load(filename)

    logger.info("applying preprocessing to raw validation data")
    y_val = apply_pre_process_forward(
        y_val_raw, y_processing_params['mean'], y_processing_params['max'])

    del y_val_raw

    if coords_train.shape[0] == 9:
        logger.info("building training set input for 10 dimensions")
        X_train = convert_input_for_ann_10D(
            times_scaled,
            coords_train[0], coords_train[1], coords_train[2],
            coords_train[3], coords_train[4], coords_train[5],
            coords_train[6], coords_train[7], coords_train[8]
        )
    elif coords_train.shape[0] == 3:
        logger.info("building training set input for 4 dimensions")
        X_train = convert_input_for_ann(
            times_scaled, coords_train[0], coords_train[1], coords_train[2])
    else:
        logger.error(
            "coords_train.shape error: only 3 or 9 supported currently")
        logger.error("Exiting")
        sys.exit(1)
    y_train = y_train.ravel().reshape(-1, 1)

    logger.info(f"X_train.shape: {X_train.shape}")
    logger.info(f"y_train.shape: {y_train.shape}")

    logger.info("saving training data")
    output_path = os.path.join(doc['output'], "X_train.npy")
    logger.info(f"saving: {output_path}")
    np.save(output_path, X_train)
    output_path = os.path.join(doc['output'], "y_train.npy")
    logger.info(f"saving: {output_path}")
    np.save(output_path, y_train)

    logger.info("freeing memory")
    del X_train
    del y_train

    if coords_val.shape[0] == 9:
        logger.info("building validation set input for 10 dimensions")
        X_val = convert_input_for_ann_10D(
            times_scaled,
            coords_val[0], coords_val[1], coords_val[2],
            coords_val[3], coords_val[4], coords_val[5],
            coords_val[6], coords_val[7], coords_val[8]
        )
    elif coords_val.shape[0] == 3:
        logger.info("building validation set input for 4 dimensions")
        X_val = convert_input_for_ann(
            times_scaled, coords_val[0], coords_val[1], coords_val[2])
    else:
        logger.error("coords_val.shape error: only 3 or 9 supported currently")
        logger.error("Exiting")
        sys.exit(1)
    y_val = y_val.ravel().reshape(-1, 1)

    logger.info(f"X_val.shape: {X_val.shape}")
    logger.info(f"y_val.shape: {y_val.shape}")

    logger.info("saving validation data")
    output_path = os.path.join(doc['output'], "X_val.npy")
    logger.info(f"saving: {output_path}")
    np.save(output_path, X_val)
    output_path = os.path.join(doc['output'], "y_val.npy")
    logger.info(f"saving: {output_path}")
    np.save(output_path, y_val)

    logger.info("freeing memory")
    del X_val
    del y_val

    overall_end_time = datetime.datetime.now()
    overall_duration = overall_end_time - overall_start_time
    logger.info(f"total time: {overall_duration}")
    logger.info("finished!")
