#!/bin/bash

OUTPUT=data_logs
mkdir -p $OUTPUT

#NAME=create_processed_training_data_1e3
#echo "running $NAME"
#nohup python ../create_processed_training_data.py -v --config-file $NAME.toml > $OUTPUT/$NAME.out 2> $OUTPUT/$NAME.err &

#NAME=create_processed_training_data_1e5
#echo "running $NAME"
#nohup python ../create_processed_training_data.py -v --config-file $NAME.toml > $OUTPUT/$NAME.out 2> $OUTPUT/$NAME.err &

#NAME=create_processed_training_data_1e6
#echo "running $NAME"
#nohup python ../create_processed_training_data.py -v --config-file $NAME.toml > $OUTPUT/$NAME.out 2> $OUTPUT/$NAME.err &

NAME=create_processed_training_data_4e6
echo "running $NAME"
nohup python ../create_processed_training_data.py -v --config-file $NAME.toml > $OUTPUT/$NAME.out 2> $OUTPUT/$NAME.err &
