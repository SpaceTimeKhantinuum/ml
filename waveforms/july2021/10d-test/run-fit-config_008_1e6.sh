#!/bin/bash

mkdir -p logs


#export TF_NUM_INTEROP_THREADS=1
#export TF_NUM_INTRAOP_THREADS=1

CONFIG="config_008_1e6"
#PART="001"
#echo "running $CONFIG part:$PART"
#nohup python ../fit.py -v --config-file $CONFIG.toml > logs/${CONFIG}_${PART}.out 2> logs/${CONFIG}_${PART}.err &


# to continue training
PART="002"
echo "running $CONFIG part:$PART"
nohup python ../fit.py -v --config-file $CONFIG.toml --force > logs/${CONFIG}_${PART}.out 2> logs/${CONFIG}_${PART}.err &
