#!/bin/bash

OUTPUT=data_logs
mkdir -p $OUTPUT

#NAME=compute_preprocessing_1e3
#echo "running $NAME"
#nohup python ../compute_preprocessing.py -v --config-file $NAME.toml > $OUTPUT/$NAME.out 2> $OUTPUT/$NAME.err &

#NAME=compute_preprocessing_1e5
#echo "running $NAME"
#nohup python ../compute_preprocessing.py -v --config-file $NAME.toml > $OUTPUT/$NAME.out 2> $OUTPUT/$NAME.err &

#NAME=compute_preprocessing_1e6
#echo "running $NAME"
#nohup python ../compute_preprocessing.py -v --config-file $NAME.toml > $OUTPUT/$NAME.out 2> $OUTPUT/$NAME.err &

NAME=compute_preprocessing_4e6
echo "running $NAME"
nohup python ../compute_preprocessing.py -v --config-file $NAME.toml > $OUTPUT/$NAME.out 2> $OUTPUT/$NAME.err &
