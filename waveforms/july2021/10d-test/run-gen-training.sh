#!/bin/bash

OUTPUT=data_logs
mkdir -p $OUTPUT

# echo "running data_generation_1e3"
# nohup python ../data_generation_10D.py -v --config-file data_generation_1e3.toml > $OUTPUT/data_generation_1e3.out 2> $OUTPUT/data_generation_1e3.err &

#echo "running data_generation_1e5"
#nohup python ../data_generation_10D.py -v --config-file data_generation_1e5.toml > $OUTPUT/data_generation_1e5.out 2> $OUTPUT/data_generation_1e5.err &

#echo "running data_generation_1e6"
#nohup python ../data_generation_10D.py -v --config-file data_generation_1e6.toml > $OUTPUT/data_generation_1e6.out 2> $OUTPUT/data_generation_1e6.err &

echo "running data_generation_4e6"
nohup python ../data_generation_10D.py -v --config-file data_generation_4e6.toml > $OUTPUT/data_generation_4e6.out 2> $OUTPUT/data_generation_4e6.err &
