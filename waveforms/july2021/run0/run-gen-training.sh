#!/bin/bash

OUTPUT=data_logs
mkdir -p $OUTPUT

NAME=data_generation_time_s1x
echo "running $NAME"
nohup python ../data_generation_10D.py -v --config-file $NAME.toml > $OUTPUT/$NAME.out 2> $OUTPUT/$NAME.err &
