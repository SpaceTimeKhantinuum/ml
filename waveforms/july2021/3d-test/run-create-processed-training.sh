#!/bin/bash

OUTPUT=data_logs
mkdir -p $OUTPUT

NAME=create_processed_training_data
echo "running $NAME"
nohup python ../create_processed_training_data.py -v --config-file $NAME.toml > $OUTPUT/$NAME.out 2> $OUTPUT/$NAME.err &
