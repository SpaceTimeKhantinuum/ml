#!/usr/bin/env python

"""
example
python data_generation_10D.py -v --config-file data_generation_10D.toml  --force

same as data_generation.py but generates data in full 10D parameter space
i.e.,

time,
mass-ratio,
chi1, theta1, phi1,
chi2, theta2, phi2,
inclination, phiRef (orientation)
"""

import numpy as np
import os
import argparse
import datetime
import multiprocessing as mp
import functools
from scipy.interpolate import InterpolatedUnivariateSpline as IUS
from tqdm import tqdm
import phenom
import wispy
import wispy.lalutils
import wispy.logger
import wispy.utils
import lalsimulation as lalsim
import tomlkit
from tomlkit import parse

def cart_to_polar(x, y, z):
    """
    cartesian to spherical polar transformation.
    phi (azimuthal angle) between [0, 2*pi]
    returns: r, theta, phi
    """
    hxy = np.hypot(x, y)
    r = np.hypot(hxy, z)
    theta = np.arctan2(hxy, z)
    phi = np.arctan2(y, x)
    phi = phi % (2 * np.pi)
    return r, theta, phi


def polar_to_cart(r, theta, phi):
    """
    spherical polar to cartesian transformation
    returns: x, y, z
    """
    x = r * np.sin(theta) * np.cos(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(theta)
    return x, y, z


def gen_data_q_chi1_theta1_phi1_chi2_theta2_phi2_time_theta_phi(
    mass_ratio_array,
    chi1_array,
    theta1_array,
    phi1_array,
    chi2_array,
    theta2_array,
    phi2_array,
    inclination_array,
    phiRef_array,
    total_mass,
    dt,
    t_min,
    t_max,
    f_min,
    distance,
    approximant,
    deltaT,
    nproc=1):

    n_samples = mass_ratio_array.shape[0]

    ps = []
    for i in range(n_samples):
        q = mass_ratio_array[i]
        chi1 = chi1_array[i]
        theta1 = theta1_array[i]
        phi1 = phi1_array[i]

        chi2 = chi2_array[i]
        theta2 = theta2_array[i]
        phi2 = phi2_array[i]

        spin1x, spin1y, spin1z = polar_to_cart(chi1, theta1, phi1)
        spin2x, spin2y, spin2z = polar_to_cart(chi2, theta2, phi2)

        # orientation
        inclination = inclination_array[i]
        phiRef = phiRef_array[i]

        m1, m2 = phenom.m1_m2_M_q(total_mass, q)
        ps.append(
            wispy.lalutils.gen_td_wf_params(
                m1=m1, m2=m2,
                S1x=spin1x, S1y=spin1y, S1z=spin1z,
                S2x=spin2x, S2y=spin2y, S2z=spin2z,
                approximant=approximant, f_min=f_min,
                distance=distance, deltaT=deltaT,
                inclination=inclination, phiRef=phiRef)
            )

    times = []
    amp = []
    phase = []

    func = functools.partial(wispy.lalutils.gen_td_wf, t_min=t_min, t_max=t_max)

    # logger.info("running parallel waveform generation")

    with mp.Pool(nproc) as pool:
        returned = pool.map(func, ps)
#     print(len(returned))
#     print(len(returned[0]))
#     print(returned[0])

    for i in range(len(returned)):
        times.append(returned[i][0])
        amp.append(returned[i][1])
        phase.append(returned[i][2])

    # find common times and interpolate data onto common time grid
    common_tmin = np.max(list(map(np.min, times)))
    common_tmax = np.min(list(map(np.max, times)))

    # logger.info(f"common_tmin = {common_tmin}")
    # logger.info(f"common_tmax = {common_tmax}")

    common_times = np.arange(common_tmin, common_tmax, dt)

    ntimes = len(common_times)
    # logger.info(f"ntimes = {ntimes}")

    amps = np.zeros(shape=(n_samples, ntimes))
    phases = np.zeros(shape=(n_samples, ntimes))

    for i in range(n_samples):
        amps[i] = IUS(times[i], amp[i])(common_times)
        phases[i] = IUS(times[i], phase[i])(common_times)

    return common_times, amps, phases


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="""generate waveform data""",
    )

    parser.add_argument(
        "--config-file", type=str, help="path to workflow config.toml file"
    )
    parser.add_argument(
        "-v",
        help="""increase output verbosity
        no -v: WARNING(")
        -v: INFO
        -vv: DEBUG""",
        action="count",
        dest="verbose",
        default=0,
    )
    parser.add_argument(
        "--force",
        action="store_true",
        help="if given then will not throw an error if output directory exists",
    )

    overall_start_time = datetime.datetime.now()

    args = parser.parse_args()
    args_dict = vars(args)

    # https://stackoverflow.com/questions/14097061/easier-way-to-enable-verbose-logging
    level = min(2, args.verbose)  # capped to number of levels
    logger = wispy.logger.init_logger(level=level)

    logger.info(f"wispy version: {wispy.__version__}")

    logger.info("==========")
    logger.info("printing command line args")
    for k in args_dict.keys():
        logger.info(f"{k}: {args_dict[k]}")
    logger.info("==========")

    with open(args.config_file, "r") as f:
        text = f.read()

    doc = parse(text)

    logger.info("==========")
    logger.info("printing toml config contents")
    wispy.utils.recursive_dict_print(doc, print_fn=logger.info)
    logger.info("==========")

    logger.info(f"making output dir: {doc['output']}")
    os.makedirs(f"{doc['output']}", exist_ok=args.force)


    logger.info("looking for [approx] section")
    try:
        doc["approx"]
    except tomlkit.exceptions.NonExistentKey as error:
        logger.error(error)
        raise error

    n_samples = int(doc['approx']['n_samples'])

    mass_ratio_array = np.random.uniform(
        doc['approx']['q_min'],
        doc['approx']['q_max'],
        n_samples)

    chi1_array = np.random.uniform(
        doc['approx']['chi1_min'],
        doc['approx']['chi1_max'],
        n_samples)
    theta1_array = np.random.uniform(
        np.cos(doc['approx']['theta1_min']),
        np.cos(doc['approx']['theta1_max']),
        n_samples)
    theta1_array = np.arccos(theta1_array)
    phi1_array = np.random.uniform(
        doc['approx']['phi1_min'],
        doc['approx']['phi1_max'],
        n_samples)

    chi2_array = np.random.uniform(
        doc['approx']['chi2_min'],
        doc['approx']['chi2_max'],
        n_samples)
    theta2_array = np.random.uniform(
        np.cos(doc['approx']['theta2_min']),
        np.cos(doc['approx']['theta2_max']),
        n_samples)
    theta2_array = np.arccos(theta2_array)
    phi2_array = np.random.uniform(
        doc['approx']['phi2_min'],
        doc['approx']['phi2_max'],
        n_samples)

    inclination_array = np.random.uniform(
        np.cos(doc['approx']['inclination_min']),
        np.cos(doc['approx']['inclination_max']),
        n_samples)
    inclination_array = np.arccos(inclination_array)
    phiRef_array = np.random.uniform(
        doc['approx']['phiRef_min'],
        doc['approx']['phiRef_max'],
        n_samples)

    deltaT = 1./doc['approx']['sample_rate']

    logger.info("starting waveform generation function")

    common_times, amps, phases = gen_data_q_chi1_theta1_phi1_chi2_theta2_phi2_time_theta_phi(
        mass_ratio_array=mass_ratio_array,
        chi1_array=chi1_array,
        theta1_array=theta1_array,
        phi1_array=phi1_array,
        chi2_array=chi2_array,
        theta2_array=theta2_array,
        phi2_array=phi2_array,
        inclination_array=inclination_array,
        phiRef_array=phiRef_array,
        total_mass=doc['approx']['total_mass'],
        dt=doc['approx']['dt'],
        t_min=doc['approx']['t_min'],
        t_max=doc['approx']['t_max'],
        f_min=doc['approx']['f_min'],
        distance=doc['approx']['distance'],
        approximant=lalsim.GetApproximantFromString(doc['approx']['approximant']),
        deltaT=deltaT,
        nproc=doc['approx']['nproc'])

    logger.info("finished waveform generation function")

    logger.info(f"common_times = {common_times.shape}")
    logger.info(f"amps = {amps.shape}")
    logger.info(f"phases = {phases.shape}")

    out = os.path.join(doc["output"], "coords.npy")
    logger.info(f"saving coords: {out}")
    np.save(
        out,
        np.stack(
            (mass_ratio_array,
            chi1_array, theta1_array, phi1_array,
            chi2_array, theta2_array, phi2_array,
            inclination_array, phiRef_array)
            ))

    out = os.path.join(doc["output"], "times.npy")
    logger.info(f"saving times: {out}")
    np.save(out, common_times)

    out = os.path.join(doc["output"], "amplitude.npy")
    logger.info(f"saving amplitude: {out}")
    np.save(out, amps)

    out = os.path.join(doc["output"], "phase.npy")
    logger.info(f"saving phase: {out}")
    np.save(out, phases)

    overall_end_time = datetime.datetime.now()
    overall_duration = overall_end_time - overall_start_time
    logger.info(f"total time: {overall_duration}")
    logger.info("finished!")
