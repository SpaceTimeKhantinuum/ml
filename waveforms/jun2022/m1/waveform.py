import numpy as np

import sklearn.preprocessing

import pycbc.types
import pycbc.waveform
import pycbc.pnutils
import pycbc.conversions

import lal
import lalsimulation as lalsim
from scipy.interpolate import InterpolatedUnivariateSpline as IUS

def StoM(S, Mtot):
    """StoM(S, Mtot)
    """
    return S / (lal.MTSUN_SI*Mtot)


def MtoS(M, Mtot):
    """MtoS(Hz, Mtot)
    """
    return M * (lal.MTSUN_SI*Mtot)


def td_amp_scale(mtot, distance):
    """
    mtot in solar masses
    distance in m
    M*G/c^2 * M_sun / dist
    """
    return mtot * lal.MRSUN_SI / distance


def MftoHz(Mf, M):
    """MftoHz(Mf, M)
    """
    return Mf / (lal.MTSUN_SI*M)


def HztoMf(Hz, M):
    """HztoMf(Hz, M)
    """
    return Hz * (lal.MTSUN_SI*M)


def generate_waveform(
    new_times_M,
    time_idxs=None, # indicies at which to return the data on
    mass_ratio=1,
    total_mass=100,
    spin1x=0,
    spin1y=0,
    spin1z=0,
    spin2x=0,
    spin2y=0,
    spin2z=0,
    inclination=0,
    coa_phase=0,
    approximant="SEOBNRv4_opt",
    distance_mpc=1,
    f_lower=10,
    delta_t=1/4096,
):
    """
    new_times_M: array of shape (num_times, ). The times in
        units of M that you want to interpolate the waveform onto.
    time_idxs: int array of len: 1 <= len(time_idxs) < len(new_times_M)
        These are the indices of the output array that you want to output.
        This is used to sample the times in the training set and test set
        differently if desired.
    """
    
    # convert total_mass and mass_ratio to mss1, mass2
    # where mass1 >= mass2
    mass1=pycbc.conversions.mass1_from_mtotal_q(total_mass, mass_ratio)
    mass2=pycbc.conversions.mass2_from_mtotal_q(total_mass, mass_ratio)

    # create parameter dict to pass to pycbc.waveform.get_td_waveform
    params = dict(
        mass1=mass1,
        mass2=mass2,
        spin1x=spin1x,
        spin1y=spin1y,
        spin1z=spin1z,
        spin2x=spin2x,
        spin2y=spin2y,
        spin2z=spin2z,
        approximant=approximant,
        f_lower=f_lower,
        delta_t=delta_t,
        distance=distance_mpc,
        inclination=inclination,
        coa_phase=coa_phase,
    )
    
    # generate hp and hc
    hp, hc = pycbc.waveform.get_td_waveform(**params)
    
    # need original delta_t (should be same as input)
    assert delta_t == hp.delta_t, "delta_t not matching!"
    # delta_t = hp.delta_t
    
    # compute amplitude and phase
    # so we can subtract off an initial phase and
    # re-interpolate data onto input grid
    # (which tends to be more reliable than hp,hc interpolation)
    amp = pycbc.waveform.utils.amplitude_from_polarizations(hp, hc).numpy()
    phase = pycbc.waveform.utils.phase_from_polarizations(hp, hc, remove_start_phase=True).numpy()

    # convert times from units of Seconds to units of M (geometric units)
    times_M = StoM(hp.sample_times.numpy(), total_mass)

    # interpolate amplitude onto input time grid (in units of M)
    new_amp = IUS(times_M, amp)(new_times_M)

    # normalise amplitude by scaling by 1 Mpc in metres
    distance_m = 1e6*distance_mpc*lal.PC_SI
    new_amp /= td_amp_scale(total_mass, distance_m)

    # interpolate phase onto input time grid (in units of M)
    new_phase = IUS(times_M, phase)(new_times_M)
    # subtract off initial phase. This helps
    # fitting the data because they have been aligned in some way
    # that is just a coordinate d.o.f.
    new_phase += -new_phase[0]

    # construct complex strain h = hp - i*hc
    h = new_amp * np.exp(-1.j * new_phase)
    
    # get hp and hc
    hp = np.real(h)
    hc = np.imag(h)
    
    if time_idxs is not None:
        hp = hp[time_idxs]
        hc = hc[time_idxs]
        
    return hp, hc


def generate_waveforms(
    new_times_M,
    mass_ratios,
    spin1xs,
    spin1ys,
    spin1zs,
    spin2xs,
    spin2ys,
    spin2zs,
    coa_phases,
    inclinations,
    time_idxs=None,
    total_mass=100,
    approximant="IMRPhenomXP",
    distance_mpc=1,
    return_hp_hc=False, # just for debugging only return hp, remove this later
):
    """
    this code can be parallelised using multiprocessing
    see: https://gitlab.com/SpaceTimeKhantinuum/ml/-/blob/master/waveforms/aug2021/data_generation_coprec.py#L63
    """
    
    # if no time indices given then just return all times
    if time_idxs is None:
        time_idxs = range(len(new_times_M))

    # fixed parameters
    params = dict(
        new_times_M=new_times_M,
        time_idxs=time_idxs,
        total_mass=total_mass,
        approximant=approximant,
        distance_mpc=distance_mpc,
    )

    n_samples = len(mass_ratios)
    
    hps=[]
    if return_hp_hc:
        hcs=[]
    for i in range(n_samples):
        
        params.update(
            dict(
                mass_ratio=mass_ratios[i],
                spin1x=spin1xs[i],
                spin1y=spin1ys[i],
                spin1z=spin1zs[i],
                spin2x=spin2xs[i],
                spin2y=spin2ys[i],
                spin2z=spin2zs[i],
                inclination=inclinations[i],
                coa_phase=coa_phases[i]
            )
        )

        if return_hp_hc:
            hp, hc = generate_waveform(**params)
        else:
            hp, _ = generate_waveform(**params)

        hps.append(hp)
        if return_hp_hc:
            hcs.append(hc)
            
    thetas = np.column_stack((mass_ratios, spin1xs, spin1ys, spin1zs, spin2xs, spin2ys, spin2zs, coa_phases, inclinations))
        
    if return_hp_hc:
        return new_times_M[time_idxs][:,np.newaxis], thetas, np.row_stack(hps), np.row_stack(hcs)
    else:
        return new_times_M[time_idxs][:,np.newaxis], thetas, np.row_stack(hps)