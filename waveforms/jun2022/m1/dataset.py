import math
import numpy as np
import tensorflow as tf


def super_index(col, row, width):
    """
    given the column and row index of a 2D array of width `width`
    return the super-index
    """
    return col + width*row


def inv_super_index(I, width):
    """
    I: super-index of a flattened 2D grid
    width: width of 2D grid
    
    returns row and column indicies of 2D array
    corresponding to the given super-index `I`.
    """
    row = I // width
    col = I % width
    return row, col



class DataGen(tf.keras.utils.Sequence):
    def __init__(self, thetas, times, ys, batch_size, shuffle=True):
        """
        thetas: array of physical parameters: shape = (num_waveforms, num_phys_feats)
        times: array of times: shape = (num_time_samples, 1)
        ys: array of targets: shape = (num_waveforms, num_time_samples)
        batch_size: batch_size of full matrix (int)
        shuffle: (True) shuffle mini-batches at each epoch


        https://medium.com/analytics-vidhya/write-your-own-custom-data-generator-for-tensorflow-keras-1252b64e41c3
        https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
        """
        self.thetas = thetas
        self.times = times
        self.ys = ys
        self.batch_size = batch_size
        self.shuffle = shuffle
        
        self.num_phys_feats = thetas.shape[1]
        self.num_time_feats = 1
        
        self.num_time_samples = len(self.times)
        self.num_waveform_samples = len(self.thetas)
        
        # this is the height of the full matrix
        self.total_number_of_points = self.num_waveform_samples * self.num_time_samples
        # this is also the width of the full matrix
        self.total_number_of_features = self.num_time_feats + self.num_phys_feats
        

        # init self.indexes
        self.on_epoch_end()

    def on_epoch_end(self):
        'Updates indices after each epoch'
        
        # self.indices is an array of the super-index
        self.indices = np.arange(self.total_number_of_points)
        if self.shuffle == True:
            np.random.shuffle(self.indices)

    def __getitem__(self, idx):

        x_start_idx = idx * self.batch_size
        x_end_idx = (idx + 1) * self.batch_size
        
        # get a batch of super-indices
        indices = self.indices[x_start_idx:x_end_idx]
        
        # print(f"indices = {indices}")
        # each of these are of length len(indicies)
        # the correspond to the indicies of the theta and time arrays
        # these are the theta and time indices for the corresponding super-index `indices`
        # theta_indices, time_indices = inv_super_index(I=indices, width=self.total_number_of_features-1)
        theta_indices, time_indices = inv_super_index(I=indices, width=self.num_time_samples)
        # theta_indices, time_indices = inv_super_index(I=indices, width=2)
        
        # time_indices, theta_indices = inv_super_index(I=indices, width=self.total_number_of_features)
        
        # print(f"len(theta_indices) = {len(theta_indices)}, len(time_indices) = {len(time_indices)}")
        # print(f"theta_indices = {theta_indices}, time_indices = {time_indices}")
        
        # get the actualy value of theta and time for this batch
        theta = self.thetas[theta_indices]
        time = self.times[time_indices]
        
        # print(f"theta: {theta}")
        # print(f"time: {time}")
        
        # full matrix
        # construct the full input matrix by broadcasting the times with the theta
        # and the adding the time dimension as the first column
        X = np.column_stack((time, (np.ones_like(time) * [th for th in theta])))
        # X = np.concatenate((time, (np.ones_like(time) * [th for th in theta])), axis=1)
        
        # construct target
        # find the values of the target for this batch
        y = self.ys[theta_indices,time_indices].reshape(-1, 1)
        
        # need to split the output like this because the first one is time
        # and the rest of the other parameters
        # can change this to return a dataframe or something so we know the names but
        # this should match the input of the network
        return (X[:,0], X[:,1:]), y
        # return X, y

    def __len__(self):
        return math.ceil(self.total_number_of_points / self.batch_size)