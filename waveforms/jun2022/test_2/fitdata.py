import numpy as np
import tensorflow as tf
import datetime
import subprocess

def check_gpu():
    print("running 'tf.config.list_physical_devices('GPU')'")
    print(tf.config.list_physical_devices("GPU"))

    try:
        print("running 'nvidia-smi -L'")
        subprocess.call(["nvidia-smi", "-L"])
    except FileNotFoundError:
        print("could not run 'nvidia-smi -L'")


def set_gpu_memory_growth():
    gpus = tf.config.list_physical_devices("GPU")
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            print("running: tf.config.experimental.set_memory_growth")
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)
            raise e
            
print("check_gpu() and set_gpu_memory_growth()")
check_gpu()
set_gpu_memory_growth()
# print("setting inter and intra threads")
# tf.config.threading.set_inter_op_parallelism_threads(1)
# tf.config.threading.set_intra_op_parallelism_threads(1)

#####
# from https://github.com/titu1994/tf_fourier_features/blob/master/tf_fourier_features/fourier_features.py
#####
class FourierFeatureProjection(tf.keras.layers.Layer):

    def __init__(self, gaussian_projection: int, gaussian_scale: float = 1.0, **kwargs):
        """
        Fourier Feature Projection layer from the paper:
        [Fourier Features Let Networks Learn High Frequency Functions in Low Dimensional Domains](https://arxiv.org/abs/2006.10739)
        Add this layer immediately after the input layer.
        Args:
            gaussian_projection: Projection dimension for the gaussian kernel in fourier feature
                projection layer. Can be negative or positive integer.
                If <=0, uses identity matrix (basic projection) without gaussian kernel.
                If >=1, uses gaussian projection matrix of specified dim.
            gaussian_scale: Scale of the gaussian kernel in fourier feature projection layer.
                Note: If the scale is too small, convergence will slow down and obtain poor results.
                If the scale is too large (>50), convergence will be fast but results will be grainy.
                Try grid search for scales in the range [10 - 50].
        """
        super().__init__(**kwargs)

        if 'dtype' in kwargs:
            self._kernel_dtype = kwargs['dtype']
        else:
            self._kernel_dtype = None

        gaussian_projection = int(gaussian_projection)
        gaussian_scale = float(gaussian_scale)

        self.gauss_proj = gaussian_projection
        self.gauss_scale = gaussian_scale

    def build(self, input_shape):
        # assume channel dim is always at last location
        input_dim = input_shape[-1]

        if self.gauss_proj <= 0:
            # Assume basic projection
            self.proj_kernel = tf.keras.layers.Dense(input_dim, use_bias=False, trainable=False,
                                                     kernel_initializer='identity', dtype=self._kernel_dtype)

        else:
            initializer = tf.keras.initializers.TruncatedNormal(mean=0.0, stddev=self.gauss_scale)
            self.proj_kernel = tf.keras.layers.Dense(self.gauss_proj, use_bias=False, trainable=False,
                                                     kernel_initializer=initializer, dtype=self._kernel_dtype)

        self.built = True

    def call(self, inputs, **kwargs):
        x_proj = 2.0 * np.pi * inputs
        x_proj = self.proj_kernel(x_proj)

        x_proj_sin = tf.sin(x_proj)
        x_proj_cos = tf.cos(x_proj)

        output = tf.concat([x_proj_sin, x_proj_cos], axis=-1)
        return output

    def get_config(self):
        config = {
            'gaussian_projection': self.gauss_proj,
            'gaussian_scale': self.gauss_scale
        }
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))
#####



def run_model(X_train, y_train, epochs=1001, threshold=1e-7, batch_size=32, validation_data=None, learning_rate=1e-3, lr_patience=200, virtual_batch_size=None, verbose=1):
    activation = 'relu'
    units = 256
    n_layers = 3
    gaussian_projection = 20
    # gaussian_projection = 5
    # gaussian_scale = 1

    # gaussian_scale = 20
    # gaussian_scale = 10
    gaussian_scale = 5
    # gaussian_scale = 1

    # model 1: only pass the time through the RFF encoding
    time_input = tf.keras.Input(shape=(1,), name='time_input')
    q_input = tf.keras.Input(shape=(1,), name='q_input')
    x_t = FourierFeatureProjection(gaussian_projection = gaussian_projection, gaussian_scale = gaussian_scale)(time_input)
    # x_q = tf.keras.layers.Dense(units, activation)(q_input)
    x = tf.keras.layers.Concatenate()([x_t, q_input])
    # x = tf.keras.layers.Concatenate()([x_t, x_q])
    for i in range(n_layers):
        x = tf.keras.layers.Dense(units, activation)(x)
        # x = tf.keras.layers.BatchNormalization(virtual_batch_size=virtual_batch_size)(x)
    x = tf.keras.layers.Dense(1)(x)
    model = tf.keras.Model(inputs=[time_input, q_input], outputs=x)

    # model 2: pass both time and mass-ratio through RFF encoding
    # model 2 doesn't work so well in the q-direction :)
    # I am guessing it's because the variation of the function in q
    # is simpler than the variation in time.
    # time_input = tf.keras.Input(shape=(1,), name='time_input')
    # q_input = tf.keras.Input(shape=(1,), name='q_input')
    # all_inputs = tf.keras.layers.Concatenate()([time_input, q_input])
    # x = FourierFeatureProjection(gaussian_projection = gaussian_projection, gaussian_scale = gaussian_scale)(all_inputs)
    # # x_q = tf.keras.layers.Dense(units, activation)(q_input)
    # # x = tf.keras.layers.Concatenate()([x_t, q_input])
    # # x = tf.keras.layers.Concatenate()([x_t, x_q])
    # for i in range(n_layers):
    #     x = tf.keras.layers.Dense(units, activation)(x)
    # x = tf.keras.layers.Dense(1)(x)
    # model = tf.keras.Model(inputs=[time_input, q_input], outputs=x)
    
    
    csv_logger = tf.keras.callbacks.CSVLogger('training.log')
    
    callbacks = [
        # tfa.callbacks.TQDMProgressBar(show_epoch_progress=False),
        # wispy.callbacks.ThresholdCallback(threshold),
        # tf.keras.callbacks.ReduceLROnPlateau(min_lr=1e-6, patience=200, factor=0.9, monitor='val_loss'),
        tf.keras.callbacks.ReduceLROnPlateau(min_lr=1e-6, patience=lr_patience, factor=0.6, monitor='val_loss'),
        # tf.keras.callbacks.ReduceLROnPlateau(min_lr=1e-6, patience=200, factor=0.1, monitor='val_loss'),
        tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=300),
        csv_logger,
    ]
    
    model.compile(loss='mse', optimizer=tf.keras.optimizers.Adam(learning_rate))

    history = model.fit(x=[X_train[:,0], X_train[:,1]], y=y_train, epochs=epochs, verbose=verbose, batch_size=batch_size, callbacks=callbacks, validation_data=validation_data)
    
    return history, model

print("loading data")

X_training = np.load("X_training.npy")
y_training = np.load("y_training.npy")
X_validation = np.load("X_validation.npy")
y_validation = np.load("y_validation.npy")


print(X_training.shape)
# epochs=2000
epochs=10000
# epochs=20
batch_size = 512
# batch_size = X_training.shape[0]
# batch_size = 32768 #2^15
virtual_batch_size = 512
learning_rate = 1e-3


lr_patience=200

print("starting fit")

start_time_fit = datetime.datetime.now()
history, model = run_model(
    X_training,
    y_training,
    epochs=epochs,
    batch_size=batch_size,
    validation_data=([X_validation[:,0], X_validation[:,1]], y_validation),
    learning_rate=learning_rate,
    threshold=1e-6,
    lr_patience=lr_patience,
    virtual_batch_size=virtual_batch_size,
    verbose=1,
    )
end_time_fit = datetime.datetime.now()
duration_fit = end_time_fit - start_time_fit
print(f"duration_fit: {duration_fit}")


print("done")