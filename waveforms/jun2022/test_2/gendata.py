import matplotlib.pyplot as plt
plt.rcParams.update({'font.size':16})

import numpy as np
import datetime

import sklearn.preprocessing
import sklearn.model_selection

import pycbc.types
import pycbc.waveform
import pycbc.pnutils
import pycbc.conversions
from pycbc.waveform import get_td_waveform
from pycbc.filter import match
from pycbc.psd import aLIGOZeroDetHighPower

import lal
from scipy.interpolate import InterpolatedUnivariateSpline as IUS


def StoM(S, Mtot):
    """StoM(S, Mtot)
    """
    return S / (lal.MTSUN_SI*Mtot)

def MtoS(M, Mtot):
    """MtoS(Hz, Mtot)
    """
    return M * (lal.MTSUN_SI*Mtot)

def td_amp_scale(mtot, distance):
    """
    mtot in solar masses
    distance in m
    M*G/c^2 * M_sun / dist
    """
    return mtot * lal.MRSUN_SI / distance

def generate_waveform(new_times_M, mass_ratio=1, total_mass=100, approximant="SEOBNRv4_opt", distance_mpc=1):
    
    mass1=pycbc.conversions.mass1_from_mtotal_q(total_mass, mass_ratio)
    mass2=pycbc.conversions.mass2_from_mtotal_q(total_mass, mass_ratio)

    # can scale amp by eta
    # eta = pycbc.conversions.eta_from_mass1_mass2(mass1, mass2)

    params = dict(
        mass1=mass1,
        mass2=mass2,
        approximant=approximant,
        f_lower=10,
        delta_t=1/4096,
        distance=distance_mpc
        )
    hp, hc = pycbc.waveform.get_td_waveform(**params)
    delta_t = hp.delta_t

    amp = pycbc.waveform.utils.amplitude_from_polarizations(hp, hc).numpy()
    phase = pycbc.waveform.utils.phase_from_polarizations(hp, hc, remove_start_phase=True).numpy()

    times_M = StoM(hp.sample_times.numpy(), total_mass)

    new_amp = IUS(times_M, amp)(new_times_M)

    distance_m = 1e6*distance_mpc*lal.PC_SI
    new_amp /= td_amp_scale(total_mass, distance_m)

    new_phase = IUS(times_M, phase)(new_times_M)
    new_phase += -new_phase[0]

    h = new_amp * np.exp(-1.j * new_phase)
    hp = np.real(h)
    # hc = np.imag(h)

    return hp


mass_ratio = 1
# mass_ratio = 0.95
total_mass = 100
approximant = "SEOBNRv4_opt"
# approximant = "SEOBNRv4P"
distance_mpc = 1


# uniform spacing throughout
# t1 = -500
t1 = -2000
# t1 = -5000
t2 = 70
dt = 0.5
# dt = 2
new_times_M = np.arange(t1, t2, dt)

# implement higher sample rate for merger

# # # region 1 inspiral
# # r1_t1 = -500
# r1_t1 = -2000
# r1_t2 = -200
# r1_dt = 4
# # r1_dt = 8
# r1_new_times_M = np.arange(r1_t1, r1_t2, r1_dt)


# # region 2 merger-ringdown
# r2_t1 = r1_t2
# r2_t2 = 70
# # r2_dt = 0.25
# r2_dt = 0.75
# r2_new_times_M = np.arange(r2_t1, r2_t2, r2_dt)

# new_times_M = np.concatenate((r1_new_times_M, r2_new_times_M))

hp = generate_waveform(new_times_M=new_times_M, mass_ratio=mass_ratio, total_mass=total_mass, approximant=approximant, distance_mpc=distance_mpc)

# plt.plot(StoM(hp.sample_times, total_mass), hp)
# plt.plot(new_times_M, hp)
# plt.savefig('1.png')
# plt.close()



# mass_ratios = np.linspace(1, 5, 100)
# mass_ratios = np.linspace(1, 5, 10)

mass_ratios = np.linspace(1, 5, 40)

# mass_ratios = np.linspace(0, 0.99, 40)

# mass_ratios = np.linspace(1, 5, 2)
# mass_ratios = np.linspace(1, 5, 5)

print("gen data")

hps = []
for mass_ratio in mass_ratios:
    hp = generate_waveform(new_times_M=new_times_M, mass_ratio=mass_ratio, total_mass=total_mass, approximant=approximant, distance_mpc=distance_mpc)
    hps.append(hp)
hps = np.array(hps)






q_idxs = np.arange(len(mass_ratios))


# if you want the training set and validation set to have
# different mass-ratios then uncomment
q_training_idxs = q_idxs[::2]


# the next lines are a bit confusing
# but 
# if you do q_idxs[1::2] then the last data point it outside the training set
# so for the training idxs I also add in the last data point
# that would have been totally excluded
q_training_idxs = np.concatenate((q_training_idxs, [q_idxs[-1]]))
q_validation_idxs = q_idxs[1:-1:2] 

# use this if you want the train and val sets to have the same mass-ratios
# but they will be using alternating time samples
# q_training_idxs = q_idxs
# q_validation_idxs = q_idxs



t_idxs = np.arange(len(new_times_M))
t_training_idxs = t_idxs[::2]
t_validation_idxs = t_idxs[1::2]




def build_input_single_q(t, q):
    """
    t is a matrix i.e. shape = (-1, 1)
    q is a float
    """
    q = np.ones_like(t) * q

    return np.column_stack((t, q))


def build_input_multiple_q(t, qs):
    """
    t is a vector
    qs is a vector
    """
    return np.row_stack([build_input_single_q(t, q) for q in qs])



t_scaler = sklearn.preprocessing.MinMaxScaler(feature_range=(-0.5, 0.5))
q_scaler = sklearn.preprocessing.MinMaxScaler(feature_range=(-0.5, 0.5))

t_scaled = t_scaler.fit_transform(new_times_M[:,np.newaxis])
q_scaled = q_scaler.fit_transform(mass_ratios[:,np.newaxis])




hps_scaler = sklearn.preprocessing.MinMaxScaler(feature_range=(-0.5, 0.5))

hps_shape = hps.shape
hps_scaled = hps_scaler.fit_transform(hps.reshape(-1, 1)).reshape(hps_shape)


X_training = build_input_multiple_q(t_scaled[t_training_idxs], q_scaled[q_training_idxs])


X_validation = build_input_multiple_q(t_scaled[t_validation_idxs], q_scaled[q_validation_idxs])


# y_training = hps[np.ix_(q_training_idxs, t_training_idxs)]
# y_training = y_training.reshape(-1, 1)
# y_validation = hps[np.ix_(q_validation_idxs, t_validation_idxs)]
# y_validation = y_validation.reshape(-1, 1)


# https://stackoverflow.com/questions/35607818/index-a-2d-numpy-array-with-2-lists-of-indices
y_training = hps_scaled[np.ix_(q_training_idxs, t_training_idxs)]
y_training = y_training.reshape(-1, 1)
y_validation = hps_scaled[np.ix_(q_validation_idxs, t_validation_idxs)]
y_validation = y_validation.reshape(-1, 1)


np.save("X_training.npy", X_training)
np.save("y_training.npy", y_training)
np.save("X_validation.npy", X_validation)
np.save("y_validation.npy", y_validation)


print("Done")