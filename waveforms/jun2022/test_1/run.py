import matplotlib.pyplot as plt
plt.rcParams.update({'font.size':16})

import numpy as np
import datetime

import sklearn.preprocessing
import sklearn.model_selection

import pycbc.types
import pycbc.waveform
import pycbc.pnutils
import pycbc.conversions
from pycbc.waveform import get_td_waveform
from pycbc.filter import match
from pycbc.psd import aLIGOZeroDetHighPower


import lal
from scipy.interpolate import InterpolatedUnivariateSpline as IUS

from tf_fourier_features import FourierFeatureProjection
import tensorflow as tf
import wispy.callbacks
import tensorflow_addons as tfa

import subprocess

def check_gpu():
    print("running 'tf.config.list_physical_devices('GPU')'")
    print(tf.config.list_physical_devices("GPU"))

    try:
        print("running 'nvidia-smi -L'")
        subprocess.call(["nvidia-smi", "-L"])
    except FileNotFoundError:
        print("could not run 'nvidia-smi -L'")


def set_gpu_memory_growth():
    gpus = tf.config.list_physical_devices("GPU")
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            print("running: tf.config.experimental.set_memory_growth")
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)
            raise e
            
print("check_gpu() and set_gpu_memory_growth()")
check_gpu()
set_gpu_memory_growth()
# print("setting inter and intra threads")
# tf.config.threading.set_inter_op_parallelism_threads(1)
# tf.config.threading.set_intra_op_parallelism_threads(1)



def StoM(S, Mtot):
    """StoM(S, Mtot)
    """
    return S / (lal.MTSUN_SI*Mtot)

def MtoS(M, Mtot):
    """MtoS(Hz, Mtot)
    """
    return M * (lal.MTSUN_SI*Mtot)

def td_amp_scale(mtot, distance):
    """
    mtot in solar masses
    distance in m
    M*G/c^2 * M_sun / dist
    """
    return mtot * lal.MRSUN_SI / distance

def generate_waveform(new_times_M, mass_ratio=1, total_mass=100, approximant="SEOBNRv4_opt", distance_mpc=1):
    
    mass1=pycbc.conversions.mass1_from_mtotal_q(total_mass, mass_ratio)
    mass2=pycbc.conversions.mass2_from_mtotal_q(total_mass, mass_ratio)

    # can scale amp by eta
    # eta = pycbc.conversions.eta_from_mass1_mass2(mass1, mass2)

    params = dict(
        mass1=mass1,
        mass2=mass2,
        approximant=approximant,
        f_lower=10,
        delta_t=1/4096,
        distance=distance_mpc
        )
    hp, hc = pycbc.waveform.get_td_waveform(**params)
    delta_t = hp.delta_t

    amp = pycbc.waveform.utils.amplitude_from_polarizations(hp, hc).numpy()
    phase = pycbc.waveform.utils.phase_from_polarizations(hp, hc, remove_start_phase=True).numpy()

    times_M = StoM(hp.sample_times.numpy(), total_mass)

    new_amp = IUS(times_M, amp)(new_times_M)

    distance_m = 1e6*distance_mpc*lal.PC_SI
    new_amp /= td_amp_scale(total_mass, distance_m)

    new_phase = IUS(times_M, phase)(new_times_M)
    new_phase += -new_phase[0]

    h = new_amp * np.exp(-1.j * new_phase)
    hp = np.real(h)
    # hc = np.imag(h)

    return hp


mass_ratio = 1
# mass_ratio = 0.95
total_mass = 100
approximant = "SEOBNRv4_opt"
# approximant = "SEOBNRv4P"
distance_mpc = 1


# uniform spacing throughout
# t1 = -500
t1 = -2000
# t1 = -5000
t2 = 70
dt = 0.5
# dt = 2
new_times_M = np.arange(t1, t2, dt)

# implement higher sample rate for merger

# # # region 1 inspiral
# # r1_t1 = -500
# r1_t1 = -2000
# r1_t2 = -200
# r1_dt = 4
# # r1_dt = 8
# r1_new_times_M = np.arange(r1_t1, r1_t2, r1_dt)


# # region 2 merger-ringdown
# r2_t1 = r1_t2
# r2_t2 = 70
# # r2_dt = 0.25
# r2_dt = 0.75
# r2_new_times_M = np.arange(r2_t1, r2_t2, r2_dt)

# new_times_M = np.concatenate((r1_new_times_M, r2_new_times_M))

hp = generate_waveform(new_times_M=new_times_M, mass_ratio=mass_ratio, total_mass=total_mass, approximant=approximant, distance_mpc=distance_mpc)

# plt.plot(StoM(hp.sample_times, total_mass), hp)
# plt.plot(new_times_M, hp)
# plt.savefig('1.png')
# plt.close()



# mass_ratios = np.linspace(1, 5, 100)
# mass_ratios = np.linspace(1, 5, 10)

mass_ratios = np.linspace(1, 5, 40)

# mass_ratios = np.linspace(0, 0.99, 40)

# mass_ratios = np.linspace(1, 5, 2)
# mass_ratios = np.linspace(1, 5, 5)
hps = []
for mass_ratio in mass_ratios:
    hp = generate_waveform(new_times_M=new_times_M, mass_ratio=mass_ratio, total_mass=total_mass, approximant=approximant, distance_mpc=distance_mpc)
    hps.append(hp)
hps = np.array(hps)






q_idxs = np.arange(len(mass_ratios))


# if you want the training set and validation set to have
# different mass-ratios then uncomment
q_training_idxs = q_idxs[::2]


# the next lines are a bit confusing
# but 
# if you do q_idxs[1::2] then the last data point it outside the training set
# so for the training idxs I also add in the last data point
# that would have been totally excluded
q_training_idxs = np.concatenate((q_training_idxs, [q_idxs[-1]]))
q_validation_idxs = q_idxs[1:-1:2] 

# use this if you want the train and val sets to have the same mass-ratios
# but they will be using alternating time samples
# q_training_idxs = q_idxs
# q_validation_idxs = q_idxs



t_idxs = np.arange(len(new_times_M))
t_training_idxs = t_idxs[::2]
t_validation_idxs = t_idxs[1::2]




def build_input_single_q(t, q):
    """
    t is a matrix i.e. shape = (-1, 1)
    q is a float
    """
    q = np.ones_like(t) * q

    return np.column_stack((t, q))


def build_input_multiple_q(t, qs):
    """
    t is a vector
    qs is a vector
    """
    return np.row_stack([build_input_single_q(t, q) for q in qs])



t_scaler = sklearn.preprocessing.MinMaxScaler(feature_range=(-0.5, 0.5))
q_scaler = sklearn.preprocessing.MinMaxScaler(feature_range=(-0.5, 0.5))

t_scaled = t_scaler.fit_transform(new_times_M[:,np.newaxis])
q_scaled = q_scaler.fit_transform(mass_ratios[:,np.newaxis])




hps_scaler = sklearn.preprocessing.MinMaxScaler(feature_range=(-0.5, 0.5))

hps_shape = hps.shape
hps_scaled = hps_scaler.fit_transform(hps.reshape(-1, 1)).reshape(hps_shape)


X_training = build_input_multiple_q(t_scaled[t_training_idxs], q_scaled[q_training_idxs])


X_validation = build_input_multiple_q(t_scaled[t_validation_idxs], q_scaled[q_validation_idxs])


# y_training = hps[np.ix_(q_training_idxs, t_training_idxs)]
# y_training = y_training.reshape(-1, 1)
# y_validation = hps[np.ix_(q_validation_idxs, t_validation_idxs)]
# y_validation = y_validation.reshape(-1, 1)


# https://stackoverflow.com/questions/35607818/index-a-2d-numpy-array-with-2-lists-of-indices
y_training = hps_scaled[np.ix_(q_training_idxs, t_training_idxs)]
y_training = y_training.reshape(-1, 1)
y_validation = hps_scaled[np.ix_(q_validation_idxs, t_validation_idxs)]
y_validation = y_validation.reshape(-1, 1)



dt_scaled = t_scaled.reshape(-1)[1]-t_scaled.reshape(-1)[0]


def run_model(X_train, y_train, epochs=1001, threshold=1e-7, batch_size=32, validation_data=None, learning_rate=1e-3, lr_patience=200, virtual_batch_size=None):
    activation = 'relu'
    units = 256
    n_layers = 3
    gaussian_projection = 20
    # gaussian_projection = 5
    # gaussian_scale = 1

    # gaussian_scale = 20
    # gaussian_scale = 10
    gaussian_scale = 5
    # gaussian_scale = 1

    # model 1: only pass the time through the RFF encoding
    time_input = tf.keras.Input(shape=(1,), name='time_input')
    q_input = tf.keras.Input(shape=(1,), name='q_input')
    x_t = FourierFeatureProjection(gaussian_projection = gaussian_projection, gaussian_scale = gaussian_scale)(time_input)
    # x_q = tf.keras.layers.Dense(units, activation)(q_input)
    x = tf.keras.layers.Concatenate()([x_t, q_input])
    # x = tf.keras.layers.Concatenate()([x_t, x_q])
    for i in range(n_layers):
        x = tf.keras.layers.Dense(units, activation)(x)
        # x = tf.keras.layers.BatchNormalization(virtual_batch_size=virtual_batch_size)(x)
    x = tf.keras.layers.Dense(1)(x)
    model = tf.keras.Model(inputs=[time_input, q_input], outputs=x)

    # model 2: pass both time and mass-ratio through RFF encoding
    # model 2 doesn't work so well in the q-direction :)
    # I am guessing it's because the variation of the function in q
    # is simpler than the variation in time.
    # time_input = tf.keras.Input(shape=(1,), name='time_input')
    # q_input = tf.keras.Input(shape=(1,), name='q_input')
    # all_inputs = tf.keras.layers.Concatenate()([time_input, q_input])
    # x = FourierFeatureProjection(gaussian_projection = gaussian_projection, gaussian_scale = gaussian_scale)(all_inputs)
    # # x_q = tf.keras.layers.Dense(units, activation)(q_input)
    # # x = tf.keras.layers.Concatenate()([x_t, q_input])
    # # x = tf.keras.layers.Concatenate()([x_t, x_q])
    # for i in range(n_layers):
    #     x = tf.keras.layers.Dense(units, activation)(x)
    # x = tf.keras.layers.Dense(1)(x)
    # model = tf.keras.Model(inputs=[time_input, q_input], outputs=x)
    
    
    csv_logger = tf.keras.callbacks.CSVLogger('training.log')
    
    callbacks = [
        tfa.callbacks.TQDMProgressBar(show_epoch_progress=False),
        wispy.callbacks.ThresholdCallback(threshold),
        # tf.keras.callbacks.ReduceLROnPlateau(min_lr=1e-6, patience=200, factor=0.9, monitor='val_loss'),
        tf.keras.callbacks.ReduceLROnPlateau(min_lr=1e-6, patience=lr_patience, factor=0.6, monitor='val_loss'),
        # tf.keras.callbacks.ReduceLROnPlateau(min_lr=1e-6, patience=200, factor=0.1, monitor='val_loss'),
        tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=300),
        csv_logger,
    ]
    
    model.compile(loss='mse', optimizer=tf.keras.optimizers.Adam(learning_rate))

    history = model.fit(x=[X_train[:,0], X_train[:,1]], y=y_train, epochs=epochs, verbose=0, batch_size=batch_size, callbacks=callbacks, validation_data=validation_data)
    
    return history, model

print(X_training.shape)
# epochs=2000
epochs=10000
# epochs=20
batch_size = 512
# batch_size = X_training.shape[0]
# batch_size = 32768 #2^15
virtual_batch_size = 512
learning_rate = 1e-3


lr_patience=200

start_time_fit = datetime.datetime.now()
history, model = run_model(
    X_training,
    y_training,
    epochs=epochs,
    batch_size=batch_size,
    validation_data=([X_validation[:,0], X_validation[:,1]], y_validation),
    learning_rate=learning_rate,
    threshold=1e-6,
    lr_patience=lr_patience,
    virtual_batch_size=virtual_batch_size
    )
end_time_fit = datetime.datetime.now()
duration_fit = end_time_fit - start_time_fit
print(f"duration_fit: {duration_fit}")


plt.figure()
plt.plot(history.history["loss"], label='loss')
plt.plot(history.history["val_loss"], label='val_loss')
plt.legend()
plt.axhline(1e-5)
plt.axhline(1e-6)
plt.axhline(3e-6, c='k')
plt.axhline(1e-7)
plt.yscale("log")
plt.savefig('loss.png')
plt.close()

try:
    plt.figure()
    plt.plot(history.history['lr'])
    plt.yscale('log')
    plt.savefig('lr.png')
    plt.close()
except:
    pass

tt = np.arange(-2000, 70, 0.5)
qq = 2
test_input = build_input_single_q(t_scaler.transform(tt[:,np.newaxis]), q_scaler.transform([[qq]])[0,0])
print(test_input.shape)
start_time=datetime.datetime.now()
yhat_test_single = model.predict([test_input[:,0], test_input[:,1]], verbose=0)
end_time=datetime.datetime.now()
duration = end_time - start_time
print(f"duration: {duration}")
print(yhat_test_single.shape)





def compare_matches(mass_ratio, dt=0.5):

    # t1 = -500
    t1 = -2000
    # t1 = -5000
    t2 = 70
    # dt = 0.5
    # dt = 0.25
    # dt = 2
    new_times_M = np.arange(t1, t2, dt)

    new_delta_t = MtoS(dt, total_mass)

    hp = generate_waveform(new_times_M=new_times_M, mass_ratio=mass_ratio, total_mass=total_mass, approximant=approximant, distance_mpc=distance_mpc)
    # test_input = build_input_single_q(t_scaled, q_scaler.transform([[mass_ratio]])[0,0])
    test_input = build_input_single_q(t_scaler.transform(new_times_M[:,np.newaxis]), q_scaler.transform([[mass_ratio]])[0,0])
    test_yhat_scaled = model.predict([test_input[:,0], test_input[:,1]], verbose=0)
    test_yhat = hps_scaler.inverse_transform(test_yhat_scaled)[:,0]
    hp1 = pycbc.types.TimeSeries(hp, delta_t=new_delta_t, dtype='float64')
    hp2 = pycbc.types.TimeSeries(test_yhat, delta_t=new_delta_t, dtype='float64')
    f_low = 20
    # Resize the waveforms to the same length
    tlen = max(len(hp1), len(hp2))
    hp1.resize(tlen)
    hp2.resize(tlen)
    # Generate the aLIGO ZDHP PSD
    delta_f = 1.0 / hp1.duration
    flen = tlen//2 + 1
    psd = aLIGOZeroDetHighPower(flen, delta_f, f_low)
    # Note: This takes a while the first time as an FFT plan is generated
    # subsequent calls are much faster.
    m, i = match(hp1, hp2, psd=psd, low_frequency_cutoff=f_low)
    # print(f'The match is: {m}')

    return m

# test_mass_ratios = np.linspace(4,6, 10)
test_mass_ratios = np.linspace(1, 5, 100)
# test_mass_ratios = np.linspace(0., 0.99, 100)


start_time=datetime.datetime.now()
matches = [compare_matches(mass_ratio, dt=0.2) for mass_ratio in test_mass_ratios]
end_time=datetime.datetime.now()
duration = end_time - start_time
print(f"duration: {duration}")


plt.figure()
plt.plot(test_mass_ratios, matches)
for m in mass_ratios:
    plt.axvline(m, c='k', ls='--')
plt.axhline(0.977, c='k', ls='--')
plt.savefig('matches_dt_0.2.png')
plt.close()

