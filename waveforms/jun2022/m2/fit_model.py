import tensorflow as tf
import numpy as np
import math
import os
import datetime

import sklearn.preprocessing
import pickle


import lalsimulation as lalsim
import pycbc.pnutils

import wispy.model_utils

import model_utils
import dataset
import waveform

import subprocess

def check_gpu():
    print("running 'tf.config.list_physical_devices('GPU')'")
    print(tf.config.list_physical_devices("GPU"))

    try:
        print("running 'nvidia-smi -L'")
        subprocess.call(["nvidia-smi", "-L"])
    except FileNotFoundError:
        print("could not run 'nvidia-smi -L'")


def set_gpu_memory_growth():
    gpus = tf.config.list_physical_devices("GPU")
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            print("running: tf.config.experimental.set_memory_growth")
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)
            raise e


if __name__ == "__main__":
    print("starting")

    print("check_gpu() and set_gpu_memory_growth()")
    check_gpu()
    set_gpu_memory_growth()

    # tag = "data"
    # tag = "data_spin"
    # tag = "data_amp_phi_1"
    # tag = "data_inc_coa"
    # tag = "data_inc_coa_mass_ratio"
    # tag = 'data_inc_coa_mass_ratio_s1z_s2z'
#     tag = 'data_inc_coa_mass_ratio_hm'
#     tag = 'data_inc_coa_mass_ratio_v4opt_2'
    tag = 'data_inc_coa_mass_ratio_v4hm_2'
#     tag = 'data_inc_coa_mass_ratio_v4hm'

#     out_tag = ""
    out_tag = "_eta_inc_scale"
    
    
    # data_to_model = "phi"
#     data_to_model = "hps"
    data_to_model = "hcs"
    y_train_filename = f"{tag}/train/{data_to_model}.npy"
    y_val_filename = f"{tag}/val/{data_to_model}.npy"
    
    # take_abs: used to take the abs of the train/val data
    # this is because when inclination > pi/2 the phase
    # changes sign. This is an attempt to simply the modelling
    # you just need to remember to do phase -> -phase when
    # inclination > pi/2
    take_abs = False
    
    # apply minmax scaler to target
    scale_y = False
    
    
    
    # apply eta pre-factor scaling
    # i.e. 1/eta
    eta_scaling = True
    
    # apply inclination pre-factor scaling
    # this is the leading order inclination factor for hp or hc
    inc_scaling = True
    
    checkpoint_filepath=f'./checkpoint_dir_{tag}{out_tag}_{data_to_model}/checkpoint'
    
    # epochs = 15
    epochs = 4
#     epochs = 10
#     epochs = 100
    # batch_size = 2048
#     batch_size = 1024
#     batch_size = 512
#     batch_size = 256
    batch_size = 128


    m1, m2 = pycbc.pnutils.mtotal_eta_to_mass1_mass2(100, waveform.eta_from_q(1))

    # f_rd_mf = waveform.HztoMf(pycbc.pnutils._get_final_freq(approx=lalsim.SEOBNRv4, m1=75, m2=25, s1z=0.99, s2z=0.99), 100)
    f_rd_mf = waveform.HztoMf(pycbc.pnutils._get_final_freq(approx=lalsim.SEOBNRv4, m1=m1, m2=m2, s1z=0., s2z=0.), 100)
    # f_rd_mf = waveform.HztoMf(pycbc.pnutils._get_final_freq(approx=lalsim.SEOBNRv4, m1=50, m2=50, s1z=0.99, s2z=0.99), 100)

    time_b_scale = np.around(f_rd_mf, 4)
    print(f"time_b_scale: {time_b_scale}")

    theta_b_scale = 1
    print(f"theta_b_scale: {theta_b_scale}")
    
    print("load training data")
    train_thetas = np.load(f"{tag}/train/theta.npy")
    train_times = np.load(f"{tag}/train/times.npy")
    # train_hps = np.load(f"{tag}/train/hps.npy")
    # train_hps = np.load(f"{tag}/train/amps.npy")
    # train_hps = np.load(f"{tag}/train/phis.npy")
    y_train = np.load(y_train_filename)
        
    if scale_y:
        n_samples_train = train_thetas.shape[0]
        n_times_samples_train = train_times.shape[0]
        y_scaler = sklearn.preprocessing.MinMaxScaler(feature_range=(-0.5, 0.5))
        y_train = y_scaler.fit_transform(y_train.reshape(-1,1)).reshape(n_samples_train, n_times_samples_train)

        pkl_filename = f"{tag}_phi_scaler.pkl"
        print(f"dumping phi scaler to pickle file: {pkl_filename}")
        with open(pkl_filename, 'wb') as file:
            pickle.dump(y_scaler, file)

    if take_abs:
        y_train = np.abs(y_train)
        
        
    if eta_scaling:
        etas = waveform.eta_from_q(train_thetas[:,0])[:,np.newaxis]
        y_train = y_train/etas
        
    if inc_scaling:
        assert data_to_model in ['hps', 'hcs']
        if data_to_model == "hps":
            incs_fac = waveform.hp_inc_prefactor(train_thetas[:,8])[:,np.newaxis]
        elif data_to_model == "hcs":
            incs_fac = waveform.hc_inc_prefactor(train_thetas[:,8])[:,np.newaxis]
        y_train = y_train/incs_fac
        
    print("load validation data")
    val_thetas = np.load(f"{tag}/val/theta.npy")
    val_times = np.load(f"{tag}/val/times.npy")
    # val_hps = np.load(f"{tag}/val/hps.npy")
    # val_hps = np.load(f"{tag}/val/amps.npy")
    # val_hps = np.load(f"{tag}/val/phis.npy")
    y_val = np.load(y_val_filename)
            
    if scale_y:
        n_samples_val = val_thetas.shape[0]
        n_times_samples_val = val_times.shape[0]
        y_val = y_scaler.transform(y_val.reshape(-1,1)).reshape(n_samples_val, n_times_samples_val)
        
    if take_abs:
        y_val = np.abs(y_val)
        

    if eta_scaling:
        etas = waveform.eta_from_q(val_thetas[:,0])[:,np.newaxis]
        y_val = y_val/etas
        
        
    if inc_scaling:
        assert data_to_model in ['hps', 'hcs']
        if data_to_model == "hps":
            incs_fac = waveform.hp_inc_prefactor(val_thetas[:,8])[:,np.newaxis]
        elif data_to_model == "hcs":
            incs_fac = waveform.hc_inc_prefactor(val_thetas[:,8])[:,np.newaxis]
        y_val = y_val/incs_fac

    theta_input_shape = train_thetas.shape[1]
    print(f"theta_input_shape: {theta_input_shape}")

    print("making traing dataset")
    train_ds = dataset.DataGen(thetas=train_thetas, times=train_times, ys=y_train, batch_size=batch_size, shuffle=True)

    del train_thetas, train_times, y_train

    print("making validation dataset")
    val_ds = dataset.DataGen(thetas=val_thetas, times=val_times, ys=y_val, batch_size=batch_size, shuffle=False) 

    del val_thetas, val_times, y_val

    total_number_of_points = train_ds.total_number_of_points
    print(f"total_number_of_points (training): {total_number_of_points}")
        
    # https://www.tensorflow.org/addons/tutorials/optimizers_cyclicallearningrate
    # epochs=104
    # batch_size=32
    # steps_per_epoch = len(X_train) // batch_size
    steps_per_epoch = math.ceil(total_number_of_points / batch_size)
    # steps_per_epoch is the number of mini-batches per epoch
    print(f"steps_per_epoch: {steps_per_epoch}")
    
    # https://www.tensorflow.org/api_docs/python/tf/keras/optimizers/schedules/CosineDecayRestarts1
    lrs = tf.keras.optimizers.schedules.CosineDecayRestarts(
        1e-3,
        # 1000,
        # 5,
        steps_per_epoch/10,
        # t_mul=2.0,
        t_mul=1.0,
        m_mul=1.0,
        alpha=0.0,
        name=None
    )
    
    
    # step = np.arange(0, epochs * steps_per_epoch)
    # lr_ = lrs(step)
    
    # plt.figure()
    # plt.plot(step/steps_per_epoch, lr_)
    # plt.xlabel("epoch")
    # plt.title("learning rate schedule")
    # plt.yscale('log')
    # plt.xscale('log')
    # plt.show()
    # plt.close()

    
    starttime = datetime.datetime.now()

    history, model = model_utils.run_model_ds(
        train_ds=train_ds,
        time_b_scale=time_b_scale,
        theta_b_scale=theta_b_scale,
        theta_input_shape=theta_input_shape,
        epochs=epochs,
        threshold=1e-7,
        validation_data=val_ds,
        verbose=1,
        learning_rate=lrs,
        fan_in=128,
        # fan_in=256,
        # n_layers=3,
        n_layers=6,
        # n_layers=12,
        units=256,
        # units=512,
        activation='relu',
        checkpoint_filepath=checkpoint_filepath,
    )

    endtime = datetime.datetime.now()
    duration = endtime - starttime
    print(f"The time cost: {duration}")

    model_savename = f"{tag}{out_tag}_{data_to_model}_saved_model.h5"
    print(f"saving model: {model_savename}")
    model.save(model_savename)

    filename = os.path.join(f"./", f"{tag}{out_tag}_{data_to_model}_history.pickle")
    print(f"saving history: {filename}")
    wispy.model_utils.save_history(history.history, filename)

    print("finished")
