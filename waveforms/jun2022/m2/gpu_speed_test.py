import numpy as np
import tensorflow as tf
import subprocess
import timeit

import model_utils

def check_gpu():                                                                 
    print("running 'tf.config.list_physical_devices('GPU')'")                    
    print(tf.config.list_physical_devices("GPU"))                                
                                                                                 
    try:                                                                         
        print("running 'nvidia-smi -L'")                                         
        subprocess.call(["nvidia-smi", "-L"])                                    
    except FileNotFoundError:                                                    
        print("could not run 'nvidia-smi -L'")                                   
                                                                                 
                                                                                 
def set_gpu_memory_growth():                                                     
    gpus = tf.config.list_physical_devices("GPU")                                
    if gpus:                                                                     
        try:                                                                     
            # Currently, memory growth needs to be the same across GPUs          
            print("running: tf.config.experimental.set_memory_growth")           
            for gpu in gpus:                                                     
                tf.config.experimental.set_memory_growth(gpu, True)              
        except RuntimeError as e:                                                
            # Memory growth must be set before GPUs have been initialized        
            print(e)                                                             
            raise e                                                              
            
print("check_gpu() and set_gpu_memory_growth()")                                 
check_gpu()                                                                      
set_gpu_memory_growth()                   

print('loading model')
model = tf.keras.models.load_model("data_inc_coa_hcs_saved_model.h5")

def build_input_single(t, *args):
    """
    t is a matrix i.e. shape = (-1, 1)
    list of parameters
    """
    theta = np.array([*args])
    theta = np.ones_like(t) * theta

    return np.column_stack((t, theta))

val_times = np.arange(-2000, 70, 0.25)[:,np.newaxis]
val_theta = np.array([1., 0., 0., 0., 0.,0., 0., 4.75376761, 0.55416115])

X_single_input_test = build_input_single(val_times, val_theta)

X_single_input_test = tf.convert_to_tensor(X_single_input_test)


def run_inference(X, model):                                            
    return model([X[:,0], X[:,1:]])

@tf.function()
def run_inference_tf_func(X, model):                                            
    return model([X[:,0], X[:,1:]])
                                                                                 
@tf.function(jit_compile=True, reduce_retracing=True)                            
def run_inference_exp_comp(X, model):                                   
    return model([X[:,0], X[:,1:]])        

print('manual run_inference 1')                                         
start_time = timeit.default_timer()                                              
_ = run_inference(X_single_input_test, model)                                    
print((timeit.default_timer() - start_time))                                     
                                                                                 
print('manual run_inference 1')                                         
start_time = timeit.default_timer()                                              
_ = run_inference(X_single_input_test, model)                                    
print((timeit.default_timer() - start_time))                                     
                                                                                 
print('manual run_inference 100')                                       
start_time = timeit.default_timer()                                              
for i in range(100):                                                             
    _ = run_inference(X_single_input_test, model)                                
print((timeit.default_timer() - start_time)/100)

print('manual run_inference_tf_func 1')                                         
start_time = timeit.default_timer()                                              
_ = run_inference_tf_func(X_single_input_test, model)                                    
print((timeit.default_timer() - start_time))                                     
                                                                                 
print('manual run_inference_tf_func 1')                                         
start_time = timeit.default_timer()                                              
_ = run_inference_tf_func(X_single_input_test, model)                                    
print((timeit.default_timer() - start_time))                                     
                                                                                 
print('manual run_inference_tf_func 100')                                       
start_time = timeit.default_timer()                                              
for i in range(100):                                                             
    _ = run_inference_tf_func(X_single_input_test, model)                                
print((timeit.default_timer() - start_time)/100)

print('manual run_inference_exp_comp 1')                                         
start_time = timeit.default_timer()                                              
_ = run_inference_exp_comp(X_single_input_test, model)                                    
print((timeit.default_timer() - start_time))                                     
                                                                                 
print('manual run_inference_exp_comp 1')                                         
start_time = timeit.default_timer()                                              
_ = run_inference_exp_comp(X_single_input_test, model)                                    
print((timeit.default_timer() - start_time))                                     
                                                                                 
print('manual run_inference_exp_comp 100')                                       
start_time = timeit.default_timer()                                              
for i in range(100):                                                             
    _ = run_inference_exp_comp(X_single_input_test, model)                                
print((timeit.default_timer() - start_time)/100)