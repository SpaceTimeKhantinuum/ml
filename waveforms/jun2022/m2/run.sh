#!/bin/bash

echo "running"
export CUDA_DEVICE_ORDER="PCI_BUS_ID"
export CUDA_VISIBLE_DEVICES=0
# nohup python fit_model.py > log_hp_2.out 2> log_hp_2.err &
# nohup python fit_model.py > log_hc_2.out 2> log_hc_2.err &
# nohup python fit_model.py > log_hp_3.out 2> log_hp_3.err &
# nohup python fit_model.py > log_hc_3.out 2> log_hc_3.err &

# nohup python fit_model.py > log_hp_4.out 2> log_hp_4.err &
# nohup python fit_model.py > log_hc_4.out 2> log_hc_4.err &


# nohup python fit_model.py > log_hp_hm.out 2> log_hp_hm.err &


# nohup python fit_model.py > log_hp_v4opt.out 2> log_hp_v4opt.err &
# nohup python fit_model.py > log_hc_v4opt.out 2> log_hc_v4opt.err &


# nohup python fit_model.py > log_hp_v4opt_2.out 2> log_hp_v4opt_2.err &
# nohup python fit_model.py > log_hc_v4opt_2.out 2> log_hc_v4opt_2.err &

# nohup python fit_model.py > log_hp_v4hm_2.out 2> log_hp_v4hm_2.err &
# nohup python fit_model.py > log_hc_v4hm_2.out 2> log_hc_v4hm_2.err &

# nohup python fit_model.py > log_hp_v4hm_2_scale.out 2> log_hp_v4hm_2_scale.err &
nohup python fit_model.py > log_hc_v4hm_2_scale.out 2> log_hc_v4hm_2_scale.err &