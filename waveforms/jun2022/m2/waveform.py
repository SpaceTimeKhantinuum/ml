import numpy as np

import multiprocessing as mp

import pycbc.types
import pycbc.waveform
import pycbc.pnutils
import pycbc.conversions

import lal
from scipy.interpolate import InterpolatedUnivariateSpline as IUS

def StoM(S, Mtot):
    """StoM(S, Mtot)
    """
    return S / (lal.MTSUN_SI*Mtot)


def MtoS(M, Mtot):
    """MtoS(Hz, Mtot)
    """
    return M * (lal.MTSUN_SI*Mtot)


def td_amp_scale(mtot, distance):
    """
    mtot in solar masses
    distance in m
    M*G/c^2 * M_sun / dist
    """
    return mtot * lal.MRSUN_SI / distance


def MftoHz(Mf, M):
    """MftoHz(Mf, M)
    """
    return Mf / (lal.MTSUN_SI*M)


def HztoMf(Hz, M):
    """HztoMf(Hz, M)
    """
    return Hz * (lal.MTSUN_SI*M)


def eta_from_q(q):
    """
    converts mass-ratio to symmetric mass-ratio
    input: q
    output: eta
    """
    return q/(1.+q)**2


def hp_inc_prefactor(inc):
    """inc in rad"""
    return (1 + np.cos(inc)**2)/2


def hc_inc_prefactor(inc):
    """inc in rad"""
    return np.cos(inc)


def generate_waveform_mp(p):
    """
    *_mp is for multiprocessing

    p: dict

    new_times_M: array of shape (num_times, ). The times in
        units of M that you want to interpolate the waveform onto.
    time_idxs: int array of len: 1 <= len(time_idxs) < len(new_times_M)
        These are the indices of the output array that you want to output.
        This is used to sample the times in the training set and test set
        differently if desired.
    """

    new_times_M=p['new_times_M']
    time_idxs=p['time_idxs'] # indicies at which to return the data on
    mass_ratio=p['mass_ratio']
    total_mass=p['total_mass']
    spin1x=p['spin1x']
    spin1y=p['spin1y']
    spin1z=p['spin1z']
    spin2x=p['spin2x']
    spin2y=p['spin2y']
    spin2z=p['spin2z']
    inclination=p['inclination']
    coa_phase=p['coa_phase']
    approximant=p['approximant']
    distance_mpc=p['distance_mpc']
    f_lower=p['f_lower']
    delta_t=p['delta_t']
    return_data=p['return_data']

    # convert total_mass and mass_ratio to mss1, mass2
    # where mass1 >= mass2
    mass1=pycbc.conversions.mass1_from_mtotal_q(total_mass, mass_ratio)
    mass2=pycbc.conversions.mass2_from_mtotal_q(total_mass, mass_ratio)

    # create parameter dict to pass to pycbc.waveform.get_td_waveform
    params = dict(
        mass1=mass1,
        mass2=mass2,
        spin1x=spin1x,
        spin1y=spin1y,
        spin1z=spin1z,
        spin2x=spin2x,
        spin2y=spin2y,
        spin2z=spin2z,
        approximant=approximant,
        f_lower=f_lower,
        delta_t=delta_t,
        distance=distance_mpc,
        inclination=inclination,
        coa_phase=coa_phase,
    )
    
    # generate hp and hc
    hp, hc = pycbc.waveform.get_td_waveform(**params)
    
    # need original delta_t (should be same as input)
    assert delta_t == hp.delta_t, "delta_t not matching!"
    # delta_t = hp.delta_t
    
    # compute amplitude and phase
    # so we can subtract off an initial phase and
    # re-interpolate data onto input grid
    # (which tends to be more reliable than hp,hc interpolation)
    amp = pycbc.waveform.utils.amplitude_from_polarizations(hp, hc).numpy()
    phase = pycbc.waveform.utils.phase_from_polarizations(hp, hc, remove_start_phase=True).numpy()

    # convert times from units of Seconds to units of M (geometric units)
    times_M = StoM(hp.sample_times.numpy(), total_mass)

    # interpolate amplitude onto input time grid (in units of M)
    new_amp = IUS(times_M, amp)(new_times_M)

    # normalise amplitude by scaling by 1 Mpc in metres
    distance_m = 1e6*distance_mpc*lal.PC_SI
    new_amp /= td_amp_scale(total_mass, distance_m)

    # interpolate phase onto input time grid (in units of M)
    new_phase = IUS(times_M, phase)(new_times_M)
    # subtract off initial phase. This helps
    # fitting the data because they have been aligned in some way
    # that is just a coordinate d.o.f.
    new_phase += -new_phase[0]
    
    if return_data == 'amp_phi':
        if time_idxs is not None:
            new_amp = new_amp[time_idxs]
            new_phase = new_phase[time_idxs]
        return dict(hp=new_amp, hc=new_phase)

    # construct complex strain h = hp - i*hc = amp * exp(-i*phase) #maybe it's +i?
    h = new_amp * np.exp(-1.j * new_phase)
    
    # get hp and hc
    hp = np.real(h)
    hc = np.imag(h)
    
    if time_idxs is not None:
        hp = hp[time_idxs]
        hc = hc[time_idxs]
        
    return dict(hp=hp, hc=hc)


def generate_waveforms_mp(
    new_times_M,
    mass_ratios,
    spin1xs,
    spin1ys,
    spin1zs,
    spin2xs,
    spin2ys,
    spin2zs,
    coa_phases,
    inclinations,
    time_idxs=None,
    total_mass=100,
    approximant="IMRPhenomXP",
    distance_mpc=1,
    delta_t=1/4096,
    f_lower=10,
    nproc=1,
    return_data='hp_hc', # 'amp_phi'
):
    """
    *_mp is for multiprocessing
    this code can be parallelised using multiprocessing
    see: https://gitlab.com/SpaceTimeKhantinuum/ml/-/blob/master/waveforms/aug2021/data_generation_coprec.py#L63
    """
    
    assert return_data in ['hp_hc', 'amp_phi']
    
    # if no time indices given then just return all times
    if time_idxs is None:
        time_idxs = range(len(new_times_M))

    # fixed parameters
    params = dict(
        new_times_M=new_times_M,
        time_idxs=time_idxs,
        total_mass=total_mass,
        approximant=approximant,
        distance_mpc=distance_mpc,
        delta_t=delta_t,
        f_lower=f_lower,
        return_data=return_data,
    )

    n_samples = len(mass_ratios)
    
    all_params = []

    for i in range(n_samples):
        params.update(
            dict(
                mass_ratio=mass_ratios[i],
                spin1x=spin1xs[i],
                spin1y=spin1ys[i],
                spin1z=spin1zs[i],
                spin2x=spin2xs[i],
                spin2y=spin2ys[i],
                spin2z=spin2zs[i],
                inclination=inclinations[i],
                coa_phase=coa_phases[i]
            )
        )

        all_params.append(params.copy())

    with mp.Pool(nproc) as pool:
        returned = pool.map(generate_waveform_mp, all_params)

    hps = np.row_stack([ret['hp'] for ret in returned])
    hcs = np.row_stack([ret['hc'] for ret in returned])
            
    thetas = np.column_stack((mass_ratios, spin1xs, spin1ys, spin1zs, spin2xs, spin2ys, spin2zs, coa_phases, inclinations))
        
    return new_times_M[time_idxs][:,np.newaxis], thetas, hps, hcs