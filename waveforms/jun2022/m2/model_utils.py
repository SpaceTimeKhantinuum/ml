import numpy as np
import os
import tensorflow as tf
import wispy.callbacks

# https://stackoverflow.com/a/71990796

TRAINING_POISON_PILL_FILE_NAME = 'stop-training'

class PoisonPillCallback(tf.keras.callbacks.Callback): 
    def on_epoch_end(self, epoch, logs={}): 
        if os.path.exists(TRAINING_POISON_PILL_FILE_NAME):
            self.model.stop_training = True
            os.remove(TRAINING_POISON_PILL_FILE_NAME)
            print(f'poison pill file "{TRAINING_POISON_PILL_FILE_NAME}" detected, stopping training')
            
            
def create_model(theta_input_shape, time_b_scale, theta_b_scale, fan_in=128, n_layers=3, units=256, activation='relu'):

    time_kernel_initializer = tf.keras.initializers.RandomNormal(mean=0., stddev=time_b_scale)
    time_bias_initializer = tf.keras.initializers.RandomUniform(minval=-1., maxval=1.)
    
    theta_kernel_initializer = tf.keras.initializers.RandomNormal(mean=0., stddev=theta_b_scale)
    theta_bias_initializer = tf.keras.initializers.RandomUniform(minval=-1., maxval=1.)
    
    time_input = tf.keras.Input(shape=(1,), name='time_input')
    theta_input = tf.keras.Input(shape=(theta_input_shape,), name='theta_input')
    
    x_time = tf.keras.layers.Dense(fan_in, kernel_initializer=time_kernel_initializer, bias_initializer=time_bias_initializer, trainable=False)(time_input)
    x_time = tf.keras.layers.Lambda(lambda x: tf.math.sin(np.pi*x))(x_time)
    
    x_theta = tf.keras.layers.Dense(fan_in, kernel_initializer=theta_kernel_initializer, bias_initializer=theta_bias_initializer, trainable=False)(theta_input)
    x_theta = tf.keras.layers.Lambda(lambda x: tf.math.sin(np.pi*x))(x_theta)
    # x_theta = tf.keras.layers.Dense(fan_in, activation='relu')(theta_input)
    
    x = tf.keras.layers.Concatenate()([x_time, x_theta])
    
    for i in range(n_layers):
        x = tf.keras.layers.Dense(units, activation)(x)
    x = tf.keras.layers.Dense(1)(x)
    model = tf.keras.Model(inputs=[time_input, theta_input], outputs=x)
    
    # print(model.summary())

    return model


def create_plain_model(theta_input_shape, fan_in=128, n_layers=3, units=256, activation='relu'):

    time_input = tf.keras.Input(shape=(1,), name='time_input')
    theta_input = tf.keras.Input(shape=(theta_input_shape,), name='theta_input')
    
    x_time = tf.keras.layers.Dense(fan_in, activation=activation)(time_input)
    
    x_theta = tf.keras.layers.Dense(fan_in, activation=activation)(theta_input)
    
    x = tf.keras.layers.Concatenate()([x_time, x_theta])
    
    for i in range(n_layers):
        x = tf.keras.layers.Dense(units, activation)(x)
    x = tf.keras.layers.Dense(1)(x)
    model = tf.keras.Model(inputs=[time_input, theta_input], outputs=x)
    
    # print(model.summary())

    return model

def run_model_ds(train_ds, time_b_scale, theta_b_scale, theta_input_shape, epochs=1001, threshold=1e-7, validation_data=None, verbose=0, learning_rate=1e-3, fan_in=128, n_layers=3, units=256, activation='relu', checkpoint_filepath=None, model_type='rff'):
    
    assert model_type in ['rff', 'plain']
    
    if model_type == 'rff':
        model = create_model(theta_input_shape=theta_input_shape, time_b_scale=time_b_scale, theta_b_scale=theta_b_scale, fan_in=fan_in, n_layers=n_layers, units=units, activation=activation)
    elif model_type == 'plain':
        model = create_plain_model(theta_input_shape=theta_input_shape, fan_in=fan_in, n_layers=n_layers, units=units, activation=activation)
    
    # https://www.tensorflow.org/api_docs/python/tf/keras/callbacks/ModelCheckpoint
    if checkpoint_filepath is None:
        checkpoint_filepath = './checkpoint_dir/checkpoint'
    model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath=checkpoint_filepath,
        save_weights_only=True,
        monitor='val_loss',
        save_best_only=True)
    
    callbacks = [
        # tfa.callbacks.TQDMProgressBar(show_epoch_progress=False),
        wispy.callbacks.ThresholdCallback(threshold),
        model_checkpoint_callback,
        PoisonPillCallback(),
        # tf.keras.callbacks.ReduceLROnPlateau(min_lr=1e-6, patience=200, factor=0.9, monitor='val_loss'),
        # tf.keras.callbacks.ReduceLROnPlateau(min_lr=1e-6, patience=200, factor=0.6, monitor='val_loss'),
        
        # tf.keras.callbacks.ReduceLROnPlateau(min_lr=1e-6, patience=80, factor=0.2, monitor='val_loss'),
        # tf.keras.callbacks.ReduceLROnPlateau(min_lr=1e-6, patience=10, factor=0.2, monitor='val_loss'),
        
        
        # tf.keras.callbacks.ReduceLROnPlateau(min_lr=1e-6, patience=200, factor=0.1, monitor='val_loss'),
        # tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=300),
        # tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=20),
    ]
    
    model.compile(loss='mse', optimizer=tf.keras.optimizers.Adam(learning_rate))

    # history = model.fit(train_ds, epochs=epochs, verbose=verbose, batch_size=batch_size, callbacks=callbacks, validation_data=validation_data)
    history = model.fit(train_ds, epochs=epochs, verbose=verbose, callbacks=callbacks, validation_data=validation_data)
    
    # The model weights (that are considered the best) are loaded into the model.
    print("loading best weights from checkpoint")
    model.load_weights(checkpoint_filepath)

    return history, model
