import datetime
import os
import numpy as np

import waveform
import sample

if __name__ == "__main__":
    print("starting")

    train_test_val = "train"
#     train_test_val = "val"
    # train_test_val = "test"
    
#     tag = "data_inc_coa_mass_ratio"
#     tag = "data_inc_coa_mass_ratio_hm"
#     tag = "data_inc_coa_mass_ratio_v4opt_2"
    tag = "data_inc_coa_mass_ratio_v4hm_2"
#     tag = "data_inc_coa_mass_ratio_s1z_s2z"
    
    output_dir = os.path.join(tag, train_test_val)
    
    # output_dir = os.path.join("data", train_test_val)
    # output_dir = os.path.join("data_spin", train_test_val)
    os.makedirs(output_dir, exist_ok=True)

    nproc=60
#     nproc=10
#     nproc=4
#     approximant="SEOBNRv4_opt"
    approximant="SEOBNRv4HM"
#     approximant="IMRPhenomXPHM"
#     approximant="IMRPhenomXP"
    # approximant="SEOBNRv4PHM"

    return_data = 'hp_hc'
    # return_data = 'amp_phi'
    
    # uniform spacing throughout
    t1 = -2000
    # t1 = -500
    # t1 = -200
    # t1 = -50
    t2 = 70
    dt = 0.25
    new_times_M = np.arange(t1, t2, dt)

    t_idxs = np.arange(len(new_times_M))
    # t_training_idxs = None
    # t_validation_idxs = None
    if train_test_val == "train":
        t_idxs_sample = t_idxs[::2]
    elif train_test_val == "val":
        t_idxs_sample = t_idxs[1::2]

    total_mass=100
    distance_mpc=1
    delta_t=1/4096
    f_lower=10

#     n_samples = 100_000
#     n_samples = 500_000
    n_samples = 50_000
#     n_samples = 500
    mass_ratio_low = 1
    mass_ratio_high = 2
#     mass_ratio_high = 4

    out = sample.draw_mass_ratio_inc_coa_phase_samples(n_samples, mass_ratio_low, mass_ratio_high)
#     out = sample.draw_mass_ratio_inc_coa_phase_s1z_s2z_samples(n_samples, mass_ratio_low, mass_ratio_high, spin1z_low=-0.99, spin1z_high=0.99, spin2z_low=-0.99, spin2z_high=0.99)
    # out = sample.draw_inc_coa_phase_samples(n_samples, mass_ratio=1)
    # out = sample.draw_single_spin_samples(n_samples)
    mass_ratios = out[0]
    spin1xs = out[1]
    spin1ys = out[2]
    spin1zs = out[3]
    spin2xs = out[4]
    spin2ys = out[5]
    spin2zs = out[6]
    coa_phases = out[7]
    inclinations = out[8]

    starttime = datetime.datetime.now()

    times, thetas, feat1, feat2 = waveform.generate_waveforms_mp(
        new_times_M,
        mass_ratios,
        spin1xs,
        spin1ys,
        spin1zs,
        spin2xs,
        spin2ys,
        spin2zs,
        coa_phases,
        inclinations,
        time_idxs=t_idxs_sample,
        total_mass=total_mass,
        approximant=approximant,
        distance_mpc=distance_mpc,
        delta_t=delta_t,
        f_lower=f_lower,
        nproc=nproc,
        return_data=return_data,
    )

    endtime = datetime.datetime.now()
    duration = endtime - starttime
    print(f"The time cost: {duration}")


    output_filename = os.path.join(output_dir, "times.npy")
    print(f"saving: {output_filename}")
    np.save(output_filename, times)

    output_filename = os.path.join(output_dir, "theta.npy")
    print(f"saving: {output_filename}")
    np.save(output_filename, thetas)
    
    if return_data == 'hp_hc':
        output_filename_feat1 = os.path.join(output_dir, "hps.npy")
        output_filename_feat2 = os.path.join(output_dir, "hcs.npy")

    elif return_data == 'amp_phi':
        output_filename_feat1 = os.path.join(output_dir, "amps.npy")
        output_filename_feat2 = os.path.join(output_dir, "phis.npy")

    print(f"saving: {output_filename_feat1}")
    np.save(output_filename_feat1, feat1)

    print(f"saving: {output_filename_feat2}")
    np.save(output_filename_feat2, feat2)

    print("finished")
