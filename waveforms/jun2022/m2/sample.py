"""
contrains functions to draw samples from different
waveform parameter distributions

in general all functions should return

9 1D arrays for:

mass_ratio, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, coa_phase, inclination
"""

import numpy as np
import pycbc.coordinates

def sample_3d_sphere_uniform(npts):
    """
    generates random x,y,z 3d vector components
    distributed uniformly (isotropic?) in a 3-sphere.
    """
    
    spin_mag = np.random.uniform(0, 1, npts)
    
    # correction to the magnitude of the vector so that
    # there isn't an over density near the centre.
    # Don't really understand though.
    R = 0.99 # this is the maximum magnitude
    spin_mag = R * np.cbrt( spin_mag )
    
    spin_phi = np.random.uniform(0, 2*np.pi, npts)
    spin_theta = np.arccos(np.random.uniform(-1, 1, npts))
    
    
    return spin_mag, spin_phi, spin_theta

def draw_mass_ratio_samples(npts, mass_ratio_low, mass_ratio_high, spin1_mag=0, spin1_phi=0, spin1_theta=0, spin2_mag=0, spin2_phi=0, spin2_theta=0, inclination=0, coa_phase=0):
    """
    draw npts samples from a uniform distribution in mass-ratio between `mass_ratio_low` and `mass_ratio_high`
    keeping all other parameters fixed
    (spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, coa_phase, inclination)
    """
    
    mass_ratio = np.random.uniform(mass_ratio_low, mass_ratio_high, npts)

    spin1_mag = np.array([spin1_mag]*npts)
    spin1_phi = np.array([spin1_phi]*npts)
    spin1_theta = np.array([spin1_theta]*npts)
    
    spin2_mag = np.array([spin2_mag]*npts)
    spin2_phi = np.array([spin2_phi]*npts)
    spin2_theta = np.array([spin2_theta]*npts)
    
    coa_phase = np.array([coa_phase]*npts)
    inclination = np.array([inclination]*npts)

    spin1x, spin1y, spin1z = pycbc.coordinates.spherical_to_cartesian(spin1_mag, spin1_phi, spin1_theta)
    spin2x, spin2y, spin2z = pycbc.coordinates.spherical_to_cartesian(spin2_mag, spin2_phi, spin2_theta)
    
    return mass_ratio, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, coa_phase, inclination


def draw_inc_coa_phase_samples(npts, mass_ratio=1, spin1_mag=0, spin1_phi=0, spin1_theta=0, spin2_mag=0, spin2_phi=0, spin2_theta=0):
    """
    draw npts samples from the inc-coa_phase with other parameters fixed
    """
    
    mass_ratio = np.array([mass_ratio]*npts)
    
    spin1_mag = np.array([spin1_mag]*npts)
    spin1_phi = np.array([spin1_phi]*npts)
    spin1_theta = np.array([spin1_theta]*npts)
    
    spin2_mag = np.array([spin2_mag]*npts)
    spin2_phi = np.array([spin2_phi]*npts)
    spin2_theta = np.array([spin2_theta]*npts)
    
    
    spin1x, spin1y, spin1z = pycbc.coordinates.spherical_to_cartesian(spin1_mag, spin1_phi, spin1_theta)
    
    spin2x, spin2y, spin2z = pycbc.coordinates.spherical_to_cartesian(spin2_mag, spin2_phi, spin2_theta)
    
    # coa_phase and inclination
    _, coa_phase, inclination = sample_3d_sphere_uniform(npts)
    
    return mass_ratio, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, coa_phase, inclination


def draw_mass_ratio_inc_coa_phase_samples(npts, mass_ratio_low, mass_ratio_high, spin1_mag=0, spin1_phi=0, spin1_theta=0, spin2_mag=0, spin2_phi=0, spin2_theta=0):
    """
    draw npts samples from mass_ratio-inc-coa_phase with other parameters fixed
    """
    
    mass_ratio = np.random.uniform(mass_ratio_low, mass_ratio_high, npts)
    
    spin1_mag = np.array([spin1_mag]*npts)
    spin1_phi = np.array([spin1_phi]*npts)
    spin1_theta = np.array([spin1_theta]*npts)
    
    spin2_mag = np.array([spin2_mag]*npts)
    spin2_phi = np.array([spin2_phi]*npts)
    spin2_theta = np.array([spin2_theta]*npts)
    
    
    spin1x, spin1y, spin1z = pycbc.coordinates.spherical_to_cartesian(spin1_mag, spin1_phi, spin1_theta)
    
    spin2x, spin2y, spin2z = pycbc.coordinates.spherical_to_cartesian(spin2_mag, spin2_phi, spin2_theta)
    
    # coa_phase and inclination
    _, coa_phase, inclination = sample_3d_sphere_uniform(npts)
    
    return mass_ratio, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, coa_phase, inclination


def draw_mass_ratio_inc_coa_phase_s1z_s2z_samples(npts, mass_ratio_low, mass_ratio_high, spin1z_low=-0.99, spin1z_high=0.99, spin2z_low=-0.99, spin2z_high=0.99):
    """
    draw npts samples from mass_ratio-spin1z-spin2z-inc-coa_phase with other parameters fixed
    """
    
    mass_ratio = np.random.uniform(mass_ratio_low, mass_ratio_high, npts)
    
    spin1x = np.array([0]*npts)
    spin1y = np.array([0]*npts)
    
    spin2x = np.array([0]*npts)
    spin2y = np.array([0]*npts)
    
    spin1z = np.random.uniform(spin1z_low, spin1z_high, npts)
    spin2z = np.random.uniform(spin2z_low, spin2z_high, npts)
    
    # coa_phase and inclination
    _, coa_phase, inclination = sample_3d_sphere_uniform(npts)
    
    return mass_ratio, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, coa_phase, inclination


def draw_single_spin_samples(npts, mass_ratio=3, spin2_mag=0, spin2_phi=0, spin2_theta=0, coa_phase=0, inclination=0):
    """
    draw npts samples from the inc-coa_phase with other parameters fixed
    """
    
    mass_ratio = np.array([mass_ratio]*npts)
    
    spin1_mag, spin1_phi, spin1_theta = sample_3d_sphere_uniform(npts)
    
    spin2_mag = np.array([spin2_mag]*npts)
    spin2_phi = np.array([spin2_phi]*npts)
    spin2_theta = np.array([spin2_theta]*npts)
    
    coa_phase = np.array([coa_phase]*npts)
    inclination = np.array([inclination]*npts)
    
    spin1x, spin1y, spin1z = pycbc.coordinates.spherical_to_cartesian(spin1_mag, spin1_phi, spin1_theta)
    
    spin2x, spin2y, spin2z = pycbc.coordinates.spherical_to_cartesian(spin2_mag, spin2_phi, spin2_theta)
    
    return mass_ratio, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, coa_phase, inclination


def draw_9d_samples(
    npts,
    mass_ratio_low=1,
    mass_ratio_high=2,
):
    """
    draw npts samples from the inc-coa_phase with other parameters fixed
    """
    
    mass_ratio = np.random.uniform(mass_ratio_low, mass_ratio_high, npts)
    
    spin1_mag, spin1_phi, spin1_theta = sample_3d_sphere_uniform(npts)
    spin2_mag, spin2_phi, spin2_theta = sample_3d_sphere_uniform(npts)
    
    _, coa_phase, inclination = sample_3d_sphere_uniform(npts)
    
    spin1x, spin1y, spin1z = pycbc.coordinates.spherical_to_cartesian(spin1_mag, spin1_phi, spin1_theta)
    
    spin2x, spin2y, spin2z = pycbc.coordinates.spherical_to_cartesian(spin2_mag, spin2_phi, spin2_theta)
    
    return mass_ratio, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z, coa_phase, inclination