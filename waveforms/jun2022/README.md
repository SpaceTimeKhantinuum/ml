# introduction

neural network based surrogate model


## setup

```
$ conda create --name tf python=3.9
$ conda activate tf
$ pip install --upgrade pip
$ pip install tensorflow
$ python3 -c "import tensorflow as tf; print(tf.reduce_sum(tf.random.normal([1000, 1000])))"
$ python3 -c "import tensorflow as tf; print(tf.config.list_physical_devices('GPU'))"
$ pip install --upgrade lalsuite pugna gw-wispy gw-phenom tensorflow-addons tomlkit tf_fourier_features pycbc scikit-learn scipy
```
