// based off of coding train
// https://www.youtube.com/watch?v=yNkAuWz5lnY&list=PLRqwX-V7Uu6YPSwT06y_AEYTqIwbeam3y&index=3

// Your code will go here
// open up your console - if everything loaded properly you should see 0.3.0
console.log('ml5 version:', ml5.version);

let classifier;
let puffin;

// uses callbacks so that it does this function
// after the main function has finished.
// this is because js is asynchronous

function modelLoaded() {
    console.log("Model is ready!!!");
    classifier.predict(puffin, gotResults);
}

// ml5 uses error first callback
function gotResults(error, results) {
    if (error) {
        console.error(error)
    } else {
        console.log(results)
        let label = results[0].className;
        fill(0);
        textSize(100);
        text(label, 10, height - 100);
    }
}

function imageReady() {
    image(puffin, 0, 0, width, height);
}

function setup() {
    createCanvas(640, 480);
    // puffin = createImg("images/puffin.png", imageReady);
    puffin = createImg("images/dog.png", imageReady);
    // puffin = createImg("http://www.image-net.org/nodes/4/02099601/52/526a137ccefa669ba3e68d39b21ba04d2422bcc1.thumb", imageReady);

    puffin.hide();
    background(0);
    // Initialize the Image Classifier method with MobileNet
    classifier = ml5.imageClassifier('MobileNet', modelLoaded);

}

function draw() {

}