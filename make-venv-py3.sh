#!/bin/sh

#short script to make a python3 virtual env

unset PYTHONPATH
python3 -m venv ${PWD}/venv

echo "unset PYTHONPATH" >> ${PWD}/venv/bin/activate

