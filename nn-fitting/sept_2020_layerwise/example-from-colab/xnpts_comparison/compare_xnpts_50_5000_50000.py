"""compares the prediction from two identical networks
with the only difference being the number of training examples
used.

model1: 50 examples
model2: 5000 examples
model3: 50000 examples

model2 is a couple orders of magnitude more accurate.

model3 is not much more accurate
but is better.

this example was not optimised
to achieve best performance from
each model though.
"""

import matplotlib
import matplotlib.pyplot as plt

import os
import datetime
import numpy as np
import tensorflow as tf

output_dir = f"comparison_xnpts_50_5000_50000"
if os.path.exists(output_dir):
    pass
else:
    try:
        os.mkdir(output_dir)
    except OSError:
        print(f"Creation of the directory {output_dir} failed")
    else:
        print(f"Successfully created the directory {output_dir} ")

X_train = np.load("run_results_xnpts_50/X_train.npy")
y_train = np.load("run_results_xnpts_50/y_train.npy")

model1_path = "run_results_xnpts_50"
model2_path = "run_results_xnpts_5000"
model3_path = "run_results_xnpts_50000"

model1 = tf.keras.models.load_model(model1_path + "/model.h5")
model2 = tf.keras.models.load_model(model2_path + "/model.h5")
model3 = tf.keras.models.load_model(model3_path + "/model.h5")

model1_yhat = model1.predict(X_train)
model2_yhat = model2.predict(X_train)
model3_yhat = model3.predict(X_train)


plt.figure()
plt.plot(X_train, y_train - model1_yhat, label='model1')
plt.plot(X_train, y_train - model2_yhat, label='model2')
plt.plot(X_train, y_train - model3_yhat, label='model3')
plt.legend()
plt.savefig(os.path.join(output_dir, "residuals.png"))
plt.close()

plt.figure()
plt.plot(X_train, np.abs(y_train - model1_yhat), label='model1')
plt.plot(X_train, np.abs(y_train - model2_yhat), label='model2')
plt.plot(X_train, np.abs(y_train - model3_yhat), label='model3')
plt.legend()
plt.yscale('log')
plt.savefig(os.path.join(output_dir, "log_residuals.png"))
plt.close()
