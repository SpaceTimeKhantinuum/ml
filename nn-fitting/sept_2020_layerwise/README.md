# layerwise training

Here I am investigating a particular kind of training strategy.

1. fit the data with a small-ish ANN
2. compute residual and re-scale
3. fit a deeper network to 2. data. deeper because, hopefully, the residuals are normally distributed.
4. repeat process

This process could be automated and perhaps combined into a single network
with normalisation layers and freezing the 'pre-trained' layers
