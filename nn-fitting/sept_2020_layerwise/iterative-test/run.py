"""
1. train a few layers
2. freeze them and add more layers
3. train again.

use some code from
https://www.tensorflow.org/guide/keras/sequential_model#transfer_learning_with_a_sequential_model
"""

import matplotlib
import matplotlib.pyplot as plt

import os
import datetime
import numpy as np
import tensorflow as tf


def generate_data(xlow=-1, xhigh=1, xnpts=50, add_noise=False, noise_std=0.1, seed=1234):
    """generates data

    Args:
        xlow (int, optional): [description]. Defaults to -1.
        xhigh (int, optional): [description]. Defaults to 1.
        xnpts (int, optional): [description]. Defaults to 50.
        add_noise (bool, optional): [description]. Defaults to False.
        noise_std (float, optional): [description]. Defaults to 0.1
        seed (int, optional): [description]. Defaults to 1234.

    Returns:
        X [np.array]: independent variable. shape = (xnpts, 1)
        y [np.array]: dependent variable. shape = (xnpts, 1)
    """

    X = np.linspace(xlow, xhigh, xnpts).reshape(-1, 1)
    y = X**2

    if add_noise:
        np.random.seed(seed)
        noise = np.random.normal(0, noise_std, size=X.shape)
        y = y + noise

    return X, y


def model1_build(input_dim, output_dim):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Input(shape=(input_dim,)))
    model.add(tf.keras.layers.Dense(128, activation='relu'))
    model.add(tf.keras.layers.Dense(128, activation='sigmoid'))
    model.add(tf.keras.layers.Dense(output_dim, activation='linear'))
    print("model.summary():")
    model.summary()
    return model


def compile_model(model, learning_rate=0.001):
    opt = tf.keras.optimizers.Adam(learning_rate=learning_rate)
    model.compile(loss='mse', optimizer=opt)


if __name__ == "__main__":

    output_dir = f"run_results"
    if os.path.exists(output_dir):
        pass
    else:
        try:
            os.mkdir(output_dir)
        except OSError:
            print(f"Creation of the directory {output_dir} failed")
        else:
            print(f"Successfully created the directory {output_dir} ")

    print("generating training data")

    X_train, y_train = generate_data(xnpts=50)

    print("saving training data")
    np.save(os.path.join(output_dir, "X_train.npy"), X_train)
    np.save(os.path.join(output_dir, "y_train.npy"), y_train)

    print("plotting training data")
    plt.figure()
    plt.scatter(X_train, y_train, label='training data')
    plt.legend()
    plt.savefig(os.path.join(output_dir, "training_data.png"))
    plt.close()

    input_dim = X_train.shape[1]
    output_dim = y_train.shape[1]

    print("building model1")

    model1 = tf.keras.models.Sequential()
    model1.add(tf.keras.layers.Input(shape=(input_dim,)))
    model1.add(tf.keras.layers.Dense(20, activation='relu'))
    model1.add(tf.keras.layers.Dense(20, activation='sigmoid'))
    model1.add(tf.keras.layers.Dense(output_dim, activation='linear'))
    print("model1.summary():")
    model1.summary()

    learning_rate = 0.001
    # learning_rate = tf.keras.optimizers.schedules.InverseTimeDecay(
    #     initial_learning_rate=0.001, decay_steps=10, decay_rate=0.5
    # )

    opt = tf.keras.optimizers.Adam(learning_rate=learning_rate)
    model1.compile(loss='mse', optimizer=opt)
    compile_model(model1, learning_rate=learning_rate)

    print("fitting model1")
    # compute running time
    starttime = datetime.datetime.now()
    history1 = model1.fit(X_train, y_train, epochs=2000, batch_size=32)
    endtime = datetime.datetime.now()
    duration = endtime - starttime
    print(f'The time cost: {duration}')

    print("saving model")
    model1.save(os.path.join(output_dir, "model.h5"))

    print("plotting history1")
    plt.figure()
    plt.plot(history1.history['loss'], label='model1')
    plt.yscale('log')
    plt.legend()
    plt.savefig(os.path.join(output_dir, "model1_history.png"))
    plt.close()

    print("model1 inference")
    model1_yhat = model1.predict(X_train)

    plt.figure()
    plt.plot(X_train, y_train, 'k-o', label="training data")
    plt.plot(X_train, model1_yhat, lw=3, label="model1 yhat")
    plt.legend()
    plt.savefig(os.path.join(output_dir, "model1_inference.png"))
    plt.close()

    plt.figure()
    plt.plot(X_train, y_train - model1_yhat, 'k-o',
             label="training - model1 yhat")
    plt.legend()
    plt.savefig(os.path.join(output_dir, "model1_inference_residual.png"))
    plt.close()

    # now lets take last layer off, freeze model1 layers and then
    # add some more layers

    print("modifying network")
    # set all layers to non trainable
    for layer in model1.layers[:-1]:
        layer.trainable = False

    model1.pop()

    print(model1.summary())

    model1.add(tf.keras.layers.Dense(20, activation='relu'))
    model1.add(tf.keras.layers.Dense(20, activation='sigmoid'))
    model1.add(tf.keras.layers.Dense(20, activation='relu'))
    model1.add(tf.keras.layers.Dense(20, activation='sigmoid'))
    model1.add(tf.keras.layers.Dense(20, activation='relu'))
    model1.add(tf.keras.layers.Dense(20, activation='sigmoid'))
    model1.add(tf.keras.layers.Dense(output_dim, activation='linear'))
    print("model1.summary():")
    model1.summary()

    opt = tf.keras.optimizers.Adam(learning_rate=learning_rate)
    model1.compile(loss='mse', optimizer=opt)
    compile_model(model1, learning_rate=learning_rate)

    print("fitting model1")
    # compute running time
    starttime = datetime.datetime.now()
    history1 = model1.fit(X_train, y_train, epochs=2000, batch_size=32)
    endtime = datetime.datetime.now()
    duration = endtime - starttime
    print(f'The time cost: {duration}')

    print("saving model")
    model1.save(os.path.join(output_dir, "model_2.h5"))

    print("plotting history1")
    plt.figure()
    plt.plot(history1.history['loss'], label='model1')
    plt.yscale('log')
    plt.legend()
    plt.savefig(os.path.join(output_dir, "model1_history_2.png"))
    plt.close()

    print("model1 inference")
    model1_yhat = model1.predict(X_train)

    plt.figure()
    plt.plot(X_train, y_train, 'k-o', label="training data")
    plt.plot(X_train, model1_yhat, lw=3, label="model1 yhat")
    plt.legend()
    plt.savefig(os.path.join(output_dir, "model1_inference_2.png"))
    plt.close()

    plt.figure()
    plt.plot(X_train, y_train - model1_yhat, 'k-o',
             label="training - model1 yhat")
    plt.legend()
    plt.savefig(os.path.join(output_dir, "model1_inference_residual_2.png"))
    plt.close()
