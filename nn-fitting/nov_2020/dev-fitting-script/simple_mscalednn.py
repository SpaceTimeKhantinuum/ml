import tensorflow as tf
import pugna.activations
import pugna.layers


def model(n_inputs, n_outputs, nlayers, units, nscales):
    """
    n_inputs: int, number of input features
    n_outputs: int, number of output features
    nlayers: int, number of hidden layers
    units: int, number of units per layer
    nscales: int, number of scales in each layer in an MscaleDNN
    """
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.InputLayer(input_shape=(n_inputs,)))
    for n in range(nlayers):
        model.add(pugna.layers.Mscale(units, nscales))
        model.add(tf.keras.layers.Activation(pugna.activations.s2relu))
    model.add(tf.keras.layers.Dense(n_outputs, activation="linear"))

    return model
