"""
to do

add validation data

add reducelronplateau

add mse log plot error

"""
import datetime
import logging
import os
import subprocess
import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as IUS

import tensorflow as tf
from sklearn.model_selection import train_test_split

import pugna.data
import pugna.layers
import pugna.activations

mpl.use("agg")
mpl.rcParams.update(mpl.rcParamsDefault)
mpl.rcParams.update({"font.size": 16})




def plot_history(history, outname):
    loss = history.history["loss"]
    plt.figure()
    plt.plot(range(len(loss)), loss)
    plt.yscale("log")
    plt.savefig(outname, bbox_inches="tight")
    plt.close()


def plot_prediction_and_error(x, y, outname, model=None, yhat=None):

    domain = np.arange(y.shape[0])

    if model:
        yhat = model.predict(x)
    yRes = y - yhat

    fig, axes = plt.subplots(1, 2, figsize=(14, 7))
    axes[0].plot(domain, y, label="data")
    axes[0].plot(domain, yhat, label="prediction")
    axes[1].plot(domain, yRes, "k-o", label="residuals")
    for ax in axes:
        ax.legend()
    plt.savefig(outname, bbox_inches="tight")
    plt.close()


def check_gpu():
    logger.info("running 'tf.config.list_physical_devices('GPU')'")
    logger.info(tf.config.list_physical_devices("GPU"))

    try:
        logger.info("running 'nvidia-smi -L'")
        subprocess.call(["nvidia-smi", "-L"])
    except FileNotFoundError:
        logger.info("could not run 'nvidia-smi -L'")


def set_threads():
    tf.config.threading.set_inter_op_parallelism_threads(1)
    tf.config.threading.set_intra_op_parallelism_threads(1)

    logger.info(
        f"tf using {tf.config.threading.get_inter_op_parallelism_threads()} inter_op_parallelism_threads thread(s)"
    )
    logger.info(
        f"tf using {tf.config.threading.get_intra_op_parallelism_threads()} intra_op_parallelism_threads thread(s)"
    )

    if "OMP_NUM_THREADS" not in os.environ:
        logger.info("'OMP_NUM_THREADS' not set. Setting it now.")
        os.environ["OMP_NUM_THREADS"] = "1"
    logger.info(f"OMP_NUM_THREADS: {os.environ['OMP_NUM_THREADS']}")

    if int(os.environ["OMP_NUM_THREADS"]) != 1:
        logger.warning(
            f"OMP_NUM_THREADS is not 1! value: {os.environ['OMP_NUM_THREADS']}"
        )


# get my pugna logger
# https://gitlab.com/SpaceTimeKhantinuum/pugna/-/blob/master/pugna/logger.py
def init_logger(level=0):
    """setup logging

    Keyword Arguments:
        level {int} -- either 0, 1 or 2. (default: {0})
            0: WARNING
            1: INFO
            2: DEBUG

    Returns:
        logger -- logger object
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    # need to do this to avoid print things twice
    # https://stackoverflow.com/a/6729713/12840171
    # and this to setup levels
    # https://stackoverflow.com/questions/14097061/easier-way-to-enable-verbose-logging
    if not logger.handlers:
        handler = logging.StreamHandler(sys.stdout)
        handler.flush = sys.stdout.flush
        if level == 0:
            handler.setLevel(logging.WARNING)
        elif level == 1:
            handler.setLevel(logging.INFO)
        elif level == 2:
            handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    else:
        logger.handlers[0].formatter = formatter
        if level == 0:
            logger.setLevel(logging.WARNING)
        elif level == 1:
            logger.setLevel(logging.INFO)
        elif level == 2:
            logger.setLevel(logging.DEBUG)
        logger.handlers[0].flush()

    return logger



def build_and_fit_model(
    x,
    y,
    verbose=False,
    batch_size=100,
    epochs=1000,
    units=300,
    nlayers=2,
    nscales=50,
    lr=0.001,
):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.InputLayer(input_shape=(2,)))
    for n in range(nlayers):
        model.add(pugna.layers.Mscale(units, nscales))
        model.add(tf.keras.layers.Activation(pugna.activations.s2relu))
    model.add(tf.keras.layers.Dense(1, activation="linear"))

    #optimizer = tf.keras.optimizers.Adam(lr)
    optimizer = tf.keras.optimizers.Adam(lr, amsgrad=True)
    #optimizer = tf.keras.optimizers.Adam(lr, amsgrad=True, clipnorm=0.5)
    #optimizer = tf.keras.optimizers.Adam(lr, amsgrad=True, clipvalue=5.0)
    #optimizer = tf.keras.optimizers.Nadam(lr)
    # optimizer = tf.keras.optimizers.SGD(lr)
    model.compile(loss="mse", optimizer=optimizer)

    history = model.fit(x, y, batch_size=batch_size, epochs=epochs, verbose=verbose)

    return history, model


#def preproc(data, method="MinMaxScaler", feature_range=(-1,1)):
def preproc(data, method="StandardScaler", feature_range=(-1,1)):
    scalers = pugna.data.make_scalers(data, method=method, feature_range=feature_range)
    data_scaled = pugna.data.apply_scaler(data, scalers)
    return data_scaled, scalers


def run_model_iter(
    x,
    y,
    tag,
    verbose=False,
    batch_size=100,
    epochs=1000,
    units=128,
    nlayers=2,
    nscales=50,
    lr=0.001,
    apply_preproc=False
):
    logger.info(f"tag: {tag}")

    if apply_preproc:
        y, scalers = preproc(y)
        plt.figure()
        plt.plot(np.arange(y.shape[0]), y)
        plt.savefig(f"{tag}_scaled.png")
        plt.close()

    logger.info(f"fitting {tag}")
    starttime = datetime.datetime.now()
    history, model = build_and_fit_model(
        x,
        y,
        epochs=epochs,
        units=units,
        nlayers=nlayers,
        nscales=nscales,
        lr=lr,
        verbose=verbose,
        batch_size=batch_size,
    )
    endtime = datetime.datetime.now()
    logger.info(f"{tag} fit")
    duration = endtime - starttime
    logger.info(f"The time cost: {duration}")

    plot_history(history, outname=f"{tag}_loss.png")

    yhat = model.predict(x)

    if apply_preproc:
        mse_scaled = tf.reduce_sum(tf.keras.losses.MSE(y, yhat))
        logger.info(f"mse (scaled): {mse_scaled}")
        y = pugna.data.apply_inverse_scaler(y, scalers)
        yhat = pugna.data.apply_inverse_scaler(yhat, scalers)

    mse = tf.math.reduce_sum(tf.keras.losses.MSE(y, yhat))
    logger.info(f"mse: {mse}")

    plot_prediction_and_error(
        x=x, y=y, yhat=yhat, outname=f"{tag}_performance.png"
    )

    yres = y - yhat

    return history, model, yres

def gen_data(num=100, noise=0):
    x = np.linspace(0,1,num)

    xx, yy = np.meshgrid(x,x)
    xx = xx.ravel()
    yy = yy.ravel()
    X = np.zeros(shape=(len(xx), 2))
    X[:,0] = xx
    X[:,1] = yy

    y = X[:,0] * np.sin(X[:,0]*10) + 1 + .4 * np.sin(X[:,1])
    if noise:
        y += np.random.uniform(0, noise, size=(len(y)))

    y = y[:,np.newaxis]

    return X, y


if __name__ == "__main__":
    logger = init_logger(level=2)
    logger.info("test")
    
    starttime = datetime.datetime.now()

    # set_threads()

    check_gpu()

    #x, y = gen_data(num=100, noise=0.)
    #x, y = gen_data(num=100, noise=0.2)
    #x, y = gen_data(num=100, noise=0.5)
    #x, y = gen_data(num=100, noise=1)
    #batch_size=int(x.shape[0]/1)

    #x, y = gen_data(num=500, noise=0)
    x, y = gen_data(num=500, noise=0.2)
    batch_size=int(x.shape[0]/100)

    logger.info(f"training set size: {x.shape}")
    logger.info(f"batch size: {batch_size}")

    # split and shuffle data
    # X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.25)

    # check shapes
    # names = ["X_train", "X_test", "y_train", "y_test"]
    # for k, v in enumerate([X_train, X_test, y_train, y_test]):
    #     logger.info(f"{names[k]}.shape:  {v.shape}")

    history_1, model_1, yres_1 = run_model_iter(
        x,
        y,
        tag="model_01",
        epochs=5000,
        units=1200,
        nlayers=4,
        nscales=50,
        lr=0.001,
        verbose=True,
        batch_size=batch_size,
    )

    history_2, model_2, yres_2 = run_model_iter(
        x,
        yres_1,
        tag="model_02",
        epochs=5000,
        units=1200,
        nlayers=4,
        nscales=10,
        lr=0.0001,
        verbose=False,
        batch_size=batch_size,
    )

    yres_n = yres_2

    lr=[0.001, 0.0001]
    apply_preproc=True
    nscales = [10, 10]
    for i in range(len(nscales)):
        j = i + 3
        logger.info(f"working: {j}")
        history_n, model_n, yres_n = run_model_iter(
            x,
            yres_n,
            tag=f"model_{j:02d}",
            epochs=5000,
            units=1200,
            nlayers=8,
            nscales=nscales[i],
            lr=lr[i],
            verbose=False,
            batch_size=batch_size,
            apply_preproc=apply_preproc
        )

    endtime = datetime.datetime.now()
    duration = endtime - starttime
    logger.info(f"The time cost: {duration}")
    logger.info("done!")
