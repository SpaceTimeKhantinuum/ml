"""
working out how to use my data with tf.data
to see if this can reduce any potential i/o bottlenecks
"""


import tensorflow as tf
import numpy as np

# x = np.array([1,2,3,4,5])
# ds = tf.data.Dataset.from_tensor_slices(x)

# print(x)

# print(ds)

# for value in ds.take(len(x)):
    # print(value)


def gen_data(num=100, noise=0):
    x = np.linspace(0,1,num)

    xx, yy = np.meshgrid(x,x)
    xx = xx.ravel()
    yy = yy.ravel()
    X = np.zeros(shape=(len(xx), 2))
    X[:,0] = xx
    X[:,1] = yy

    y = X[:,0] * np.sin(X[:,0]*10) + 1 + .4 * np.sin(X[:,1])
    if noise:
        y += np.random.uniform(0, noise, size=(len(y)))

    y = y[:,np.newaxis]

    return X, y

x, y = gen_data(num=10)

print(x.shape)
print(y.shape)

ds = tf.data.Dataset.from_tensor_slices((x, y))

print(ds)




