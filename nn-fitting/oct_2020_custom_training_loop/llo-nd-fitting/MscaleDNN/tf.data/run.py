"""
to do

add validation data

add reducelronplateau

add mse log plot error

"""
import datetime
import logging
import os
import subprocess
import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as IUS

import tensorflow as tf
from sklearn.model_selection import train_test_split

import pugna.data
import pugna.layers
import pugna.activations

mpl.use("agg")
mpl.rcParams.update(mpl.rcParamsDefault)
mpl.rcParams.update({"font.size": 16})


def plot_history(history, outname):
    loss = history.history["loss"]
    plt.figure()
    plt.plot(range(len(loss)), loss, label="loss")

    if "val_loss" in history.history.keys():
        val_loss = history.history["val_loss"]
        plt.plot(range(len(val_loss)), val_loss, ls="--", label="val")

    plt.yscale("log")
    plt.legend()
    plt.savefig(outname, bbox_inches="tight")
    plt.close()


def plot_prediction_and_error(x, y, outname, model=None, yhat=None):

    domain = np.arange(y.shape[0])

    if model:
        yhat = model.predict(x)
    yRes = y - yhat

    fig, axes = plt.subplots(1, 2, figsize=(14, 7))
    axes[0].plot(domain, y, label="data")
    axes[0].plot(domain, yhat, label="prediction")
    axes[1].plot(domain, yRes, "k-o", label="residuals")
    for ax in axes:
        ax.legend()
    plt.savefig(outname, bbox_inches="tight")
    plt.close()


def check_gpu():
    logger.info("running 'tf.config.list_physical_devices('GPU')'")
    logger.info(tf.config.list_physical_devices("GPU"))

    try:
        logger.info("running 'nvidia-smi -L'")
        subprocess.call(["nvidia-smi", "-L"])
    except FileNotFoundError:
        logger.info("could not run 'nvidia-smi -L'")


def set_threads():
    tf.config.threading.set_inter_op_parallelism_threads(1)
    tf.config.threading.set_intra_op_parallelism_threads(1)

    logger.info(
        f"tf using {tf.config.threading.get_inter_op_parallelism_threads()} inter_op_parallelism_threads thread(s)"
    )
    logger.info(
        f"tf using {tf.config.threading.get_intra_op_parallelism_threads()} intra_op_parallelism_threads thread(s)"
    )

    if "OMP_NUM_THREADS" not in os.environ:
        logger.info("'OMP_NUM_THREADS' not set. Setting it now.")
        os.environ["OMP_NUM_THREADS"] = "1"
    logger.info(f"OMP_NUM_THREADS: {os.environ['OMP_NUM_THREADS']}")

    if int(os.environ["OMP_NUM_THREADS"]) != 1:
        logger.warning(
            f"OMP_NUM_THREADS is not 1! value: {os.environ['OMP_NUM_THREADS']}"
        )


# get my pugna logger
# https://gitlab.com/SpaceTimeKhantinuum/pugna/-/blob/master/pugna/logger.py
def init_logger(level=0):
    """setup logging

    Keyword Arguments:
        level {int} -- either 0, 1 or 2. (default: {0})
            0: WARNING
            1: INFO
            2: DEBUG

    Returns:
        logger -- logger object
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    # need to do this to avoid print things twice
    # https://stackoverflow.com/a/6729713/12840171
    # and this to setup levels
    # https://stackoverflow.com/questions/14097061/easier-way-to-enable-verbose-logging
    if not logger.handlers:
        handler = logging.StreamHandler(sys.stdout)
        handler.flush = sys.stdout.flush
        if level == 0:
            handler.setLevel(logging.WARNING)
        elif level == 1:
            handler.setLevel(logging.INFO)
        elif level == 2:
            handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    else:
        logger.handlers[0].formatter = formatter
        if level == 0:
            logger.setLevel(logging.WARNING)
        elif level == 1:
            logger.setLevel(logging.INFO)
        elif level == 2:
            logger.setLevel(logging.DEBUG)
        logger.handlers[0].flush()

    return logger


def build_and_fit_model(
    train_ds,
    verbose=False,
    epochs=1000,
    units=300,
    nlayers=2,
    nscales=50,
    lr=0.001,
    validation_data=None,
):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.InputLayer(input_shape=(2,)))
    for n in range(nlayers):
        model.add(pugna.layers.Mscale(units, nscales))
        model.add(tf.keras.layers.Activation(pugna.activations.s2relu))
    model.add(tf.keras.layers.Dense(1, activation="linear"))

    # optimizer = tf.keras.optimizers.Adam(lr)
    optimizer = tf.keras.optimizers.Adam(lr, amsgrad=True)
    # optimizer = tf.keras.optimizers.Adam(lr, amsgrad=True, clipnorm=0.5)
    # optimizer = tf.keras.optimizers.Adam(lr, amsgrad=True, clipvalue=5.0)
    # optimizer = tf.keras.optimizers.Nadam(lr)
    # optimizer = tf.keras.optimizers.SGD(lr)
    model.compile(loss="mse", optimizer=optimizer)

    history = model.fit(
        train_ds, epochs=epochs, verbose=verbose, validation_data=validation_data
    )

    return history, model


# def preproc(data, method="MinMaxScaler", feature_range=(-1,1)):
def preproc(data, method="StandardScaler", feature_range=(-1, 1)):
    scalers = pugna.data.make_scalers(data, method=method, feature_range=feature_range)
    data_scaled = pugna.data.apply_scaler(data, scalers)
    return data_scaled, scalers


def run_model_iter(
    train_ds,
    tag,
    verbose=False,
    epochs=1000,
    units=128,
    nlayers=2,
    nscales=50,
    lr=0.001,
    validation_data=None,
):
    logger.info(f"tag: {tag}")

    logger.info(f"fitting {tag}")
    starttime = datetime.datetime.now()
    history, model = build_and_fit_model(
        train_ds,
        epochs=epochs,
        units=units,
        nlayers=nlayers,
        nscales=nscales,
        lr=lr,
        verbose=verbose,
        validation_data=validation_data,
    )
    endtime = datetime.datetime.now()
    logger.info(f"{tag} fit")
    duration = endtime - starttime
    logger.info(f"The time cost: {duration}")

    plot_history(history, outname=f"{tag}_loss.png")

    return history, model


def gen_data(num=100, noise=0):
    x = np.linspace(0, 1, num)

    xx, yy = np.meshgrid(x, x)
    xx = xx.ravel()
    yy = yy.ravel()
    X = np.zeros(shape=(len(xx), 2))
    X[:, 0] = xx
    X[:, 1] = yy

    y = X[:, 0] * np.sin(X[:, 0] * 10) + 1 + 0.4 * np.sin(X[:, 1])
    if noise:
        y += np.random.uniform(0, noise, size=(len(y)))

    y = y[:, np.newaxis]

    return X, y


if __name__ == "__main__":
    logger = init_logger(level=2)
    logger.info("test")

    starttime = datetime.datetime.now()

    # set_threads()

    check_gpu()

    # x, y = gen_data(num=100, noise=0.)
    # x, y = gen_data(num=500, noise=0.2)
    x, y = gen_data(num=500, noise=0.5)
    # split and shuffle data
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)

    xshape = x_train.shape[0]

    BATCH_SIZE = int(xshape / 5)
    SHUFFLE_BUFFER_SIZE = xshape

    logger.info(f"training set size: {x_train.shape}")
    logger.info(f"batch size: {BATCH_SIZE}")

    del x
    del y

    # check shapes
    names = ["x_train", "x_test", "y_train", "y_test"]
    for k, v in enumerate([x_train, x_test, y_train, y_test]):
        logger.info(f"{names[k]}.shape:  {v.shape}")

    train_ds = tf.data.Dataset.from_tensor_slices((x_train, y_train))
    test_ds = tf.data.Dataset.from_tensor_slices((x_test, y_test))

    train_ds = train_ds.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)
    train_ds = train_ds.prefetch(tf.data.experimental.AUTOTUNE)
    test_ds = test_ds.batch(BATCH_SIZE)

    history_1, model_1 = run_model_iter(
        train_ds,
        tag="model_01",
        epochs=1000,
        units=1200,
        nlayers=4,
        nscales=50,
        lr=0.001,
        verbose=True,
        validation_data=test_ds,
    )

    model_1.evaluate(test_ds)

    endtime = datetime.datetime.now()
    duration = endtime - starttime
    logger.info(f"The time cost: {duration}")
    logger.info("done!")
