# Design

to explore different neural networks and perform repeat analyses to check for convergence
you can use this.

first make a new directory to store this experiment
The experiment is tied to a particular dataset.

uses https://pypi.org/project/tomlkit/ package.

```
mkdir experiment_name
cd experiment_name
# make config.toml
# run gen_run_scripts.py --config config.toml
```


this should make a bunch of scripts that will run the fitting.

it also should make a post-processing script to collate results.
