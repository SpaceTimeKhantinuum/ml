import templates

from tomlkit import parse
from string import Template
import os
import shutil
import argparse
import tempfile
import datetime


def write_to_file(templ_str, filename):
    with open(filename, "w") as f:
        f.write(templ_str)


def list_to_str(lst, delim=" "):
    """
    if lst is a list then concatonate
    the list into a space separated list
    """
    try:
        return delim.join(map(str, lst))
    except:
        return lst


# def make_tag(layers, units, scales, epochs):
#     layers = list_to_str(layers, delim="_")
#     units = list_to_str(units, delim="_")
#     scales = list_to_str(scales, delim="_")
#     tag = f"L_{layers}_U_{units}_S_{scales}_E_{epochs}"
#     return tag


def make_tag(prefix="exp"):
    """
    use temp file and datetime to get a unique name that begins
    with 'prefix', has a middle that is the data and ends
    with a random string.

    by default prefix is 'exp' that is short for experiment.

    name template: '[prefix]_[%y%m%d]_[random]'
    """
    date_now = datetime.datetime.now().strftime("%y%m%d")
    with tempfile.NamedTemporaryFile(prefix=f"{prefix}_{date_now}_") as temp:
        tag = temp.name.split("/")[-1]
    return tag


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="""outputs scripts to run fitting
        """,
    )

    parser.add_argument(
        "--config-file", type=str, help="path to workflow config.toml file"
    )
    parser.add_argument(
        "--force",
        action="store_true",
        help="if given then will not throw an error if output directory exists",
    )

    args = parser.parse_args()
    args_dict = vars(args)

    print("==========")
    print("printing command line args")
    for k in args_dict.keys():
        print(f"{k}: {args_dict[k]}")
    print("==========")

    with open(args.config_file, "r") as f:
        text = f.read()

    doc = parse(text)

    for k in doc.keys():
        print(f"{k}: {doc[k]}")

    # print(doc.keys())

    # tag = make_tag(
    #     layers=doc["layers"],
    #     units=doc["units"],
    #     scales=doc["scales"],
    #     epochs=doc["epochs"],
    # )
    tag = make_tag()

    print(f"making output directory: {tag}")
    os.makedirs(f"{tag}", exist_ok=args.force)

    print(f"copying {args.config_file} to output directory")
    shutil.copy(args.config_file, tag)

    layers = list_to_str(doc["layers"])
    scales = list_to_str(doc["scales"])
    units = list_to_str(doc["units"])

    run_all_str = templates.template_bash_shbang

    for i in range(doc["runs"]["repeats"]):
        index = f"{i:02}"
        name = f"test_{index}"

        kwargs = dict(
            name=name,
            layers=layers,
            units=units,
            scales=scales,
            epochs=doc["epochs"],
            x=doc["data"]["x"],
            y=doc["data"]["y"],
            xval=doc["data"]["xval"],
            yval=doc["data"]["yval"],
            gpu_devices=doc["runs"]["gpus"][i],
        )

        pugna_fit_str = Template(templates.template_pugna_fit).substitute(**kwargs)

        # print(pugna_fit_str)

        run_i_str = templates.template_bash_shbang + "\n\n" + pugna_fit_str
        outname = f"run_{index}.sh"
        write_to_file(run_i_str, outname)
        os.chmod(outname, 0o775)

        shutil.copy(outname, tag)
        os.remove(outname)

        logfile = f"run_{index}.log"
        run_all_str += "\n\n" + Template(templates.template_nohup).substitute(
            name=outname, logfile=logfile
        )

    outname = "run_all.sh"
    write_to_file(run_all_str, outname)
    os.chmod(outname, 0o775)
    shutil.copy(outname, tag)
    os.remove(outname)
