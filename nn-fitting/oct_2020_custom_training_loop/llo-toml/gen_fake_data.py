import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

output_name = "data"
os.makedirs(f"{output_name}", exist_ok=True)

x = np.linspace(0, np.pi, 1000)[:, np.newaxis]

y = np.sin(23 * x) + np.sin(137 * x) + np.sin(203 * x)


plt.figure(figsize=(14, 4))
plt.plot(x, y)
plt.savefig(f"{output_name}/data0.png")
plt.close()

X_train, X_test, y_train, y_test = train_test_split(
    x, y, test_size=0.33, random_state=42
)

print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)

plt.figure(figsize=(14, 4))
plt.plot(X_train, y_train, "x", label="train")
plt.plot(X_test, y_test, "o", label="test")
plt.legend()
plt.savefig(f"{output_name}/data.png")
plt.close()


np.save(f"{output_name}/x.npy", X_train)
np.save(f"{output_name}/y.npy", y_train)
np.save(f"{output_name}/x_val.npy", X_test)
np.save(f"{output_name}/y_val.npy", y_test)
