"""
reads different runs, with the possibility of having repeats for each run
and makes some comparisons and plots
"""

import matplotlib as mpl

mpl.use("agg")
import matplotlib.pyplot as plt

mpl.rcParams.update({"font.size": 16})
from matplotlib.lines import Line2D
import matplotlib.colors as mcolors
from cycler import cycler

import numpy as np
import glob
import os
from tomlkit import parse

import pugna.model_utils


def find_configs(prefix="exp"):
    configs = glob.glob(f"{prefix}*/config.toml")
    return configs


if __name__ == "__main__":

    configs = find_configs(prefix="exp_")

    results = []

    for config in configs:
        print(f"reading config: {config}")
        with open(config, "r") as f:
            text = f.read()
        doc = parse(text)

        tag = config.split("/")[0]

        # add here a check that there exists at least one complete
        # set of results in 'test_*' sub-dir

        results.append({"tag": tag, "doc": doc})

    for result in results:
        tag = result["tag"]
        print(f"working: {tag}")

        test_dirs = [f.split("/")[-1] for f in glob.glob(f"{tag}/test_*")]

        result.update({"tests": {}})

        for test_dir in test_dirs:
            print(f"\t {test_dir}")
            fpath = os.path.join(result["tag"], test_dir, "history.pickle")
            try:
                history_file = glob.glob(fpath)[0]
                history_file = os.path.abspath(history_file)
                # print(f"\t {history_file}")
                history = pugna.model_utils.load_history(history_file)
                result["tests"].update({test_dir: {"history": history}})
            except IndexError:
                print("\t could not find history file")
                continue

    # print(results[0])
    # print(results[0]["tests"]["test_0"].keys())
    # print(results[0]["tests"]["test_0"]["history"].keys())

    clist = list(mcolors.TABLEAU_COLORS.values()) + list(mcolors.BASE_COLORS.values())
    lslist = ["-"]

    _cc = cycler(color=clist) * cycler(linestyle=lslist)
    cc = []
    for d in _cc:
        cc.append(d)

    fig, ax = plt.subplots(figsize=(8, 10))
    for i, result in enumerate(results):
        tag = result["tag"]
        test_names = result["tests"].keys()
        for test_name in test_names:
            y = result["tests"][test_name]["history"]["loss"]
            yval = result["tests"][test_name]["history"]["val_loss"]
            x = np.arange(len(y))

            ax.plot(x, y, color=cc[i]["color"], ls=cc[i]["linestyle"], alpha=0.5)
            ax.plot(x, yval, color=cc[i]["color"], ls="--", alpha=0.5)

    ax.set_yscale("log")
    ax.set_xlabel("epochs")
    ax.set_ylabel("loss")

    custom_lines = []
    for i in range(len(results)):
        row = Line2D([0], [0], color=cc[i]["color"], lw=2, ls=cc[i]["linestyle"])
        custom_lines.append(row)

    tags = [result["tag"] for result in results]
    legend = ax.legend(custom_lines, tags, loc="center left", bbox_to_anchor=(1, 0.5))
    # save figure without legend being cut off
    # https://stackoverflow.com/a/10154763/12840171
    fig.savefig("test_plot.png", bbox_extra_artists=(legend,), bbox_inches="tight")

    ax.set_xscale("log")
    fig.savefig("test_plot_logx.png", bbox_extra_artists=(legend,), bbox_inches="tight")
    plt.close(fig)
