template_bash_shbang = """#!/bin/bash"""

template_nohup = """nohup ./$name > $logfile 2>&1 &"""

template_pugna_fit = """pugna_fit \\
-v \\
--output-dir $name \\
--X-data-train $x \\
--y-data-train $y \\
--X-data-val $xval \\
--y-data-val $yval \\
--nlayers $layers \\
--units $units \\
--nscales $scales \\
--activations s2relu \\
--dropouts 0 \\
--batch-norms False \\
--epochs $epochs \\
--learning-rate 0.001 \\
--lrs-name CosineDecayRestarts \\
--lrs-cosine-first-decay-steps 2 \\
--adam-amsgrad \\
--scale-names linear \\
--gpu-devices $gpu_devices"""
