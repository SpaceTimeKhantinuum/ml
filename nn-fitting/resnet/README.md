# idea

adding in a layer so that we model the residual between the input and the output. the idea is that this will focus the neural net to work on modelling what it doesn't know.

Turns out this is actually a resnet

 - https://paperswithcode.com/method/residual-connection
 - https://github.com/DowellChan/ResNetRegression/
 - https://www.pnas.org/content/pnas/117/13/7052.full.pdf (page 10)
 - https://www.semanticscholar.org/paper/Deep-Residual-Learning-for-Image-Recognition-He-Zhang/2c03df8b48bf3fa39054345bafabfeff15bfd11d#citing-papers
