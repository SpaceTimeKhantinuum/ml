import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as IUS
from sklearn.preprocessing import MinMaxScaler, StandardScaler

def load_data(filename="./data_to_fit.txt"):
    """
    load 1d example dataset
    returns
        X, y (each a 1D np array)
    """
    Xy = np.loadtxt(filename)
    # sort by X
    X, y = Xy[np.argsort(Xy[:, 0])].T
    return X, y

def resample_data(X, y, xmin=None, xmax=None, npts=1000, k=1):
    """
    given input X, y data
    mask, interpolate and resample y-data
    returns
        X, y (each a 1D np array)
    """
    
    if xmin is None:
        xmin = X[0]
    if xmax is None:
        xmax = X[-1]
    mask = (X>=xmin) & (X<=xmax)
    
    iy = IUS(X[mask], y[mask], k=k)
    
    Xnew = np.linspace(xmin, xmax, npts)
    
    return Xnew, iy(Xnew)

def scale_1d_data(data, scaler_func_name="minmax"):
    """
    scaler_func_name: can be "minmax" or "standard"
    """
    data = data.reshape(-1,1)
    if scaler_func_name == "minmax":
        scaler = MinMaxScaler()
    elif scaler_func_name == "standard":
        scaler = StandardScaler()
    scaled=scaler.fit_transform(data)
    return scaled, scaler
