
echo "generating training data"
wispy_generate_training_data --config-file training_data.toml -v --force

echo "generating validation data"
wispy_generate_training_data --config-file validation_data.toml -v --force
