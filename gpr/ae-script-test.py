# tensorflow
import tensorflow as tf
import tensorflow_addons as tfa

from tensorflow import keras
from tensorflow.keras import layers

# check version
print('Using TensorFlow v%s' % tf.__version__)
acc_str = 'accuracy' if tf.__version__[:2] == '2.' else 'acc'

# helpers
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('ggplot')

# fix the seed for consistent interpretation of the results
import random as python_random
python_random.seed(0)
np.random.seed(0)
tf.random.set_seed(0)

from tqdm.auto import tqdm


from scrinet.interfaces import lalutils

import lal
import lalsimulation as lalsim

import utils

from scipy.interpolate import InterpolatedUnivariateSpline as IUS
import phenom

import datetime

def set_gpu_memory_growth():
    gpus = tf.config.list_physical_devices('GPU')
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            print("running: tf.config.experimental.set_memory_growth")
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)

 # https://stackoverflow.com/questions/59563085/how-to-stop-training-when-it-hits-a-specific-validation-accuracy
class ThresholdCallback(tf.keras.callbacks.Callback):
    def __init__(self, val):
        super(ThresholdCallback, self).__init__()
        self.val = val
    def on_epoch_end(self, epoch, logs=None):
        if logs.get('loss') <= self.val:
            self.model.stop_training = True

def gen_model_waveforms(approx, qlist, npts, t_min, t_max, nrfiles=None):
    if approx == "NR":
        return [utils.WaveformGeneration(nrfile=nrfiles[key], npts=npts, t_min=t_min, t_max=t_max) for key in nrfiles]
    else:
        lal_approx = lalsim.GetApproximantFromString(approx)
        return [utils.WaveformGeneration(approximant=lal_approx, q=q, npts=npts, t_min=t_min, t_max=t_max) for q in qlist]


def make_training_set(wfs, attr, scale_by_eta=False):
    """
    make a numpy array of training data
    """
    x = np.array([wf.__getattribute__('q') for wf in wfs])
    y = np.array([wf.__getattribute__(attr) for wf in wfs])
    
    if scale_by_eta:
        eta = phenom.eta_from_q(x)[:, np.newaxis]
        return {'x':x, 'y':y/eta}
    else:
        return {'x':x, 'y':y}

print('setting gpu memory growth')
set_gpu_memory_growth()
    
# define hyper-parameters

# approxs = ["SEOBNRv4_opt", "IMRPhenomB", "IMRPhenomC", "IMRPhenomD", "SEOBNRv4", "SEOBNRv2", "NR"]
# approxs = ["SEOBNRv4_opt", "IMRPhenomD", "SEOBNRv4", "SEOBNRv2", 'IMRPhenomT', 'IMRPhenomXAS', "NR"]
# approxs = ["SEOBNRv4_opt", "IMRPhenomD", "NR"]
# approxs = ["SEOBNRv4_opt", "SEOBNRv4"]
# approxs = ["SEOBNRv4", "IMRPhenomD"]
#approxs = ["SEOBNRv4_opt", "IMRPhenomD"]
approxs = ["SEOBNRv4_opt", "IMRPhenomT"]
# approxs = ["IMRPhenomD"]


start_approx = approxs[0]
q_min = 1
# q_max = 2
q_max = 19
# q_min = 19
# q_max = 30
dq = 0.25

q_arr = np.arange(q_min, q_max, dq)

# time points
npts=1000
# npts=5000
# t_min=-500
t_min=-2000
t_max=75

# data_to_model = 'Reh22'
# data_to_model = 'amp'
data_to_model = 'phase'
# data_to_model = 'freq'

# scale_by_eta=True
scale_by_eta=False

print('generating waveforms')

model_wfs = {}
for approx in approxs:
    model_wfs.update({approx:gen_model_waveforms(approx, q_arr, npts, t_min=t_min, t_max=t_max)})


model_ts = {}
for approx in approxs:
    model_ts.update({approx:make_training_set(model_wfs[approx], data_to_model, scale_by_eta=scale_by_eta)})

common_times = model_wfs[start_approx][0].times 

# the training set will just be the concatonated list of all waveforms
# from each model
train_x = np.concatenate([model_ts[k]['x'] for k in model_ts.keys()])
train_y = np.concatenate([model_ts[k]['y'] for k in model_ts.keys()])

print(f"train_x.shape = {train_x.shape}")
print(f"train_y.shape = {train_y.shape}")

# setup ae

def build_encoder(input_shape, latent_dim, act="relu"):
    # build the encoder
    timeseries_input = keras.Input(shape=(input_shape, ))
    x = layers.Dense(128, activation=act)(timeseries_input)
#     x = layers.BatchNormalization()(x)
#     x = layers.Dense(64, activation=act)(x)
    x = layers.Dense(16, activation=act)(x)
#     x = layers.Dense(16, activation="tanh")(x)
    latent_output = layers.Dense(latent_dim)(x)
    encoder_AE = keras.Model(timeseries_input, latent_output)
#     encoder_AE.summary()
    
    return encoder_AE

def build_decoder(latent_dim, act="relu"):
    # build the decoder
    latent_input = keras.Input(shape=(latent_dim,))
    x = layers.Dense(16, activation=act)(latent_input)
#     x = layers.BatchNormalization()(x)
#     x = layers.Dense(64, activation=act)(x)
    x = layers.Dense(128, activation=act)(x)
#     x = layers.Dense(128, activation="tanh")(x)
    timeseries_output = layers.Dense(input_shape, activation="linear")(x)
    decoder_AE = keras.Model(latent_input, timeseries_output)
#     decoder_AE.summary()
    
    return decoder_AE

def build_and_compile_autoencoder(input_shape, encoder_AE, decoder_AE, opt):
    # build the AE
    timeseries_input = keras.Input(shape=(input_shape, ))
    latent = encoder_AE(timeseries_input)
    timeseries_output = decoder_AE(latent)
    ae_model = keras.Model(timeseries_input, timeseries_output)
#     ae_model.summary()

    ae_model.compile(optimizer=opt, loss='mse')
    
    return ae_model

# latent dimension
# latent_dim = 1
latent_dim = 2
# latent_dim = 3
# latent_dim = 4
# latent_dim = 10
# latent_dim = 100

act = "relu"
# act = "tanh"

input_shape = train_y.shape[1] # number of time points

learning_rate=0.001
amsgrad=True
# opt = tf.keras.optimizers.Adam(learning_rate=learning_rate, amsgrad=amsgrad)
# opt = tfa.optimizers.NovoGrad(learning_rate=learning_rate, amsgrad=amsgrad)
opt = tfa.optimizers.RectifiedAdam(learning_rate=learning_rate)
# opt = tfa.optimizers.LAMB(learning_rate=learning_rate)
# opt = tfa.optimizers.Yogi(learning_rate=learning_rate)
# opt = tf.keras.optimizers.Adadelta(learning_rate=learning_rate)
# opt = tf.keras.optimizers.Adagrad(learning_rate=learning_rate)
# opt = tf.keras.optimizers.Adamax(learning_rate=learnng_rate)
# opt = tf.keras.optimizers.Nadam(learning_rate=learning_rate)

# opt = tfa.optimizers.Lookahead(opt)

encoder_AE = build_encoder(input_shape, latent_dim, act)
decoder_AE = build_decoder(latent_dim, act)
ae_model = build_and_compile_autoencoder(input_shape, encoder_AE, decoder_AE, opt)
# encoder_AE.summary()
# decoder_AE.summary()
# ae_model.summary()


callbacks = []

# first_decay_steps = 20
# first_decay_steps = 400
# first_decay_steps = 1200*4
# learning_rate_fn = tf.keras.experimental.CosineDecayRestarts(learning_rate, first_decay_steps)

# learning_rate_fn = tfa.optimizers.ExponentialCyclicalLearningRate(learning_rate, 1e-8, first_decay_steps)
# learning_rate_fn = tfa.optimizers.ExponentialCyclicalLearningRate(1e-8, learning_rate, first_decay_steps, gamma=0.999)

initial_learning_rate=1e-6
maximal_learning_rate=1e-2
step_size=200
gamma=0.7
# initial_learning_rate=1e-3
# maximal_learning_rate=1e-2
# step_size=1000
# gamma=1

learning_rate_fn = tfa.optimizers.ExponentialCyclicalLearningRate(
            initial_learning_rate=initial_learning_rate,
            maximal_learning_rate=maximal_learning_rate,
            step_size=step_size,
            scale_mode="cycle",
            gamma=gamma)


callbacks.append(tf.keras.callbacks.LearningRateScheduler(learning_rate_fn))

tqdm_callback = tfa.callbacks.TQDMProgressBar(show_epoch_progress=False)
callbacks.append(tqdm_callback)

callbacks.append(ThresholdCallback(1e-4)) # for the phase if it hasn't been normalised
# callbacks.append(ThresholdCallback(1e-7))


# epochs=100000
# epochs=1000
# epochs=100000
# epochs=100
epochs=5000
# train the AE
starttime = datetime.datetime.now()
ae_history = ae_model.fit(train_y, train_y, epochs=epochs, callbacks=callbacks, batch_size=32, verbose=0)
endtime = datetime.datetime.now()
duration = endtime - starttime
print("fit complete")
print(f"fit duration: {duration}")
# , validation_split=0.2


plt.figure()
plt.plot(ae_history.history['loss'])
# plt.plot(ae_history.history['val_loss'])
plt.yscale('log')
plt.xscale('log') 
plt.savefig('loss.png')
plt.close()






