# toy example not using NR

 - Similar to Yoshinta, Ohme +
 - Take two models:
    - model A: less accurate / faster
    - model B: more accurate / slower
 - build basis of model A
    - `alpha_A` = projection coefficients of model A
 - project model B onto model A basis
    - `model_B` = projection coefficients of B onto basis A
 - compute the difference between the `alpha_A` and `alpha_B` at
the `model_B` locations.
    - model this difference
